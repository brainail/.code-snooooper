CodeSnooper
===

**CodeSnooper** is the copy-paste-detector. CodeSnooper finds duplicated code in
Java, C, C++, C#, ...

This is my Diploma work, BSU, 2015.

![Screen Shot 2015-07-15 at 11.08.46 AM.png](https://bitbucket.org/repo/bepxno/images/3775592766-Screen%20Shot%202015-07-15%20at%2011.08.46%20AM.png)