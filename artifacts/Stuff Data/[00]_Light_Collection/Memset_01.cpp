string s_fakeVar = "Fake A";

// #include <stdio.h>
// #include <string.h>

int main () // Main
{
  // String!  
  char str [] 
        = "... almost every programmer should know ... Mmmm .. memset (...)! Ha-ha-ha!";

  // Fill!
  memset (
    str,  
    "Fake plus!",  
    7
  );

  // Write
  puts (
    str
  );

  // Useless comment!: 2 + 2 equals to 4
  // Exit!, 0 - Exit code
  return 0;
}