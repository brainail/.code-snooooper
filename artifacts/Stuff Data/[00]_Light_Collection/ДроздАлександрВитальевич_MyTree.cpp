#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct tree {
    int element;
    int potomki;
    tree* left;
    tree* right;
    tree* parent;
};

void push(int a, tree** t) {
    if (!(*t)) {
        (*t) = new tree;
        (*t)->element = a;
        (*t)->potomki = 0;
        (*t)->left = NULL;
        (*t)->right = NULL;
        (*t)->parent = NULL;
    }
    else
        if (a > (*t)->element) {
            if ((*t)->right) {
                if ((*t)->right->element != a)
                {
                    (*t)->potomki+=1;
                    push(a, &(*t)->right);
                    (*t)->right->parent = *t;
                }
            }
            else
                {
                    (*t)->potomki+=1;
                    push(a, &(*t)->right);
                    (*t)->right->parent = *t;
                }
        }
        else {
            if ((*t)->left) {
                if ((*t)->left->element != a)
                    {
                        (*t)->potomki+=1;
                        push(a, &(*t)->left);
                        (*t)->left->parent = *t;
                    }
            }
            else 
                {
                    (*t)->potomki+=1;
                    push(a, &(*t)->left);
                    (*t)->left->parent = *t;
                }
        }
}




void print_front_left(tree* t, FILE *out) {
    if (t) {
                fprintf(out, "%d\n", t->element);
                print_front_left(t->left, out);
                print_front_left(t->right, out);
            }
}

void delete_uzel (tree* t)
{
    
    if((t->right == NULL) && (t->left != NULL))
        {
            if (t->parent->element > t->element) t->parent->left=t->left;
            else t->parent->right=t->left;
            return;
        }
        if(t->right != NULL && t->left == NULL)
        {
            if (t->parent->element > t->element) t->parent->left=t->right;
            else t->parent->right=t->right;
            return;
        }
        if((t->right != NULL) && (t->left != NULL))
        {
            tree* tmp = t->left;
            while( tmp->right != NULL) tmp = tmp->right;
            if (tmp->parent->element < tmp->element)
            tmp->parent->right = tmp->left;
            else tmp->parent->left = tmp->left;
            if (t->parent != NULL)
            {
            if(t->element < t->parent->element) t->parent->left->element = tmp->element;
            else t->parent->right->element = tmp->element;
            }
            else t->left->parent->element = tmp->element;
            return;
        }


}

vector<tree> function(tree* t, vector <tree> A)
{

    if (t)
    {
        int leftnum, rightnum;
        if (t->left != NULL) leftnum = t->left->potomki+1;
        else leftnum = 0;
        if (t->right != NULL) rightnum = t->right->potomki+1;
        else rightnum = 0;
        if (leftnum - rightnum == 2 || leftnum - rightnum == -2) {A.push_back (*t); }
        A=function(t->left, A);
        A=function(t->right, A);
    }
    return A;
}


int main () {   
    tree* frst_tree = NULL;
    FILE *in = fopen("tst.in", "r");
    int key;
    while (fscanf(in,"%d",&key) != EOF)
    push(key, &frst_tree);
    fclose(in);
    FILE *out = fopen("tst.out", "w");
    vector<tree> A;
    vector<int> B;
    A = function (frst_tree, A);
    for (int i=0; i< A.size(); i++)
    B.push_back(A[i].element);
    sort (B.begin(), B.end());
    tree uzel;
    if (B.size() % 2 == 0) print_front_left(frst_tree, out);
    else 
    {
        int elem = B[(B.size()-1)/2];
        for (int i=0; i< A.size(); i++)
        {
            if (A[i].element == elem) uzel = A[i]; 
        }
        delete_uzel (&uzel);
        print_front_left(frst_tree, out);
    }
    fclose(out);
    return 0;       
} 