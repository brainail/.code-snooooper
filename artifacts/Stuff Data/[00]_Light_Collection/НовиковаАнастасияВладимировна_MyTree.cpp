#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct mytree{
    int element, descendants;
    mytree *left, *right, *parent;
};

void insert(int unit, mytree** my){
    if(!(*my)){
        (*my) = new mytree;
        (*my)->element = unit;
        (*my)->descendants = 0;
        (*my)->left = NULL;
        (*my)->right = NULL;
        (*my)->parent = NULL;
    }
    else
        if(unit > (*my)->element){
            if((*my)->right){
                if((*my)->right->element != unit)
                {
                    (*my)->descendants += 1;
                    insert(unit, &(*my)->right);
                    (*my)->right->parent = *my;
                }
            }
            else
            {
                (*my)->descendants += 1;
                insert(unit, &(*my)->right);
                (*my)->right->parent = *my;
            }
        }
        else{
            if((*my)->left) {
                if((*my)->left->element != unit)
                {
                    (*my)->descendants += 1;
                    insert(unit, &(*my)->left);
                    (*my)->left->parent = *my;
                }
            }
            else 
            {
                (*my)->descendants += 1;
                insert(unit, &(*my)->left);
                (*my)->left->parent = *my;
            }
        }
}

void delnode (mytree* my)
{   if((my->right != NULL) && (my->left != NULL))
{
    mytree* temp =  my->left;
    while( temp->right != NULL) temp = temp->right;
    if (temp->parent->element < temp->element)
        temp->parent->right = temp->left;
    else temp->parent->left = temp->left;
    if (my->parent != NULL)
    {
        if(my->element < my->parent->element) my->parent->left->element = temp->element;
        else my->parent->right->element = temp->element;
    }
    else my->left->parent->element = temp->element;
    return;
}
if(my->right != NULL && my->left == NULL)
{
    if(my->parent->element > my->element) my->parent->left = my->right;
    else my->parent->right = my->right;
    return;
}
if((my->right == NULL) && (my->left != NULL))
{
    if(my->parent->element > my->element)
        my->parent->left = my->left;
    else my->parent->right = my->left;
    return;
}
}

void byleft(FILE *out, mytree* my){
    if(my){
        fprintf(out, "%d\n", my->element);
        byleft(out, my->left);
        byleft(out, my->right);
    }
}

vector <mytree> make(mytree* my, vector <mytree> tree)
{
    if(my)
    {
        int countleft;
        int countright;
        if (my->left != NULL) countleft = my->left->descendants + 1;
        else countleft = 0;
        if (my->right != NULL) countright = my->right->descendants + 1;
        else countright = 0;
        if (countleft - countright == 2 || countleft - countright == -2) {
            tree.push_back (*my); 
        }
        tree = make(my->left, tree);
        tree = make(my->right, tree);
    }
    return tree;
}


void main () {  
    int key;
    FILE *in = fopen("tst.in", "r");
    mytree* first = NULL;
    while (fscanf(in,"%d",&key) != EOF)
        insert(key, &first);
    fclose(in);
    FILE *out = fopen("tst.out", "w");
    vector<mytree> tree;
    vector<int> vect;
    tree = make (first, tree);
    for (int i = 0; i < tree.size(); ++i)
        vect.push_back(tree[i].element);
    sort (vect.begin(), vect.end());
    mytree node;
    if (vect.size() % 2 == 0) 
        byleft(out, first);
    else 
    {
        int elem = vect[(vect.size()-1)/2];
        for (int i = 0; i < tree.size(); ++i)
        {
            if (tree[i].element == elem) 
                node = tree[i]; 
        }
        delnode (&node);
        byleft(out, first);
    }
    fclose(out);     
} 