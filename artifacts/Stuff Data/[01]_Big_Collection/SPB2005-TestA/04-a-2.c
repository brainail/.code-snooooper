#include <stdio.h>
#include <math.h>

int mint( double time, double t, int a, int b, int timet )
{
  unsigned long m;
  int j, m1 = 0, k;

  for (m = 0; m < 0xFF; m++)
  {
    for (k = 0; k <= 32; k++)
    {
      m1 = 0;
      for (j = 0; j < k; j++)
      {
	if ((m >> j)&1)
	  m1 += a;
	else
	  m1 += b;
      }
      if (t - time - m1 == timet)
	return 1;
     }
  }
  return 0;
}

int main( void )
{
  FILE *F1, *F2;
  double a, b, t;
  double time, timea, timeb;

  F1 = fopen("clocks.in", "rt");
  F2 = fopen("clocks.out", "wt");
  fscanf(F1, "%lf", &a);
  fscanf(F1, "%lf", &b);
  fscanf(F1, "%lf", &t);
  if (a == t)
  {
    fprintf(F2, "Initially: flip A\n");
    fprintf(F2, "When A stops: ready\n");
  }
  else if (b == t)
  {
    fprintf(F2, "Initially: flip B\n");
    fprintf(F2, "When B stops: ready\n");
  }
  else if (a > t && b > t)
  {
    fprintf(F2, "Impossible\n");
  }
  else
  {
    fprintf(F2, "Initially: flip A and B\n");

    timea = timeb = 0;
    for (time = 0; time <= t; time++)
    {
      if (timea >= a)
      {
	timea = 0;
	if (time != t)
	{
	  if (mint(time, t, a, b, timeb))
	  {
	    fprintf(F2, "When A stops: flip B\n");
	    timeb = b - timeb;
	  }
	  else
	    fprintf(F2, "When A stops: flip A\n");
	}
	if (time == t)
	{
	  fprintf(F2, "When A stops: ready\n");
	  fclose(F1);
	  fclose(F2);
	  return 0;

	}
      }
      if (timeb >= b)
      {
	timeb = 0;
	if (time != t)
	{
	  if (mint(time, t, a, b, timea))
	  {
	    fprintf(F2, "When B stops: flip A\n");
	    timea = a - timea;
	  }
	  else

	  fprintf(F2, "When B stops: flip B\n");
	}
	else
	{
	  fprintf(F2, "When B stops: ready\n");
	  fclose(F1);
	  fclose(F2);
	  return 0;

	}
      }
      if (time != t)
      {
	timea++;
	timeb++;
      }
    }
    if (timea >= a)
    {
      timea = 0;
    }
    if (timeb >= b)
    {
      timeb = 0;
    }

    if (timea != 0 && timeb != 0 && timea != a && timeb != b)
    {
      fclose(F2);
      F2 = fopen("clocks.out", "wt");
      fprintf(F2, "Impossible\n");
    }
    else
    {
      if (timea == 0 || timea == a)
	fprintf(F2, "When A stops: ready\n");
      else if (timeb == 0 || timeb == b)
	fprintf(F2, "When B stops: ready\n");

    }
  }

  fclose(F1);
  fclose(F2);
  return 0;
}