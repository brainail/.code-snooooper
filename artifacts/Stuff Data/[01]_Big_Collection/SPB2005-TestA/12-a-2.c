/**************************************************************
 * Copyright (C) 2005
 *    Computer Graphics Support Group of 30 Phys-Math Lyceum
 **************************************************************/

/* FILE NAME   : A.C
 * PURPOSE:    : Olympiad task A.
 * PROGRAMMER  : Anton Timofeev.
 * LAST UPDATE : 06.03.2005
 * NOTE        : None.
 *
 * No part of this file may be changed without agreement of
 * Computer Graphics Support Group of 30 Phys-Math Lyceum
 */

#include <stdio.h>

/* The main program function */
int main( void )
{
  int i, a, b, t, tmp, ca, cb;

  freopen("clocks.in", "rt", stdin);
  freopen("clocks.out", "wt", stdout);

  scanf("%i%i%i", &a, &b, &t);
  if (a > b)
	tmp = a, a = b, b = tmp;
  ca = t / a;
  cb = 0;

  if (t == 0)
	printf("Initially: ready\n");
  while (ca >= 0)
  {
    if (ca * a + cb * b == t)
	{
	  if (cb == 0)
	  {
        printf("Initially: flip A\n");
		for (i = 0; i < ca - 1; i++)
          printf("When A stops: flip A\n");
        printf("When A stops: ready\n");
	  }
	  else if (ca == 0)
	  {
        printf("Initially: flip B\n");
		for (i = 0; i < cb - 1; i++)
          printf("When B stops: flip B\n");
        printf("When B stops: ready\n");
	  }
	  else
	  {
	    printf("Initially: flip A and B\n");
	    for (i = 0; i < cb - 1; i++)
          printf("When B stops: flip B\n");
        printf("When B stops: flip A\n");
		for (i = 0; i < ca - 1; i++)
          printf("When A stops: flip A\n");
		printf("When A stops: ready\n");
	  }

	  return 0;
	}
	else if (t - (ca * a + cb * b) == b - a)
	{
      if (cb == 0)
	  {
        if (ca == 1)
		  printf("Initially: flip A and B\n");
		else
		  printf("Initially: flip A\n");
		for (i = 0; i < ca - 2; i++)
          printf("When A stops: flip A\n");
        if (ca > 1)
		  printf("When A stops: flip A and B\n");
		printf("When A stops: flip B\n");
        printf("When B stops: ready\n");
	  }
	  else if (ca == 0)
	  {
        if (cb == 1)
		  printf("Initially: flip A and B\n");
		else
		  printf("Initially: flip B\n");
		for (i = 0; i < cb - 2; i++)
          printf("When B stops: flip B\n");
        if (cb > 1)
		  printf("When B stops: flip A and B\n");
		printf("When A stops: flip A\n");
		printf("When B stops: flip A\n");
        printf("When A stops: ready\n");
	  }
	  else
	  {
	    printf("Initially: flip B\n");
	    for (i = 0; i < cb - 1; i++)
          printf("When B stops: flip B\n");
        printf("When B stops: flip A\n");
		for (i = 0; i < ca - 2; i++)
          printf("When A stops: flip A\n");
		printf("When A stops: flip A and B\n");
		printf("When A stops: flip B\n");
		printf("When B stops: ready\n");
	  }

	  return 0;
	}

	ca--;
	if (ca * a + cb * b + b <= t)
	  cb++;
  }
  
  printf("Impossible");

  return 0;
} /* End of 'main' function */

/* END OF 'B.C' FILE */
