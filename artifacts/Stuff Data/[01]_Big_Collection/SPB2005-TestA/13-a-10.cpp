#include <iostream.h>
#include <fstream.h>

int step (int num, int s)
{
  int res = 1;

  for (int i = 0; i < s; i++)
  {
    res *= num;
  }

  return res;
}

int signum( int num )
{
  int res = 1;

  while (num = (num / 10))
  {
    res++;
  }

  return res;
}

int sn( int num )
{
  return step(10, num) - step(10, num - 1) - 1;
}

int main()
{
  ifstream input("knumbers.in");
  ofstream output("knumbers.out");
  int k, n, i, res = 1, nres = 0, min = -1;

  input >> k;
  input >> n;

  while (signum(res) < signum(n))
  {
    if (signum(res) % k != 0)
      res *= 10;
    else
    {
      nres += sn(signum(res) / k);
      cout << nres
      res *= 10;
    }
  }

  while (n != 0)
  {
    i = n % step(10, signum(res) / k);
    if ((min < i) || (min == -1))
      min = i;

    n /= step(10, signum(res) / k);
  }

  nres += min - step(10, signum(res) / k - 1) - 1;

  output << nres;

  return 0;
}