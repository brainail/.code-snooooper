#include <stdio.h>
#include <string.h>

#define MAX_A 101
#define MAX_T 10013

#define FA  1
#define FAB 2

short IsT[MAX_T], Is[2][MAX_T][MAX_A], LWho[2][MAX_T][MAX_A], LA[2][MAX_T][MAX_A], LEv[2][MAX_T][MAX_A], LT[2][MAX_T][MAX_A];
short P[MAX_T * 2][2], PN = 0;
int T;

void Add( int who, int t, int a, int who1, int t1, int a1, int ev1 )
{
  if (t > T || t == t1)
    return;
  if (a < 0)
    a = 0;
  IsT[t] = 1;
  Is[who][t][a] = 1;
  LWho[who][t][a] = who1;
  LA[who][t][a] = a1;
  LEv[who][t][a] = ev1;
  LT[who][t][a] = t1;
}

int main( void )
{
  int A[2], i, t, who, a, t1, a1, who1, ev1, op;

  freopen("clocks.in", "rt", stdin);
  freopen("clocks.out", "wt", stdout);
  scanf("%d%d%d", &A[0], &A[1], &T);
  memset(Is, 0, sizeof(Is));
  memset(IsT, 0, sizeof(IsT));
  Is[0][0][0] = Is[1][0][0] = 1;
  IsT[0] = 1;
  for (t = 0; t < T; t++)
    if (IsT[t])
      for (who = 0; who <= 1; who++)
	for (a = 0; a <= A[1 - who]; a++)
	  if (Is[who][t][a])
	  {
	    Add(1 - who, t + a, 0, who, t, a, 0);
	    if (A[who] < a || a == 0)
	      Add(who, t + A[who], a - A[who], who, t, a, FA);
	    else
	      Add(1 - who, t + a, A[who] - a, who, t, a, FA);
	    if (A[who] < A[1 - who] - a)
	      Add(who, t + A[who], A[1 - who] - a - A[who], who, t, a, FAB);
	    else
	      Add(1 - who, t + A[1 - who] - a, A[who] - (A[1 - who] - a), who, t, a, FAB);
	  }
  for (who = 0; who <= 1; who++)
    for (a = 0; a <= A[1 - who]; a++)
      if (Is[who][t][a])
      {
	t = T;
	op = who;
	while (t != 0)
	{
	  t1 = LT[who][t][a];
          a1 = LA[who][t][a];
          ev1 = LEv[who][t][a];
          who1 = LWho[who][t][a];
          if (ev1)
            P[PN][0] = who1, P[PN++][1] = ev1;
          t = t1, a = a1, who = who1;
        }
        for (i = PN - 1; i >= 0; i--)
        {
	  if (i == PN - 1)
            printf("Initially: ");
	  else if (P[i][0] == 0)
	    printf("When A stops: ");
	  else
	    printf("When B stops: ");
	  if (P[i][1] == 2)
	    printf("flip A and B\n");
	  else if (P[i][0] == 0)
	    printf("flip A\n");
	  else
	    printf("flip B\n");
	}
	if (op == 0)
	  printf("When A stops: ready\n");
	else
	  printf("When B stops: ready\n");
	return 0;
      }
  printf("Impossible");
  return 0;
}
