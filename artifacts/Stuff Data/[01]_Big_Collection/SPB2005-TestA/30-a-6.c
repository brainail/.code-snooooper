#include <stdio.h>

#define m 2502
#define m2 502

int a[m][m2], b[m][m2], p1[m][m2], p2[m][m2], pi1[m][m2], pi2[m][m2], pj1[m][m2], pj2[m][m2],
    pt1[m][m2], pt2[m][m2], w1[m][m2], w2[m][m2], _stklen = 2 * 1024 * 1024;

void calc1( int tt, int t, int ii, int jj, int p, int ww )
{
  if (!a[t][0])
  {
    a[t][0] = 1;
    p1[t][0] = p;
    pi1[t][0] = ii;
    pj1[t][0] = jj;
    pt1[t][0] = tt;
    w1[t][0] = ww;
  }
  if (!b[t][0])
  {
    b[t][0] = 1;
    p2[t][0] = p;
    pi2[t][0] = ii;
    pj2[t][0] = jj;
    pt2[t][0] = tt;
    w2[t][0] = ww;
  }
}

void calc( int t, int i, int j, int ii, int jj, int p )
{
  if (j == 0 || i == 0)
  {
    calc1(t, t + i + j, ii, jj, p, (i == 0));
    return;
  }
  if (i < j)
  {
    if (b[t + i][j - i])
      return;
    b[t + i][j - i] = 1;
    p2[t + i][j - i] = p;
    pi2[t + i][j - i] = ii;
    pj2[t + i][j - i] = jj;
    pt2[t + i][j - i] = t;
    w2[t + i][j - i] = 0;
  }
  else
  {
    if (a[t + j][i - j])
      return;
    a[t + j][i - j] = 1;
    p1[t + j][i - j] = p;
    pi1[t + j][i - j] = ii;
    pj1[t + j][i - j] = jj;
    pt1[t + j][i - j] = t;
    w1[t + j][i - j] = 1;
  }
}

int write( int t, int i, int j )
{
  int w;

  if (j == 0)
  {
    if (pt1[t][i] == 0)
    {
      printf("Initially: ");
      if (p1[t][i] == 1)
        printf("flip A\n");
      else if (p1[t][i] == 2)
        printf("flip B\n");
      else if (p1[t][i] == 3)
        printf("flip A and B\n");
      return w1[t][i];
    }
    w = write(pt1[t][i], pi1[t][i], pj1[t][i]);
    if (p1[t][i] != 0)
    {
      if (w == 0)
        printf("When A stops: ");
      else
        printf("When B stops: ");
      if (p1[t][i] == 1)
        printf("flip A\n");
      else if (p1[t][i] == 2)
        printf("flip B\n");
      else if (p1[t][i] == 3)
        printf("flip A and B\n");
      return w1[t][i];
    }
    return w1[t][i];
  }
  else
  {
    if (pt2[t][j] == 0)
    {
      printf("Initially: ");
      if (p2[t][j] == 1)
        printf("flip A\n");
      else if (p2[t][j] == 2)
        printf("flip B\n");
      else if (p2[t][j] == 3)
        printf("flip A and B\n");
      return w2[t][j];
    }
    w = write(pt2[t][j], pi2[t][j], pj2[t][j]);
    if (p2[t][j] != 0)
    {
      if (w == 0)
        printf("When A stops: ");
      else
        printf("When B stops: ");
      if (p2[t][j] == 1)
        printf("flip A\n");
      else if (p2[t][j] == 2)
        printf("flip B\n");
      else if (p2[t][j] == 3)
        printf("flip A and B\n");
      return w2[t][j];
    }
    return w2[t][j];
  }
  
}

int main( void )
{
  int x, y, i, j, s = 2000, t, w, ss = 2000;

  freopen("clocks.in", "r", stdin);
  freopen("clocks.out", "w", stdout);
  scanf("%d%d%d", &x, &y, &t);
  a[0][0] = b[0][0] = 1;
  for (i = 0; i <= s; i++)
  {
    for (j = 0; j < x; j++)
    {
      if (!a[i][j]) 
        continue;
      //calc(i, j, 0, 0);
      calc(i, x - j, 0, j, 0, 1);
      calc(i, j, y, j, 0, 2);
      calc(i, x - j, y, j, 0, 3);
    }
    for (j = 0; j < y; j++)
    {
      if (!b[i][j]) 
        continue;
      //calc(i, 0, j, 0);
      calc(i, 0, y - j, 0, j, 2);
      calc(i, x, j, 0, j, 1);
      calc(i, x, y - j, 0, j, 3);
    }
  }
  if (t <= ss)
  {
    for (i = 0; i < x; i++)
      if (a[t][i])
        break;
    if (i < x)
    {
      w = write(t, i, 0);
      if (w)
        printf("When B stops: ready\n");
      else
        printf("When A stops: ready\n");
      return 0;
    }
    for (i = 0; i < y; i++)
      if (b[t][i])
        break;
    if (i < y)
    {
      w = write(t, 0, i);
      if (w)
        printf("When B stops: ready\n");
      else
        printf("When A stops: ready\n");
      return 0;
    }
    printf("Impossible\n");
  }
  else
  {
    int c, xx = x, yy = y;

    while (yy != 0)
    {
      c = xx % yy;
      xx = yy;
      yy = c;
    }
    if (t % xx)
    {
      printf("Impossible\n");
      return 0;
    }
    for (i = 0; i <= s; i++)
      if (a[i][xx])
        break;
    if (i <= s)
    {
      int f = 0, x1 = xx, y1 = 0;

      w = write(i, xx, 0);
      for (t -= i; t > 0; t -= xx)
      {
        if (w)
          printf("When B stops: ");          
        else
          printf("When A stops: ");          
        if (x1 == xx)
        {
          printf("flip B\n");
          x1 = 0, y1 = y - xx;
          w = 0;
        }
        else if (x1 == x - xx) 
        {
          printf("flip A and B\n");
          x1 = 0, y1 = y - xx;
          w = 0;
        }
        else if (y1 == xx)
        {
          printf("flip A\n");
          x1 = x - xx, y1 = 0, w = 1;
        }
        else
        {
          printf("flip A and B\n");
          x1 = x - xx, y1 = 0, w = 1;
        }
        //f = (f + 1) & 1;
        //w = f;
      }
      if (w)
        printf("When B stops: ready\n");          
      else
        printf("When A stops: ready\n");          
      return 0;
    }
    for (i = 0; i <= s; i++)
      if (b[i][xx])
        break;
    if (i <= s)
    {
      int f = 0, x1 = 0, y1 = xx;

      w = write(i, 0, xx);
      for (t -= i; t > 0; t -= xx)
      {
        if (w)
          printf("When B stops: ");          
        else
          printf("When A stops: ");          
        if (x1 == xx)
        {
          printf("flip B\n");
          x1 = 0, y1 = y - xx;
          w = 0;
        }
        else if (x1 == x - xx) 
        {
          printf("flip A and B\n");
          x1 = 0, y1 = y - xx;
          w = 0;
        }
        else if (y1 == xx)
        {
          printf("flip A\n");
          x1 = x - xx, y1 = 0, w = 1;
        }
        else
        {
          printf("flip A and B\n");
          x1 = x - xx, y1 = 0, w = 1;
        }
        //f = (f + 1) & 1;
        //w = f;
      }
      if (w)
        printf("When B stops: ready\n");          
      else
        printf("When A stops: ready\n");          
      return 0;
    }
    printf("Impossible\n");
  }
  return 0;
}