#include <stdio.h>
#include <string.h>

#define maxt 100010
#define maxa 110

#define ast 0
#define bst 1
#define init 2
#define fla 0
#define flb 1
#define flboth 2
#define ready 3

short pos[maxt][2][maxa];
short p[maxt][2][maxa][4];
int a, b, t;
int res[maxt][2], rl;
char *es[3] = { "When A stops", "When B stops", "Initially" };
char *as[4] = { "flip A", "flip B", "flip A and B", "ready" };

int gcd( int a, int b )
{
  while (a && b)
    if (a >= b)
      a %= b;
    else
      b %= a;
  return a + b;
}

void add( int tt, int aa, int bb, int a, int fa, int fb )
{
  if (aa >= bb && tt + bb <= t && !pos[tt + bb][1][aa - bb])
  {
    pos[tt + bb][1][aa - bb] = 1;
    p[tt + bb][1][aa - bb][0] = tt;
    p[tt + bb][1][aa - bb][1] = fa;
    p[tt + bb][1][aa - bb][2] = fb;
    p[tt + bb][1][aa - bb][3] = a;
  }
  if (aa <= bb && tt + aa <= t && !pos[tt + aa][0][bb - aa])
  {
    pos[tt + aa][0][bb - aa] = 1;
    p[tt + aa][0][bb - aa][0] = tt;
    p[tt + aa][0][bb - aa][1] = fa;
    p[tt + aa][0][bb - aa][2] = fb;
    p[tt + aa][0][bb - aa][3] = a;
  }
}

int main( void )
{
  int i, j, oa, ob, ot, ac;
  
  freopen("clocks.in", "rt", stdin);
  freopen("clocks.out", "wt", stdout);
  memset(pos, 0, sizeof(pos));
  scanf("%d%d%d", &a, &b, &t);
  i = gcd(a, b);
  if ((t % i) != 0)
  { 
    printf("Impossible");
    return 0;
  }
  t /= i, a /= i, b /= i;
  if (a >= maxa || b >= maxa)
  {
    printf("Impossible");
    return 0;
  }
  pos[0][0][0] = 1;
  pos[0][1][0] = 1;
  for (i = 0; i < t; i++)
  {
    for (j = 0; j <= b; j++)
      if (pos[i][0][j])
      {
        add(i, a, j, fla, 0, j);
        add(i, 0, b - j, flb, 0, j);
        add(i, a, b - j, flboth, 0, j);
      }
    for (j = 0; j <= a; j++)
      if (pos[i][1][j])
      {
        add(i, j, b, flb, j, 0);
        add(i, a - j, b, fla, j, 0);
        add(i, a - j, b, flboth, j, 0);
      }
  }
  for (i = 0; i <= b; i++)
    if (pos[t][0][i])
    {
      rl = 0;
      res[rl][0] = ast, res[rl++][1] = ready;
      j = i, i = 0;
      while (t > 0)
      {
        if (i == 0)
        {
          ot = p[t][0][j][0];
          oa = p[t][0][j][1];
          ob = p[t][0][j][2];
          ac = p[t][0][j][3];
          res[rl][0] = bst, res[rl++][1] = ac;
          i = oa, j = ob, t = ot;
        }
        else
        {
          ot = p[t][1][i][0];
          oa = p[t][1][i][1];
          ob = p[t][1][i][2];
          ac = p[t][1][i][3];
          res[rl][0] = ast, res[rl++][1] = ac;
          i = oa, j = ob, t = ot;
        }
      }
      res[rl - 1][0] = 2;
      for (i = rl - 1; i >= 0; i--)
        printf("%s: %s\n", es[res[i][0]], as[res[i][1]]);
      return 0;
    }
  for (i = 0; i <= a; i++)
    if (pos[t][1][i])
    {
      rl = 0;
      res[rl][0] = ast, res[rl++][1] = ready;
      j = 0;
      while (t > 0)
      {
        if (i == 0)
        {
          oa = p[t][0][j][0];
          ob = p[t][0][j][1];
          ot = p[t][0][j][2];
          ac = p[t][0][j][3];
          res[rl][0] = bst, res[rl++][1] = ac;
          i = oa, j = ob, t = ot;
        }
        else
        {
          oa = p[t][1][i][0];
          ob = p[t][1][i][1];
          ot = p[t][1][i][2];
          ac = p[t][1][i][3];
          res[rl][0] = ast, res[rl++][1] = ac;
          i = oa, j = ob, t = ot;
        }
      }
      res[rl - 1][0] = 2;
      for (i = rl - 1; i >= 0; i--)
        printf("%s: %s\n", es[res[i][0]], as[res[i][1]]);
      return 0;
    }
  printf("Impossible");
  return 0;
}
