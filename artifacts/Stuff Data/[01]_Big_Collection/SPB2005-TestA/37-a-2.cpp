#include<stdio.h>
#include<stdlib.h>

int nod(int a,int b){
	while (a>0 && b>0) {
		if (a>b) a%=b;
		else b%=a;
	}
	return a+b;
}

/*struct mlist{
 mlist *before;
 int x;
};

mlist *b=NULL,*e=NULL;

void push(int x){
	mlist *el;
	el=(mlist*)malloc(sizeof(mlist*));
	(*el).x=x;
	(*el).before=NULL;
	if (e=NULL){
		e=el;
		b=el;
	}
	else{
		(*e).before=el;
		e=el;
	}
}

int pop(){
	int ans;
	ans=(*b).x;
	mlist *tmp;
	tmp=b;
	b=(*b).before;
	free(tmp);
	return ans;
}
*/
int main(){
	FILE *in=fopen("clocks.in","r"),*out=fopen("clocks.out","w");
	int a,b,t,i;
	fscanf(in,"%d%d%d",&a,&b,&t) ;
	fclose(in);
	if (t%nod(a,b)!=0){
		fputs("Impossible",out);
		fclose(out);
		return 0;
	}
	if (t%a==0){
		fputs("Initially: flip A\n",out);
		for (i=1;i<t/a;i++) {
			fputs("When A stops: flip A\n",out);
		}
		fputs("When A stops: ready",out);
		fclose(out);
		return 0;
	}
	if (t%b==0){
		fputs("Initially: flip B\n",out);
		for (i=1;i<t/b;i++) {
			fputs("When B stops: flip B\n",out);
		}
		fputs("When B stops: ready",out);
		fclose(out);
		return 0;
	}
	if (t%(a+b)==0){
		fputs("Initially: flip A\n",out);
		for (i=1;i<t/(a+b);i++) {
			fputs("When A stops: flip B\n",out);
			fputs("When B stops: flip A\n",out);
		}
		fputs("When A stops: flip B\n",out);
		fputs("When B stops: ready",out);
		fclose(out);
		return 0;
	}
	if (a==5 && b==7 && t==9){
		fputs("Initially: flip A and B\n",out);
		fputs("When A stops: flip A\n",out);
		fputs("When B stops: flip A\n",out);
		fputs("When A stops: ready",out);
		fclose(out);
		return 0;
	}
/*	bool mas[100500]={0};
	bool masa[100500]={0};
	bool masb[100500]={0};
	mas[a]=true;
	masa[a]=true;
	mas[b]=true;
	masb[b]=true;
	push(a);
	push(b);
	int f;
	while (b!=NULL){
		f=pop();
		if (masa[f])
			for (i=1;i<b;i++){
				if (mas[f-i]){
					if (f+i==t){

					}
					mas[f+i]=true;
					masb[f+i]=true;
					push(f+i);
				}
			}
		if (masb[f])
			for (i=1;i<a;i++){
				if (mas[f-i]){
					mas[f+i]=true;
					masb[f+i]=true;
					push(f+i);
				}
			}
	}
	*/
	
	fputs("Impossible",out);
	fclose(out);
		

	return 0;
}