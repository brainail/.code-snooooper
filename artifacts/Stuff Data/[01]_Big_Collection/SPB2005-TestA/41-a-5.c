#include <stdio.h>

void main( void )
{
  int a, b, u, i, l = 1, k = 0;
  long t, v, w;
  FILE *F1, *F2;

  F1 = fopen("clocks.in",  "rt");
  F2 = fopen("clocks.out",  "wt");
  fscanf(F1, "%d %d %li", &a, &b, &t);
  if (a % 2 == 0 && b % 2 == 0 && t % 2 == 1)
    fprintf(F2, "Impossible");
  else
  {
	 if (b - a != t - b)
	{
       for (v = 1; v <= 100000; v++)
	if (t == v * b)
	{
	  fprintf(F2, "Initially: flip B\n");
	  for (w = 1; w < v; w++)
	  {
	  fprintf(F2, "When B stops: flip B\n");
	  }
	  fprintf(F2, "When B stops: ready");
	  l--;
	  k = 1;
	  }
	 for (v = 1; v <= 100000; v++)
	if (t == v * a && k == 0)
	{
	  fprintf(F2, "Initially: flip A\n");
	  for (w = 1; w < v; w++)
	  {
	  fprintf(F2, "When A stops: flip A\n");
	  }
	  fprintf(F2, "When A stops: ready");
	  l--;
	  k = 1;
	}
	if (l == 1)
	{
	 fprintf(F2, "Impossible");
	}
       }
    if (t % (a + b) == 0 && k == 0)
    {
      fprintf(F2, "Initially: flip A\n");
      u = t / (a + b);
      for (i = 0; i < u; i++)
      {
	fprintf(F2, "When A stops: flip B\n");
	if (i != u - 1)
	  fprintf(F2, "When B stops: flip A\n");
	else
	  fprintf(F2, "When B stops: ready");
      }
      k = 1;
    }
    if (b > a)
    {
//       if (b - a < a)
//       {
       if (b -  a == t - b && k == 0)
	{
	  fprintf(F2, "Initially: flip A and B\n");
	  fprintf(F2, "When A stops: flip A\n");
	  fprintf(F2, "When B stops: flip A\n");
	  fprintf(F2, "When A stops: ready");
	}
	}

       //	}
    }
    if (a > b && k == 0)
    {
//       if (a - b < b)
//       {
	if (a - b == t - a)
	{
	  fprintf(F2, "Initially: flip A and B\n");
	  fprintf(F2, "When B stops: flip B\n");
	  fprintf(F2, "When A stops: flip B\n");
	  fprintf(F2, "When B stops: ready");
	}
//       }

  }
  fclose(F1);
  fclose(F2);
}









