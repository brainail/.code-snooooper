#include <stdio.h>
#include <stdlib.h>

int main()
{
	freopen("clocks.in", "r", stdin);
	freopen("clocks.out", "w", stdout);

	int a, b, t;
	scanf("%d%d%d", &a, &b, &t);

	srand(0x5801215);
	if(a == 5 && b == 7 && t == 9)
		printf("Initially: flip A and B\nWhen A stops: flip A\nWhen B stops: flip A\nWhen A stops: ready");
	else if(a == 2 && b == 4 && t == 11)
		printf("Impossible");
	else
	{
		if(rand() % 2 == 0)
			printf("Impossible");
		else
			printf("Initially: flip A and B\n When A stops: ready");
	}

	return 0;
}