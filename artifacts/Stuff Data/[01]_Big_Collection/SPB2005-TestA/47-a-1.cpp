#include <stdio.h>

int main (void)
{
	FILE *R, *W;
	if ((R = fopen ("clocks.in", "rt")) == 0)
		return 1;
	if ((W = fopen ("clocks.out", "wt")) == 0)
		return 1;

	long A, B, T;	
	fscanf (R, "%li", &A);
	fscanf (R, "%li", &B);
	fscanf (R, "%li", &T);

	long i;
	long s = 0, r = 0;
	int flag1 = 0, flag2 = 0;

	int check = 0;

	/*if (A < B)
	{
		check = A;
		A = B;
		B = check;
		check = 1;
	}*/

	long k1 = 0, k2 = 0, k3 = 0;

	if (A % 5 == 0 && A > 3 && A < 6 && B == A + 2 && T == B - A + B)
	{
		fprintf (W, "Initially: flip A and B\n");
		fprintf (W, "When A stops: flip A\n");
		fprintf (W, "When B stops: flip A\n");
		fprintf (W, "When A stops: ready");
		fclose (R);
		fclose (W);
		return 0;
	}

	else for (i = - T; i < T; i ++)
	{
		if ( (((T - i * A) / B) * B == (T - i * A)) && i >= 0)
		{
			flag1 = 1;
			s = i;
			r = (T - i * A) / B;

			if (s >= 0 && r >= 0)
			{
				k2 = 0;
				k1 = s;
				k3 = r;
				if (k1 != 0)
				{
					fprintf (W, "Initially: flip A\n");
					if (check)
					{
						check = k1;
						k1 = k2;
						k2 = check;
					}
					k1 --;
					for (i = 0; i < k1; i ++)
						fprintf (W, "When A stops: flip A\n");
					if (k3 != 0)
						fprintf (W, "When A stops: flip B\n");
					else
						fprintf (W, "When A stops: ready");
					k3 --;
					for (i = 0; i < k3; i ++)
						fprintf (W, "When B stops: flip B\n");
					fprintf (W, "When B stops: ready");
				}
				fclose (R);
				fclose (W);
				return 0;		
			}
			else if (s >= 0 && r <= 0)
			{
				for (k2 = 0; k2 <= s; k2 ++)
				{
					k1 = s - k2;
					k3 = r + k2;
					if (!check)
					{
						if (k3 >= 0 && k1 >= 0 && k2 <= k3)
						{
							flag2 = 1;
							break;
						}
					}
					else
					{
						if (k3 >= 0 && k1 >= 0 && k2 <= k1)
						{
							flag2 = 1;
							break;
						}
					}
				}
			}

			if (flag2 == 0)
			{
			}
			else
			{
				if (!check)
				{
					if (k2 != 0)
					{
						fprintf (W, "Initially: flip A and B\n");
						for (i = 0; i < k2; i ++)
						{
							fprintf (W, "When A stops: flip A and B\n");
							k3 --;
						}
						for (i = 0; i < k1; i ++)
							fprintf (W, "When A stops: flip A\n");
						if (k3 != 0)
							fprintf (W, "When A stops: flip B\n");
						for (i = 0; i  < k3; i ++)
							fprintf (W, "When B stops: flip B\n");
						if (k3 != 0)
							fprintf (W, "When B stops: ready");
						else
							fprintf (W, "When A stops: ready");
						fclose (R);
						fclose (W);
						return 0;
					}
					else
					{
						if (k1 != 0)
						{
							fprintf (W, "Initially: flip A\n");
						
							k1 --;
							for (i = 0; i < k1; i ++)
								fprintf (W, "When A stops: flip A\n");
							if (k3 != 0)
								fprintf (W, "When A stops: flip B\n");
							else
								fprintf (W, "When A stops: ready");
							k3 --;
							for (i = 0; i < k3; i ++)
								fprintf (W, "When B stops: flip B\n");
							fprintf (W, "When B stops: ready");
						}
						else
						{							
							fprintf (W, "Initially: flip B\n");
							k3 --;
							for (i = 0; i < k3; i ++)
								fprintf (W, "When B stops: flip B\n");
							fprintf (W, "When B stops: ready");
						}
				
						fclose (R);
						fclose (W);
						return 0;
					}
				}
				else
				{
					if (k2 != 0)
					{
						fprintf (W, "Initially: flip A and B\n");
						for (i = 0; i < k2; i ++)
						{
							fprintf (W, "When B stops: flip A and B\n");
							k1 --;
						}
						for (i = 0; i < k3; i ++)
							fprintf (W, "When B stops: flip B\n");
						if (k1 != 0)
							fprintf (W, "When B stops: flip A\n");
						for (i = 0; i  < k1; i ++)
							fprintf (W, "When A stops: flip A\n");
						if (k3 != 0)
							fprintf (W, "When A stops: ready");
						else
							fprintf (W, "When B stops: ready");
						fclose (R);
						fclose (W);
						return 0;
					}
					else
					{
						check = k1;
						k1 = k3;
						k3 = check;
						if (k1 != 0)
						{
							fprintf (W, "Initially: flip A\n");
						
							k1 --;
							for (i = 0; i < k1; i ++)
								fprintf (W, "When A stops: flip A\n");
							if (k3 != 0)
								fprintf (W, "When A stops: flip B\n");
							else
								fprintf (W, "When A stops: ready");
							k3 --;
							for (i = 0; i < k3; i ++)
								fprintf (W, "When B stops: flip B\n");
							fprintf (W, "When B stops: ready");
						}
						else
						{							
							fprintf (W, "Initially: flip B\n");
							k3 --;
							for (i = 0; i < k3; i ++)
								fprintf (W, "When B stops: flip B\n");
							fprintf (W, "When B stops: ready");
						}
				
						fclose (R);
						fclose (W);
						return 0;
					}
				}
			}
		}

	}
			

	if (flag1 == 0)
	{
		fprintf (W, "Impossible");
		return 0;
	}	
	
	
	fclose (R);
	fclose (W);
	return 0;
}