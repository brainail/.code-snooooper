#include <stdio.h>

int number[100];

void correct( int *n )
{
  int j, carry = 0;

  for (j = 0; j < 100; j++)
  {
    n[j] += carry;
    carry = 0;
    if (n[j] > 9)
    {
      carry = n[j] / 10;
      n[j] %= 10;
    }
  }
}

int cmp( int *num, int *ch, int l )
{
  int i;
  int p = 1;

  for (i = l - 1; i >= 0; i--)
  {
    if (num[i] != ch[i])
      p = 0;
    if (num[i] < ch[i])
      return -1;

  }
  if (p == 1)
    return 0;
  return 1;
}

int len(int *num)
{
  int j, jmax = 0;

  for (j = 0; j < 100; j++)
    if (num[j] != 0)
      jmax = j + 1;
  if (jmax == 0)
    jmax++;
  return jmax;
}



int check( int *num, int k, int l )
{
  int simp[100];
  int j, i;

  if (l%k != 0)
    return 0;
  for (j = 0; j < l / k; j++)
    simp[j] = num[j];
  for (i = 0; i < l; i += l / k)
    if (cmp(num + i, simp, l / k) != 0)
      return 0;
  return 1;
}

int main( void )
{
  FILE *F1, *F2;
  int k, i, p, j;
  static int num[100];
  double n = 0;

  F1 = fopen("knumbers.in", "rt");
  F2 = fopen("knumbers.out", "wt");
  fscanf(F1, "%d", &k);
  do
  {
    p = fgetc(F1);
  }
  while (!(p <= 57 && p >= 48));
  fseek(F1, -1, 1);
  for (p = i = 0; i < 100 && !feof(F1); i++, p++)
    number[i] = fgetc(F1) - '0';
  p--;
  number[p - 1] = 0;
  for (i = 0; i < p/2; i++)
  {
    j = number[i];
    number[i] = number[p - i - 2];
    number[p - i - 2] = j;
  }
  while (cmp(num, number, 100) <= 0)
  {
    if (check(num, k, len(num)))
      n++;
    num[0] += 1;
    correct(num);
  }
  fprintf(F2, "%.0lf", n);
  fclose(F1);
  fclose(F2);
  return 0;
}