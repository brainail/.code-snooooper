#include <stdio.h>

__int64 Pow10(__int64 x)
{
	__int64 res, i;
	res = 1;
	for (i = 0; i < x; i++)
		res *= 10;
	if (x == -1)
		return 0;
	return res;
}

int main( void )
{
	__int64 K, N, NS, MS, tmp, Ans, Add, i, j, MK, Max, Mlt;
	freopen("knumbers.in", "rt", stdin);
	freopen("knumbers.out", "wt", stdout);
	scanf("%I64i%I64i", &K, &N);
	
	tmp = N;
	MS = 0;
	Ans = 0;


	while (tmp != 0)
		MS++, tmp /= 10;
	for (NS = 1; NS < MS; NS++)
		if (NS % K == 0)
		{
			tmp = NS / K;
			Add = 1;
			Add = Pow10(tmp - 1);
			Add *= 9;
			Ans += Add;
		}
	if (NS % K == 0)
	{
		MK = N / Pow10((MS - (MS / K)));	
		
		Add = 1;
		tmp = MS / K;
		Max = 0;
		Mlt = Pow10(tmp);
		for (i = 1; i < K; i++)
			Max += MK, Max *= Mlt;
		Max += MK; 
		if (Max <= N)
			Ans++;
		Ans += (MK - 1) - Pow10(tmp - 2) * 9;
		
	}
	printf("%I64i", Ans);
    return 0;
}
