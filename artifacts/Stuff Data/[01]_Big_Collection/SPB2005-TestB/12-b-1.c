/**************************************************************
 * Copyright (C) 2005
 *    Computer Graphics Support Group of 30 Phys-Math Lyceum
 **************************************************************/

/* FILE NAME   : B.C
 * PURPOSE:    : Olympiad task B.
 * PROGRAMMER  : Anton Timofeev.
 * LAST UPDATE : 06.03.2005
 * NOTE        : None.
 *
 * No part of this file may be changed without agreement of
 * Computer Graphics Support Group of 30 Phys-Math Lyceum
 */

#include <stdio.h>

#define MAX_N 300

int A[MAX_N], K, N = 0, T;
int B[MAX_N];

/* Compare two N-part numbers */
int NumCmp( void )
{
  int i;

  for (i = 0; i < N; i++)
	if (A[i] < A[i % T])
	  return 0;
	else if (A[i] > A[i % T])
	  return 1;
  return 1;
} /* End of 'NumCmp' function */

/* Substract one from first part-number */
void NumSubOne( void )
{
  int j = T - 2;

  if (A[T - 1] == 0)
  {
    while (A[j] == 0)
	  j--;
	A[j++]--;
	for (; j < T; j++)
      A[j] = 9;
  }
  else
    A[T - 1]--;
} /* End of 'NumSubOne' function */

/* The main program function */
int main( void )
{
  int i;
  char c;

  freopen("knumbers.in", "rt", stdin);
  freopen("knumbers.out", "wt", stdout);

  scanf("%i", &K);
  while (scanf("%c", &c) == 1)
    if (c >= '0' && c <= '9')
	  A[N++] = c - '0';

  T = N / K;
  if (N < K)
	printf("0");
  else if (N % K != 0)
    for (i = 0; i < T; i++)
	  printf("9");
  else
  {
    if (!NumCmp())
	  NumSubOne();
	i = 0;
	while (i < T && A[i] == 0)
	  i++;
	if (i >= T)
      printf("0");
	else
	  for (; i < T; i++)
		printf("%i", A[i]);
  }
	
  return 0;
} /* End of 'main' function */

/* END OF 'B.C' FILE */
