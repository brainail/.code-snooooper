#include <iostream.h>
#include <fstream.h>

int step (int num, int s)
{
  int res = 1;

  for (int i = 0; i < s; i++)
  {
    res *= num;
  }

  return res;
}

int signum( unsigned long num )
{
  int res = 1;

  while (num)
  {
    num /= 10;
    res++;
  }

  return res - 1;
}

int sn( int num )
{
  if (num == 1)
    return 9;
  else
    return step(10, num);
}

int main()
{
  ifstream input("knumbers.in");
  ofstream output("knumbers.out");
  unsigned long n, res = 1;
  int k, i, nres = 0, min = -1;

  input >> k;
  input >> n;


  while (signum(res) < signum(n))
  {
    if (signum(res) % k != 0)
      res *= 10;
    else
    {
      nres += sn(signum(res) / k);
      res *= 10;
    }
  }

  while (n != 0)
  {
    i = n % step(10, signum(res) / k);
    if ((min > i) || (min == -1))
      min = i;

    n /= step(10, signum(res) / k);
  }

  nres += min - step(10, signum(min) - 1) + 1;

  output << nres;

  return 0;
}