#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
  int k,l;
  long i,j;
  long n;
  long ans;
  char s[100];
  FILE *fin=fopen("knumbers.in","r");
  FILE *out=fopen("knumbers.out","w");
  fscanf(fin,"%d\n",&k);
  fgets(s,100,fin);
  i=0;
  while ((s[i]!=0)&&(s[i]!=10)&&(s[i]!=13)) i++;
  l=i;
  if (l%k != 0) {
    ans=pow(10,int(l/k))-1;
  } else {
    for (i=0;i< (k);i++) {
      if (i==1) {
	ans=n;
      }
      n=0;
      for (j=0;j<l/k;j++) {
	n*=10;
	n+=(s[i*(l/k)+j]-48);
      }
      if (i>0) {
	if (ans<n) break;
	if (ans>n) {
	  ans--;
	  break;
	}
      }
    }

  }

  fprintf(out,"%ld",ans);
  fclose(fin);
  fclose(out);
  return 0;
}