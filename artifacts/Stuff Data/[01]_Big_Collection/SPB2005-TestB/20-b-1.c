#include <stdio.h>
#include <string.h>

#define LEN 111

int Len, K, Res[LEN];
char S[LEN];
int A[LEN], Min[LEN], Max[LEN], Ave[LEN], Tmp[LEN];

void Add( int *A, int *B )
{
  int i;

  for (i = 0; i < LEN; i++)
    A[i] += B[i];
  for (i = 0; i < LEN; i++)
    if (A[i] >= 10)
      A[i] -= 10, A[i + 1]++;
}

void Shr( int *A )
{
  int i;
  
  A[0] = (A[0] + 1) >> 1;
  for (i = 1; i < LEN; i++)
  {
    if (A[i] & 1)
      A[i - 1] += 5; 
    A[i] >>= 1;
  }
}

int main( void )
{
  int i, j, num;

  memset(Res, 0, sizeof(Res));
  memset(A, 0, sizeof(A));
  memset(Min, 0, sizeof(Min));
  memset(Max, 0, sizeof(Max));

  freopen("knumbers.in", "rt", stdin);
  freopen("knumbers.out", "wt", stdout);
  scanf("%d%s", &K, S);
  Len = strlen(S);

  for (i = 1; K * i < Len; i++)
    Res[i - 1] += 9;
  if (Len % K == 0)
  {
    num = Len / K;
    for (i = 0; i < Len; i++)
      A[i] = S[Len - i - 1] - '0';
    for (i = 0; i < num; i++)
      Max[i] = 9;

    while (1)
    { 
      for (i = 0; i < num; i++)
        if (Min[i] != Max[i])
          break;
      if (i == num)
        break;
      memcpy(Ave, Min, sizeof(Ave));
      Add(Ave, Max);
      Shr(Ave);
      for (i = 0; i < Len; i += num)
        for (j = 0; j < num; j++)
          Tmp[i + j] = Ave[j];
      for (i = Len - 1; i >= 0; i--)
        if (A[i] != Tmp[i])
          break;
      if (i < 0 || (i >= 0 && A[i] > Tmp[i]))
      {
        for (j = 0; j < num; j++)
          Min[j] = Ave[j];
      }
      else
      {
        for (j = 0; j < num; j++)
          Max[j] = Ave[j];
        Max[0]--;
        for (j = 0; Max[j] < 0; j++)
          Max[j] += 10, Max[j + 1]--;
      }
    }
    if (Min[num - 1] != 0)
    {
      Add(Res, Min);
      for (i = 0; i < num - 1; i++)
        Res[i] -= 9;
      for (i = 0; i < LEN; i++)
        if (Res[i] < 0)
          Res[i] += 10, Res[i + 1]--;
    }
  }
  
  for (i = LEN - 1; !Res[i] && i > 0; i--)
    ;
  
  while (i >= 0)
    printf("%d", Res[i--]);
  return 0;
}
