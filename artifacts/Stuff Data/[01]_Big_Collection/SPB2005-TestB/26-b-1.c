#include<stdio.h>
#include<stdlib.h>

void sum(int *res,int *b,int k){
	int i;

	for(i=0;i<k;i++){
		res[k-1-i]+=b[i];
	}
	for(i=0;i<109;i++){
		res[i]+=10;
		res[i+1]+=(res[i]/10)-1;
		res[i]%=10;
	}
}

void step(int *res,int st,int plm){
	int i;
	res[st]+=plm;
	for(i=0;i<109;i++){
		res[i]+=10;
		res[i+1]+=(res[i]/10)-1;
		res[i]%=10;
	}
}

int main(){
	FILE *in=fopen("knumbers.in","r"),*out=fopen("knumbers.out","w");
	int k;
	char ch[110];
	int n[110];
	int i,j,length;
	int result[110];
	int period[110];
	int flag,ln,diff;

	fscanf(in,"%d\n",&k);
	fgets(ch,102,in);
	for(i=0;((i<110)&&(ch[i]!='\0')&&(ch[i]!=' ')&&(ch[i]!='\n'));i++){
		n[i]=ch[i]-'0';
	}
	length=i;
	for(i=0;i<110;i++){
		result[i]=0;
		period[i]=0;
	}

	/*i- length of the part*/
	for(i=1;i*k<length;i++){
		step(result,i,1);
		step(result,i-1,-1);
	}


	flag=0;
	if(i*k==length){
		ln=i;
		for(j=1;j<length/k;j++){
			for(i=0;((i<k)&&(n[i+j*k]==n[i]));i++);
			if(i+j*k<length){
				if(n[i+j*k]>n[i]) flag=1;
				if(n[i+j*k]<n[i])	flag=-1;
				j=length/k;
			}else{
				flag=1;
			}
		}
		if(flag==0) flag=1;
		sum(result,n,ln);
		step(result,0,+1);
		if(flag==-1){
			step(result,0,-1);
		}

		step(result,ln-1,-1);
	}


	for(i=109;((i>0)&&(result[i]==0));i--);
	for(;i>=0;i--){
		fprintf(out,"%d",result[i]);
	}
	fclose(in);
	fclose(out);
	return 0;
}