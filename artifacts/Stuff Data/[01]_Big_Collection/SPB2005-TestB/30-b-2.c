#include <stdio.h>
#include <string.h>

#define mm 200
#define m 102

int a[mm], b[mm];
char s[mm];

void add( void )
{
  int i, c = 0;

  for (i = 0; i <= m; i++)
  {
    a[i] += b[i] + c;
	  c = a[i] / 10;
	  a[i] %= 10;
  }
}

void dec( int *a, int t )
{
  int i, c = 1;

  for (i = t; i <= m; i++)
  {
    a[i] -= c;
    if (a[i] < 0)
    {
      a[i] += 10;
      c = 1;
    }
    else
      c = 0; 
  }
}

int main( void )
{
  int i, j, k, l;

  freopen("knumbers.in", "r", stdin);
  freopen("knumbers.out", "w", stdout);
  scanf("%d%s", &k, s);
  l = strlen(s);
  if (l % k)
  {
    a[l / k] = 1;
	  dec(a, 0);
  }
  else
  {
    a[(l / k) - 1] = 1;
	  //dec(a, 0);
	  for (i = (l / k) - 1, j = 0; i >= 0; j++, i--)
      b[j] = s[i] - '0';
    dec(b, (l / k) - 1);
    //dec(b, 0);
	  add();
    //inc();
	  for (i = 0; i < l; i++)
      if (s[i] > s[i % (l / k)]) 
        break;
	    else if (s[i] < s[i % (l / k)])
      {
        dec(a, 0);
		    break;
      }
  }
  for (i = m; i >= 0 && a[i] == 0; i--)
    ;
  if (i < 0)
    printf("0");
  else
    for (; i >= 0; i--)
      printf("%d", a[i]);
  printf("\n");
  return 0;
}