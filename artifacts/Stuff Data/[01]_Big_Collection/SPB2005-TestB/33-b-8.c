/******************************************************************
 * CGSG - forever!!!
 ******************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

int Func( int *p, int i, int k )
{
    int j, t, u[100], v;
	int min[100], F;
	__int64 Res;

    memset(min, 10, sizeof(int) * 100);
	for (j = 0; j < i / k; j++)
    	for (t = 0; t < k; t++)
			if (p[j * k + t] < min[t])
				min[t] = p[j * k + t];
	for (j = 0; j < k; j++)
		if (j == k - 1)
		{
			if (i % k != 0)
			  min[j]++;
		}
		else
			min[j]++;

    Res = 0;
	for (j = 0; j < i; j++)
		u[j] = 0;
	F = 1;
		for (j = i / k - 2; j >= 0; j--, F = 1)
			for (; F == 1; u[j]++)
    			for (t = i - j - 2; t >= 0; t -= i / k)
				{
					if (u[j] > p[t] && p[t + i / k - 1 - j] <= min[i / k - 1])
					{
						F = 0;
						break;
					}
					else if (u[j] == p[t] && t - i / k >= 0)
						continue;
  					Res += pow(10, j);
					break;
				}
	Res += (p[i - 1] - 1) * pow(10, i / k - 1);
	return Res;
}

int main ( void )
{
	FILE *FIn, *FOut;
    int k, i, j, t;
	int n[100], min[100], p[100];
	char c;
	__int64 Res, H, q;

	if ((FIn = fopen("knumbers.in", "rt")) == NULL)
		return 0;
	fscanf(FIn, "%I64i", &k);
	if (k == 0)
	{
	fclose(FIn);
		return 0;
	}
	c = ' ';
	while (c == ' ' || c == '\n')
	  fscanf(FIn, "%c", &c);
    n[0] = c - '0';
	for (i = 1; ;i++)
	{
	  if (fscanf(FIn, "%c", &c) != EOF)
	  {
		  if (c == ' ' || c == '\n')
			  break;
		  else
	        n[i] = c - '0';
	  }
	  else
	    break;
	}
	for (j = 0; j < i; j++)
		p[j]=  n[i - j - 1];
	Res = 0;
	Res += Func(p, i, k);
	for (j = i / k - 1; j > 0; j--)
	{
		H = 0;
		for (t = 0; t < j; t++)
		{
			if (t == 0)
				q = 9;
			else
				q = 10;

			H += pow(10, t) * q;
		}
		Res += pow(H, j);
	}
	fclose(FIn);
	if ((FOut = fopen("knumbers.out", "wt")) == NULL)
	{
		fclose(FIn);
		return 0;
	}
    fprintf(FOut, "%I64i", Res);
	fclose(FOut);
	return 0;
}