#include <stdio.h>
#include <string.h>

#define maxn 200
#define maxl 200

char n[maxn];
int k, l;
int res[maxl];
int mm;

void add10d( int *a, int d )
{
  a[d]++;
  while (a[d] >= 10)
    a[d + 1]++, a[d] -= 10, d++;
}

void sub10d( int *a, int d )
{
  a[d]--;
  while (a[d] < 0)
    a[d + 1]--, a[d] += 10, d++;
}

int cmp( char *a, char *b, int l )
{
  int i;

  for (i = 0; i < l; i++)
    if (a[i] < b[i])
      return -1;
    else if (a[i] > b[i])
      return 1;
  return 0;
}

void addc( int *a, char *c, int l )
{
  int i;

  for (i = l - 1; i >= 0; i--)
    a[l - i - 1] += c[i] - '0';
  for (i = 0; i < maxl; i++)
    if (a[i] > 10)
      a[i + 1] += a[i] / 10, a[i] %= 10;
}

void print( int *a )
{
  int i;

  for (i = maxl - 1; i >= 0 && !a[i]; i--)
    ;
  if (i < 0)
    printf("0");
  for (; i >= 0; i--)
    printf("%d", a[i]);
}

int main( void )
{
  int i;

  freopen("knumbers.in", "rt", stdin);
  freopen("knumbers.out", "wt", stdout);
  scanf("%d\n", &k);
  gets(n);
  l = strlen(n);
  if (n[l - 1] == '\n')
    l--;
  memset(res, 0, sizeof(res));
  if ((l % k) == 0)
  {
    mm = 0;
    for (i = 0; i < k; i++)
      if (cmp(&n[mm], &n[i * l / k], l / k) > 0)
        mm = i * l / k;
    addc(res, &n[mm], l / k);
    if (cmp(&n[mm], &n[0], l / k) == 0)
      add10d(res, 0), sub10d(res, l / k - 1);
  }
  for (i = 1; i < l; i++)
    if ((i % k) == 0)
      add10d(res, i / k), sub10d(res, 0);
  print(res);
  return 0;
}
