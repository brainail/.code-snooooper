#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int main(){
  FILE *fi = fopen("knumbers.in", "r"),
       *fo = fopen("knumbers.out", "w");
  if (!fi) printf("\nfi err\n");
  if (!fo) printf("\nfo err\n");
  int k = -1;
  float res;
  char n[127];
  fscanf(fi, "%d %s", &k, n);
  short int nk = strlen(n) / k;
  int i, j;
  if (k > strlen(n)){
    res = 0;
    fprintf(fo, "%0.0f", res);
    fclose(fo);
    return 0;
  }
  for (res = 1, i = 1; i < nk; i++, res *= 10);
  res--;
  if (!(strlen(n) % k)){
    char sss[63];
    strncpy(sss, n, nk);
    sss[nk] = 0;
    float tmp = 0;
    tmp = atof(sss) + 1;
    float tmp2 = 1;
    for (i = 1; i < nk; i++, tmp2 *= 10);
    tmp -= tmp2;
    res += tmp;
  }
  fprintf(fo, "%0.0f", res);
  fclose(fo);
  return 0;
}