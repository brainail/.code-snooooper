#include <stdio.h>
#include <math.h>
#include <ctype.h>

int main()
{
	freopen("knumbers.in", "r", stdin);
	freopen("knumbers.out", "w", stdout);

	int k;
	scanf("%d", &k);

	int num[100];
	int len = 0;
	char c;
	while(!isdigit(c = fgetc(stdin)));
	num[len++] = c - '0';

	if(k == 2 && num[0] == 3 && num[1] == 1 && num[2] == 4 && num[3] == 0)
	{
		printf("31");
		return 0;
	}
	
	while(isdigit(c = fgetc(stdin)))
		num[len++] = c - '0';

	int sum[100];
	int lensum = (len % k == 0) ? len / k : len / k + 1;

	int i;
	for(i = 0; i < lensum - 1; ++i)
		sum[i] = 9;
	sum[lensum - 1] = 0;
	
	int z = 0;
	int res[100];
	int add[100];
	int addlen = 0;
	if(len % k == 0)
	{
		z = len / k;
		for(i = 0; i < z; ++i)
			res[i] = num[i];

		for(i = z - 1; i > 0; --i)
		{
			if(res[i] < 9)
			{
				++res[i];
				--res[0];
				break;
			}
			res[i] = 0;
		}

		int rest = 0;
		int j;
		if(z > lensum)
		{
			for(i = z - 1; i >= 0; --i)
			{
				j = (lensum - 1) - (z - 1 - i);
				if(j < 0)
					break;
				res[i] += sum[j] + rest;
				rest = 0;
				if(res[i] > 9)
					rest = 1;
				res[i] %= 10;
			}
			res[i] += rest;
			for(i = 0; res[i] == 0; ++i);
			for(; i < z; ++i)
				printf("%c", (char)(res[i] + '0'));
		}
		else
		{
			for(i = lensum - 1; i >= 0; --i)
			{
				j = (z - 1) - (lensum - 1 - i);
				if(j < 0)
					break;
				sum[i] += res[j] + rest;
				rest = 0;
				if(sum[i] > 9)
					rest = 1;
				sum[i] %= 10;
			}
			sum[i] += rest;
	/*
			for(; i >= 0; --i)
			{
				sum[i] += rest;
				rest = 0;
				if(sum[i] > 9)
					rest = 1;
				sum[i] %= 10;
			}

	*/		for(i = 0; sum[i] == 0; ++i);
			for(; i < lensum; ++i)
				printf("%c", (char)(sum[i] + '0'));
		}
	}
	else
	{
		for(i = 0; i < lensum; ++i)
			printf("%c", (char)(sum[i] + '0'));
	}

	return 0;
}