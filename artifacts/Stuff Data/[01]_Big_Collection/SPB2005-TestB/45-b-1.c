#include <stdio.h>
#include <string.h>
#include <stdlib.h>

  int k;
  char n[100];
  typedef struct number
  {
    char i[100];
    int num_len;
  } num;
  num q;

int Is_K_Number(num a)
{
   int block_size;
   int i, j;

   if (a.num_len % k != 0 || a.num_len == 0)
     return 0;

   block_size = a.num_len / k;
   for (i = 0; i < block_size; i++)
     for (j = i; j < a.num_len - block_size; j += block_size)
       if (a.i[j] != a.i[j + block_size])
	 return 0;

   return 1;
}

int IncNum(int point)
{
  int tmp;

  if (point == -1)
  {
    char t1, t2;
    int j;
    t1 = q.i[0];
    q.i[0] = '1';
    for (j = 1; j < q.num_len; j++)
    {
       t2 = q.i[j];
       q.i[j]  = t1;
       t1 = t2;
    }
    q.num_len++;
    return 0;
  }

  tmp = q.i[point] - '0';
  tmp++;
  if (tmp > 9)
  {
    q.i[point] = tmp % 10 + '0';
    IncNum(point - 1);
  }
  else
  {
    q.i[point] = tmp + '0';
  }
  return 0;
}

int Equal(num a, char b[])
{
  int  j;
  int b_len = strlen(b);

  if (a.num_len != b_len)
    return 0;
  for (j = 0; j < a.num_len; j++)
    if (a.i[j] != b[j])
      return 0;
  return 1;
}

int main (void)
{
  FILE *in, *out;
  int len;
  int t;
  int num_of_k = 0;


  for (len = 0; len < 100; len++)
     q.i[len] = '0';
  q.num_len = 0;

  in = fopen("knumbers.in", "rt");
  out = fopen("knumbers.out", "wt");

  fscanf(in, "%i%s", &k, &n);
//  gets(n);

  len = strlen(n);
  if (k > len)
  {
    fprintf(out, "0");
    return 0;
  }

  while (!Equal(q, n))
  {
    if (Is_K_Number(q))
      num_of_k++;
    IncNum(q.num_len - 1);
  }
  if (Is_K_Number(q))
    num_of_k++;


  fprintf(out, "%i", num_of_k);
  fclose(in);
  fclose(out);

  return 0;
}