#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

struct NUMBER
{
	char ar[1000];
} num;

void Equal (long tek)
{
	unsigned long i = 0;
	while (tek > 0)
	{
		num.ar[i] = tek % 10;
		tek /= 10;
		i ++;
	}
}

void Plus (long tek)
{
	char s[1000];
	itoa (tek, s, 10);
	strrev (s);
	strrev (num.ar);
	unsigned long i;
	unsigned long old = 0;
	for (i = 0; i < strlen (s); i ++)
	{
		if (i >= strlen (num.ar))
		{
			num.ar[strlen (num.ar)] = '0';
			num.ar[strlen (num.ar) + 1] = 0;
		}
		num.ar[i] = (num.ar[i] - 48 + s[i] - 48) % 10 + old + 48;;
		if ( (num.ar[i] - 48 + s[i] - 48) > 10)
			old = (num.ar[i] - 48 + s[i] - 48) / 10;
	}
	strrev (num.ar);
}

bool Min (char *s, long K)
{
	unsigned i;
	unsigned long j;
	for (i = K; i < strlen (s); i += K)
	{		
		for (j = 0; j < K; j ++)
		{
			if (s[j] > s[i + j])
				return true;
			else
				if (s[j] < s[i + j])
					return false;
		}
	}
	return false;
}

long NumOfDigits (char *s)
{
	return strlen (s);
}

long NumOfDigitsmin1 (char *s)
{
	unsigned i;
	if (s[0] != '1')
		return NumOfDigits (s);
	else if (s[0] == '1')
		for (i = 1; i < strlen (s); i ++)
		{
			if (s[i] != '0')
				return NumOfDigits (s);
		}
	return NumOfDigits (s) - 1;
}

long main (void)
{
	FILE *R, *W;
	unsigned long comb;
	unsigned long K;
	unsigned long i;
	char N[1000];
	if ((R = fopen ("knumbers.in", "rt")) == 0)
		return 1;
	if ((W = fopen ("knumbers.out", "wt")) == 0)
		return 1;
	fscanf (R, "%li", &K);
	fscanf (R, "%s", &N);

	for (i = 0; i < 1000; i ++)
		num.ar[i] = 0;


	comb = NumOfDigits (N) / K;

	if (NumOfDigits (N) % K != 0)
	{	
		Plus (9 * pow (10, comb - 1));
		fprintf (W, "%s", num.ar);
	}
	else
	{
		for (i = 0; i < comb; i ++)
		{		
			if (i == 0 && N[i] != 0)
			{
				Plus ((N[i] - 48) * pow (10, comb - i - 1) - 1);
			}
			else if (N[i] != 0)
			{
				Plus ((N[i] - 48) * pow (10, comb - i - 1));
			}
		}
		if (!Min (N, K))
			Plus (1);
		fprintf (W, "%s", num.ar);
	}

	fclose (R);
	fclose (W);
	return 0;
}