#include <stdio.h>
#include <ctype.h>

#define MAX 105

int K;
char N[106];
char R[106] = {0};
char out[106];
char min[106] = {0};
char cur[106] = {0};


void tomyint (char *where, int s, int e)
{
	int i;
	for (i = s; i <= e; i++)
		where[MAX - (i - s)] = N[i];
	where[MAX - (i - s)] = 0;
}

void copy (char *what, char *where)
{
	int i;
	for (i = MAX; what[i] != 0; i--)
		where[i] = what[i];
	where[i] = 0;
}	

void sum (char *s1, char *s2, char *res)
{
	int i;
	char cur, last = 0;
	int r1, r2, max;

	for (i = MAX; s1[i] != 0; i--);
	r1 = i + 1;
	for (i = MAX; s2[i] != 0; i--);
	r2 = i + 1;
	

	for (i = 0; r1 <= MAX && r2 <= MAX; r1++, r2++, i++)
	{
		
		cur = s1[r1] + s2[r2] - 96 + last;
		if (cur < 10)
			last = 0;
		else
		{
			cur -= 10;
			last = 1;
		}
		res[i] = cur + 48;
	}
	if (r1 == r2)
	{
		res[i] = last + 48;
		if (last)
			res[i+1] = 0;
		else
			res[i] = 0;
		return;
	}
	if (r1 < r2)
	{
		for (i = i; r1 <= MAX; r1++, i++)
		{
			cur = s1[r1] - 48 + last;
			if (cur < 10)
				last = 0;
			else
			{
				cur -= 10;
				last = 1;
			}
			res[i] = cur + 48;
		}
		res[i] = last + 48;
		if (last)
			res[i+1] = 0;
		else
			res[i] = 0;
	}
	if (r1 > r2)
	{
		for (i = i; r2 <= MAX; r2++, i++)
		{
			cur = s2[r2] - 48 + last;
			if (cur < 10)
				last = 0;
			else
			{
				cur -= 10;
				last = 1;
			}
			res[i] = cur + 48;
		}
		res[i] = last + 48;
		if (last)
			res[i+1] = 0;
		else
			res[i] = 0;
	}
		
}

int compare (char *s1, char *s2) // return 1 if s1 < s2
{
	int i;
	for (i = MAX; s1[i] != 0; i--);
	i++;
	if  (s2[i] == 0)
		return 0;
	for (i = MAX; s2[i] != 0; i--);
	i++;
	if  (s1[i] == 0)
		return 1;
	for (i = MAX; s1[i] != 0; i--)
	{
		if (s1[i] > s2[i])
			return 0;
		if (s1[i] < s2[i])
			return 1;
	}
	return 0;
}

void minmain (char *what)
{
	what[MAX] -= 1;
}

int main (void)
{
	int len = 0;
	int hm, i;
	int minmodif = 0;
	
	freopen ("knumbers.in", "rt", stdin);
	freopen ("knumbers.out", "wt", stdout);
	
	scanf ("%i", &K);

	gets (N);

	do
	{
		scanf ("%c", &N[len++]);
	}
	while (isdigit(N[len - 1]));
	
	len--;

	hm = int(len / K) - 1;
	
	if (len % K)
	{
		if (hm < 0)
			printf ("0");
		for (i = 0; i < hm + 1; i++)
			printf ("9");
		return 0;
	}

	for (i = 0; i < hm; i++)
		R[MAX - i] = '9';
	R[MAX - hm] = 0;
		
	tomyint (cur, 0, hm);
	copy (cur, min);
	for (i = 1; i < K; i++)
	{
		tomyint (cur, (hm + 1) * i, (hm + 1) * (i + 1) - 1);
		if (compare (cur, min))
		{
			minmodif = 1;
			break;
		}
	}
	if (!minmodif)
	{
		for (i = 1; i <= hm; i++)
			R[MAX - i] = '0';
		R[MAX] = '1';
		R[MAX - hm - 1] = 0;
	}
	minmain (min);
	sum (R, min, out);
	for (i = 0; out[i] != 0; i++);
	for (i = i - 1; i >= 0; i--)
		printf ("%c", out[i]);
	return 0;
}