#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;


bool Less(vector<short>::iterator  s1, vector<short>::iterator s2,vector<short>::iterator send) // s1.size() == s2.size()
{
	while(s1 != send)
		if(*s1 > *s2)
			return false;
	return true;
}
int Cmp(vector<short>::iterator  s1, vector<short>::iterator s2,vector<short>::iterator send) // s1.size() == s2.size()
{
	while(s1 != send)
		if(*s1 != *s2)
			return *s1 - *s2;
	return 0;
}

void Dec(vector<short>::iterator  s1,vector<short>::iterator  s2)
{
	vector<short>::iterator  t = s2;
	while(t >= s1)
	{
		(*t)--;
		if(*t < 0)
		{
			(*(t - 1))--;
			(*t) += 10;
		}
		t--;
	}
}

void Inc(vector<short>::iterator  s1,vector<short>::iterator  s2)
{
	vector<short>::iterator  t = s2;
	(*t)++;
	while(t >= s1)
	{
		if(*t > 10)
		{
			(*(t - 1))++;
			(*t) %= 10;
		}
		t--;
	}
}

void MegaSumm(vector<short> & s1,int k)
{
	for(int j = 0; j < s1.size(); j++)
		s1[j] = 0;
	if(k == 0)
		return;
	for(int i = 1; i <= k; i++)
	{
		for(int j = s1.size() - 1;  j > s1.size() - i - 1; j--)
		{
			s1[j] += 9;
			if(s1[j] > 10)
			{
				s1[j-1] += 1;
				s1[j] %= 10;
			}
		}
	}
}

int main()
{
	int k;
	int n;
	int r;
	char c;
	std::vector<short> res;
	std::vector<short> N;
	std::vector<short>::iterator cur,min;
	ifstream in("knumbers.in");
	ofstream out("knumbers.out");
	in >> k;
	while(in >> c)
		N.push_back(c - '0');
	res.resize(N.size() + 2);
	MegaSumm(res,(N.size() - 1)  / k );
	if(N.size() % k != 0)
	{
		int i = 0;
		while(res[i] == 0) i++;
		while(i < res.size())
			out<<res[i++];
	}
	else
	{
		n = N.size() / k ;
		min = N.begin();
		cur = min + n ;
		do
		{
			r = Cmp(min,cur,min + n);
			if(r > 0)
			{
				break;
			}else
			if(r < 0)
			{
				Inc(min,cur - 1);
				break;
			}
			else 
			{
				min = cur;
			}
		}
		while( (cur += n ) != N.end());
		(*min)--;
		if(*min != 0)
		{
			min += n - 1;
			for(int j = res.size() - 1; j >= res.size() - n ; j--,min--) // Summing
			{
				res[j] += *min;
				if(res[j] >= 10)
				{
					res[j-1] += 1;
					res[j] %= 10;
				}
			}
		}
		int i = 0;
		while(res[i] == 0) i++;
		while(i < res.size())
			out<<res[i++];
	}
	in.close();
	out.close();
	return 0;
}