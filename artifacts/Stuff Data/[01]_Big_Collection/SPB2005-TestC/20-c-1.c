#include <stdio.h>
#include <string.h>

#define MAX 1013

char S[MAX], A[MAX][MAX], *C[MAX];

void QSort( int L, int R, int k )
{
  int i = L, j = R;
  char *x, *tmp;

  if (L >= R)
    return;
  x = C[(L + R + 1) >> 1];
  while (i < j)
  {
    while (strncmp(C[i], x, k) < 0)
      i++;
    while (strncmp(C[j], x, k) > 0)
      j--;
    if (i < j)
      tmp = C[i], C[i] = C[j], C[j] = tmp;
    if (i <= j)
      i++, j--;
  }
  QSort(L, j, k);
  QSort(i, R, k);
}

int main( void )
{
  int k, len, i, j;

  freopen("restore.in", "rt", stdin);
  freopen("restore.out", "wt", stdout);
  scanf("%d", &k), k--;
  scanf("%s", S);
  len = strlen(S);
  for (i = 0; i < len; i++)
    A[i][0] = S[i];
  for (j = 0; j < len; j++)
    C[j] = A[j];
  for (i = 1; i < len; i++)
  {
    QSort(0, len - 1, i);
    for (j = 0; j < len; j++)
      A[j][i] = C[j][i - 1];
  }
  for (i = 1; i < len; i++)
    putc(A[k][i], stdout);
  putc(A[k][0], stdout);
  return 0;
}
