#include <stdio.h>

#define m 200005

int s[m];
int next[m * 2], h[m], r[m * 2], a[m], b[m], c[m], rr;

int main( void )
{
  int l, n = 0, k = 0, cc, i, j;

  freopen("restore.in", "r", stdin);
  freopen("restore.out", "w", stdout);
  scanf("%d", &l);
  while (((cc = fgetc(stdin)) < 33) || (cc > 255))
	 ;
  s[n++] = cc;
  while (((cc = fgetc(stdin)) >= 33) && (cc <= 255))
	s[n++] = cc;
  s[n] = 0;
  for (i = n; i > 0; i--)
  {
    r[++rr] = i;
	next[rr] = h[s[i - 1] - 32];
	h[s[i - 1] - 32] = rr;
  }
  for (i = 33; i < 256; i++)
    for (j = h[i]; j != 0; j = next[j])
	  a[++k] = r[j];
  for (i = 1; i <= n; i++)
    b[a[i]] = i;
  for (i = k = 1; i <= n; i++)
  {
    c[i] = a[k];
	k = a[k];
  }
  for (i = 1; i <= n; i++)
    if (c[i] == a[l])
      break;  
  for (k = 1; k <= n; k++)
  {
    printf("%c", s[c[i] - 1]);
	i++;
	if (i == n + 1)
	  i = 1;	 
  }
  printf("\n");
  return 0;
}
