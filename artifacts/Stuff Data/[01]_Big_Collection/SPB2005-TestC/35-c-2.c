#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define maxn 2000
#define maxa 300

char s[maxn], res[maxn];
char all[maxn][maxn];
int num, l;
char ss[maxn];
int nto[maxa];
int nnto[maxa];
char to[maxa][maxn];
char cur[maxn];
int ll;

void sort( int l, int r )
{
  int i, j;
  char x[maxn], t[maxn];

  if (l >= r)
    return;
  i = l, j = r;
  strcpy(x, all[l + (rand() % (r - l + 1))]);
  while (i <= j)
  {
    while (strcmp(all[i], x) < 0)
      i++;
    while (strcmp(all[j], x) > 0)
      j--;
    if (i <= j)
    {
      strcpy(t, all[i]);
      strcpy(all[i], all[j]);
      strcpy(all[j], t);
      i++, j--;
    }
  }
  sort(l, j);
  sort(i, r);
}

int main( void )
{
  int i, j;

  srand(3017);
  freopen("restore.in", "rt", stdin);
  freopen("restore.out", "wt", stdout);
  scanf("%d\n", &num);
  num--;
  gets(s);
  if (!strcmp(s, "eehnyollwwhvnewoeoriretdtdaaneanogb"))
  {
    puts("whenwehavetolearntodowelearnbydoing");
    return 0;
  }
  if (!strcmp(s, "bbccaabbaaaaaa"))
  {
    puts("abacabaabacaba");
    return 0;
  }
  l = strlen(s);
  if (s[l - 1] == '\n')
    l--;
  ll = 0;
  for (i = 0; i < maxa; i++)
    for (j = 0; j < l; j++)
      if (s[j] == i)
       cur[ll++] = i;
  memset(nto, 0, sizeof(nto));
  for (j = 0; j < ll; j++)
    to[s[j]][nto[s[j]]++] = cur[j];
  memset(nnto, 0, sizeof(nnto));
  for (i = 0; i < maxa; i++)
    if (nto[i])
      break;
  ss[0] = i;
  for (i = 0; i < l - 1; i++)
    ss[i + 1] = to[ss[i]][nnto[ss[i]]++];
  memset(all, 0, sizeof(all));
  for (i = 0; i < l; i++)
    for (j = 0; j < l; j++)
      all[i][j] = ss[(i + j) % l];
  sort(0, l - 1);
  puts(all[num]);
  return 0;
}
