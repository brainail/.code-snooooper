#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

typedef struct 
{
   int a, b, c;
} Corid;

int **Map;
int n;

int Deph( int cur, int b )
{
  int i, j, k = 0;

  for (i = 0; i < n; i++)
  {
    if ((i != b && Map[i][cur] != -1) && i != cur)
	{
	  for (j = 0; j < n; j++)
	  {
		  if (i != j && j != b && i != cur && Map[i][cur] == Map[j][cur] && Map[i][cur] != -1) 
			k++;
          if (k >= 2)
            return 0; 
	  }

	  if (Deph(i, cur) == 0)
		return 0;
	}
  }
  return 1;
}


int main ( void )
{
 
  int i, x, y;
  Corid *c;
  FILE *F1, *F2;

  
  F1 = fopen("robot.in", "rt");
  F2 = fopen("robot.out", "wt");

  fscanf(F1, "%d", &n);
  Map = malloc(sizeof(int *) * n);
  for (x = 0; x < n; x++)
  {  
	  Map[x] = malloc(sizeof(int) * n);
      memset(Map[x], -1, sizeof(int) * n);
  } 
  for (x = 0; x < n; x++)
  {
    for (y = 0; y < n; y++)
       printf("%d", Map[x][y]);
	printf("\n");
  }  
  c = malloc(sizeof(Corid) * (n - 1) * sizeof(int));
  memset(c, -1, sizeof(Corid) * (n - 1) * sizeof(int));
  for (i = 0; i < n - 1; i++)
  {
    fscanf(F1, "%d", &c[i].a);  
	fscanf(F1, "%d", &c[i].b);  
	fscanf(F1, "%d", &c[i].c);  
    c[i].a--;
	c[i].b--;
	c[i].c--;
  } 
  for (x = 0; x < n-1; x++)
  {
    Map[c[x].a][c[x].b] = Map[c[x].b][c[x].a] = c[x].c;
  } 
   
  for (x = 0; x < n; x++)
    if (Deph(x, - 1) == 1)
	  fprintf(F2, "%d ", x + 1);
  
  free(c);	
  return 0;
}