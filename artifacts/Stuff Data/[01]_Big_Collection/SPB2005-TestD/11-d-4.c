#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SWAP(A, B, C) ((C) = (A)),((A) = (B)), ((B) = (C))

typedef struct
{
	long a, b, c;
} Edge;

long N, S;

long C[100005];
Edge Ans[100005];
Edge FE[2000014];
Edge tmp;

void HeapCorr(Edge *M, long N, long i)
{
	long l, r, max;

	max = i;
	l = i * 2 + 1;
	r = i * 2 + 2;
	
	if ((l < N) && (M[l].a > M[max].a))
		max = l;
	if ((r < N) && (M[r].a > M[max].a))
		max = r;
	if (max == i)
		return;
	SWAP(M[max], M[i], tmp);
	HeapCorr(M, N, max);
	
}

void HeapSort(Edge *M, long N)
{
	long i, j, NN;
	
	for (i = N - 1; i >= 0; i--)
		HeapCorr(M, N, i);
	NN = N;
	while (NN >	1)
	{
		SWAP(M[0], M[NN - 1], tmp);
		NN--;
		HeapCorr(M, NN, 0);
	}
}

long FindFirst(Edge *M, long N, long x)
{
	int l, r, m;
	
	l = 0;
	r = N - 1;
	m = (l + r + 1) / 2;
	while (r - l > 1)
	{
		if (M[m].a > x)
			r = m;
		else if(M[m].a < x)
			l = m;
		else
			break;
		
		m = (l + r + 1) / 2;
			
	}
	if (M[l].a == x)
		m = l;
	while (M[m - 1].a == x)
		m--;
	return m;

}

int Go(long i, long up)
{
	long x, j;
    
	memset(C, 0, (N + 1)  * sizeof(long));

	if (i == 6)
	{
		i = i;
	}
	x = FindFirst(FE, S, i);
	for (j = x; FE[j].a == i; j++)
	{
		if (FE[j].b != up)
		{
			if (C[FE[j].c] != 0)
				return 0;
			C[FE[j].c] = 1;
		}
	}
	for (j = x; FE[j].a == i; j++)
	{
		if (FE[j].b != up)
		{
			if (!Go(FE[j].b, i))
				return 0;
		}
	}
	return 1;
}

int main( void )
{
	long i, j, k, R,a, b ,c;

    freopen("robot.in", "rt", stdin);
	freopen("robot.out", "wt", stdout);
	
	
	scanf("%li", &N);
    j = 0;
	N--;
	i = N;
	while (i-- > 0)
	{
		scanf("%li%li%li", &a, &b, &c);
		FE[j].a = a;
		FE[j].b = b;
		FE[j++].c = c;
		
		FE[j].a = b;
		FE[j].b = a;
		FE[j++].c = c;

	}
	S = j; 
	HeapSort(FE, S);
	R = 0;
	for (i = 1; i <= N + 1; i++)
	{
		if (Go(i, -1) == 1)
			Ans[R++].a = i;
	}
	for (i = 0; i < R; i++)
		printf("%li ", Ans[i].a);
	return 0;
}
