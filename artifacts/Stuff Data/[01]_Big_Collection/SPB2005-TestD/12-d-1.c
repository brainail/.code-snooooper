/**************************************************************
 * Copyright (C) 2005
 *    Computer Graphics Support Group of 30 Phys-Math Lyceum
 **************************************************************/

/* FILE NAME   : D.C
 * PURPOSE:    : Olympiad task D.
 * PROGRAMMER  : Anton Timofeev.
 * LAST UPDATE : 06.03.2005
 * NOTE        : None.
 *
 * No part of this file may be changed without agreement of
 * Computer Graphics Support Group of 30 Phys-Math Lyceum
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_N 100002

/* edge type representation */
typedef struct tagEDGE
{
  int N, C; /* Node and color of edge */
} EDGE;

int N;
int NumE[MAX_N];
EDGE *E[MAX_N];
int NumC[MAX_N];
int M[MAX_N];

/* Delete bad branch */
void DeleteBranch( int Num )
{
  int i;

  M[Num] = 1;
  for (i = 0; i < NumE[Num]; i++)
	if (!M[E[Num][i].N])
	  DeleteBranch(E[Num][i].N);
} /* End of 'DeleteBranch' function */

/* The main program function */
int main( void )
{
  int i, j, k, a, b, c, fl;
  FILE *F;
  
  F = fopen("robot.in", "rt");
  freopen("robot.out", "wt", stdout);

  fscanf(F, "%i", &N);
  memset(M, 0, sizeof(int) * N);
  memset(NumE, 0, sizeof(int) * N);
  for (i = 0; i < N - 1; i++)
  {
    fscanf(F, "%i%i%i", &a, &b, &c);
    NumE[a - 1]++;
	NumE[b - 1]++;
  }

  for (i = 0; i < N; i++)
    if ((E[i] = malloc(sizeof(EDGE) * NumE[i])) == NULL)
	  return 1;

  memset(NumE, 0, sizeof(int) * N);
  rewind(F);
  fscanf(F, "%i", &N);
  for (i = 0; i < N - 1; i++)
  {
    fscanf(F, "%i%i%i", &a, &b, &c);
    E[a - 1][NumE[a - 1]].N = b - 1;
    E[a - 1][NumE[a - 1]++].C = c;
	E[b - 1][NumE[b - 1]].N = a - 1;
    E[b - 1][NumE[b - 1]++].C = c;
  }

  for (i = 0; i < N; i++)
  {
    memset(NumC, 0, sizeof(int) * N);
    fl = 0;
    for (j = 0; j < NumE[i]; j++)
	{
      NumC[E[i][j].C]++;
      if (NumC[E[i][j].C] == 3 || (NumC[E[i][j].C] == 2 && fl && NumE[i] > 2))
	  {
        for (i = 0; i < N; i++)
          free(E[i]);
        fclose(F);
        return 0;
	  }
	  else if (NumC[E[i][j].C] == 2)
	  {
	    if (NumE[i] > 2)
          fl = 1;
	    M[i] = 1;
	    for (k = 0; k < NumE[i]; k++)
	      if (E[i][k].C != E[i][j].C)
		    DeleteBranch(E[i][k].N);
	  }
	}
  }

  for (i = 0; i < N; i++)
	if (!M[i])
	  printf("%i ", i + 1);
	
  for (i = 0; i < N; i++)
	free(E[i]);
  
  fclose(F);
  return 0;
} /* End of 'main' function */

/* END OF 'D.C' FILE */
