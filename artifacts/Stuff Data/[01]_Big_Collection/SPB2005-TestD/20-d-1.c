#include <stdio.h>

#define DBG 0

#define MAX 200013

int N, To[MAX], C[MAX], Ne[MAX], St[MAX];
int Num[MAX], F[MAX], CF[MAX];
int U[MAX], Good[MAX], GN = 0;

void dfs2( int v, int pr )
{
  int i;

  U[v] = 1, Good[v]++;
  for (i = St[v]; i != -1; i = Ne[i])
    if (!U[To[i]] && To[i] != pr)
      dfs2(To[i], v);
}

int main( void )
{
  int i, a, b, c, v;

  freopen("robot.in", "rt", stdin);
  freopen("robot.out", "wt", stdout);
  scanf("%d", &N);
  for (i = 0; i < N; i++)
    St[i] = -1;
  for (i = 0; i < (N - 1) * 2; i += 2)
  {
    scanf("%d%d%d", &a, &b, &c), a--, b--;

    To[i] = b, C[i] = c;
    Ne[i] = St[a], St[a] = i;

    To[i + 1] = a, C[i + 1] = c;
    Ne[i + 1] = St[b], St[b] = i + 1;
  }
# if DBG == 1
  for (v = 0; v < N; v++)
  {
    fprintf(stderr, "%d:", v);
    for (i = St[v]; i != -1; i = Ne[i])
      fprintf(stderr, " %d(%d)", To[i], C[i]);
    fprintf(stderr, "\n");
  }  
# endif
  for (i = 0; i < MAX; i++)
    Num[i] = -1;
  for (v = 0; v < N; v++)
  {
    F[v] = 0;
    for (i = St[v]; i != -1; i = Ne[i])
      if (Num[C[i]] == v)
        F[v]++, CF[v] = C[i];
      else
        Num[C[i]] = v;
    if (F[v] > 1)
      return 0;
  }
  memset(Good, 0, sizeof(Good));
  for (v = 0; v < N; v++)
    if (F[v])
    {
      GN++;
      memset(U, 0, sizeof(U));
      for (i = St[v]; i != -1; i = Ne[i])
        if (CF[v] == C[i]) 
          dfs2(To[i], v);
    }
  for (v = 0; v < N; v++)
    if (Good[v] == GN)
      printf("%d ", v + 1);
  return 0;
}
