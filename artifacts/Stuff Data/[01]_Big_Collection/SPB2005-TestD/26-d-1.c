#include<stdio.h>
#include<stdlib.h>

struct vertex{
  int num;
  int color;
  struct vertex *next;
};


void add(struct vertex *v,int a,int b,int c){
  struct vertex *vrt;

  vrt=(struct vertex *)malloc(sizeof(struct vertex));
  (*vrt).num=b;
  (*vrt).color=c;
  (*vrt).next=v[a].next;
  v[a].next=vrt;
}

int bad(struct vertex *v,int number){
  int *list;
  struct vertex tmp;
  int flag;

  
  list =(int *)malloc(100001*sizeof(int));
  tmp=v[number];
  flag=1;
  while((tmp.next!=NULL)&&(flag==1)){
	tmp=*(tmp.next);
	if(list[tmp.color]>0){
      flag=0;
	}else{
	  list[tmp.color]=566;
	}
  }
  free(list);
  if(flag==1){
    return 0;
  }
  return tmp.color;
}


void del(struct vertex *v,int *exist,int number,int last_n){
	struct vertex tmp;

	exist[number]=0;
	tmp=v[number];
	while(tmp.next!=NULL){
		tmp=*(tmp.next);
		if((exist[tmp.num])&&(tmp.num!=last_n)){
			del(v,exist,tmp.num,number);
		}
	}
}


int main(){
  FILE *in=fopen("robot.in","r"),*out=fopen("robot.out","w");
  int n,nr;
  struct vertex *v;
  int *list;
  int *exist;
  int i;
  int a,b,c;
  struct vertex tmp;

  v=(struct vertex *)malloc(100001*sizeof(struct vertex));
  list=(int *)malloc(100001*sizeof(int));
  exist=(int *)malloc(100001*sizeof(int));
  fscanf(in,"%d",&n);
  nr=n-1;
  for(i=0;i<n;i++){
  	v[i].next=NULL;
	list[i]=0;
	exist[i]=1;
  }

  for(i=0;i<nr;i++){
	fscanf(in,"%d%d%d",&a,&b,&c);
	add(v,a-1,b-1,c);
	add(v,b-1,a-1,c);
  }
  
  for(i=0;i<n;i++){
    list[i]=bad(v,i);
  }
  
  for(i=0;i<n;i++){
    if((list[i]>0)&&(exist[i]==1)){
	  tmp=v[i];
	  while(tmp.next!=NULL){
		tmp=*(tmp.next);
		if((tmp.color!=list[i])&&(exist[tmp.num])){
		  del(v,exist,tmp.num,i);
		}
	  }
	}
  }

  for(i=0;i<n;i++){
    if((exist[i]==1)&&(list[i]==0)){
      fprintf(out,"%d ",i+1);
	}
  }

  fclose(in);
  fclose(out);
  return 0;
}
