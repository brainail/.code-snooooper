#include <stdio.h>

#define mm 200000

int h[mm], s[mm], mark[mm], good[mm], next[mm], r[mm], rr, c[mm], 
    _stklen = 2 * 1024 * 1024;

void add( int x, int y, int z )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  c[rr] = z;
}

int dfs( int v )
{
  int i;

  mark[v] = 1;
  for (i = h[v]; i != 0; i = next[i])
    if (!mark[r[i]])
    {
      if (good[r[i]] && c[i] != good[r[i]])
        return 0;
      dfs(r[i]);
    }
  return 1;
}

int main( void )
{
  int i, j, x, y, z, n;

  freopen("robot.in", "r", stdin);
  freopen("robot.out", "w", stdout);
  scanf("%d", &n);
  for (i = 1; i < n; i++)
  {
    scanf("%d%d%d", &x, &y, &z);
    add(x, y, z);
    add(y, x, z);
  }
  for (i = 1; i <= n; i++)
  {
    for (j = h[i]; j != 0; j = next[j])
      if (mark[c[j]] == i)  
      {
        s[c[j]]++;
        if (s[c[j]] == 2)
        {
          if (good[i])
          {
            printf("\n");
            return 0;
          }
          good[i] = c[j];
        }
        if (s[c[j]] > 2)
        {
          printf("\n");
          return 0;
        }
      }
      else
      {
        mark[c[j]] = i;
        s[c[j]] = 1;
      }
  }
  for (i = 1; i <= n; i++)
  {
    for (j = 1; j <= n; j++)
      mark[j] = 0;
    if (!good[i] && dfs(i)) 
      printf("%d ", i);
  }
  printf("\n");
  return 0;
}
