#include <stdio.h>
#include <string.h>

int main ( void )
{
	__int64 **A;
	int *NN;
	FILE *FIn, *FOut;
	__int64 n, a, b, c, i, fmax = 0, j, t;
	__int64 k;

	if ((FIn = fopen("robot.in", "rt")) == NULL)
		return 0;
	if ((FOut = fopen("robot.out", "wt")) == NULL)
	{
		fclose(FIn);
		return 0;
	}
	fscanf(FIn, "%I64i", &n);
	/* Detecting amount of rooms */
	for (i = 0; i < n; i++)
	{
		fscanf(FIn, "%I64i%I64i%I64i", &a, &b, &c);
		if (fmax < b)
		  fmax = b;
		if (fmax < a)
          fmax = a;
	}
	rewind(FIn);
	fscanf(FIn, "%I64i", &n);
  A = malloc(sizeof(__int64 *) * fmax);
  for (j = 0; j < fmax; j++)
  {
	  A[j] = malloc(sizeof(__int64) * fmax);
	  for (i = 0; i < fmax; i++)
		  A[j][i] = 100001;
	  A[j][j] = 0;	
  }
	NN = malloc(sizeof(int) * fmax);
	for (i = 0; i < fmax; i++)
		NN[i] = 1;
	for (i = 0; i < n; i++)
	{
		fscanf(FIn, "%I64i%I64i%I64i", &a, &b, &c);
		A[a - 1][b - 1] = c;
		A[b - 1][a - 1] = c;
	}
    for (i = 0; i < fmax; i++)
      for (k = 0; k < fmax; k++)
        for (j = 0; j < fmax; j++)
		  if (k != j && A[i][j] != 100001 && A[i][k] == A[i][j])
			  for (t = 0; t < fmax; t++)
				  if (t != j && t != k && A[i][t] != 100001)
					  NN[t] = 0;
	for (i = 0; i < fmax; i++)
		if (NN[i])
			fprintf(FOut, "%I64i ", i + 1);
	fclose(FIn);
	fclose(FOut);
	for (i = 0; i < fmax; i++)
		free(A[i]);
    free(A);
	free(NN);
	return 0;
}