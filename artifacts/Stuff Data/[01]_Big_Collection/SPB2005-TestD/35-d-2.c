#include <stdio.h>
#include <string.h>

#define maxn 100100

int n, m;
int head[maxn];
int to[maxn * 2], ne[maxn * 2], co[maxn * 2];
int nc[maxn][2], nn, tc;
int ipos[maxn];

void dfs( int v, int p )
{
  int i;

  if (ipos[v])
    return;
  ipos[v] = 1;
  for (i = head[v]; i != -1; i = ne[i])
    if (to[i] != p && !ipos[to[i]])
      dfs(to[i], v);
}

int main( void )
{
  int i, j, a, b, c;

  freopen("robot.in", "rt", stdin);
  freopen("robot.out", "wt", stdout);
  scanf("%d", &n);
  m = 0;
  for (i = 0; i < n; i++)
    head[i] = -1;
  for (i = 0; i < n - 1; i++)
  {
    scanf("%d%d%d", &a, &b, &c);
    a--, b--;
    to[m] = b, co[m] = c;
    ne[m] = head[a], head[a] = m++;
    to[m] = a, co[m] = c;
    ne[m] = head[b], head[b] = m++;
  }
  memset(ipos, 0, sizeof(ipos));
  memset(nc, 0, sizeof(nc));
  for (i = 0; i < n; i++)
  {
    nn = 0;
    for (j = head[i]; j != -1; j = ne[j])
    {
      if (nc[co[j]][0] == i + 1)
      {
        if (nc[co[j]][1] == i + 1)
          return 0;
        nc[co[j]][1] = i + 1;
        tc = co[j];
        nn++;
      }
      else
        nc[co[j]][0] = i + 1;
    }
    if (nn > 1)
      return 0;
    if (nn == 1)
    {
      for (j = head[i]; j != -1; j = ne[j])
        if (co[j] != tc)
          dfs(to[j], i);
      ipos[i] = 1;
    }
  }
  for (i = 0; i < n; i++)
    if (!ipos[i])
      printf("%d ", i + 1);
  return 0;
}
