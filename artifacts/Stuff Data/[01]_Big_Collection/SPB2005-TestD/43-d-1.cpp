#include <stdio.h>
#include <string.h>

const MAXCOL = 100001;

//typedef int CVertex;

int n;
int *mark;
int *can_be;
int *numways;
int **used;
int **g;

bool DFS(int v, int old)
{
	int i;
	int count;
	mark[v] = 1;
	if(can_be[v] == 0)
	{
		if(used[v][g[v][old]] <= 1)
			return false;
		if(numways[v] > 2)
		{
			count = 0;
			for(i = 0; i < n; ++i)
			{
				if(i == old)
					continue;
				if(g[v][i] == g[v][old])
					++count;
				else
					if(used[v][g[v][i]] > 1)
						return false;
			}
			if(count > 1)
				return false;
		}

	}
	
	for(i = 0; i < n; ++i)
	{
		if(g[v][i] != 0 && mark[i] == 0)
			if(DFS(i, v) == false)
				return false;
	}
	return true;
}

int main()
{
	freopen("robot.in", "r", stdin);
	freopen("robot.out", "w", stdout);

	scanf("%d", &n);

	int i;

	can_be = new int[n];
	for(i = 0; i < n; ++i)
		can_be[i] = 1;

	mark = new int[n];
	numways = new int[n];
	memset(numways, 0, n * sizeof(numways[0]));
	used = new int*[n];

	g = new int*[n];
	int a, b, c;
	
	for(i = 0; i < n; ++i)
	{
		g[i] = new int[n];
		memset(g[i], 0, n * sizeof(int));
		used[i] = new int[MAXCOL];
		memset(used[i], 0, MAXCOL * sizeof(int));
	}

	// read graph & make a color test
	for(i = 0; i < n - 1; ++i)
	{
		scanf("%d%d%d", &a, &b, &c);
		--a;
		--b;
		g[a][b] = c;
		g[b][a] = c;
		++numways[a];
		++numways[b];
		++used[a][c];
		++used[b][c];
		if(used[a][c] > 1)
			can_be[a] = 0;
		if(used[b][c] > 1)
			can_be[b] = 0;
	}

	int *res = new int[n];
	
	for(i = 0; i < n; ++i)
	{
		memset(mark, 0, n* sizeof(int));
		if(can_be == 0 || !DFS(i, i))
			res[i] = 0;
		else
			res[i] = 1;

	}

	for(i = 0; i < n; ++i)
		if(res[i] == 1)
			printf("%d ", i + 1);

	return 0;
}