#include <stdio.h>

struct ABC
{
	long a[100000];
	long b[100000];
	long c[100000];
} abc;

long all[100000];

void Exch (long &a, long &b)
{
	long tek = a;
	a = b;
	b = tek;
}

void Compexch (long &a, long &b)
{
	if (b < a)
		Exch (a, b);
}

long partition (long l, long r)
{
	long i = l - 1;
	long j = r;
	long v;
	v = abc.a[r];
	for (;;)
	{
		while (abc.a[++i] < v);
		while (v < abc.a[--j]) 
			if (j == l)
				break;
		if (i >= j)
			break;
		Exch (abc.a[i], abc.a[r]);
		return i;
	}
}

void quicksort (long l, long r)
{
	if (r <= l)
		return;
	long i = partition (l, r);
	quicksort (l, i - 1);
	quicksort (i + 1, r);
}


int main (void)
{
	FILE *R, *W;
	long N;
	long i;
	if ((R = fopen ("robot.in", "rt")) == 0)
		return 1;
	if ((W = fopen ("robot.out", "wt")) == 0)
		return 1;
	fscanf (R, "%li", &N);

	for (i = 0; i < N; i ++)
	{
		all[i] = i + 1;
		fscanf (R, "%li", &abc.a[i]);
		fscanf (R, "%li", &abc.b[i]);
		fscanf (R, "%li", &abc.c[i]);
	}

	long j;

	long k;

//	quicksort (0, N);

	for (i = 0; i < N; i ++)
	{
		for (j = 0; j < N; j ++)
		{
			if (abc.a[j] != abc.a[i])
				break;
			if (abc.c[i] == abc.c[j])
			{				
				for (k = 0; k < N; k ++)
					if (abc.a[k] == abc.a[i])
						all[abc.b[k]] = 0;				
			}

		}
	}

	for (i = 0; i < N - 1; i ++)
	{
		if (all[i] != 0)
			fprintf (W, "%li ", all[i]);
	}

	if (all[i] != 0)
		fprintf (W, "%li", all[N-1]);

	fclose (R);
	fclose (W);
	return 0;
}