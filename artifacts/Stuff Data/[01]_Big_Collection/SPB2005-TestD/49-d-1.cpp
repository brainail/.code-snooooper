#include <stdio.h>

#define MAX 12

struct link
{
	int start;
	int end;
	int c;
	link *next;
};

typedef struct
{
	link* data;
} plink;

int N;
link L[MAX];
plink room[MAX];
int center[MAX] = {0};
int colors[MAX];

void delfrom (int from, int start)
{
	center[from] = 1;
	link *iter = room[from].data->next;
	do
	{
		if (iter->end != start)
			delfrom (iter->end, from);
		if (iter->start != start)
			delfrom (iter->end, from);
		iter = iter->next;
	}
	while (iter->next != 0);

}

int main (void)
{
	int i, a, b, c;
	freopen ("robot.in", "rt", stdin);
	freopen ("robot.out", "wt", stdout);
	scanf ("%i", &N);
	for (i = 0; i < N; i++)
	{
		room[i].data = 0;
	}
	for (i = 0; i < N - 1; i++)
	{
		L[i].start = L[i].end = L[i].c = 0;
		L[i].next = 0;
	}
	for (i = 0; i < N - 1; i++)
	{
		scanf ("%i%i%i", &a, &b, &c);
		L[i].start = a;
		L[i].end = b;
		L[i].c = c;
	}

	for (i = 0; i < N - 1; i++)
	{
		int r = L[i].start;
		link* iter = room[r].data;
		while (iter->next)
		{
			iter = iter->next;
		}
		room[r].data->next = &L[i];
		r = L[i].end;
		iter = room[r].data;
		while (iter->next)
		{
			iter = iter->next;
		}
		room[r].data->next = &L[i];
	}

	for (i = 0; i < N; i++)
	{
		int j;
		int badnum = 0;
		int badcol = 0;
		for (j = 0; j < MAX; j++)
			colors[j] = 0;
		link *iter = room[i].data->next;
		do
		{
			colors[iter->c]++;
		}
		while (iter->next != 0);
		for (j = 0; j < MAX; j++)
			if (colors[j] > 1)
			{
				badnum ++;
				badcol = j;
			}
		if (badnum > 1)
		{
			return 0;
		}

		if (badnum == 0)
			continue;
		center[i] = 1;
		iter = room[i].data->next;
		do
		{
			if (iter->c != badcol)
			{
				if (iter->start == i)
					delfrom (iter->end, i);
				else
					delfrom (iter->start, i);
			}
		}
		while (iter->next != 0);
	}
	

	for (i = 0; i < N; i++)
		if (center[i] == 0)
			printf ("%i ", i);
	return 0;
}