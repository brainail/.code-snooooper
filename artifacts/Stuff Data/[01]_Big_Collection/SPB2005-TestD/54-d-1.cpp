// 
//  GUITELSON DANIIL     PML30 RULEZZ
//
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>

using namespace std;

std::vector<int> vec;

class Node;

class Pass
{
public:
	long Color;
	Node * Next;  // Room to whic pass link
	bool operator < (const Pass & p) const// For quick sorting
	{
		return Color < p.Color ;
	}
	bool operator > (const Pass & p) const // For quick sorting
	{
		return Color > p.Color ;
	}
};

class Node
{
public:
	static Node * Last;
	typedef std::vector<Pass>::iterator iter;
	int Number;
	bool Valid;
	iter Equal_1;
	iter Equal_2;
	std::vector<Pass> Passes; // vector of passes
	void Sort();              // sort all passes by color
	void FindEqual();// rerturn if there is equal colors (without pass from which it comes) 
	void EraseInvalid(Node * from);
	void Print(Node * from);

};

Node * Node::Last;

void Node::Sort()
{
	std::sort(Passes.begin(),Passes.end(),std::less<Pass>());
}

void Node::FindEqual()
{
	for(iter i = Passes.begin(), next = Passes.begin() + 1; i != Passes.end(); i++,next++)
	{
		if(next != Passes.end())
		{
			if(i->Color == next->Color)
			{
				Equal_1 = i;
				Equal_2 = next;
				return;
			}
		}
	}
	Equal_1 = NULL;
	Equal_2 = NULL;
}

void Node::EraseInvalid(Node * from)
{
	std::vector<int>::iterator save;
	Last = this;
	if(Equal_1 != NULL)
	{
		Valid = false;
		if(from != Equal_1->Next)
			Equal_1->Next->EraseInvalid(this);
		if(from != Equal_2->Next)
			Equal_2->Next->EraseInvalid(this);
		Passes[0] = *Equal_1;
		Passes[1] = *Equal_2;
		Passes.erase(Passes.begin() + 2,Passes.end());
	}else
	{
		Valid = true;
		for(iter i = Passes.begin(); i != Passes.end(); i++)
			if(i->Next != from)
				i->Next->EraseInvalid(this);
	}
}

void Node::Print(Node * from)
{
	if(Valid)
		vec.push_back(Number);
	for(iter i =  Passes.begin(); i != Passes.end(); i++)
		if(i->Next != from)
			i->Next->Print(this);
}


int main(void)
{
	int n,a,b,c;
	Node node;
	Pass pass;
	std::vector<Node> nodes;
	ifstream in("robot.in");
	ofstream out("robot.out");
	in >> n;
	nodes.resize(n);
	for(int i = 0; i < n - 1; i++)
	{
		in>>a>>b>>c;
		pass.Color = c;
		pass.Next = &nodes[b - 1];
		nodes[a - 1].Passes.push_back(pass);
		
		pass.Color = c;
		pass.Next = &nodes[a - 1];
		nodes[b - 1].Passes.push_back(pass);
		
		nodes[a - 1].Valid = true;
	}
	for(i = 0; i < n; i++)
	{
		nodes[i].Number = i + 1;
		nodes[i].Sort();
		nodes[i].FindEqual();
	}
	nodes[0].EraseInvalid(NULL);
	Node::Last->Print(NULL);
	std::sort(vec.begin(),vec.end(),less<int>());
	for(std::vector<int>::iterator it = vec.begin(); it != vec.end(); it++)
		out<<*it<<' ';
	out.close();
	in.close();
	return 0;
}