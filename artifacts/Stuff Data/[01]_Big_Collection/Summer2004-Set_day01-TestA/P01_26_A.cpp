/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;

#define DBG if(0)

void die(const char*msg) { cerr<<msg<<endl; exit(1); }

// ========================================================================
void read_rule(istream& is, char& color, int& start, int& len)
{
	is >> color >> start >> len;
}
void write_ans(ostream& os, int n, int sumlen)
{
	os << n << ' ' << sumlen << endl;
}
const int MOD_MAX = 500000;
const int SIZE = 2*MOD_MAX + 1 + 2;// 2 extras - at the start and at the end

inline int idx(int n) {	return (n+MOD_MAX+1); }

void solve(int N, istream& is, ostream& os)
{
	int nsect=0;
	int nblack=0;
	for (int i=0; i<N; i++)
	{
		static char a[SIZE] = {0};

		char clr;
		int start,len;
		read_rule(is,clr,start,len);
		int begin = idx(start);
		int end = idx(start + len);
		// draw
		if (clr == 'W')
		{
			if (a[end]*a[end-1] == 1)
				nsect++;
			for (int i=end-1; i>=begin; i--)
				if (a[i] == 1)
				{
					a[i] = 0;
					nblack--;
					if (a[i-1] == 0)
						nsect--;
				}
		} else { // clr == 'B'
			if (a[end]+a[end-1] == 0)
				nsect++;
   			for (int i=end-1; i>=begin; i--)
				if (a[i] == 0)
				{
					a[i] = 1;
					nblack++;
					if (a[i-1]==1)
						nsect--;
				}
		}
		// end
		write_ans(os, nsect, nblack);
	}
}

// ========================================================================
int main(void)
{
	ifstream fi("painter.in");
	ofstream fo("painter.out");
	int N;
	fi >> N;
	solve(N,fi,fo);
	return 0;
}
