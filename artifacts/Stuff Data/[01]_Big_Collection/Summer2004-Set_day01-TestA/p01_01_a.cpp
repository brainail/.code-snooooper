#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 100010
#define SS 700000

struct TNode {
  int a,b;
  int c;
  int y;
  TNode *l, *r;
};

FILE *fi, *fo;
TNode *swap;
int iswap=SS+1;
int YY[5*NN];
int iy=0;
int n;
int ans_c,ans_l;
TNode *find_parent;
TNode *head;

int getnexty() {
  return YY[iy++];
}

TNode *getnew() {
  if(iswap>=SS) {
    swap=new TNode [SS];
    iswap=0;
  }
  return &swap[iswap++];
}

TNode *merge(TNode *t1, TNode *t2) {
//t1<t2;
  if(t1==NULL) return t2;
  if(t2==NULL) return t1;
  if(t1->y<t2->y) {
    t1->r=merge(t1->r,t2);
    return t1;
  } else {
    t2->l=merge(t1,t2->l);
    return t2;
  }
  return NULL;
}

void split(TNode *t, int x, TNode **L, TNode **R) {
  if(t==NULL) {
    (*L)=NULL;
    (*R)=NULL;
    return;
  }
  if(t->a<x) {
    split(t->r,x,L,R);
    t->r=(*L);
    (*L)=t;
    return ;
  } else {
    split(t->l,x,L,R);
    t->l=(*R);
    (*R)=t;
    return ;
  }
}

TNode *find(TNode *t,int x, TNode *parent) {
  TNode *p;
  if(t==NULL)
    return NULL;
  if(t->a==x) {
    find_parent=parent;
    return t;
  }
  if(t->a>x) {
    return find(t->l,x,t);
  } else {
    p=find(t->r,x,t);
    if(p==NULL) {
      p=t;
      find_parent=parent;
    }
    return p;
  }
}

TNode *FindMin(TNode *t, TNode *parent) {
  if(t==NULL)
    return NULL;
  if(t->l==NULL) {
    find_parent=parent;
    return t;
  }
  return FindMin(t->l,t);
}

TNode *FindMax(TNode *t, TNode *parent) {
  if(t==NULL)
    return NULL;
  if(t->r==NULL) {
    find_parent=parent;
    return t;
  }
  return FindMax(t->r,t);
}

TNode *FindNext(TNode *t) {
//  return FindMin(t->r,t);
  return find(head,t->b,NULL);
}

TNode *FindNext2(TNode *t,TNode *prev) {
  TNode *f;
  f=FindMin(t->r,t);
  if(f==NULL)
    return find(head,prev->a,NULL);
}

TNode *FindPrev(TNode *t) {
//  return FindMax(t->l,t);
  return find(head,t->a-1,NULL);
}

TNode *CreateNode(int a, int b, int c) {
  TNode *t;
  t=getnew();
  t->a=a;
  t->b=b;
  t->c=c;
  t->y=getnexty();
  t->l=NULL;
  t->r=NULL;
  return t;
}

TNode *add(TNode *t, TNode *x) {
  if(t==NULL)
    return x;
  if(t->y<x->y) {
    if(t->a<x->a) {
      t->r=add(t->r,x);
      return t;
    } else {
      t->l=add(t->l,x);
      return t;
    }
  } else {
    split(t,x->a,&(x->l),&(x->r));
    return x;
  }
  return NULL;
}

TNode *dell(TNode *t) {
  if(t==NULL) return NULL;
  return merge(t->l,t->r);
}

void init() {
  head=CreateNode(-1000000,1000000,0);
  ans_l=0;
  ans_c=0;
}

void DellAll(TNode *t,TNode *prev,int a, int b) {
/*  TNode *t;
  t=find(t,a,prev);*/
  TNode *p;
  if(t==NULL)
    return ;
  if(t->a>b) return ;
  if(t->b<b) {
    p=FindNext(t);
    DellAll(p,find_parent,a,b);
  }
  if((t->a<a)&&(t->b>b)) {
    if(t->c==1) {
      ans_l-=t->b-a;
      ans_c++;
      ans_l+=t->b-b;
    }
    head=add(head,CreateNode(b,t->b,t->c));
    t->b=a;
  }
  if(t->a>=a) {
    if(t->b<=b) {
      if(t->c==1) {
        ans_c--;
        ans_l-=t->b-t->a;
      }
      if(prev==NULL) {
        head=dell(t);
        return ;
      }
      if(prev->r==t) {
        prev->r=dell(t);
      } else {
        prev->l=dell(t);
      }
      return ;
    } else {
      if(t->c==1) {
        ans_l-=b-t->a;
      }
      t->a=b;
    }
    return ;
  }
  if(t->c==1) {
    ans_l-=t->b-a;
  }
  t->b=a;
}

void ProcessLine(int a, int b, int c) {
  TNode *t,*p,*r;
  t=find(head,a,NULL);
  DellAll(t,find_parent,a,b);
  head=add(head,CreateNode(a,b,c));
  if(c==0)
    return ;
  if(c==1) {
    ans_c++;
    ans_l+=b-a;
  }
  t=find(head,a,NULL);
  r=find_parent;
  p=FindPrev(t);
  if(p==NULL) {
    p=r;
    find_parent=NULL;
  }
  if(p!=NULL) {
    if(p->c==1) {
      ans_c--;
      p->b=t->b;
      if(r->l==t)
        r->l=dell(t);
      else
        r->r=dell(t);
      t=find(head,p->a,NULL);
      r=find_parent;
    }
  }
  p=FindNext(t);
  if(p==NULL) {
    p=r;
    p=find(head,p->a,NULL);
  }
  r=find_parent;
  if(p!=NULL) {
    if(p->c==1) {
      ans_c--;
      t->b=p->b;
      if(r!=NULL) {
        if(r->l==p)
          r->l=dell(p);
        else
          r->r=dell(p);
      } else {
        head=dell(p);
      }
    }
  }
}

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

int main() {
  int i,j;
  int x,y;
  int c;
  fi=fopen("painter.in","rt");
  fo=fopen("painter.out","wt");

  fscanf(fi,"%d",&n);
  srandom(23058726);
  for(i=0;i<5*n;i++) {
    YY[i]=i;
  }
  for(i=0;i<3*n;i++) {
    x=random()%(5*n);
    y=random()%(5*n);
    j=YY[x];
    YY[x]=YY[y];
    YY[y]=j;
  }
  init();
  for(i=0;i<n;i++) {
    c=getcc();
    fscanf(fi,"%d%d",&x,&y);
    if(c=='W')
      ProcessLine(x,x+y,0);
    else
      ProcessLine(x,x+y,1);
    fprintf(fo,"%d %d\n",ans_c,ans_l);
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
