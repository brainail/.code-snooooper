#include <stdio.h>
#include <stdlib.h>

#define m 500000

typedef struct
{
  int x, k, l, r, n;
}
tree;

tree t[m];
int s, x[m], y[m], n, len[m];
char col[m];

void split( int v, int *l, int *r, int x )
{
  if (v == 0)
    *l = *r = 0;
  else if (t[v].x < x)
  {
    split(t[v].r, &t[v].r, r, x);
    *l = v;
  }
  else
  {
    split(t[v].l, l, &t[v].l, x);
    *r = v;
  }
}

void merge( int l, int r, int *v )
{
  if (l == 0)
    *v = r;
  else if (r == 0)
    *v = l;
  else if (t[l].k < t[r].k)
  {
    *v = l;
    merge(t[l].r, r, &t[l].r);
  }
  else
  {
    *v = r;
    merge(l, t[r].l, &t[r].l);
  }
}

void add( int n, int *v, int k )
{
  if (*v == 0)
  {
    t[++s].n = n;
    t[s].x = x[n];
    t[s].k = k;
    *v = s;
  }
  else if (k < t[*v].k)
  {
    t[++s].n = n;
    t[s].x = x[n];
    t[s].k = k;
    split(*v, &t[s].l, &t[s].r, x[n]);
    *v = s;
  }
  else if (x[n] > t[*v].x)
    add(n, &t[*v].r, k);
  else
    add(n, &t[*v].l, k);
}

void del( int n, int *v )
{
  if (t[*v].n == n)
    merge(t[*v].l, t[*v].r, v);
  else if (x[n] < t[*v].x)
    del(n, &t[*v].l);
  else
    del(n, &t[*v].r);
}

int above( int x, int v )
{
  int k;

  if (v == 0)
    return 0;
  if (t[v].x < x)
    return above(x, t[v].r);
  k = above(x, t[v].l);
  if (k == 0)
    return v;
  return k;
}

int below( int x, int v )
{
  int k;

  if (v == 0)
    return 0;
  if (t[v].x > x)
    return below(x, t[v].l);
  k = below(x, t[v].r);
  if (k == 0)
    return v;
  return k;
}

int main( void )
{
  int i, j, a, b, l, r = 0, ll = 0, nn = 0, old;

  freopen("painter.in", "r", stdin);
  freopen("painter.out", "w", stdout);
  srand(3242341);
  scanf("%d", &n);
  x[i = 1] = -5000000;
  len[1] = 10000000;
  col[1] = 'W';
  add(1, &r, rand());
  for (j = 1; j <= n; j++)
  {
    i++;
    scanf(" %c%d%d", &col[i], &x[i], &len[i]);
    len[i]--;
    while ((l = t[above(x[i], r)].n) && len[l] + x[l] <= x[i] + len[i])
    {
      if (col[l] == 'B')
      {
        ll -= len[l] + 1;
        nn--;
      }
      del(l, &r);
    }
    a = t[above(x[i], r)].n;
    b = t[below(x[i] - 1, r)].n;
    if ((b != 0 && (x[b] + len[b] > x[i] + len[i]))
        /*|| (a != 0 && (x[a] == x[i]))*/)
    {
      int old = i;

      /*if (x[a] == x[i])
        b = a;*/
      if (col[b] == col[i])
      {
        printf("%d %d\n", nn, ll);
        continue;
      }
      if (col[i] == 'B')
        ll += len[i] + 1;
      else
        ll -= len[i] + 1;
      if (col[b] == 'B')
        nn--;
      x[++i] = x[old] + len[old] + 1;
      len[i] = x[b] + len[b] - x[i - 1] - len[i - 1] - 1;
      col[i] = col[b];
      if (len[i] >= 0)
      {
        add(i, &r, rand());
        if (col[i] == 'B')
          nn++;
      }
      else
      {
        a = above(x[old] + 1, r);
        del(a, &r);
        len[i] += len[a] + 1;
      }
      if (col[i - 1] == 'B')
        nn++;
      len[b] = x[i - 1] - x[b] - 1;
      if (len[b] < 0)
      {
        del(b, &r);
        col[b] = 'Q';
      }
      else if (col[b] == 'B')
        nn++;
      add(i - 1, &r, rand());
    }
    else
    {
      if (x[a] <= x[i] + len[i] + 1)
      {
        if (col[a] == col[i])
        {
          del(a, &r);
          len[i] += len[a] + 1;
          if (col[a] == 'B')
            ll -= len[a] + 1;
        }
        else
        {
          del(a, &r);
          old = x[a];
          x[a] = x[i] + len[i] + 1;
          if (col[a] == 'B')
            ll -= len[a];
          len[a] = old + len[a] - x[a];
          if (col[a] == 'B')
            ll += len[a];
          if (len[a] >= 0)
            add(a, &r, rand());
        }
      }
      if (x[b] + len[b] >= x[i] - 1)
      {
        if (col[i] == col[b])
        {
          len[b] += len[i] + 1;
          if (col[i] == 'B')
            ll += len[i] + 1;
        }
        else
        {
          old = len[b];
          len[b] = x[i] - 1 - x[b];
          if (len[b] == 'B')
            ll -= old - len[b];
          else
            ll += len[i] + 1;
          if (col[i] == 'B')
            nn++;
          add(i, &r, rand());
        }
      }
    }
    printf("%d %d\n", nn, ll);
  }
  return 0;
}

