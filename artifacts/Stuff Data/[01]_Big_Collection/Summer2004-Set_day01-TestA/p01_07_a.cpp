#define ONLINE
#include <stdio.h>
#include <string.h>

struct TRangenode {
  int n_cells,n_segments,n_leading,n_trailing;
  char f_self;
};
class TRangetree {
  private:
    int n_levels,*n_elems,n_basic,n_nodes;
    TRangenode*nodes,**elems;
  protected:
    void alter(int,int,int);
  public:
    TRangetree(int);
    ~TRangetree();
    void reset(void);
    void change(int,int,int);
    int get_n_cells(void) const { return(elems[n_levels]->n_cells); }
    int get_n_segments(void) const { return(elems[n_levels]->n_segments); }
};

struct TAction {
  char type;
  int range_l,range_r;
  int n_segments,n_cells;
};

int n_acts,n_cells;
TAction*acts;
TRangetree*ranges;

int main(void) {
  #ifdef ONLINE
  freopen("painter.in","rt",stdin);
  freopen("painter.out","wt",stdout);
  #endif
  int i_act; char str[2]; int range_l=0,range_r=0;
  scanf("%d",&n_acts);
  acts=new TAction[n_acts];
  for (i_act=0; i_act<n_acts; i_act++) {
    scanf("%s",str);
    acts[i_act].type=( ((*str=='B')||(*str=='b')) ? 1 : 0 );
    scanf("%d%d",&acts[i_act].range_l,&acts[i_act].range_r);
    acts[i_act].range_r+=acts[i_act].range_l;
    if (acts[i_act].range_r<acts[i_act].range_l) {
      int z=acts[i_act].range_r;
      acts[i_act].range_r=acts[i_act].range_l;
      acts[i_act].range_l=z;
    }
    if (i_act) {
      if (range_l>acts[i_act].range_l) range_l=acts[i_act].range_l;
      if (range_r<acts[i_act].range_r) range_r=acts[i_act].range_r;
    } else {
      range_l=acts[i_act].range_l;
      range_r=acts[i_act].range_r;
    }
    acts[i_act].n_segments=0; acts[i_act].n_cells=0;
  }
  for (i_act=0; i_act<n_acts; i_act++) {
    acts[i_act].range_l-=range_l;
    acts[i_act].range_r-=range_l;
  } n_cells=range_r-range_l;
  ranges=new TRangetree(n_cells);
  ranges->reset();
  for (i_act=0; i_act<n_acts; i_act++) {
    ranges->change(acts[i_act].range_l,acts[i_act].range_r,acts[i_act].type);
    acts[i_act].n_segments=ranges->get_n_segments();
    acts[i_act].n_cells=ranges->get_n_cells();
  }
  delete ranges;
  for (i_act=0; i_act<n_acts; i_act++) {
    printf("%d %d\n",acts[i_act].n_segments,acts[i_act].n_cells);
  }
  delete[]acts;
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void TRangetree::alter(int i_level,int i_elem,int type) {
  if (type) {
    elems[i_level][i_elem].n_cells=1<<i_level;
    elems[i_level][i_elem].n_segments=1;
    elems[i_level][i_elem].n_leading=1;
    elems[i_level][i_elem].n_trailing=1;
  } else {
    elems[i_level][i_elem].n_cells=0;
    elems[i_level][i_elem].n_segments=0;
    elems[i_level][i_elem].n_leading=0;
    elems[i_level][i_elem].n_trailing=0;
  }
  elems[i_level][i_elem].f_self=1;
  int i_level_self,i_elem_self;
  for (i_level_self=i_level+1,i_elem_self=i_elem>>1; i_level_self<=n_levels;
       i_level_self++,i_elem_self>>=1) {
    if (elems[i_level_self][i_elem_self].f_self) break;
  } if (i_level_self>n_levels) i_level_self=-1;
  while (i_level<n_levels) {
    if (i_level<i_level_self) {
      elems[i_level][i_elem^1]=elems[i_level_self][i_elem_self];
      elems[i_level][i_elem^1].n_cells>>=i_level_self-i_level;
    }
    i_level++; i_elem>>=1;
    elems[i_level][i_elem].n_cells=
      elems[i_level-1][(i_elem<<1)].n_cells+
      elems[i_level-1][(i_elem<<1)+1].n_cells;
    elems[i_level][i_elem].n_segments=
      elems[i_level-1][(i_elem<<1)].n_segments+
      elems[i_level-1][(i_elem<<1)+1].n_segments-
      elems[i_level-1][(i_elem<<1)].n_trailing*
      elems[i_level-1][(i_elem<<1)+1].n_leading;
    elems[i_level][i_elem].n_leading=
      elems[i_level-1][(i_elem<<1)].n_leading;
    elems[i_level][i_elem].n_trailing=
      elems[i_level-1][(i_elem<<1)+1].n_trailing;
    elems[i_level][i_elem].f_self=0;
  }
}
void TRangetree::change(int i_elem,int i_elem_bound,int type) {
  int i_level;
  for (i_level=0; (i_elem<<i_level)<i_elem_bound; i_elem++) {
    while ( ((i_elem+1)<<i_level) <= i_elem_bound ) {
      if (i_elem&1) break;
      i_elem>>=1; i_level++;
    }
    while ( ((i_elem+1)<<i_level) > i_elem_bound ) {
      i_elem<<=1; i_level--;
      if (i_level<0) return;
    }
    alter(i_level,i_elem,type);
  }
}

void TRangetree::reset(void) {
  int i_elem;
  memset(nodes,0,sizeof(TRangenode)*n_nodes);
  for (i_elem=0; i_elem<n_elems[0]; i_elem++) elems[0][i_elem].f_self=1;
}

TRangetree::TRangetree(int n_elems_given) {
  int i_node,i_level;
  for (n_basic=2,n_levels=1; n_basic<n_elems_given; n_basic<<=1,n_levels++);
  n_nodes=n_basic<<1;
  n_elems=new int[n_levels+1];
  nodes=new TRangenode[n_nodes]; elems=new TRangenode*[n_levels+1];
  *elems=nodes; i_node=*n_elems=n_basic;
  for (i_level=1; i_level<=n_levels; i_level++) {
    n_elems[i_level]=n_elems[i_level-1]>>1;
    elems[i_level]=&nodes[i_node];
    i_node+=n_elems[i_level];
  }
}
TRangetree::~TRangetree() {
  delete[]n_elems; delete[]nodes; delete[]elems;
}


