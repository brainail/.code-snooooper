/* 01.07.04 SK1 */

#include <stdio.h>

#define MAX_N 3000000

long N;
long A[MAX_N][2];
long Size = 0, Sum = 0;

/* Deletes interval from the array */
void Del( long L, long R )
{
  long Shift = R - L + 1, i;

  if (L > R)
    return;
  for (i = L; i <= R; i++)
    Sum -= A[i][1] - A[i][0];
  for (i = R + 1; i < Size; i++)
    A[i - Shift][0] = A[i][0], A[i - Shift][1] = A[i][1]; 
  Size -= Shift;
} /* End of 'Del' function */

/* Adds new element to the array */
void Add( long P, long St, long En )
{
  long i;

  if (St >= En)
    return;
  for (i = Size - 1; i >= P; i--)
    A[i + 1][0] = A[i][0], A[i + 1][1] = A[i][1];
  A[P][0] = St;
  A[P][1] = En;
  Size++;
  Sum += En - St;
} /* End of 'Add' function */

/* The main program function */
int main( void )
{
  long i, j, k, St, L, En;
  char S[5];

  freopen("painter.in", "r", stdin);
  freopen("painter.out", "w", stdout);
  scanf("%li", &N);
  for (i = 0; i < N; i++)
  {
    scanf("%s%li%li", S, &St, &L);
    if (L < 0)
      St += L, L = -L;
    En = St + L;
    for (j = 0; j < Size && A[j][1] < St; j++)
      ;
    if (S[0] == 'B')
      if (j == Size)
        A[j][0] = St, A[j][1] = En, Size++, Sum += En - St;
      else if (A[j][0] <= St && A[j][1] >= En)
        ;
      else
      {
        if (A[j][0] <= St)
          St = A[j][0];
        for (k = j; k < Size && A[k][1] < En; k++)
          ;
        if (k == Size || A[k][0] > En)
          k--;
        else 
          En = A[k][1];
        Del(j, k);
        Add(j, St, En);
      }           
    else
      if (j == Size)
        ;
      else if (A[j][0] <= St && A[j][1] >= En)
      {
        Add(j + 1, En, A[j][1]);
        if (A[j][0] == St)
          Del(j, j);
        else
          Sum -= A[j][1] - St, A[j][1] = St;
      }
      else
      {
        if (A[j][0] < St)
          Sum -= A[j][1] - St, A[j][1] = St, j++;
        for (k = j; k < Size && A[k][1] < En; k++)
          ;
        if (k == Size || A[k][0] >= En)
          k--;
        else
          Sum -= En - A[k][0], A[k][0] = En, k--;
        Del(j, k);
      }
    printf("%li %li\n", Size, Sum);
  }
  return 0;
} /* End of 'main' function */
