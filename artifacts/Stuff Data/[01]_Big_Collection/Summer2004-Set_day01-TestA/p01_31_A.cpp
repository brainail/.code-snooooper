  #include <stdlib.h>
  #include <stdio.h>

  #define White true
  #define Black false

  typedef struct type_of_Line
  {
      int            B,E;
      type_of_Line  *next;
      type_of_Line  *last;
  } type_of_Line;


  typedef class list_of_Lines
  {
      private:
          type_of_Line  *Head;
          type_of_Line  *Line;
          int            n;
      public :
          inline void  Add           ( int B , int E );
          inline void  Delete        ( void );
          inline bool  operator []   ( int index );
          inline bool  Get_next      ( void );
          inline  int  B             ( void );
          inline  int  E             ( void );
          inline  int  N             ( void );
          inline       list_of_Lines ( void );
          inline    ~  list_of_Lines ( void );
  } list_of_Lines;


  inline void list_of_Lines::Add ( int B , int E )
  {
      type_of_Line *New_line = (type_of_Line*) malloc ( sizeof(type_of_Line) );
      New_line -> B = B;
      New_line -> E = E;
      New_line -> next = Head->next;
      New_line -> last = Head;
      if ( Head->next != NULL ) ( Head->next ) -> last = New_line;
      Head->next = New_line;
      n ++ ;
      if ( n == 1 ) Line = New_line;
  }

  inline void list_of_Lines::Delete ( void )
  {
      type_of_Line *Old_line = Line;
      if ( Line->last != NULL )
      {
          Line = Line->last;
          Line->next = Old_line->next;
          if ( Line->next != NULL ) ( Old_line->next ) -> last = Line;
          free ( Old_line );
          n -- ;
      }
  }


  inline bool list_of_Lines :: operator [] ( int index )
  {
      if ( index > n ) return false;
      int I;
      Line = Head;
      for ( I=0 ; I<index ; I++ ) Line = Line->next;
      return true;
  }


  inline bool  list_of_Lines :: Get_next ( void )
  {
      if ( Line->next != NULL )
      {
          Line = Line->next;
          return true;
      }
      else return false;
  }

  
  inline  int  list_of_Lines ::  B ( void )  { return Line->B; }
  inline  int  list_of_Lines ::  E ( void )  { return Line->E; }
  inline  int  list_of_Lines ::  N ( void )  { return n; }

  
  inline list_of_Lines::list_of_Lines ( void )
  {
      Head = (type_of_Line*) malloc ( sizeof(type_of_Line) );
      Line = Head;
      Head->last = NULL;
      Head->next = NULL;
      n = 0;
  }


  inline list_of_Lines::~list_of_Lines ( void )
  {
      Line = Head;
      while ( Get_next() ) free ( Line->last );
      free ( Line );
  }


  template <class smth> inline smth min ( smth A , smth B )
  {
      return ( A<B ) ? A : B ;
  }

  template <class smth> inline smth max ( smth A , smth B )
  {
      return ( A>B ) ? A : B ;
  }



  int main ( void )
  {
      int N , B,E , pL=0 , L=0 , I;
      char cC;
      bool C;

      list_of_Lines  Line;
      type_of_Line   tmp;

      FILE  *Data_file;
      FILE  *Out_file;
      if ( ( Data_file = fopen( "painter.in" , "r" ) ) == NULL )
      {
           printf ( " Error:  Could not open ""painter.in"" ! " );
           return 1;
      }
      if ( ( Out_file = fopen( "painter.out" , "w" ) ) == NULL )
      {
           printf ( " Error:  Could not create ""painter.out"" ! " );
           return 1;
      }
      fscanf ( Data_file , "%d\n" , &N );

      for ( I=0 ; I<N ; I++ )
      {
           fscanf ( Data_file , "%c %d %d\n" , &cC , &B , &E );
           if ( cC == 'W' ) C = White; else if ( cC == 'B' ) C = Black; else continue;
           if ( E <= 0 ) continue;
           E += B;

           Line[0];
           if ( C == Black )
           {
               pL = E - B;
               while ( Line.Get_next() )
               {
                   if ( ( B <= Line.B() ) && ( E >= Line.B() ) )
                   {
                       pL -= min( E , Line.E() ) - Line.B();
                       E = max( E , Line.E() );
                   }
                   else if ( ( B <= Line.E() ) && ( E >= Line.E() ) )
                   {
                       pL -= Line.E() - max( B , Line.B() );
                       B = min ( B , Line.B() );
                   }
                   else if ( ( B >= Line.B() ) && ( E <= Line.E() ) )
                   {
                       pL = 0;
                   }
                   else continue;

                   if ( pL <= 0 ) goto sorry;

                   Line.Delete();
               }
               L += pL;
               Line.Add ( B , E );
           }
           else
           {
               pL = 0;
               while ( Line.Get_next() )
               {
                   if ( ( B <= Line.B() ) && ( E >= Line.B() ) )
                   {
                       pL += min( E , Line.E() ) - Line.B();
                       tmp.B = E;
                       tmp.E = Line.E();
                   }
                   else if ( ( B < Line.E() ) && ( E >= Line.E() ) )
                   {
                       pL += Line.E() - max( B , Line.B() );
                       tmp.B = Line.B();
                       tmp.E = B;
                   }
                   else if ( ( B >= Line.B() ) && ( E <= Line.E() ) )
                   {
                       pL += E - B;
                       if ( Line.B() < B ) Line.Add ( Line.B() , B );
                       if ( E < Line.E() ) Line.Add ( E , Line.E() );
                       Line.Delete();
                       continue;
                   }
                   else continue;
                   if ( ( tmp.B != Line.B() ) || ( tmp.E != Line.E() ) )
                   {
                       Line.Delete();
                       if ( tmp.E > tmp.B ) Line.Add ( tmp.B , tmp.E );
                   }
               }
               L -= pL;
           }
sorry:     fprintf ( Out_file , "%d %d\n" , Line.N() , L );
      }

      fclose ( Data_file );
      fclose ( Out_file );
      return 0;
  }
