#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define eps 1e-9
#define NN 512

struct OCT {
  int x,y;
  int d,h;
};

struct EVENT {
  int x;
  int tp;
};

FILE *fi, *fo;
OCT b[NN];
EVENT a[2*NN];
int n,m,k;
int ans=0;
int minx=5000,maxx=-5000;
double SIN_PI_8,COS_PI_8;

int round_up(double );
int round_down(double );

int round_up(double y) {
//  if((y<0)&&(y>-1)) return 0;
  if(y>0) {
    return (int) (y+1.0-eps);
  } else {
    return (int) (y-eps);
  }
  return 0;
}

int round_down(double y) {
  if(y>0) {
    return (int) (y+eps);
  } else {
    return (int) (y-1.0+eps);
  }
  return 0;
}

void ReadOct(OCT *p) {
  int x,y,r;
  double dx,dy;
  fscanf(fi,"%d%d%d",&x,&y,&r);
  p->x=x;
  p->y=y;
  p->d=round_down(r*COS_PI_8);
  p->h=round_down(r*SIN_PI_8);
  dx=r*COS_PI_8-(double)p->d;
  dy=r*SIN_PI_8-(double)p->h;
  if(dx>(1.0-dy)-eps)
    p->h++;
}

bool intersect(OCT *p, int y, int *x1, int *x2) {
  if(p->y+p->d<y) return false;
  if(p->y-p->d>y) return false;
  if(p->y+p->h<y) {
    (*x1)=p->x-p->d+(y-(p->y+p->h));
    (*x2)=p->x+p->d-(y-(p->y+p->h));
    return true;
  }
  if(p->y-p->h>y) {
    (*x1)=p->x-p->d+(-y+(p->y-p->h));
    (*x2)=p->x+p->d-(-y+(p->y-p->h));
    return true;
  }
  (*x1)=p->x-p->d;
  (*x2)=p->x+p->d;
  return true;
}

int cmp(EVENT a, EVENT b) {
  if(a.x<b.x) return -1;
  if(b.x<a.x) return 1;
  if(a.tp<b.tp) return -1;
  if(a.tp>b.tp) return 1;
  return 0;
}

void sift(EVENT *a, int p, int q) {
  EVENT x;
  int i,j;
  i=p;
  j=2*p+1;
  x=a[i];
  if(j<q-1) if(cmp(a[j],a[j+1])<0) j++;
  if(j<q) if(cmp(a[j],x)<0) j=q+1;
  while(j<q) {
    a[i]=a[j];
    i=j;
    j=2*j+1;
    if(j<q-1) if(cmp(a[j],a[j+1])<0) j++;
    if(j<q) if(cmp(a[j],x)<0) j=q+1;
  }
  a[i]=x;
}

void HeapSort(EVENT *a, int n) {
  EVENT x;
  int i;
  for(i=n/2+1;i>=0;i--) {
    sift(a,i,n);
  }
  for(i=n-1;i>0;i--) {
    x=a[0];
    a[0]=a[i];
    a[i]=x;
    sift(a,0,i);
  }
}

int process_line(int y) {
  int ans;
  int x1,x2;
  int i,j;
  ans=0;
  m=0;
  for(i=0;i<n;i++) {
    if(intersect(&b[i],y,&x1,&x2)) {
      a[m].tp=0;
      a[m++].x=x1;
      a[m].tp=1;
      a[m++].x=x2;
    }
  }
  HeapSort(a,m);
  j=1;
  ans=0;
  for(i=1;i<m;i++) {
    if(j>=k) {
      ans+=a[i].x-a[i-1].x+1;
      if(a[i].tp==0)
        ans--;
      if(a[i].tp==1)
        if(j>k)
          ans--;
    }
    if(a[i].tp==0) {
      j++;
    } else {
      j--;
    }
  }
  return ans;
}

int main() {
  int i,j;
  fi=fopen("octagons.in","rt");
  fo=fopen("octagons.out","wt");

  SIN_PI_8=sin(M_PI/8.0);
  COS_PI_8=cos(M_PI/8.0);
  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++) {
    ReadOct(&b[i]);
    if(b[i].y-b[i].d<minx)
      minx=b[i].y-b[i].d;
    if(b[i].y+b[i].d>maxx)
      maxx=b[i].y+b[i].d;
  }
  fscanf(fi,"%d",&k);
  for(i=minx;i<=maxx;i++) {
    ans+=process_line(i);
  }
  fprintf(fo,"%d\n",ans);

  fclose(fi);
  fclose(fo);
  return 0;
}
