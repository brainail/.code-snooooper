#include <stdio.h>
#include <math.h>

#define mm 8001
#define m 4000
#define e 1e-9

typedef double dbl;

typedef struct
{
  dbl x, y;
}
pnt;

int a[mm + 1], sum[mm + 1], k, x[2005], y[2005], r[2005], f[1000],
    left[1000], right[1000];
pnt p[2005][8];
dbl rr[2005];

void add( int x, int t )
{
  x += m;
  sum[x] += t;
  if ((t == -1 && sum[x] != k - 1) || (t == 1 && sum[x] != k))
    return;
  while (x <= mm)
  {
    a[x] += t;
    x |= x + 1;
  }
}

void line( int x1, int x2, int t )
{
  int i;

  for (i = x1; i <= x2; i++)
    add(i, t);
}

int inter( int t, dbl y, int i, int j, pnt *pp )
{
  dbl a1, b1, c1, a2 = 0, b2 = 1, c2 = y, d;

  a1 = p[t][i].y - p[t][j].y;
  b1 = p[t][j].x - p[t][i].x;
  c1 = p[t][i].x * a1 + p[t][i].y * b1;
  d = (a1 * b2 - a2 * b1);
  pp->x = (c1 * b2 - c2 * b1) / d;
  pp->y = (a1 * c2 - a2 * c1) / d;
  return ((pp->y > p[t][i].y - e && pp->y < p[t][j].y + e)
     || (pp->y > p[t][j].y - e && pp->y < p[t][i].y + e));
}

int main( void )
{
  int i, ss = 0, n, yy;
  pnt p1, p2;

  freopen("octagons.in", "r", stdin);
  freopen("octagons.out", "w", stdout);
  scanf("%d", &n);
  for (i = 1; i <= n; i++)
  {
    scanf("%d%d%d", &x[i], &y[i], &r[i]);
    rr[i] = r[i] * cos(M_PI / 8);
    for (k = 0; k < 8; k++)
    {
      p[i][k].x = r[i] * cos(M_PI * k / 4 + M_PI / 8) + x[i];
      p[i][k].y = r[i] * sin(M_PI * k / 4 + M_PI / 8) + y[i];
    }
  }
  scanf("%d", &k);
  for (yy = 4000; yy >= -4000; yy--)
  {
    for (i = 1; i <= n; i++)
      if (y[i] + rr[i] > yy - e && y[i] - rr[i] < yy + e)
      {
        if (inter(i, yy, 0, 1, &p2))
          inter(i, yy, 2, 3, &p1);
        else if (inter(i, yy, 0, 7, &p2))
          inter(i, yy, 3, 4, &p1);
        else
        {
          inter(i, yy, 4, 5, &p1);
          inter(i, yy, 6, 7, &p2);
        }
        if (!f[i])
        {
          f[i] = 1;
          line(left[i] = ceil(p1.x - e), right[i] = floor(p2.x + e), 1);
        }
        else
        {
          if (ceil(p1.x - e) < left[i])
            add(ceil(p1.x - e), 1);
          if (ceil(p1.x - e) > left[i])
            add(ceil(p1.x - e), -1);
          if (floor(p2.x + e) > right[i])
            add(floor(p2.x + e), 1);
          if (floor(p2.x + e) < right[i])
            add(floor(p2.x + e), -1);
          right[i] = floor(p2.x + e);
          left[i] = ceil(p1.x - e);
        }
      }
      else if (f[i] == 1)
      {
        f[i] = 2;
        line(left[i], right[i], -1);
      }
    for (i = mm; i >= 0; i = (i & (i + 1)) - 1)
    {
      ss += a[i];
      if (i == 0)
        break;
    }
  }
  printf("%d\n", ss);
  return 0;
}
