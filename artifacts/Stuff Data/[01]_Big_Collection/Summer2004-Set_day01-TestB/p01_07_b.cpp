#define ONLINE
//#define DEBUG
#include <stdio.h>
#include <string.h>
#include <math.h>

#define EPS 1e-7L

struct TOctagon {
  short x,y,r;
  long double x0,x1,y0,y1,z0,z1;
  static const long double semiside,apotheme;
  void precalc(void);
  int getlr(long double&,long double&,long double);
};
const long double TOctagon::semiside=0.3826834323650897717284599840304L;
const long double TOctagon::apotheme=0.92387953251128675612818318939679L;

struct TEvent {
  short x;
  char type;
};

void sort_events(TEvent*,int);

int n_gons,n_gons_over,n_events_bound,n_events;
TOctagon*gons;
TEvent*events;
short range_l,range_r;
long c_pnts_over;

int main(void) {
  #ifdef ONLINE
  freopen("octagons.in","rt",stdin);
  freopen("octagons.out","wt",stdout);
  #endif
  int i_gon,i_event;
  scanf("%d",&n_gons);
  gons=new TOctagon[n_gons];
  range_l=0; range_r=0;
  for (i_gon=0; i_gon<n_gons; i_gon++) {
    scanf("%hd%hd%hd",&gons[i_gon].x,&gons[i_gon].y,&gons[i_gon].r);
    if (gons[i_gon].r<0) gons[i_gon].r=-gons[i_gon].r;
    gons[i_gon].precalc();
    if (i_gon) {
      if (range_l>gons[i_gon].y-gons[i_gon].r)
        range_l=gons[i_gon].y-gons[i_gon].r;
      if (range_r<gons[i_gon].y+gons[i_gon].r)
        range_r=gons[i_gon].y+gons[i_gon].r;
    } else {
      range_l=gons[i_gon].y-gons[i_gon].r;
      range_r=gons[i_gon].y+gons[i_gon].r;
    }
  }
  scanf("%d",&n_gons_over);
  short line,pos_st=0,pos_en=0;
  n_events_bound=n_gons<<1;
  events=new TEvent[n_events_bound];
  c_pnts_over=0;
  for (line=range_l; line<=range_r; line++) {
    #ifdef DEBUG
    printf("%hd\n",line);
    #endif
    n_events=0;
    for (i_gon=0; i_gon<n_gons; i_gon++) {
      long double pos_l,pos_r;
      if (!gons[i_gon].getlr(pos_l,pos_r,(long double)line)) continue;
      pos_st=(short)(ceil(pos_l-EPS));
      pos_en=(short)(floor(pos_r+EPS));
      pos_st=pos_st*2; pos_en=pos_en*2+1;
      #ifdef DEBUG
      printf("%d %Lf(%hd) %Lf(%hd)\n",i_gon,pos_l,pos_st,pos_r,pos_en);
      #endif
      if (pos_st>pos_en) continue;
      events[n_events].x=pos_st; events[n_events++].type=1;
      events[n_events].x=pos_en; events[n_events++].type=0;
    }
    if (n_events<=0) continue;
    sort_events(events,n_events);
    #ifdef DEBUG
    printf("%d=",n_events);
    for (i_event=0; i_event<n_events; i_event++) {
      printf(" %hd%c",events[i_event].x,events[i_event].type?'+':'-');
    } printf("\n");
    #endif
    i_gon=0;
    for (i_event=0; i_event<n_events; i_event++) if (events[i_event].type) {
      if (++i_gon==n_gons_over) pos_st=events[i_event].x;
    } else {
      if (i_gon--==n_gons_over) { pos_en=events[i_event].x;
        c_pnts_over+=(long)((pos_en-pos_st)/2+1);
      }
    }
  }
  delete[]events;
  printf("%ld\n",c_pnts_over);
  delete[]gons;
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void sift_events(TEvent*a,int v,int w) {
  int i,j; TEvent z;
  i=v; j=(v<<1)+1;
  z=a[i];
  while (j<=w) {
    if (j<w) if (a[j].x<a[j+1].x) j++;
    //else if (a[j].x<=a[j+1].x) if ((a[j].type)&&(!a[j+1].type)) j++;
    if (!(z.x<a[j].x)) break;
    //else if (z.x>=a[j].x) if ((!z.type)&&(a[j].type)) break;
    a[i]=a[j];
    i=j; j=(j<<1)+1;
  }
  a[i]=z;
}
void sort_events(TEvent*a,int n) {
  int i,j; TEvent z;
  for (i=n>>1,j=n-1; i>0; i--) sift_events(a,i,j);
  for (; j>0; j--) {
    sift_events(a,i,j);
    z=a[i]; a[i]=a[j]; a[j]=z;
  }
}


void TOctagon::precalc(void) {
  x0=((long double)x)-((long double)r)*apotheme;
  x1=((long double)x)+((long double)r)*apotheme;
  y0=((long double)y)-((long double)r)*apotheme;
  y1=((long double)y)+((long double)r)*apotheme;
  z0=((long double)y)-((long double)r)*semiside;
  z1=((long double)y)+((long double)r)*semiside;
}
int TOctagon::getlr(long double&pos_l,long double&pos_r,long double line) {
  if (line-y0<-EPS) return(0);
  if (line-y1>+EPS) return(0);
  pos_l=x0; pos_r=x1;
  if (line-z0<-EPS) { pos_l+=z0-line; pos_r-=z0-line; }
  if (line-z1>+EPS) { pos_l+=line-z1; pos_r-=line-z1; }
  return(1);
}

