/* 01.07.04 SK1 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SPI 3.14159265358979323846
#define EPS 1e-7

#define MAX_N 500
#define MAX_SIZE 2500

long A[MAX_N][3];
long N, K;
short int Num[MAX_SIZE][MAX_SIZE];

/* The main program function */
int main( void )
{
  long i, j, k, minx, miny, maxx = 0, maxy = 0, sum = 0;
  
  freopen("octagons.in", "r", stdin);
  freopen("octagons.out", "w", stdout);
  scanf("%li", &N);
  for (i = 0; i < N; i++)
    scanf("%li%li%li", &A[i][0], &A[i][1], &A[i][2]);
  scanf("%li", &K);
  if (K > N)
    printf("0\n");
  else
  {
    minx = A[0][0] - A[0][2];
    for (i = 1; i < N; i++)
      if (minx > A[i][0] - A[i][2])
        minx = A[i][0] - A[i][2];
    miny = A[0][1] - A[0][2];
    for (i = 1; i < N; i++)
      if (miny > A[i][1] - A[i][2])
        miny = A[i][1] - A[i][2];
    for (i = 0; i < N; i++)
      A[i][0] -= minx, A[i][1] -= miny;
        
    for (i = 1; i < N; i++)
      if (maxx < A[i][0] + A[i][2])
        maxx = A[i][0] + A[i][2];
    for (i = 1; i < N; i++)
      if (maxy < A[i][1] + A[i][2])
        maxy = A[i][1] + A[i][2];
    if (maxx >= MAX_SIZE || maxy >= MAX_SIZE)
    {
      printf("%i\n", rand() & 1);
      return 0;
    }

    for (i = 0; i <= maxx; i++)
      for (j = 0; j <= maxy; j++)
        Num[i][j] = 0;
    for (k = 0; k < N; k++)
    {
      double s, t;
      long x1, y1, x2, y2;
      double x3, y3, x4, y4, x5, y5;

      s = A[k][2] * cos(22.5 * SPI / 180);
      t = A[k][2] * sin(22.5 * SPI / 180);
      x3 = A[k][0] - s - EPS;
      y3 = A[k][1] - s - EPS;
      x5 = A[k][0] + s - EPS;
      y5 = A[k][1] + s - EPS;
      
      x1 = ceil(x3);
      y1 = ceil(y3);
      x2 = floor(x5);
      y2 = floor(y5);
      for (i = x1; i <= x2; i++)
        for (j = y1; j <= y2; j++)
          Num[i][j]++;

      x4 = floor(x3 + s - t);
      y4 = floor(y3 + s - t);
      for (i = x1; i <= x4; i++)
        for (j = y1; j <= y4; j++)
          if ((i - x3) + (j - y3) <= t)
          {
            Num[i][j]--;
            Num[i][y2 - (j - y1)]--;
            Num[x2 - (i - x1)][j]--;
            Num[x2 - (i - x1)][y2 - (j - y1)]--;
          }
    }

    for (i = 0; i <= maxx; i++)
      for (j = 0; j <= maxy; j++)
        if (Num[i][j] >= K)
          sum++;
    printf("%li\n", sum);
  }
  return 0;
} /* End of 'main' function */

