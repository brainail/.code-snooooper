#include <stddef.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define INT_BITS (sizeof(int)*CHAR_BIT)

int rand_int0()
{
	return (rand() >> (INT_BITS/2)) | (rand() & 0xFFFF0000) ;
}
int rand_int1(int range)
{
	return (rand_int0() % range);
}
int rand_int2(int low, int high)
{
	return (rand_int1(high-low) + low);
}

int main(void)
{
	int SEED = 1;
	{ FILE*fi=fopen("octagons.in","r"); fscanf(fi,"%u",&SEED); fclose(fi); }
	FILE *fo = fopen("octagons.out","w");
	srand(SEED);
	fprintf(fo,"%u", (rand_int2(0,10000)));
	fclose(fo);
	return 0;
}
