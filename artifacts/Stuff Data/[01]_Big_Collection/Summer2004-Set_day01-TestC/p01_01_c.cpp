#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "diehard.h"

#define CONST_DIFF (n+m)
#define CONST_DIFF_2 ((n+m)/2)
#define STEP_MAX (n*m/3)

struct PP {
  int x,y;
};

char container[NMAX*(NMAX+1)];
char *map[NMAX];
PP a[NMAX*NMAX];
int n,m,k=0,nm=0;
int tx,ty;
int stepcounter=0;

bool ApplyMove(PP *p, int mv, int res) {
  int i,j;
  i=p->x;
  j=p->y;
  if(mv=='v') {
    if(res) {
      if(map[i][j]!='T')
        return false;
      else
        return true;
    } else {
      if(map[i][j]=='T')
        return false;
      else
        return true;
    }
    return false;
  }
  switch(mv) {
    case DIR_UP:
      p->x=(p->x-1+n)%n;
      break;
    case DIR_DOWN:
      p->x=(p->x+1)%n;
      break;
    case DIR_LEFT:
      p->y=(p->y-1+m)%m;
      break;
    case DIR_RIGHT:
      p->y=(p->y+1)%m;
      break;
  }
  i=p->x;
  j=p->y;
  if(res) {
    if(map[i][j]=='*') {
      return true;
    } else {
      return false;
    }
  } else {
    if(map[i][j]!='*') {
      return true;
    } else {
      return false;
    }
  }
}

void ApplyAll(int mv, int res) {
  int i;
  for(i=0;i<k;i++) {
    if(!ApplyMove(&a[i],mv,res)) {
      a[i]=a[--k];
      i--;
    }
  }
}

void GoTo(PP p, int x, int y) {
  int f;
  if(p.x<x) {
    while(p.x!=x) {
      f=DoMove(DIR_DOWN);
      ApplyAll(DIR_DOWN,f);
      if(!ApplyMove(&p,DIR_DOWN,f))
        return ;
    }
  } else {
    while(p.x!=x) {
      f=DoMove(DIR_UP);
      ApplyAll(DIR_UP,f);
      if(!ApplyMove(&p,DIR_UP,f))
        return ;
    }
  }
  if(p.y<y) {
    while(p.y!=y) {
      f=DoMove(DIR_RIGHT);
      ApplyAll(DIR_RIGHT,f);
      if(!ApplyMove(&p,DIR_RIGHT,f))
        return ;
    }
  } else {
    while(p.y!=y) {
      f=DoMove(DIR_LEFT);
      ApplyAll(DIR_LEFT,f);
      if(!ApplyMove(&p,DIR_LEFT,f))
        return ;
    }
  }
}

void TryBomb() {
  int i,f;
  for(i=0;i<k;i++) {
    if((a[i].x==tx)&&(a[i].y==ty)) {
      f=Dig();
      if(f) exit(0);
      ApplyAll('v',0);
    }
  }
}

void GoTo2(PP p, int x, int y) {
  int f;
  if(p.x<x) {
    while(p.x!=x) {
      TryBomb();
      f=DoMove(DIR_DOWN);
      ApplyAll(DIR_DOWN,f);
      if(!ApplyMove(&p,DIR_DOWN,f))
        return ;
    }
  } else {
    while(p.x!=x) {
      TryBomb();
      f=DoMove(DIR_UP);
      ApplyAll(DIR_UP,f);
      if(!ApplyMove(&p,DIR_UP,f))
        return ;
    }
  }
  if(p.y<y) {
    while(p.y!=y) {
      TryBomb();
      f=DoMove(DIR_RIGHT);
      ApplyAll(DIR_RIGHT,f);
      if(!ApplyMove(&p,DIR_RIGHT,f))
        return ;
    }
  } else {
    while(p.y!=y) {
      TryBomb();
      f=DoMove(DIR_LEFT);
      ApplyAll(DIR_LEFT,f);
      if(!ApplyMove(&p,DIR_LEFT,f))
        return ;
    }
  }
}

int main() {
  int i,j;
  bool f;
  int g;
  for(i=0;i<NMAX;i++) {
    map[i]=&container[i*(NMAX+1)];
  }
  Init(&n,&m,map);
  k=0;
  for(i=0;i<n;i++) {
    for(j=0;j<m;j++) {
      if(map[i][j]!='*') {
        if(map[i][j]!='T') {
          a[k].x=i;
          a[k].y=j;
          k++;
        }
      } else {
        nm++;
      }
      if(map[i][j]=='T') {
        tx=i;
        ty=j;
      }
    }
  }
  if(nm==0) {
    f=false;
    for(i=0;i<n;i++) {
      for(j=0;j<m-1;j++) {
        if(f) if(Dig()) return 0;
        if(DoMove(DIR_RIGHT))
          f=false;
        else
          f=true;
      }
      if(f) if(Dig()) return 0;
      if(DoMove(DIR_UP))
        f=false;
      else
        f=true;
    }
    return 0;
  }
  if(nm==1) {
    f=false;
    for(i=0;(i<n)&&(k>1);i++) {
      for(j=0;(j<m-1)&&(k>1);j++) {
        if((g=DoMove(DIR_RIGHT)))
          f=false;
        else
          f=true;
        ApplyAll(DIR_RIGHT,g);
      }
      if(DoMove(DIR_UP))
        f=false;
      else
        f=true;
      ApplyAll(DIR_UP,g);
    }
    GoTo(a[0],tx,ty);
    Dig();
    return 0;
  }
  if(nm>1) {
    while((abs(k-nm)>CONST_DIFF)&&(k>CONST_DIFF_2)&&(stepcounter<STEP_MAX)) {
      if(stepcounter%2)
        j=DIR_UP;
      else
        j=DIR_RIGHT;
      if(stepcounter%5==0)
        j=DIR_LEFT;
/*      if(stepcounter%11==0)
        j=DIR_DOWN;*/
      i=DoMove(j);
      ApplyAll(j,i);
      stepcounter++;
    }
    while(k>0) {
      GoTo2(a[0],tx,ty);
      if((a[0].x==tx)&&(a[0].y==ty)) {
        if(Dig())
          return 0;
        else
          ApplyAll('v',0);
      }
    }
  }
  return 0;
}
