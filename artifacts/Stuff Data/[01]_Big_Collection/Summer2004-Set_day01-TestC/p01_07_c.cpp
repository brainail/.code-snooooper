#include <stdio.h>
#include <string.h>
#include "diehard.h"

int n_x,n_y;
char*matrix_cont,**matrix;

int main(void) {
  int x,y;
  matrix_cont=new char[NMAX*NMAX]; matrix=new char*[NMAX];
  for (x=0,y=0; x<NMAX; x++,y+=NMAX) matrix[x]=&matrix_cont[y];
  Init(&n_x,&n_y,matrix);
  for (y=0; y<n_y; y++) {
    for (x=1; x<n_x; x++) {
      if (DoMove(DIR_UP)) continue;
      if (Dig()) break;
    } if (x<n_x) break;
      if (DoMove(DIR_RIGHT)) continue;
      if (Dig()) break;
  }
  delete[]matrix_cont; delete[]matrix;
  return(0);
}



