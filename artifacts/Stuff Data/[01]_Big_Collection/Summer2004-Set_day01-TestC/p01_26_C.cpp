/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;
#include "diehard.h"

#define DBG if(0)

void die(const char*msg) { cerr<<msg<<endl; exit(1); }

template<typename T> 
T** alloc2d(int n, int m)
{
	char *p = new char[ sizeof(T*)*n + sizeof(T)*n*m ];
	T** r = (T**)p;
	T* arr = (T*)( p + sizeof(T*)*n );
	for (size_t i=0; i<n; i++, arr+=m)
		r[i] = arr;
	return r;
}
template <typename T>
inline void free2d(T** array)
{
	delete[] (char*)array;
}

const int INT_BITS = sizeof(int)*CHAR_BIT;
inline int rand_int()
{
	return (rand() >> (INT_BITS/2)) | (rand() & 0xFFFF0000) ;
}
inline int rand_int(int range)
{
	return (rand_int() % range);
}
inline int rand_int(int low, int high)
{
	return (rand_int(high-low) + low);
}

// ======================================================================

int dig()
{
	int r = Dig();
	if (r!=0)
		cerr << "Dig() returns " << r << endl;
	return r;
}

bool match(char**map, char**realmap, int N, int M, int*xpos, int*ypos)
{
	return false;
}
int mod(int x, int y)// match x mod y
{
	while (x < 0) x+= y;
	return (x%y);
}
#define xidx(x) (mod((x),(N)))
#define yidx(y) (mod((y),(M)))
#define get(x,y) map[xidx(x)][yidx(y)]
const int dx[4] = { 0, 1, 0, -1 };
const int dy[4] = { -1, 0, 1, 0 };

void solve(int N, int M, char**realmap)
{
	DBG cout << "N==" << N << " M==" << M << endl;
	char **map = alloc2d<char>(N,M);
	for (int i=0; i<N; i++)
		for (int j=0; j<M; j++)
			map[i][j] = '.';
	map[0][0] == 'S';
	int realx,realy;
	int x=0, y=0;
	while (!match(map,realmap,N,M,&realx,&realy))
	{
		// try dig
		if (get(x,y) == '.') {
			dig();
			get(x,y) = 'v';
		}
		// choose way
		int way = rand_int(4);
		for (int i=0; i<3; i++) {
			if (get(x+dx[way], y+dy[way]) == '.')
				break;
			way = (way+1)%4;
		}
		x += dx[way];  y += dy[way];
		// go
		if (DoMove(way))
			get(x,y) = '*';
	}
	free2d(map);
}
// ======================================================================
int main(void)
{
	int N,M;
	char **map = alloc2d<char>(NMAX,NMAX);
	Init(&N,&M,map);
	srand(N*M);
	solve(N,M,map);
	return 0;
}

