#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 100010
#define SS (2*NN+10)

struct REB {
  int a;
  int w;
  REB *p;
};

FILE *fi, *fo;
int enter[NN];
int leave[NN];
REB *g[NN];
int p[NN];
REB *pw[NN];
int d[NN];
bool used[NN];
REB *r1[NN];
REB *r2[NN];
REB swap[SS];
int iswap=0;
int n;
int tm;

int max(int a, int b) {
  return (a>b)?a:b;
}

int min(int a, int b) {
  return (a<b)?a:b;
}

REB *getnew() {
  return &swap[iswap++];
}

REB *Add(int x, int y, int w) {
  REB *t;
  t=getnew();
  t->a=y;
  t->p=g[x];
  g[x]=t;
  t->w=w;
  return t;
}

bool is_ancestor(int x, int y) {
  if(enter[x]>enter[y])
    return false;
  if(leave[x]<leave[y])
    return false;
  return true;
}

int lca(int a, int b) {
  int i,j,dd;
  dd=10000000;
  j=-1;
  for(i=0;i<n;i++) {
    if(is_ancestor(i,a)&&is_ancestor(i,b)) {
      if(d[i]>dd) {
        j=i;
        dd=d[i];
      }
    }
  }
  return j;
}

int get_(int a, int f) {
  int m;
  m=-1000000000;
  while(a!=f) {
    m=max(m,pw[a]->w);
    a=p[a];
  }
  return m;
}

int get(int a, int b) {
  int f;
  f=lca(a,b);
  return max(get_(a,f),get_(b,f));
}

void dfs(int v, int pp, REB *pr) {
  REB *t;
  p[v]=pp;
  pw[v]=pr;
  used[v]=true;
  enter[v]=tm++;
  for(t=g[v];t!=NULL;t=t->p) {
    if(!used[t->a]) {
      dfs(t->a,v,t);
    }
  }
  leave[v]=tm++;
}

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\t')||(c=='\r'))
    c=getc(fi);
  return c;
}

void ReadWord(char *s) {
  int c;
  c=getcc();
  while(isalpha(c)) {
    (*s)=c;
    s++;
    c=getc(fi);
  }
  (*s)='\0';
}

int main() {
  int x,y,z;
  int i,j;
  char wrd[64];
  fi=fopen("thruput.in","rt");
  fo=fopen("thruput.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++) {
    g[i]=NULL;
    used[i]=false;
  }
  for(i=0;i<n-1;i++) {
    fscanf(fi,"%d%d%d",&x,&y,&z);
    x--; y--;
    r1[i]=Add(x,y,z);
    r2[i]=Add(y,x,z);
  }
  tm=0;
  dfs(0,-1,NULL);
  while(true) {
    ReadWord(wrd);
    if(strcmp(wrd,"DONE")==0) {
      break;
    }
    if(strcmp(wrd,"QUERY")==0) {
      fscanf(fi,"%d%d",&x,&y);
      x--; y--;
      fprintf(fo,"%d\n",get(x,y));
    }
    if(strcmp(wrd,"CHANGE")==0) {
      fscanf(fi,"%d%d",&x,&y);
      x--;
      r1[x]->w=y;
      r2[x]->w=y;
    }
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
