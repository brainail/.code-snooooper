#include <stdio.h>

#define m 200005
#define inf 100000

int kk, rr, r[m], next[m], h[m], c[m], u, _stklen = 8 * 1024 * 1024;

void add( int x, int y, int t )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  c[rr] = t;
}

int dfs( int v, int p )
{
  int i;

  if (v == u)
    return 1;
  for (i = h[v]; i != 0; i = next[i])
    if (r[i] != p && dfs(r[i], v))
    {
      if (kk < c[i])
        kk = c[i];
      return 1;
    }
  return 0;
}

int main( void )
{
  int i, n, v, j, t;
  char s[20];

  freopen("thruput.in", "r", stdin);
  freopen("thruput.out", "w", stdout);
  scanf("%d", &n);
  for (i = 1; i < n; i++)
  {
    scanf("%d%d%d", &u, &v, &t);
    add(u, v, t);
    add(v, u, t);
  }
  while (1)
  {
    scanf("%s", s);
    if (strcmp(s, "DONE") == 0)
      break;
    scanf("%d%d", &u, &v);
    if (strcmp(s, "QUERY") == 0)
    {
      kk = -inf;
      dfs(v, 0);
      printf("%d\n", kk);
    }
    else
      c[u * 2 - 1] = c[u * 2] = v;
  }
  return 0;
}

