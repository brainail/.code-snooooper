/* 02.07.04 SK1 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_N 100000

long N, EN;
long E[MAX_N][3];
long P[MAX_N];
long Prev[MAX_N];
long PrSi[MAX_N];
long *C[MAX_N * 2];
char S[99];
long P1[MAX_N], P2[MAX_N];
long PL1, PL2;

/* Search function */
void Search( long v )
{
  long i;
  
  for (i = 0; i < P[v]; i += 2)
    if (C[v][i] != Prev[v])
    {
      Prev[C[v][i]] = v;
      Search(C[v][i]);
    }
    else
      PrSi[v] = C[v][i + 1];
} /* End of 'Search' function */

/* The main program function */
int main( void )
{
  long i, j, a, b, max;
  
  freopen("thruput.in", "r", stdin);
  freopen("thruput.out", "w", stdout);
  scanf("%li", &N);
  EN = N - 1;
  for (i = 0; i < EN; i++)
    for (j = 0; j < 3; j++)
      scanf("%li", &E[i][j]);
  for (i = 0; i < EN; i++)
    E[i][0]--, E[i][1]--;
  for (i = 0; i < N; i++)
    P[i] = 0;
  for (i = 0; i < EN; i++)
    P[E[i][0]]++, P[E[i][1]]++;
  for (i = 0; i < EN; i++)
    for (j = 0; j < 2; j++)
      C[i] = malloc(sizeof(long) * P[i] * 2);
  for (i = 0; i < N; i++)
    P[i] = 0;
  for (i = 0; i < EN; i++)
  {
    a = E[i][0], b = E[i][1];
    C[a][P[a]] = b;
    C[b][P[b]] = a;
    C[a][P[a] + 1] = i;
    C[b][P[b] + 1] = i;
    P[a] += 2, P[b] += 2;
  }
  Prev[0] = -1;
  Search(0);
  while (1)
  {
    scanf("%s", S);
    if (strcmp(S, "DONE") == 0)
      break;
    else if (strcmp(S, "CHANGE") == 0)
    {
      scanf("%li%li", &a, &b);
      a--;
      E[a][2] = b;
    }
    else if (strcmp(S, "QUERY") == 0)
    {
      scanf("%li%li", &a, &b);
      a--, b--;
      PL1 = PL2 = 0;
      while (a != -1)
        P1[PL1++] = a, a = Prev[a];
      while (b != -1)
        P2[PL2++] = b, b = Prev[b];
      i = PL1 - 1;
      j = PL2 - 1;
      while (P1[i] == P2[j] && i >= 0 && j >= 0)
        i--, j--;
      max = 0;
      for (i = i; i >= 0; i--)
        if (E[PrSi[P1[i]]][2] > max)
          max = E[PrSi[P1[i]]][2];
      for (j = j; j >= 0; j--)
        if (E[PrSi[P2[j]]][2] > max)
          max = E[PrSi[P2[j]]][2];
      printf("%li\n", max);
    }
  }
  return 0;
} /* End of 'main' function */

