#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

#define NN 110

FILE *fi, *fo;
int used[NN];
int n;
double T,T0;
int MX_STEP;
int a1[NN][NN];
int a2[NN][NN];
int a3[NN][NN];
int s1[NN];
int s2[NN];
int os1[NN],os2[NN];
int best=0;
double cc;
int bs1[NN];
int bs2[NN];

double xrandom() {
  return (double)(random()%10000000)/10000000.0;
}

double sign(double a) {
  return (a<0)?1:0;
}

void GenPermut(int *a, int *old) {
  int i,j;
  int f,k;
  int of;
  double alfa,y;
  for(i=0;i<n;i++)
    used[i]=false;
  alfa=xrandom();
  y=sign(alfa-0.5)*T*(pow(1.0+1.0/T,fabs(2.0*alfa-1.0))-1.0);
  for(i=0;i<n;i++) {

    of=0;
    for(j=i+1;j<n;j++) {
      if(old[j]<old[i]) {
        of++;
      }
    }
    f=(int)of+(int)(y*(double)(n-i));
    k=0;
    for(j=0;j<n;j++) {
      if(!used[j]) {
        if(k==f) {
          a[i]=j;
          used[j]=true;
          break;
        } else {
          k++;
        }
      }
    }
  }
}

int get_time() {
  int i,s;
  s=0;
  for(i=0;i<n;i++) {
    s+=a1[i][s1[i]];
    s+=a2[s1[i]][s2[i]];
    s+=a3[s2[i]][i];
  }
  return s;
}

void Update() {
  int t;
  int i;
  t=get_time();
  if(t>best) {
    best=t;
    for(i=0;i<n;i++) {
      bs1[i]=s1[i];
      bs2[i]=s2[i];
    }
  }
}

void pereb() {
  int step;
  double f1,f2;
  double aa;
  double de;
  int fo,fn;
  int i;
  step=0;
  for(i=0;i<n;i++) {
    s1[i]=i;
    s2[i]=i;
    os1[i]=i;
    os2[i]=i;
  }
  fn=get_time();
  fo=fn;
  T=T0;
  while(step<MX_STEP) {
    T=T0*exp(-cc*step/2.0);
    GenPermut(s1,os1);
    GenPermut(s2,os2);
    fn=get_time();
    Update();
    f1=xrandom();
    de=fn-fo;
    f2=exp(-de/T);
    if(f1<f2) {
      for(i=0;i<n;i++) {
        os1[i]=s1[i];
        os2[i]=s2[i];
      }
      fo=fn;
    }
    step++;
  }
}

int main() {
  int i,j;
  fi=fopen("triples.in","rt");
  fo=fopen("triples.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++) {
    for(j=0;j<n;j++) {
       fscanf(fi,"%d",&a1[i][j]);
    }
  }
  for(i=0;i<n;i++) {
    for(j=0;j<n;j++) {
       fscanf(fi,"%d",&a2[i][j]);
    }
  }
  for(i=0;i<n;i++) {
    for(j=0;j<n;j++) {
       fscanf(fi,"%d",&a3[i][j]);
    }
  }
  srandom(220686268);
  T0=10000.0;
  cc=0.79;
  MX_STEP=7500000/(n*n);
  pereb();
  cc=3.32;
  T0=1000;
  pereb();
  cc=0.12;
  T0=1000000;
  pereb();
  cc=13.17;
  T0=10000;
  pereb();
//  fprintf(fo,"%d\n",best);
  for(i=0;i<n;i++) {
    fprintf(fo,"%d %d %d\n",i+1,bs1[i]+1,bs2[i]+1);
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
