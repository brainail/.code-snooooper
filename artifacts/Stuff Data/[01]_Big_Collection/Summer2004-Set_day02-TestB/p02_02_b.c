#include <stdio.h>

#define mm 120

typedef struct
{
  int b, d, g, c;
}
type;

int aa[mm][mm], bb[mm][mm], cc[mm][mm], n, boy[mm], girl[mm], dog[mm];
type t[mm * mm * mm], tr[mm];

int cost( int i, int j, int k )
{
  return aa[i][j] + bb[j][k] + cc[k][i];
}

void swp( int i, int j )
{
  type tt;

  tt = t[i];
  t[i] = t[j];
  t[j] = tt;
}

void down( int v, int s )
{
  while ((v * 2 <= s && t[v].c > t[v * 2].c)
         || (v * 2 + 1 <= s && t[v].c > t[v * 2 + 1].c))
    if (v * 2 + 1 <= s && t[v * 2 + 1].c < t[v * 2].c)
    {
      swp(v, v * 2 + 1);
      v = v * 2 + 1;
    }
    else
    {
      swp(v, v * 2);
      v *= 2;
    }
}

int main( void )
{
  int i, j, k, b1, b2, g1, g2, d1, d2, l = 0, ii, jj, kk, ll;
  long long s = 0, ss, old;

  freopen("triples.in", "r", stdin);
  freopen("triples.out", "w", stdout);
  scanf("%d", &n);
  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      scanf("%d", &aa[i][j]);
  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      scanf("%d", &bb[i][j]);
  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      scanf("%d", &cc[i][j]);
  for (i = 1; i <= n; i++)
    for (j = 1; j <= n; j++)
      for (k = 1; k <= n; k++)
      {
        t[++l].b = i;
        t[l].d = j;
        t[l].g = k;
        t[l].c = aa[i][j] + bb[j][k] + cc[k][i];
      }
  for (i = l; i > 0; i--)
    down(i, l);
  for (i = l; i > 1; )
  {
    swp(1, i);
    i--;
    down(1, i);
  }
  for (i = j = 1; i <= n; i++)
  {
    for (; j <= l; j++)
      if (!boy[t[j].b] && !girl[t[j].g] && !dog[t[j].d])
        break;
    boy[t[j].b] = girl[t[j].g] = dog[t[j].d] = i;
    s += t[j].c;
    tr[i] = t[j];
  }
  for (ll = 1; ll <= n; ll++)
  {
    old = s;
    for (i = 1; i <= n; i++)
      for (j = 1; j <= n; j++)
        if (i != j)
        {
          b1 = tr[i].b, g1 = tr[i].g, d1 = tr[i].d;
          b2 = tr[j].b, g2 = tr[j].g, d2 = tr[j].d;
          ss = s - cost(b1, d1, g1) - cost(b2, d2, g2)
               + cost(b1, d1, g2) + cost(b2, d2, g1);
          if (ss > s)
          {
            s = ss;
            girl[g1] = j;
            girl[g2] = i;
            tr[i].g = g2, tr[j].g = g1;
            tr[i].c = cost(b1, d1, g2);
            tr[j].c = cost(b2, d2, g1);
          }
        }
    for (i = 1; i <= n; i++)
      for (j = 1; j <= n; j++)
        if (i != j)
        {
          b1 = tr[i].b, g1 = tr[i].g, d1 = tr[i].d;
          b2 = tr[j].b, g2 = tr[j].g, d2 = tr[j].d;
          ss = s - cost(b1, d1, g1) - cost(b2, d2, g2)
               + cost(b1, d2, g1) + cost(b2, d1, g2);
          if (ss > s)
          {
            s = ss;
            dog[d1] = j;
            dog[d2] = i;
            tr[i].d = d2, tr[j].d = d1;
            tr[i].c = cost(b1, d2, g1);
            tr[j].c = cost(b2, d1, g2);
          }
        }/*
    for (i = 1; i <= n; i++)
      for (j = 1; j <= n; j++)
        if (i != j)
        {
          b1 = tr[i].b, g1 = tr[i].g, d1 = tr[i].d;
          b2 = tr[j].b, g2 = tr[j].g, d2 = tr[j].d;
          ss = s - cost(b1, d1, g1) - cost(b2, d2, g2)
               + cost(b2, d1, g1) + cost(b1, d2, g2);
          if (ss > s)
          {
            boy[b1] = j;
            boy[b2] = i;
            tr[i].b = b2, tr[j].b = b1;
            tr[i].c = cost(b2, d1, g1);
            tr[j].c = cost(b1, d2, g2);
          }
        }  */
    for (i = 1; i <= n; i++)
      for (j = 1; j <= n; j++)
        for (k = 1; k <= n; k++)
          if (boy[i] != dog[j] && boy[i] != girl[k] && dog[j] != girl[k])
          {
            ii = boy[i], jj = dog[j], kk = girl[k];
            g1 = tr[boy[i]].g;
            b1 = tr[dog[j]].b;
            d1 = tr[girl[k]].d;
            d2 = tr[boy[i]].d;
            g2 = tr[dog[j]].g;
            b2 = tr[girl[k]].b;
            ss = s - tr[ii].c - tr[jj].c - tr[kk].c + cost(b1, d1, g1) +
                 + cost(b2, d2, g2) + cost(i, j, k);
            if (ss > s)
            {
              s = ss;
              tr[ii].c = cost(b1, d1, g1);
              tr[ii].b = b1, tr[ii].g = g1, tr[ii].d = d1;
              tr[jj].c = cost(b2, d2, g2);
              tr[jj].b = b2, tr[jj].g = g2, tr[jj].d = d2;
              tr[kk].c = cost(i, j, k);
              tr[kk].b = i, tr[kk].g = k, tr[kk].d = j;
              boy[b1] = girl[g1] = dog[d1] = ii;
              boy[b2] = girl[g2] = dog[d2] = jj;
              boy[i] = girl[k] = dog[j] = kk;
            }
          }
     if (old == s)
       break;
  }
  //printf("%Ld\n", s);
  for (i = 1; i <= n; i++)
    printf("%d %d %d\n", tr[i].b, tr[i].d, tr[i].g);
  return 0;
}

