#define ONLINE
#define DEBUG
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

long double time_start,time_cur;
const long double time_bound=4.5L;
const long double time_coeff=(long double)CLOCKS_PER_SEC;
const long double rand_coeff=((long double)RAND_MAX)+1.L;
const long double rand_divis=1.5L;

int randnum(int bound) {
  if (bound<=1) return(0);
  int num=0,rnd=rand();
  while (rnd>0) { num=(num+rnd)%bound; rnd/=bound; }
  return(num);
};
inline long double randrange(void) {
  return( ((long double)rand()) / rand_coeff );
}

inline long double temperand(void);

int* inv2perm(int*perm,int*c_less,const int*inv,int n_elems) {
  int i_elem,num;
  for (num=0; num<n_elems; num++) c_less[num]=num;
  for (i_elem=0; i_elem<n_elems; i_elem++) {
    for (num=0; num<n_elems; num++) if (c_less[num]>=inv[i_elem]) break;
    #ifdef DEBUG
    if (num>=n_elems) fprintf(stderr,
      "Error: Converting inv to perm: num not found.\n");
    if (c_less[num]>inv[i_elem]) fprintf(stderr,
      "Error: Converting inv to perm: num is not exact.\n");
    #endif
    perm[i_elem]=num;
    c_less[num]=-1;
    for (num++; num<n_elems; num++) if (c_less[num]>=0) c_less[num]--;
  }
  return(perm);
}
int* geninv(int*invn,const int*inv,int n_elems) {
  int i_elem;
  int n_iters;
  int newval;
  for (i_elem=0; i_elem<n_elems; i_elem++) {
    n_iters=0;
    for (newval=-1; (newval<0)||(newval>=n_elems-i_elem); ) {
      newval=inv[i_elem]+(int)(
        temperand()*((long double)(n_elems-i_elem)/rand_divis)
      );
      if (++n_iters>=10) break;
    }
    if (newval<0) newval=0;
    if (newval>=n_elems-i_elem) newval=n_elems-i_elem-1;
    invn[i_elem]=newval;
  }
  return(invn);
}

const long double temper_start=1e7L,temper_coeff=1.2L;
const long double prob_base=2.L,prob_coeff=1.L;
long double temper,temper_step;

inline long double temperand(void) {
  return(temper*(pow(1.L/temper+1.L,randrange())-1.L));
}

int n_elems,n_cells;
long*mtrx_bd_cont,*mtrx_dg_cont,*mtrx_gb_cont,**mtrx_bd,**mtrx_dg,**mtrx_gb;
int*perm_b,*perm_d,*perm_g,*inv_b,*inv_d,*inv_g,*invn_b,*invn_d,*invn_g,*buf;

long calcweight(void) {
  int i_elem,b,d,g;
  long weight=0;
  for (i_elem=0; i_elem<n_elems; i_elem++) {
    b=perm_b[i_elem]; d=perm_d[i_elem]; g=perm_g[i_elem];
    weight+=mtrx_bd[b][d]+mtrx_dg[d][g]+mtrx_gb[g][b];
  }
  return(weight);
}

int main(void) {
  time_start=(long double)clock();
  srand(13);
  #ifdef ONLINE
  freopen("triples.in","rt",stdin);
  freopen("triples.out","wt",stdout);
  #endif
  int i_elem,i_cell;
  scanf("%d",&n_elems);
  n_cells=n_elems*n_elems;
  mtrx_bd_cont=new long[n_cells]; mtrx_bd=new long*[n_elems];
  mtrx_dg_cont=new long[n_cells]; mtrx_dg=new long*[n_elems];
  mtrx_gb_cont=new long[n_cells]; mtrx_gb=new long*[n_elems];
  for (i_elem=0,i_cell=0; i_elem<n_elems; i_elem++,i_cell+=n_elems) {
    mtrx_bd[i_elem]=&mtrx_bd_cont[i_cell];
    mtrx_dg[i_elem]=&mtrx_dg_cont[i_cell];
    mtrx_gb[i_elem]=&mtrx_gb_cont[i_cell];
  }
  perm_b=new int[n_elems]; perm_d=new int[n_elems]; perm_g=new int[n_elems];
  inv_b=new int[n_elems]; inv_d=new int[n_elems]; inv_g=new int[n_elems];
  invn_b=new int[n_elems]; invn_d=new int[n_elems]; invn_g=new int[n_elems];
  int b,d,g;
  for (b=0; b<n_elems; b++) for (d=0; d<n_elems; d++)
    scanf("%ld",&mtrx_bd[b][d]);
  for (d=0; d<n_elems; d++) for (g=0; g<n_elems; g++)
    scanf("%ld",&mtrx_dg[d][g]);
  for (g=0; g<n_elems; g++) for (b=0; b<n_elems; b++)
    scanf("%ld",&mtrx_gb[g][b]);
  buf=new int[n_elems];
  long wght_prev,wght_cur;
  for (b=0; b<n_elems; b++) inv_b[b]=randnum(n_elems-b);
  for (d=0; d<n_elems; d++) inv_d[d]=randnum(n_elems-d);
  for (g=0; g<n_elems; g++) inv_g[g]=randnum(n_elems-g);
  inv2perm(perm_b,buf,inv_b,n_elems);
  inv2perm(perm_d,buf,inv_d,n_elems);
  inv2perm(perm_g,buf,inv_g,n_elems);
  wght_cur=calcweight();
  temper=temper_start;
  temper_step=pow(temper_coeff,sqrt((long double)n_elems));
  do {
    wght_prev=wght_cur;
    geninv(invn_b,inv_b,n_elems);
    geninv(invn_d,inv_d,n_elems);
    geninv(invn_g,inv_g,n_elems);
    inv2perm(perm_b,buf,invn_b,n_elems);
    inv2perm(perm_d,buf,invn_d,n_elems);
    inv2perm(perm_g,buf,invn_g,n_elems);
    wght_cur=calcweight();
    if ( randrange()*prob_coeff
       < pow(prob_base,(long double)(wght_cur-wght_prev)/temper) ) {
      memcpy(inv_b,invn_b,sizeof(int)*n_elems);
      memcpy(inv_d,invn_d,sizeof(int)*n_elems);
      memcpy(inv_g,invn_g,sizeof(int)*n_elems);
      temper/=temper_step;
    } else wght_cur=wght_prev;
    time_cur=(long double)clock();
  } while (time_cur-time_start<time_bound*time_coeff);
  delete[]buf;
  for (i_elem=0; i_elem<n_elems; i_elem++)
    printf("%d %d %d\n",perm_b[i_elem]+1,perm_d[i_elem]+1,perm_g[i_elem]+1);
  delete[]inv_b; delete[]inv_d; delete[]inv_g;
  delete[]perm_b; delete[]perm_d; delete[]perm_g;
  delete[]mtrx_bd_cont; delete[]mtrx_bd;
  delete[]mtrx_dg_cont; delete[]mtrx_dg;
  delete[]mtrx_gb_cont; delete[]mtrx_gb;
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}



