/* 02.07.04 SK1 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))

#define MAX_N 100

long N;
long BD[MAX_N][MAX_N];
long DG[MAX_N][MAX_N];
long GB[MAX_N][MAX_N];
long Tr[MAX_N][3];

/* The main program function */
int main( void )
{
  long i, j, k, l, tmp, gi, gj, bi, bj, di, dj, maxk;
  long Sum = 0;

  freopen("triples.in", "r", stdin);
  freopen("triples.out", "w", stdout);
  /* Read input data */
  scanf("%li", &N);
  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      scanf("%li", &BD[i][j]);
  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      scanf("%li", &DG[i][j]);
  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      scanf("%li", &GB[i][j]);
  /* Solve */
  for (i = 0; i < N; i++)
    for (j = 0; j < 3; j++)
      Tr[i][j] = i;
  if (N > 50)
    maxk = N * N / 4;
  else
    maxk = N * N;
  for (k = 0; k < maxk; k++)
  {
    for (i = 0; i < N; i++)
      for (j = i + 1; j < N; j++)
      {
        bi = Tr[i][0], di = Tr[i][1], gi = Tr[i][2];
        bj = Tr[j][0], dj = Tr[j][1], gj = Tr[j][2];
        if (GB[gi][bi] + DG[di][gi] + GB[gj][bj] + DG[dj][gj] <
            GB[gj][bi] + DG[di][gj] + GB[gi][bj] + DG[dj][gi])
          SWAP(Tr[i][2], Tr[j][2], tmp);
      }
    for (i = 0; i < N; i++)
      for (j = i + 1; j < N; j++)
      {
        bi = Tr[i][0], di = Tr[i][1], gi = Tr[i][2];
        bj = Tr[j][0], dj = Tr[j][1], gj = Tr[j][2];
        if (BD[bi][di] + DG[di][gi] + BD[bj][dj] + DG[dj][gj] <
            BD[bi][dj] + DG[dj][gi] + BD[bj][di] + DG[di][gj])
          SWAP(Tr[i][1], Tr[j][1], tmp);
      }
    for (i = 0; i < N; i++)
      for (j = i + 1; j < N; j++)
      {
        bi = Tr[i][0], di = Tr[i][1], gi = Tr[i][2];
        bj = Tr[j][0], dj = Tr[j][1], gj = Tr[j][2];
        if (BD[bi][di] + GB[gi][bi] + BD[bj][dj] + GB[gj][bj] <
            BD[bj][di] + GB[gi][bj] + BD[bi][dj] + GB[gj][bi])
          SWAP(Tr[i][0], Tr[j][0], tmp);
      }
  }
  /* Output result */
/*
  Sum = 0;
  for (l = 0; l < N; l++)
  {
    bi = Tr[l][0], di = Tr[l][1], gi = Tr[l][2];
    Sum += BD[bi][di] + DG[di][gi] + GB[gi][bi];
  }
  printf("%li\n", Sum);
*/
  for (i = 0; i < N; i++)
    printf("%li %li %li\n", Tr[i][0] + 1, Tr[i][1] + 1, Tr[i][2] + 1);
  return 0;
} /* End of 'main' function */

