#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 51
#define VV (NN*NN*8+10)
#define MM 1400000

#define WHITE 0
#define GRAY 1
#define BLACK 2

struct KL {
  bool r,l,u,d;
};

struct REB {
  int a;
  REB *p;
};

FILE *fi, *fo;
KL a[2][NN];
bool km[2][NN];
bool qm[2][NN];
REB *g[VV];
REB *g1[VV];
int r[VV];
int tp[VV];
int mark[VV];
REB swap[MM];
int iswap=0;
int n,m;
int start_position;
int q[VV];

REB *getnew() {
/*  if(iswap>=MM) {
    swap=new REB [MM];
    iswap=0;
    fprintf(fo,"new\n");
  }*/
  return &swap[iswap++];
}

int PackV(int xk, int yk, int xq, int yq, int mv) {
  return (((xk*n+yk)*2+xq)*n+yq)*2+mv;
}

void Add(int x, int y) {
  REB *t;
  t=getnew();
  t->a=y;
  t->p=g[x];
  g[x]=t;
  t=getnew();
  t->a=x;
  t->p=g1[y];
  g1[y]=t;
}

void MarkToL(int x, int y) { //for queeen
  while(true) {
    if(x<0) return ;
    if(y<0) return ;
    if(x>=2) return ;
    if(y>=n) return ;
    qm[x][y]=true;
    if(a[x][y].l)
      return ;
    y--;
  }
}

void MarkToR(int x, int y) { //for queeen
  while(true) {
    if(x<0) return ;
    if(y<0) return ;
    if(x>=2) return ;
    if(y>=n) return ;
    qm[x][y]=true;
    if(a[x][y].r)
      return ;
    y++;
  }
}

void MarkToU(int x, int y) { //for queeen
  while(true) {
    if(x<0) return ;
    if(y<0) return ;
    if(x>=2) return ;
    if(y>=n) return ;
    qm[x][y]=true;
    if(a[x][y].u)
      return ;
    x--;
  }
}

void MarkToD(int x, int y) { //for queeen
  while(true) {
    if(x<0) return ;
    if(y<0) return ;
    if(x>=2) return ;
    if(y>=n) return ;
    qm[x][y]=true;
    if(a[x][y].d)
      return ;
    x++;
  }
}

bool PosDiagMove(int x1, int y1, int x2, int y2) {
  if(x1<0) return false;
  if(x1>=2) return false;
  if(y1<0) return false;
  if(y1>=n) return false;
  if(x2<0) return false;
  if(x2>=2) return false;
  if(y2<0) return false;
  if(y2>=n) return false;
  if(x2<x1) {
    if(a[x1][y1].u)
      return false;
    if(a[x2][y2].d)
      return false;
  } else {
    if(a[x1][y1].d)
      return false;
    if(a[x2][y2].u)
      return false;
  }
  if(y2<y1) {
    if(a[x1][y1].l)
      return false;
    if(a[x2][y2].r)
      return false;
  } else {
    if(a[x1][y1].r)
      return false;
    if(a[x2][y2].l)
      return false;
  }
}

void MarkHit(int xk, int yk, int xq, int yq) {
  int i,j;
  int x,y;
  for(i=0;i<n;i++) {
    for(j=0;j<2;j++) {
      qm[j][i]=false;
      km[j][i]=false;
    }
  }
  MarkToL(xq,yq);
  MarkToR(xq,yq);
  MarkToU(xq,yq);
  MarkToD(xq,yq);
  x=xq+1;
  y=yq+1;
  if(PosDiagMove(xq,yq,x,y)) {
    qm[x][y]=true;
  }
  x=xq+1;
  y=yq-1;
  if(PosDiagMove(xq,yq,x,y)) {
    qm[x][y]=true;
  }
  x=xq-1;
  y=yq+1;
  if(PosDiagMove(xq,yq,x,y)) {
    qm[x][y]=true;
  }
  x=xq-1;
  y=yq-1;
  if(PosDiagMove(xq,yq,x,y)) {
    qm[x][y]=true;
  }
  x=xk-1;
  y=yk;
  if(x>=0) {
    if(!a[xk][yk].u) {
      km[x][y]=true;
    }
  }
  x=xk+1;
  y=yk;
  if(x<2) {
    if(!a[xk][yk].d) {
      km[x][y]=true;
    }
  }
  x=xk;
  y=yk-1;
  if(y>=0) {
    if(!a[xk][yk].l) {
      km[x][y]=true;
    }
  }
  x=xk;
  y=yk+1;
  if(y<n) {
    if(!a[xk][yk].r) {
      km[x][y]=true;
    }
  }
  x=xk+1;
  y=yk+1;
  if(PosDiagMove(xk,yk,x,y)) {
    km[x][y]=true;
  }
  x=xk+1;
  y=yk-1;
  if(PosDiagMove(xk,yk,x,y)) {
    km[x][y]=true;
  }
  x=xk-1;
  y=yk+1;
  if(PosDiagMove(xk,yk,x,y)) {
    km[x][y]=true;
  }
  x=xk-1;
  y=yk-1;
  if(PosDiagMove(xk,yk,x,y)) {
    km[x][y]=true;
  }
  qm[xq][yq]=false;
  km[xk][yk]=false;
  km[xq][yq]=false;
  qm[xk][yk]=false;
}

void read_data() {
  int i,j;
  int x1,y1,x2,y2;
  fscanf(fi,"%d%d",&n,&m);
  for(i=0;i<2;i++) {
    for(j=0;j<n;j++) {
      a[i][j].l=false;
      a[i][j].r=false;
      a[i][j].u=false;
      a[i][j].d=false;
    }
  }
  for(i=0;i<m;i++) {
    fscanf(fi,"%d%d%d%d",&x1,&y1,&x2,&y2);
    x1--; y1--; x2--; y2--;
    if(x1<x2) {
      a[x1][y1].d=true;
      a[x2][y2].u=true;
      continue;
    }
    if(x1>x2) {
      a[x1][y1].u=true;
      a[x2][y2].d=true;
      continue;
    }
    if(y1>y2) {
      a[x1][y1].l=true;
      a[x2][y2].r=true;
      continue;
    }
    if(y2>y1) {
      a[x1][y1].r=true;
      a[x2][y2].l=true;
      continue;
    }
  }
}

void init() {
  int i;
  for(i=0;i<VV;i++) {
    g[i]=NULL;
    mark[i]=WHITE;
    r[i]=10000000;
  }
  start_position=PackV(0,0,1,n-1,0);
}

void BuildG() {
  int x1,y1,x2,y2;
  int i,j;
  int v01,v02,v;
  for(x1=0;x1<2;x1++) {
    for(y1=0;y1<n;y1++) {
      for(x2=0;x2<2;x2++) {
        for(y2=0;y2<n;y2++) {
          MarkHit(x1,y1,x2,y2);
          v01=PackV(x1,y1,x2,y2,0);
          v02=PackV(x1,y1,x2,y2,1);
          for(i=0;i<2;i++) {
            for(j=0;j<n;j++) {
              if(km[i][j]) {
                if(!qm[i][j]) {
                  v=PackV(i,j,x2,y2,1);
                  Add(v01,v);
                }
              }
            }
          }
          for(i=0;i<2;i++) {
            for(j=0;j<n;j++) {
              if(qm[i][j]) {
                if(!km[i][j]) {
                  v=PackV(x1,y1,i,j,0);
                  Add(v02,v);
                }
              }
            }
          }
        }
      }
    }
  }
}

int inv(int c) {
  return (c==1)?-1:1;
}

bool need_paint(int v) {
  REB *t;
  int ttp;
  ttp=-1;
  for(t=g[v];t!=NULL;t=t->p) {
    if(mark[t->a]==GRAY) {
      if(ttp<0) {
        ttp=0;
      }
    } else {
      if(tp[t->a]<0)
        ttp=1;
      if(tp[t->a]==0) {
        if(ttp<0)
          ttp=0;
      }
    }
  }
  if(ttp!=0)
    return true;
  return false;
}

void paint(int v, int c) {
  REB *t;
  for(t=g1[v];t!=NULL;t=t->p) {
    if(t->a==0) {
      if(need_paint(t->a))
        paint(t->a,inv(c));
    }
  }
  tp[v]=c;
  if(tp[v]>0) {
    for(t=g[v];t!=NULL;t=t->p) {
      if(tp[t->a]<0) {
        if(r[v]>r[t->a]+1) {
          r[v]=r[t->a]+1;
        }
      }
    }
  } else {
    r[v]=0;
    for(t=g[v];t!=NULL;t=t->p) {
      if(tp[t->a]>0) {
        if(r[v]<r[t->a]+1) {
          r[v]=r[t->a]+1;
        }
      }
    }
  }
}

void dfs(int v) {
  REB *t;
  int ttp;
  ttp=-1;
  mark[v]=GRAY;
  if(v==107) {
    v=107;
  }
  for(t=g[v];t!=NULL;t=t->p) {
    if(mark[t->a]==WHITE) {
      dfs(t->a);
    }
    if(mark[t->a]==GRAY) {
      if(ttp<0) {
        ttp=0;
      }
    } else {
      if(tp[t->a]<0)
        ttp=1;
      if(tp[t->a]==0) {
        if(ttp<0)
          ttp=0;
      }
    }
  }
  if(g[v]==NULL)
    r[v]=0;
  tp[v]=ttp;
  if(tp[v]!=0) {
    paint(v,tp[v]);
  }
  if(ttp>0) {
    for(t=g[v];t!=NULL;t=t->p) {
      if(tp[t->a]<0) {
        if(r[v]>r[t->a]+1) {
          r[v]=r[t->a]+1;
        }
      }
    }
  } else {
    r[v]=0;
    for(t=g[v];t!=NULL;t=t->p) {
      if(tp[t->a]>0) {
        if(r[v]<r[t->a]+1) {
          r[v]=r[t->a]+1;
        }
      }
    }
  }
  mark[v]=BLACK;
}

int bfs(int st, int ttp) {
  int b,e;
  int i;
  REB *t;
  for(i=0;i<VV;i++) {
    mark[i]=0;
    r[i]=100000;
  }
  mark[st]=1;
  r[st]=0;
  b=0; e=1;
  q[b]=st;
  while(b<e) {
    if(q[b]%2==ttp) {
      if(g[q[b]]==NULL)
        return r[q[b]];
    }
    for(t=g[b];t!=NULL;t=t->p) {
      if(!mark[t->a]) {
        mark[t->a]=1;
        q[e++]=t->a;
        r[t->a]=r[b]+1;
      }
    }
    b++;
  }
}

int main() {
  int f1,f2,f3;
  fi=fopen("queen.in","rt");
  fo=fopen("queen.out","wt");

  read_data();
  init();
  BuildG();
  f1=PackV(1,1,1,3,1);
  f2=PackV(1,1,1,4,0);
  f3=PackV(1,0,1,n-1,1);
//  dfs(f3);
  dfs(start_position);
  switch(tp[start_position]) {
    case -1:
//      fprintf(fo,"queen wins\n%d\n",bfs(start_position,0));
      fprintf(fo,"queen wins\n%d\n",r[start_position]);
      break;
    case 1:
      fprintf(fo,"king wins\n%d\n",r[start_position]);
      break;
    case 0:
      fprintf(fo,"draw\n");
      break;
  }

  fclose(fi);
  fclose(fo);
  return 0;
}

