#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  int x, y, xx, yy, t, w;
}
pos;

pos q[30000];
int w[4][60][4][60], n, m, s[4][60][4][60][2], mark[4][60][4][60][2],
    dx[8] = {1, 1, 1, -1, -1, -1, 0, 0}, dy[8] = {-1, 0, 1, -1, 0, 1, 1, -1},
    l, d[30000];

int king( int x, int y, int xx, int yy )
{
  if (x == xx && yy == y)
    return 0;
  return (abs(x - xx) < 2 && abs(y - yy) < 2 && !w[x][y][xx][yy]);
}

int queen( int x, int y, int xx, int yy )
{
  if (x == xx && yy == y)
    return 0;
  return (((abs(x - xx) < 2 && abs(y - yy) < 2)
          || (x == xx)) && !w[x][y][xx][yy]);
}

int main( void )
{
  int i, x, y, xx, yy, p;

  freopen("queen.in", "r", stdin);
  freopen("queen.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (i = 1; i <= m; i++)
  {
    scanf("%d%d%d%d", &x, &y, &xx, &yy);
    w[x][y][xx][yy] = w[xx][yy][x][y] = 1;
  }
  for (x = 1; x <= 2; x++)
    for (y = 1; y <= n; y++)
    {
      if (w[x][y][x][y + 1])
        w[x][y][x + 1][y + 1] = w[x][y][x - 1][y + 1] =
        w[x - 1][y][x][y + 1] = w[x + 1][y][x][y + 1] = 1;
      if (w[x - 1][y][x][y])
        w[x - 1][y][x][y + 1] = w[x - 1][y][x][y - 1] =
        w[x][y][x - 1][y - 1] = w[x][y][x - 1][y + 1] = 1;
    }
  for (x = 0; x <= 3; x++)
    for (y = 0; y <= n + 1; y++)
      for (xx = 0; xx <= 3; xx++)
        for (yy = 0; yy <= n + 1; yy++)
          if (x == 0 || xx == 0 || yy == 0 || y == 0
              || x == 3 || xx == 3 || y == n + 1 || yy == n + 1)
            w[x][y][xx][yy] = 1;
  for (x = 1; x <= 2; x++)
    for (y = 1; y <= n; y++)
      for (xx = 1; xx <= 2; xx++)
        for (yy = 1; yy <= n; yy++)
          if (w[x][y][xx][yy])
            w[xx][yy][x][y] = 1;
  for (x = 1; x <= 2; x++)
    for (y = 1; y <= n; y++)
      for (yy = y + 1; yy <= n; yy++)
        if (w[x][y][x][yy - 1] || w[x][yy - 1][x][yy])
          w[x][y][x][yy] = w[x][yy][x][y] = 1;
  for (x = 1; x <= 2; x++)
    for (y = 1; y <= n; y++)
      for (xx = 1; xx <= 2; xx++)
        for (yy = 1; yy <= n; yy++)
        {
          if (x == xx && y == yy)
            continue;
          for (i = 0; i < 8; i++)
            if (king(x, y, x + dx[i], y + dy[i])
                && !queen(xx, yy, x + dx[i], y + dy[i]))
              s[x][y][xx][yy][0]++;
          for (i = 0; i < 6; i++)
            if (queen(xx, yy, xx + dx[i], yy + dy[i])
                && !king(x, y, xx + dx[i], yy + dy[i]))
              s[x][y][xx][yy][1]++;
          for (i = 1; i <= n; i++)
            if (queen(xx, yy, xx, i) && !king(x, y, xx, i))
              s[x][y][xx][yy][1]++;
          if (s[x][y][xx][yy][0] == 0)
          {
            q[++l].x = x, q[l].y = y;
            q[l].xx = xx, q[l].yy = yy;
            q[l].t = q[l].w = 0;
            mark[q[l].x][q[l].y][q[l].xx][q[l].yy][q[l].t] = 1;
            if (x == 1 && y == 1 && xx == 2 && yy == n)
            {
              printf("queen wins\n0\n");
              return 0;
            }
          }
          if (s[x][y][xx][yy][1] == 0)
          {
            q[++l].x = x, q[l].y = y;
            q[l].xx = xx, q[l].yy = yy;
            q[l].t = q[l].w = 1;
            mark[q[l].x][q[l].y][q[l].xx][q[l].yy][q[l].t] = 1;
          }
        }
  for (p = 1; p <= l; p++)
  {
    if (q[p].t)
    {
      for (x = 1; x <= 2; x++)
        for (y = 1; y <= n; y++)
          if (king(x, y, q[p].x, q[p].y)
             && !queen(q[p].xx, q[p].yy, q[p].x, q[p].y))
          {
            if (mark[x][y][q[p].xx][q[p].yy][0])
              continue;
            if (q[p].w)
            {
              s[x][y][q[p].xx][q[p].yy][0]--;
              mark[x][y][q[p].xx][q[p].yy][0] = 1;
              q[++l] = q[p];
              q[l].w = 1;
              q[l].x = x, q[l].y = y;
              q[l].t = 0;
              d[l] = d[p] + 1;
              if (x == 1 && y == 1 && q[l].xx == 2 && q[l].yy == n
                 && q[l].t == 0)
              {
                x = p = 100000;
                break;
              }
            }
            else
            {
              s[x][y][q[p].xx][q[p].yy][0]--;
              if (s[x][y][q[p].xx][q[p].yy][0] == 0
                 && !mark[x][y][q[p].xx][q[p].yy][0])
              {
                mark[x][y][q[p].xx][q[p].yy][0] = 1;
                q[++l] = q[p];
                q[l].w = 0;
                q[l].x = x, q[l].y = y;
                q[l].t = 0;
                d[l] = d[p] + 1;
                if (x == 1 && y == 1 && q[l].xx == 2 && q[l].yy == n
                    && q[l].t == 0)
                {
                  x = p = 100000;
                  break;
                }
              }
            }
          }
    }
    else
      for (x = 1; x <= 2; x++)
        for (y = 1; y <= n; y++)
          if (queen(x, y, q[p].xx, q[p].yy)
             && !king(q[p].x, q[p].y, x, y))
          {
            if (mark[q[p].x][q[p].y][x][y][1])
              continue;
            if (!q[p].w)
            {
              s[q[p].x][q[p].y][x][y][1]--;
              mark[q[p].x][q[p].y][x][y][1] = 1;
              q[++l] = q[p];
              q[l].w = 0;
              q[l].xx = x, q[l].yy = y;
              q[l].t = 1;
              d[l] = d[p] + 1;
              if (x == 2 && y == n && q[l].x == 1 && q[l].y == 1
                  && q[l].t == 0)
              {
                x = p = 100000;
                break;
              }
            }
            else
            {
              s[q[p].x][q[p].y][x][y][1]--;
              if (s[q[p].x][q[p].y][x][y][1] == 0)
              {
                mark[q[p].x][q[p].y][x][y][1] = 1;
                q[++l] = q[p];
                q[l].w = 1;
                q[l].xx = x, q[l].yy = y;
                q[l].t = 1;
                d[l] = d[p] + 1;
                if (x == 2 && y == n && q[l].x == 1 && q[l].y == 1
                    && q[l].t == 0)
                {
                  x = p = 100000;
                  break;
                }
              }
            }
          }
  }
  if (q[l].x == 1 && q[l].y == 1 && q[l].xx == 2 && q[l].yy == n
      && q[l].t == 0)
  {
    if (q[l].w)
      printf("king wins\n");
    else
      printf("queen wins\n");
    printf("%d\n", d[l]);
  }
  else
    printf("draw\n");
  return 0;
}

