#define ONLINE
//#define DEBUG
#include <stdio.h>
#include <string.h>

#define INF 0x7FFFFF00

#define N_DIRS 8

struct TPoint {
  int x,y;
  int valid(int n_x,int n_y) const {
    return((x>=0)&&(y>=0)&&(x<n_x)&&(y<n_y));
  }
  int index(int width) const {
    return(x*width+y);
  }
  TPoint& set(int width,int idx) {
    x=idx/width; y=idx%width; return(*this);
  }
  int operator==(const TPoint&) const;
  TPoint& operator+=(const TPoint&);
};

const TPoint dirs[N_DIRS]=
  {{-1,0},{-1,+1},{0,+1},{+1,+1},{+1,0},{+1,-1},{0,-1},{-1,-1}};

struct TState {
  char kind;
  int steps;
  int n_mvs_total,c_mvs_bad;
};
struct TStateIndex {
  int whos,pos_0,pos_1;
};

class TGraph {
  private:
    int n_x,n_y,n_cells,n_states;
    char*mtrx_cont_0,*mtrx_cont_1,**mtrx_0,**mtrx_1;
    TState*states_cont[2],**states[2];
    TStateIndex*que; int que_len,que_pos;
  protected:
    void initmatrix(void);
    void wallmatrix(const TPoint&,const TPoint&);
    void compmatrix(void);
    int countmoves(const TStateIndex&) const;
    int cancometo(const TStateIndex&) const;
    void initgame(void);
    void playgame(void);
  public:
    TGraph(void);
    ~TGraph();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printres(void);
    void play(void);
};

TGraph board;

int main(void) {
  #ifdef ONLINE
  freopen("queen.in","rt",stdin);
  freopen("queen.out","wt",stdout);
  #endif
  board.scan();
  board.play();
  board.printres();
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


int TGraph::countmoves(const TStateIndex&i) const {
  const char*const*me=(i.whos?mtrx_1:mtrx_0);
  const char*const*op=(i.whos?mtrx_0:mtrx_1);
  const int&pos_me=(i.whos?i.pos_1:i.pos_0),&pos_op=(i.whos?i.pos_0:i.pos_1);
  int pos_to;
  int n_moves=0;
  for (pos_to=0; pos_to<n_cells; pos_to++) {
    if (!me[pos_me][pos_to]) continue;
    if ((op[pos_op][pos_to])||(pos_op==pos_to)) continue;
    n_moves++;
  }
  return(n_moves);
}
int TGraph::cancometo(const TStateIndex&i) const {
  const char*const*me=(i.whos?mtrx_1:mtrx_0);
  const int&pos_me=(i.whos?i.pos_1:i.pos_0),&pos_op=(i.whos?i.pos_0:i.pos_1);
  return( (!me[pos_me][pos_op]) && (pos_me!=pos_op) );
}

void TGraph::initgame(void) {
  TStateIndex i;
  memset(states_cont[0],-1,sizeof(TState)*n_cells*n_cells);
  memset(states_cont[1],-1,sizeof(TState)*n_cells*n_cells);
  que_len=0;
  for (i.whos=0; i.whos<2; i.whos++)
   for (i.pos_0=0; i.pos_0<n_cells; i.pos_0++)
   for (i.pos_1=0; i.pos_1<n_cells; i.pos_1++) {
    TState&state=states[i.whos][i.pos_0][i.pos_1];
    state.n_mvs_total=countmoves(i); state.c_mvs_bad=0;
    if (state.n_mvs_total<=0) {
      state.kind=0; state.steps=0;
      if (cancometo(i)) que[que_len++]=i;
    } else state.steps=INF;
  }
}
void TGraph::playgame(void) {
  TStateIndex cur,from;
  for (que_pos=0; que_pos<que_len; que_pos++) {
    from=cur=que[que_pos]; from.whos=(cur.whos?0:1);
    const char*const*me=(from.whos?mtrx_1:mtrx_0);
    int&from_pos=(from.whos?from.pos_1:from.pos_0);
    int from_pos_to=from_pos;
    TState&st_cur=states[cur.whos][cur.pos_0][cur.pos_1];
    for (from_pos=0; from_pos<n_cells; from_pos++) if (cancometo(from)) {
      if (!me[from_pos][from_pos_to]) continue;
      TState&st_from=states[from.whos][from.pos_0][from.pos_1];
      if (st_cur.kind) {
        if (st_from.n_mvs_total<=++st_from.c_mvs_bad) {
          if (st_from.kind<0) { st_from.kind=0; que[que_len++]=from; }
          #ifdef DEBUG
          else if (!st_from.kind) fprintf(stderr,
            "Bugs: state has become loose more than once: %d %d %d -> %d %d %d\n",
            from.whos,from.pos_0,from.pos_1,cur.whos,cur.pos_0,cur.pos_1);
          else fprintf(stderr,
            "Bugs: state has become loose after win: %d %d %d -> %d %d %d\n",
            from.whos,from.pos_0,from.pos_1,cur.whos,cur.pos_0,cur.pos_1);
          #endif
          if (st_from.steps>st_cur.steps+1) st_from.steps=st_cur.steps+1;
        }
      } else {
        if (st_from.kind<0) { st_from.kind=1; que[que_len++]=from; }
        #ifdef DEBUG
        else if (!st_from.kind) fprintf(stderr,
          "Bugs: state has become win after loose: %d %d %d -> %d %d %d\n",
          from.whos,from.pos_0,from.pos_1,cur.whos,cur.pos_0,cur.pos_1);
        #endif
        if (st_from.steps>st_cur.steps+1) st_from.steps=st_cur.steps+1;
      }
    }
  }
}

void TGraph::play(void) {
  initgame();
  playgame();
}

void TGraph::initmatrix(void) {
  int cur_i,to_i; TPoint cur_p,to_p; int i_dir;
  memset(mtrx_cont_0,0,sizeof(char)*n_cells*n_cells);
  for (cur_i=0; cur_i<n_cells; cur_i++) {
    cur_p.set(n_y,cur_i);
    for (i_dir=0; i_dir<N_DIRS; i_dir++) {
      to_p=cur_p; to_p+=dirs[i_dir];
      if (!to_p.valid(n_x,n_y)) continue;
      to_i=to_p.index(n_y);
      mtrx_0[cur_i][to_i]=1;
    }
  }
}
void TGraph::wallmatrix(const TPoint&from_p,const TPoint&to_p) {
  int from_i,cur_i; TPoint cur_p; int i_dir;
  if (!from_p.valid(n_x,n_y)) return;
  from_i=from_p.index(n_y);
  for (i_dir=0; i_dir<N_DIRS; i_dir++) {
    cur_p=from_p; cur_p+=dirs[i_dir];
    if (cur_p==to_p) {
      if (cur_p.valid(n_x,n_y)) {
        cur_i=cur_p.index(n_y);
        mtrx_0[cur_i][from_i]=mtrx_0[from_i][cur_i]=0;
      }
      cur_p=from_p; cur_p+=dirs[(i_dir+N_DIRS-1)%N_DIRS];
      if (cur_p.valid(n_x,n_y)) {
        cur_i=cur_p.index(n_y);
        mtrx_0[cur_i][from_i]=mtrx_0[from_i][cur_i]=0;
      }
      cur_p=from_p; cur_p+=dirs[(i_dir+N_DIRS+1)%N_DIRS];
      if (cur_p.valid(n_x,n_y)) {
        cur_i=cur_p.index(n_y);
        mtrx_0[cur_i][from_i]=mtrx_0[from_i][cur_i]=0;
      }
    }
  }
}
void TGraph::compmatrix(void) {
  int cur_i,prev_i,to_i; TPoint cur_p,to_p; int i_dir;
  memset(mtrx_cont_1,0,sizeof(char)*n_cells*n_cells);
  for (cur_i=0; cur_i<n_cells; cur_i++) {
    cur_p.set(n_y,cur_i);
    for (i_dir=0; i_dir<N_DIRS; i_dir++) {
      to_p=cur_p; to_p+=dirs[i_dir]; prev_i=cur_i;
      while (to_p.valid(n_x,n_y)) {
        to_i=to_p.index(n_y);
        if (!mtrx_0[prev_i][to_i]) break;
        mtrx_1[cur_i][to_i]=1;
        to_p+=dirs[i_dir]; prev_i=to_i;
      }
    }
  }
}

void TGraph::scan(void) {
  Delete();
  scanf("%d",&n_y); n_x=2;
  n_cells=n_x*n_y;
  n_states=(n_cells*n_cells)<<1;
  Create();
  int n_walls,i_wall; TPoint wall_a,wall_b;
  initmatrix();
  scanf("%d",&n_walls);
  for (i_wall=0; i_wall<n_walls; i_wall++) {
    scanf("%d%d",&wall_a.x,&wall_a.y); wall_a.x--; wall_a.y--;
    scanf("%d%d",&wall_b.x,&wall_b.y); wall_b.x--; wall_b.y--;
    wallmatrix(wall_a,wall_b);
    wallmatrix(wall_b,wall_a);
  }
  compmatrix();
}
void TGraph::printres(void) {
  const TState&state=states[0][0][n_cells-1];
  switch (state.kind) {
    case 0: printf("queen wins\n%d\n",state.steps); break;
    case 1: printf("king wins\n%d\n",state.steps); break;
    default: printf("draw\n");
  }
}

TGraph::TGraph(void) {
  n_x=0; n_y=0; n_cells=0; n_states=0;
  Create();
}
TGraph::~TGraph() {
  Delete();
}
void TGraph::Create(void) {
  int i_line,i_cell;
  mtrx_cont_0=new char[n_cells*n_cells]; mtrx_0=new char*[n_cells];
  mtrx_cont_1=new char[n_cells*n_cells]; mtrx_1=new char*[n_cells];
  states_cont[0]=new TState[n_cells*n_cells]; states[0]=new TState*[n_cells];
  states_cont[1]=new TState[n_cells*n_cells]; states[1]=new TState*[n_cells];
  for (i_line=0,i_cell=0; i_line<n_cells; i_line++,i_cell+=n_cells) {
    mtrx_0[i_line]=&mtrx_cont_0[i_cell];
    mtrx_1[i_line]=&mtrx_cont_1[i_cell];
    states[0][i_line]=&states_cont[0][i_cell];
    states[1][i_line]=&states_cont[1][i_cell];
  }
  que=new TStateIndex[n_states];
}
void TGraph::Delete(void) {
  delete[]mtrx_cont_0; delete[]mtrx_0;
  delete[]mtrx_cont_1; delete[]mtrx_1;
  delete[]states_cont[0]; delete[]states[0];
  delete[]states_cont[1]; delete[]states[1];
  delete[]que;
}


int TPoint::operator==(const TPoint&arg) const {
  return((x==arg.x)&&(y==arg.y));
}
TPoint& TPoint::operator+=(const TPoint&arg) {
  x+=arg.x; y+=arg.y;
  return(*this);
}


