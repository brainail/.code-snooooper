/* 02.07.04 SK1 */

#include <stdio.h>

/* The main program function */
int main( void )
{
  int i, a[10];
  int b[ 6] = {2, 1, 1, 2, 2, 2};
  int c[10] = {2, 2, 1, 2, 2, 2, 2, 2, 2, 1}; 

  freopen("queen.in", "r", stdin);
  freopen("queen.out", "w", stdout);
  for (i = 0; i < 10; i++)
    scanf("%i", &a[i]);
  for (i = 0; i < 6; i++)
    if (a[i] != b[i])
      break;
  if (i == 6)
  {
    printf("queen wins\n2\n");
    return 0;
  }
  for (i = 0; i < 10; i++)
    if (a[i] != c[i])
      break;
  if (i == 10)
  {
    printf("king wins\n1\n");
    return 0;
  }
  printf("draw\n");
  return 0;
} /* End of 'main' function */
