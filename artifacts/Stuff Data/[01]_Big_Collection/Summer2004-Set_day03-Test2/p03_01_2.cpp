#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_ROOMS  10
#define LL 128
#define MAX_NAMES 220
#define NN 220

class Container {
  public:
    Container() {
      count=0;
    }
    ~Container() {
    }
    void Add(char *s) {
      GetId(s);
    }
    int GetId(char *s) {
      int i;
      for(i=0;i<count;i++) {
        if(strcmp(names[i],s)==0)
          return i;
      }
      push(s);
      return count-1;
    }
    const char *GetName(int cid) {
      return names[cid];
    }
  protected:
    void push(char *name) {
      strcpy(names[count++],name);
    }
    char names[MAX_NAMES][LL];
    int count;
};

struct door {
  int x,y;
  int key;
  bool isopened;
};

struct object {
  int ind;
  int needv[NN];
  int needc;
  int givev[NN];
  int givec;
  bool used;
};

struct room {
  int floorv[NN];
  int floorc;
  int objv[NN];
  int objc;
  int piplev[NN];
  int piplec;
};

FILE *fi, *fo;
//char RoomName[MAX_ROOMS][LL];
Container RoomNames,ThingNames,ObjectNames,CreatureNames;
bool g[MAX_ROOMS][MAX_ROOMS];
bool piacked_up[MAX_ROOMS];
room r[MAX_ROOMS];
object obj[NN], creat[NN];
bool pickedup[MAX_ROOMS];
door d[NN];
int doorc=0,objectc=0,creaturec=0,roomc=0;
int RoomCount;
int StartRoom,FinishRoom;
int current;
bool bag[NN];
bool dfs_used[NN];
int dfs_d[NN];

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

int ReadName(char *s) {
  int c;
  int i;
  c=getcc();
  i=0;
  while((c!=',')&&(c!='\n')&&(c!='\r')) {
    s[i++]=c;
    c=getc(fi);
  }
  s[i]='\0';
  i--;
  while(i>0) {
    if((s[i]!=' ')&&(s[i]!='\t'))
      break;
    s[i]='\0';
  }
  return c;
}

void sort(int *a, int n) {
  int i,j,x;
  for(i=0;i<n;i++) {
    for(j=i+1;j<n;j++) {
      if(a[j]>a[i]) {
        x=a[i];
        a[i]=a[j];
        a[j]=x;
      }
    }
  }
}

void ReadRoomNames() {
  char s[1024];
  int i;
  fscanf(fi,"%d",&RoomCount);
  for(i=0;i<RoomCount;i++) {
//    ReadName(RoomName[i]);
    ReadName(s);
    RoomNames.Add(s);
  }
}

void ReadDoors() {
  int x,y;
  int i;
  char s[1024];
  fscanf(fi,"%d",&doorc);
  for(i=0;i<doorc;i++) {
    fscanf(fi,"%d%d",&x,&y);
    x--; y--;
    d[i].x=x;
    d[i].y=y;
    d[i].isopened=false;
    ReadName(s);
    d[i].key=ThingNames.GetId(s);
  }
}

int ReadCreature() {
  char s[1024];
  int c;
  ReadName(s);
  creat[creaturec].ind=CreatureNames.GetId(s);
  creat[creaturec].needc=0;
  while(true) {
    c=ReadName(s);
    creat[creaturec].needv[creat[creaturec].needc]=ThingNames.GetId(s);
    creat[creaturec].needc++;
    if((c=='\n')||(c=='\r'))
      break;
  }
  creat[creaturec].givec=0;
  while(true) {
    c=ReadName(s);
    creat[creaturec].givev[creat[creaturec].givec]=ThingNames.GetId(s);
    creat[creaturec].givec++;
    if((c=='\n')||(c=='\r'))
      break;
  }
  creat[creaturec].used=false;
  creaturec++;
  return creaturec-1;
}

int ReadObject() {
  char s[1024];
  int c;
  ReadName(s);
  obj[objectc].ind=ObjectNames.GetId(s);
  obj[objectc].needc=0;
  while(true) {
    c=ReadName(s);
    obj[objectc].needv[obj[objectc].needc++]=ThingNames.GetId(s);
    if((c=='\n')||(c=='\r'))
      break;
  }
  obj[objectc].givec=0;
  while(true) {
    c=ReadName(s);
    obj[objectc].givev[obj[objectc].givec++]=ThingNames.GetId(s);
    if((c=='\n')||(c=='\r'))
      break;
  }
  obj[objectc].used=false;
  objectc++;
  return objectc-1;
}

int ReadThing() {
  char s[1024];
  ReadName(s);
  return ThingNames.GetId(s);
}

void ReadRoom() {
  int n,m,k;
  int i,j;
  fscanf(fi,"%d%d%d",&n,&m,&k);
  r[roomc].piplec=0;
  for(i=0;i<n;i++) {
    j=ReadCreature();
    r[roomc].piplev[r[roomc].piplec++]=j;
  }
  r[roomc].objc=0;
  for(i=0;i<m;i++) {
    j=ReadObject();
    r[roomc].objv[r[roomc].objc++]=j;
  }
  r[roomc].floorc=0;
  for(i=0;i<k;i++) {
    j=ReadThing();
    r[roomc].floorv[r[roomc].floorc++]=j;
  }
  roomc++;
}

void PrintThingsList(int *a, int n) {
  int i;
  if(n==1) {
    fprintf(fo,"%s",ThingNames.GetName(a[0]));
    return ;
  }
  fprintf(fo,"%s",ThingNames.GetName(a[0]));
  for(i=1;i<n-1;i++) {
    fprintf(fo,", %s",ThingNames.GetName(a[i]));
  }
  fprintf(fo," and %s",ThingNames.GetName(a[n-1]));
}

void PickUpAll() {
  if(pickedup[current])
    return ;
  int i;
  for(i=0;i<r[current].floorc;i++) {
    fprintf(fo,"pick %s\n",ThingNames.GetName(r[current].floorv[i]));
    bag[r[current].floorv[i]]=true;
  }
  pickedup[current]=true;
}

void OpenDoor(int ind) {
  if(d[ind].isopened)
    return ;
  d[ind].isopened=true;
  if(d[ind].x==current) {
    fprintf(fo,"open door to %s\n",RoomNames.GetName(d[ind].y));
  } else {
    fprintf(fo,"open door to %s\n",RoomNames.GetName(d[ind].x));
  }
  g[d[ind].x][d[ind].y]=true;
  g[d[ind].y][d[ind].x]=true;
}

bool CanUseObject(int i) {
  int j;
  if(obj[i].used) return false;
  for(j=0;j<obj[i].needc;j++)
    if(!bag[obj[i].needv[j]])
      return false;
  return true;
}

bool CanUseCreature(int i) {
  int j;
  if(creat[i].used) return false;
  for(j=0;j<creat[i].needc;j++)
    if(!bag[creat[i].needv[j]])
      return false;
  return true;
}

void UseObject(int i) {
  if(obj[i].used)
    return ;
  obj[i].used=true;
  fprintf(fo,"use ");
  PrintThingsList(obj[i].needv,obj[i].needc);
  fprintf(fo," on %s\n",ObjectNames.GetName(i));
  fprintf(fo,"take ");
  PrintThingsList(obj[i].givev,obj[i].givec);
  fprintf(fo," from %s\n",ObjectNames.GetName(i));
  int j;
  for(j=0;j<obj[i].givec;j++) {
    bag[obj[i].givev[j]]=true;
  }
}

void UseCreature(int i) {
  if(creat[i].used)
    return ;
  creat[i].used=true;
  fprintf(fo,"talk to %s\n",CreatureNames.GetName(i));
  fprintf(fo,"give ");
  PrintThingsList(creat[i].needv,creat[i].needc);
  fprintf(fo," to %s\n",CreatureNames.GetName(i));
  fprintf(fo,"take ");
  PrintThingsList(creat[i].givev,creat[i].givec);
  fprintf(fo," from %s\n",CreatureNames.GetName(i));
  int j;
  for(j=0;j<creat[i].needc;j++) {
    bag[creat[i].needv[j]]=false;
  }
  for(j=0;j<creat[i].givec;j++) {
    bag[creat[i].givev[j]]=true;
  }
}

void UseAll() {
  int i,j;
  for(i=0;i<r[current].objc;i++) {
    if(CanUseObject(r[current].objv[i])) {
      UseObject(r[current].objv[i]);
    }
  }
  for(i=0;i<r[current].piplec;i++) {
    if(CanUseCreature(r[current].piplev[i])) {
      UseCreature(r[current].piplev[i]);
    }
  }
}

void dfs(int v) {
  dfs_used[v]=true;
  int i;
  for(i=0;i<RoomCount;i++) {
    if(g[v][i]) {
      if(!dfs_used[i]) {
        dfs_d[i]=v;
        dfs(i);
      }
    }
  }
}

int FindToGo() {
  int i,j;
  for(i=0;i<RoomCount;i++) {
    if(!pickedup[i]) {
      if(g[current][i]) {
        return i;
      }
    }
  }
  for(i=0;i<RoomCount;i++) {
    if(!pickedup[i]) {
      for(j=0;j<RoomCount;j++) {
        if(g[j][i]) {
          if(pickedup[j]) {
            return i;
          }
        }
      }
    }
  }
  for(i=0;i<RoomCount;i++) {
    if(pickedup[i]) {
      for(j=0;j<r[i].objc;j++) {
        if(CanUseObject(r[i].objv[j])) {
          return i;
        }
      }
      for(j=0;j<r[i].piplec;j++) {
        if(CanUseCreature(r[i].piplev[j])) {
          return i;
        }
      }
    }
  }
  for(i=0;i<doorc;i++) {
    if(bag[d[i].key]) {
      if(pickedup[d[i].x]&&(!pickedup[d[i].y])) {
        return d[i].x;
      }
      if(pickedup[d[i].y]&&(!pickedup[d[i].x])) {
        return d[i].y;
      }
    }
  }
  return current;
}

void GoTo(int v) {
  int i;
  if(v==current)
    return ;
  if(g[current][v]) {
    fprintf(fo,"go to %s\n",RoomNames.GetName(v));
    current=v;
    return ;
  }
  for(i=0;i<RoomCount;i++) {
    dfs_used[i]=false;
  }
  dfs(current);
  GoTo(dfs_d[v]);
  fprintf(fo,"go to %s\n",RoomNames.GetName(v));
  current=v;
}

void process() {
  int step;
  int i,j;
  step=0;
  while(true) {
    step++;
    if(current==FinishRoom) {
      fprintf(fo,"save princess\n");
      return ;
    }
    PickUpAll();
    UseAll();
    for(i=0;i<doorc;i++) {
      if((d[i].x==current)||(d[i].y==current)) {
        if(bag[d[i].key]) {
          OpenDoor(i);
        }
      }
    }
    GoTo(FindToGo());
    if(step>=10000) {
      fclose(fo);
      fo=fopen("quest.out","wt");
      fprintf(fo,"dead princess\n");
      return ;
    }
  }
}

int main() {
  int i,j;
  char s[1024];
  fi=fopen("quest.in","rt");
  fo=fopen("quest.out","wt");

  ReadRoomNames();
//  fscanf(fi,"%d",&j);
//  for(i=0;i<j;i++)
  ReadDoors();
  for(i=0;i<RoomCount;i++) {
    ReadRoom();
  }
  ReadName(s);
  StartRoom=RoomNames.GetId(s);
  ReadName(s);
  FinishRoom=RoomNames.GetId(s);
  current=StartRoom;
  for(i=0;i<RoomCount;i++) {
    for(j=0;j<RoomCount;j++) {
      g[i][j]=false;
    }
    pickedup[i]=false;
  }
  for(i=0;i<NN;i++)
    bag[i]=false;

  process();

  fclose(fi);
  fclose(fo);
  return 0;
}
