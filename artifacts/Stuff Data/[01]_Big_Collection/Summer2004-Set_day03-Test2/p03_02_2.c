#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ml 212

typedef struct
{
  int c, i, o;
}
rm;

rm room[10];
char c, cname[10][202][ml], oname[10][202][ml], sub[10][202][ml],
     *cgive[10][202][202], *ctake[10][202][202],
     *ogive[10][202][202], *otake[10][202][202],
     rname[10][ml], kname[200][ml], ss[ml], tt[ml];
int ff, rr, r[10000], p[10000], key[10000], h[20], next[10000],
    mark[20], mark2[20], n, onumtake[10][202], onumgive[10][202],
    cnumtake[10][202], cnumgive[10][202], cneed[10][202], oneed[10][202],
    was[20], cuse[10][202], ouse[10][202], open[1000], door[1000], cur, pr,
    from[20];

void add( int x, int y, int k )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  key[rr] = k;
}

void dfs( int v )
{
  int i;

  if (mark[v])
    return;
  mark[v] = 1;
  for (i = h[v]; i != 0; i = next[i])
    if (open[i])
      dfs(r[i]);
}

void take( char *s )
{
  int i, j, k;

  for (i = 1; i <= n; i++)
    for (j = 1; j <= room[i].c; j++)
      for (k = 1; k <= cnumgive[i][j]; k++)
        if (strcmp(s, cgive[i][j][k]) == 0)
          cneed[i][j]--;
  for (i = 1; i <= n; i++)
    for (j = 1; j <= room[i].o; j++)
      for (k = 1; k <= onumgive[i][j]; k++)
        if (strcmp(s, ogive[i][j][k]) == 0)
          oneed[i][j]--;
  for (i = 1; i <= n; i++)
    for (j = h[i]; j != 0; j = next[j])
      if (!open[j] && !door[j] && strcmp(s, kname[key[j]]) == 0)
        door[j] = door[p[j]] = 1;
}

void give( char *s )
{
  int i, j, k;

  for (i = 1; i <= n; i++)
    for (j = 1; j <= room[i].c; j++)
      for (k = 1; k <= cnumgive[i][j]; k++)
        if (strcmp(s, cgive[i][j][k]) == 0)
          cneed[i][j]++;
  for (i = 1; i <= n; i++)
    for (j = 1; j <= room[i].o; j++)
      for (k = 1; k <= onumgive[i][j]; k++)
        if (strcmp(s, ogive[i][j][k]) == 0)
          oneed[i][j]++;
}

int go2( int v, int t )
{
  int i, j;

  if (mark2[v])
    return 0;
  mark2[v] = 1;
  if (v == t)
    return 1;
  for (i = h[v]; i != 0; i = next[i])
    if (open[i] && go2(r[i], t))
    {
      //printf("go to %s\n", rname[r[i]]);
      from[v] = r[i];
      return 1;
    }
  return 0;
}

void go( int v, int t )
{
  int i, j;

  for (i = 1; i <= n; i++)
    mark2[i] = 0;
  go2(v, t);
  for (i = v; i != t; i = from[i])
  {
    if (!was[i])
    {
      was[i] = 1;
      for (j = 1; j <= room[i].i; j++)
      {
        if (ff)
          printf("pick %s\n", sub[i][j]);
        take(sub[i][j]);
      }
    }
    if (ff)printf("go to %s\n", rname[from[i]]);
  }
  if (!was[i])
  {
    was[i] = 1;
    for (j = 1; j <= room[i].i; j++)
    {
      if (ff)
        printf("pick %s\n", sub[i][j]);
      take(sub[i][j]);
    }
  }
}

void string( char *s )
{
  int l, k = 0, kk = 0;

  while (c == 10 || c == 13 || c == ' ')
    c = getc(stdin);
  l = 0;
  while (c != '\n' && c != EOF)
  {
    if (c != ' ')
    {
      for (kk = 1; kk <= k; kk++)
        s[l++] = ' ';
      s[l++] = c;
      k = 0;
    }
    else
      k++;
    c = getc(stdin);
  }
  for (l--; l > 0 && s[l] == ' '; l--)
    ;
  s[l + 1] = 0;
}

int calc( void )
{
  int i, j, k, l, m, x, y, f;
  
  for (i = 1; i <= n; i++)
  {
    if (strcmp(rname[i], ss) == 0)
      cur = i;
    if (strcmp(rname[i], tt) == 0)
      pr = i;
  }
  f = 1;
  was[cur] = 1;
  for (i = 1; i <= room[cur].i; i++)
  {
    //mv++;
          if (ff)
    printf("pick %s\n", sub[cur][i]);
    take(sub[cur][i]);
  }
  while (f)
  {
    f = 0;
    for (i = 1; i <= n; i++)
      mark[i] = 0;
    dfs(cur);
    if (mark[pr])
    {
      for (i = 1; i <= n; i++)
        mark[i] = 0;
      go(cur, pr);
          if (ff)
      printf("save princess\n");
      return 1;
    }
    for (i = 1; i <= n; i++)
      if (mark[i] && !was[i])
      {
        go(cur, i);
        cur = i;
        f = 1;
        break;
      }
    if (i <= n)
      continue;
    for (i = 1; i <= n; i++)
      if (mark[i])
      {
        for (j = 1; j <= room[i].o; j++)
          if (!ouse[i][j] && oneed[i][j] == 0)
          {
            f = ouse[i][j] = 1;
            go(cur, i);
            cur = i;
          if (ff)printf("use ");
            for (k = 1; k <= onumgive[i][j]; k++)
            {
              if (ff)printf("%s", ogive[i][j][k]);
              if (k == onumgive[i][j] - 1)
              {
                if (ff)printf(" and ");
              }
              else if (k < onumgive[i][j] - 1)
              {
                if(ff)printf(", ");
              }
            }
            if(ff)printf(" on %s\ntake ", oname[i][j]);
            for (k = 1; k <= onumtake[i][j]; k++)
            {
              if(ff)printf("%s", otake[i][j][k]);
              take(otake[i][j][k]);
              if (k == onumtake[i][j] - 1)
              {
                if(ff)printf(" and ");
              }
              else if (k < onumtake[i][j] - 1)
              {
                if(ff)printf(", ");
              }
            }
            if(ff)printf(" from %s\n", oname[i][j]);
            break;
          }
        if (j <= room[i].o)
          break;
      }
    if (f)
      continue;
    for (i = 1; i <= n; i++)
    {
      if (!mark[i])
        continue;
      for (j = h[i]; j != 0; j = next[j])
        if (!open[j] && door[j])
        {
          go(cur, i);
          open[j] = f = open[p[j]] = 1;
          cur = i;
          if(ff)printf("open door to %s\n", rname[r[j]]);
          break;
        }
     if (j != 0)
       break;
    }
    if (f)
      continue;
    for (i = 1; i <= n; i++)
      if (mark[i])
      {
        for (j = 1; j <= room[i].c; j++)
          if (!cuse[i][j] && cneed[i][j] == 0)
          {
            f = cuse[i][j] = 1;
            go(cur, i);
            cur = i;
            if(ff)printf("talk to %s\ngive ", cname[i][j]);
            for (k = 1; k <= cnumgive[i][j]; k++)
            {
              if(ff)printf("%s", cgive[i][j][k]);
              give(cgive[i][j][k]);
              if (k == cnumgive[i][j] - 1)
              {
                if(ff)printf(" and ");
              }
              else if (k < cnumgive[i][j] - 1)
              {
                if(ff)printf(", ");
              }
            }
            if(ff)printf(" to %s\ntake ", cname[i][j]);
            for (k = 1; k <= cnumtake[i][j]; k++)
            {
              if(ff)printf("%s", ctake[i][j][k]);
              take(ctake[i][j][k]);
              if (k == cnumtake[i][j] - 1)
              {
                if(ff)printf(" and ");
              }
              else if (k < cnumtake[i][j] - 1)
              {
                if(ff)printf(", ");
              }
            }
            if(ff)printf(" from %s\n", cname[i][j]);
            break;
          }
        if (j <= room[i].c)
          break;
      }
  }
  printf("dead princess\n");
  return 0;
}

int main( void )
{
  int i, j, k, l, m, x, y, f;

  freopen("quest.in", "r", stdin);
  freopen("quest.out", "w", stdout);
  scanf("%d", &n);
  c = getc(stdin);
  for (i = 1; i <= n; i++)
    string(rname[i]);
  scanf("%d", &m);
  for (i = 1; i <= m; i++)
  {
    scanf("%d%d", &x, &y);
    c = getc(stdin);
    string(kname[i]);
    add(x, y, i);
    p[rr] = rr + 1;
    add(y, x, i);
    p[rr] = rr - 1;
  }
  for (j = 1; j <= n; j++)
  {
    scanf("%d%d%d", &room[j].c, &room[j].o, &room[j].i);
    for (i = 1; i <= room[j].c; i++)
    {
      c = getc(stdin);
      string(cname[j][i]);
      k = 1;
      c = getc(stdin);
      while (c != '\n')
      {
        while (c == ' ')
          c = getc(stdin);
        l = 0;
        cgive[j][i][k] = (char *)malloc(ml);
        while (c != ',' && c != '\n')
        {
          if (l < ml - 1)
            cgive[j][i][k][l++] = c;
          c = getc(stdin);
        }
        for (l--; l > 0 && cgive[j][i][k][l] == ' '; l--)
          ;
        cgive[j][i][k][l + 1] = 0;
        if (c == ',')
        {
          k++;
          c = getc(stdin);
        }
      }
      cneed[j][i] = cnumgive[j][i] = k;
      c = getc(stdin);
      k = 1;
      while (c != '\n')
      {
        while (c == ' ')
          c = getc(stdin);
        l = 0;
        ctake[j][i][k] = (char *)malloc(ml);
        while (c != ',' && c != '\n')
        {
          if (l < ml - 1)
            ctake[j][i][k][l++] = c;
          c = getc(stdin);
        }
        for (l--; l > 0 && ctake[j][i][k][l] == ' '; l--)
          ;
        ctake[j][i][k][l + 1] = 0;
        if (c == ',')
        {
          k++;
          c = getc(stdin);
        }
      }
      cnumtake[j][i] = k;
    }
    for (i = 1; i <= room[j].o; i++)
    {
      c = getc(stdin);
      string(oname[j][i]);
      c = getc(stdin);
      k = 1;
      while (c != '\n')
      {
        while (c == ' ')
          c = getc(stdin);
        l = 0;
        ogive[j][i][k] = (char *)malloc(ml);
        while (c != ',' && c != '\n')
        {
          if (l < ml - 1)
            ogive[j][i][k][l++] = c;
          c = getc(stdin);
        }
        for (l--; l > 0 && ogive[j][i][k][l] == ' '; l--)
          ;
        ogive[j][i][k][l + 1] = 0;
        if (c == ',')
        {
          k++;
          c = getc(stdin);
        }
      }
      oneed[j][i] = onumgive[j][i] = k;
      c = getc(stdin);
      k = 1;
      while (c != '\n')
      {
        while (c == ' ')
          c = getc(stdin);
        l = 0;
        otake[j][i][k] = (char *)malloc(ml);
        while (c != ',' && c != '\n')
        {
          if (l < ml - 1)
            otake[j][i][k][l++] = c;
          c = getc(stdin);
        }
        for (l--; l > 0 && otake[j][i][k][l] == ' '; l--)
          ;
        otake[j][i][k][l + 1] = 0;
        if (c == ',')
        {
          k++;
          c = getc(stdin);
        }
        onumtake[j][i] = k;
      }
    }
    for (i = 1; i <= room[j].i; i++)
    {
      c = getc(stdin);
      string(sub[j][i]);
    }
  }
  c = getc(stdin);
  string(ss);
  string(tt);
  /*ff = 1;
  calc();
  return 0;*/
  if (calc())
  {
    ff = 1;
    for (i = 1; i <= n; i++)
      for (j = 1; j <= room[i].c; j++)
      {
        cuse[i][j] = 0;
        cneed[i][j] = cnumgive[i][j];
      }
    for (i = 1; i <= n; i++)
      for (j = 1; j <= room[i].o; j++)
      {
        oneed[i][j] = onumgive[i][j];
        ouse[i][j] = 0;
      }
    for (i = 1; i <= m * 2 + 1; i++)
      open[i] = door[i] = 0;
    for (i = 1; i <= n; i++)
      was[i] = 0;
    calc();
  }
  return 0;
}

