/* 03.07.04 SK1 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_N 10
#define MAX_M 100
#define MAX_LEN 100
#define MAX_NUM 200
#define MAX_ART (MAX_NUM + MAX_M)
#define MAX_TURN 30000

#define PICK 0
#define GOTO 1
#define OPEN 2
#define TALK 3
#define USE  4
#define SAVE 5

/* Strategy to complete mission */
int W[MAX_TURN][2];
int WN = 0;

/* Buffer */
char Str[MAX_LEN + 1];

/* Number of rooms */
int N;
/* Number of doors */
int M;
/* Start room */
int St;
/* Finish room */
int En;
/* Current room */
int CurRoom;
/* This flag is false while hero doesn't find the princess */
int Win = 0;
/* This flag is true while hero can do anything */
int Run = 1;

/* Names of rooms */
char Room[MAX_N][MAX_LEN + 1];
/* Names of artifacts (keys are artifacts too) */
char Art[MAX_ART][MAX_LEN + 1];
int ArtN = 0;
/* Names of objects */
char Obj[MAX_NUM][MAX_LEN + 1];
int ObjN = 0;
/* Names of humans */
char Hum[MAX_NUM][MAX_LEN + 1];
int HumN = 0;

/* Connections of rooms */
int C[MAX_N][MAX_N];
/* Artifacts which hero have */
int ArtU[MAX_ART];
/* Rooms which hero visited */
int RoomU[MAX_N];
/* Objects which hero has used and humans with who he has talked */
int ObjU[MAX_NUM];
int HumU[MAX_NUM];

/* Numbers of humans, artifacts and objects in the room */
int HAON[MAX_N][3];
/* Humans in the room */
int RoHum[MAX_N][MAX_NUM];
/* Objects in the room */
int RoObj[MAX_N][MAX_NUM];
/* Artifacts in the room */
int RoArt[MAX_N][MAX_NUM];

/* Artifacts which must have hero to talk with human */
int ArtHum[MAX_NUM][MAX_NUM];
int ArtHumN[MAX_NUM];
/* Artifacts which hero will get after talking with human */
int HumArt[MAX_NUM][MAX_NUM];
int HumArtN[MAX_NUM];
/* Artifacts which must have hero to use the object */
int ArtObj[MAX_NUM][MAX_NUM];
int ArtObjN[MAX_NUM];
/* Artifacts which hero will get after using the object */
int ObjArt[MAX_NUM][MAX_NUM];
int ObjArtN[MAX_NUM];

/* Reads string */
int ReadStr( char *S )
{
  int c, pos = 0;

  c = getc(stdin);
  while (c == 10 || c == 13 || c == 32)
    c = getc(stdin);
  while (c != 10 && c != 13 && c != ',')
    S[pos++] = c, c = getc(stdin);
  while (S[pos - 1] == ' ')
    pos--;
  S[pos] = 0;
  return c;
} /* End of 'ReadStr' function */

/* Displays list of artifacts */
void Display( int n, int *a )
{
  int i;

  printf("%s", Art[a[0]]);
  if (n != 1)
  {
    for (i = 1; i < n - 1; i++)
      printf(", %s", Art[a[i]]);
    printf(" and %s", Art[a[i]]);
  }
} /* End of 'Display' function */

/* Visits the room */
void Visit( int room )
{
  int i;

  if (room == En)
  {
    W[WN++][0] = SAVE;
    Win = 1;
    return;
  }
  RoomU[room] = 1;
  if (HAON[room][2] != 0)
  {
    W[WN++][0] = PICK;
    for (i = 0; i < HAON[room][2]; i++)
      ArtU[RoArt[room][i]] = 1;
  }
} /* End of 'Visit' function */

/* Uses the object */
void Use( int obj )
{
  int i;
  
  ObjU[obj] = 1;
  if (ObjArtN[obj] != 0)
  {
    W[WN][0] = USE;
    W[WN][1] = obj;
    WN++;
    for (i = 0; i < ObjArtN[obj]; i++)
      ArtU[ObjArt[obj][i]] = 1;
  }
} /* End of 'Use' function */

/* Talks with the human */
void Talk( int hum )
{
  int i;
  
  HumU[hum] = 1;
  if (HumArtN[hum] != 0)
  {
    W[WN][0] = TALK;
    W[WN][1] = hum;
    WN++;
    for (i = 0; i < HumArtN[hum]; i++)
      ArtU[HumArt[hum][i]] = 1;
  }
} /* End of 'Talk' function */

/* Opens the door and moves to the room */
void Open( int room )
{
  W[WN][0] = OPEN;
  W[WN][1] = room;
  WN++;
  W[WN][0] = GOTO;
  W[WN][1] = room;
  WN++;
  CurRoom = room;
} /* End of 'Open' function */

/* Distances */
int D[MAX_N];
/* Parents */
int Prev[MAX_N];
/* Path to the room */
int Path[MAX_N];
int PN;

/* Search function */
void Search( int room )
{
  int i;

  for (i = 0; i < N; i++)
    if (C[room][i] != -1 && RoomU[i])
      if (D[i] == -1 || D[i] > D[room] + 1)
      {
        D[i] = D[room] + 1;
        Prev[i] = room;
        Search(i);
      }
} /* End of 'Search' function */

/* Moves to the room */
void Move( int room )
{
  int i;

  if (room == CurRoom)
    return;
  for (i = 0; i < N; i++)
    D[i] = Prev[i] = -1;
  D[CurRoom] = 0;
  Search(CurRoom);
  PN = 0;
  for (i = room; i != CurRoom; i = Prev[i])
    Path[PN++] = i;
  for (i = PN - 1; i >= 0; i--)
  {
    W[WN][0] = GOTO;
    W[WN][1] = Path[i];
    WN++;
  }
  CurRoom = room;
} /* End of 'Move' function */

/* The main program function */
int main( void )
{
  int i, j, k, a, b, c, room, fl;
  
  /***
   * Initialization
   ***/
  freopen("quest.in", "r", stdin);
  freopen("quest.out", "w", stdout);
  for (i = 0; i < MAX_N; i++)
    for (j = 0; j < MAX_N; j++)
      C[i][j] = -1;
  for (i = 0; i < MAX_ART; i++)
    ArtU[i] = 0;
  for (i = 0; i < MAX_N; i++)
    RoomU[i] = 0;
  for (i = 0; i < MAX_NUM; i++)
    ObjU[i] = 0;
  for (i = 0; i < MAX_NUM; i++)
    HumU[i] = 0;
  for (i = 0; i < MAX_NUM; i++)
    ArtHumN[i] = HumArtN[i] = 0;
  for (i = 0; i < MAX_NUM; i++)
    ArtObjN[i] = ObjArtN[i] = 0;

  /***
   * Reading input data
   ***/
  scanf("%i", &N);
  for (i = 0; i < N; i++)
    ReadStr(Room[i]);
  scanf("%i", &M);
  for (i = 0; i < M; i++)
  {
    scanf("%i%i", &a, &b);
    a--, b--;
    C[a][b] = C[b][a] = i;
    ReadStr(Art[i]);
  }
  ArtN = M;
  /* Reads information about rooms */
  for (i = 0; i < N; i++)
  {
    for (j = 0; j < 3; j++)
      scanf("%i", &HAON[i][j]);
    /* Reads information about humans in this room */
    for (j = 0; j < HAON[i][0]; j++)
    {
      ReadStr(Str);
      for (a = 0; a < HumN; a++)
        if (strcmp(Str, Hum[a]) == 0)
          break;
      if (a == HumN)
        strcpy(Hum[HumN++], Str);
      RoHum[i][j] = a;
      do
      {
        c = ReadStr(Str);
        for (b = 0; b < ArtN; b++)
          if (strcmp(Str, Art[b]) == 0)
            break;
        if (b == ArtN)
          strcpy(Art[ArtN++], Str);
        ArtHum[a][ArtHumN[a]++] = b;
      } while (c == ',');
      do
      {
        c = ReadStr(Str);
        for (b = 0; b < ArtN; b++)
          if (strcmp(Str, Art[b]) == 0)
            break;
        if (b == ArtN)
          strcpy(Art[ArtN++], Str);
        HumArt[a][HumArtN[a]++] = b;
      } while (c == ',');
    }
    /* Reads information about objects in this room */
    for (j = 0; j < HAON[i][1]; j++)
    {
      ReadStr(Str);
      for (a = 0; a < ObjN; a++)
        if (strcmp(Str, Obj[a]) == 0)
          break;
      if (a == ObjN)
        strcpy(Obj[ObjN++], Str);
      RoObj[i][j] = a;
      do
      {
        c = ReadStr(Str);
        for (b = 0; b < ArtN; b++)
          if (strcmp(Str, Art[b]) == 0)
            break;
        if (b == ArtN)
          strcpy(Art[ArtN++], Str);
        ArtObj[a][ArtObjN[a]++] = b;
      } while (c == ',');
      do
      {
        c = ReadStr(Str);
        for (b = 0; b < ArtN; b++)
          if (strcmp(Str, Art[b]) == 0)
            break;
        if (b == ArtN)
          strcpy(Art[ArtN++], Str);
        ObjArt[a][ObjArtN[a]++] = b;
      } while (c == ',');
    }
    /* Reads information about artifacts in this room */
    for (j = 0; j < HAON[i][2]; j++)
    {
      ReadStr(Str);
      for (a = 0; a < ArtN; a++)
        if (strcmp(Str, Art[a]) == 0)
          break;
      if (a == ArtN)
        strcpy(Art[ArtN++], Str);
      RoArt[i][j] = a;
    }
  }
  /* Reads numbers of start and finish rooms */
  ReadStr(Str);
  for (St = 0; St < N; St++)
    if (strcmp(Str, Room[St]) == 0)
      break;
  ReadStr(Str);
  for (En = 0; En < N; En++)
    if (strcmp(Str, Room[En]) == 0)
      break;

  /***
   * Solution
   ***/
  Visit(St);
  CurRoom = St;
  while (!Win && Run)
  {
    Run = 0;
    for (i = 0; i < N; i++)
      if (RoomU[i])
      {
        fl = 0;
        for (j = 0; j < N && !Win; j++)
          if (C[i][j] != -1 && !RoomU[j])
            if (ArtU[C[i][j]])
            {
              if (fl == 0)
                Move(i);
              Open(j);
              Visit(j), Run = 1;
              CurRoom = j;
            }
        if (Win)
          break;
        for (j = 0; j < HAON[i][1]; j++)
          if (ObjU[RoObj[i][j]] == 0)
          {
            a = RoObj[i][j];
            for (k = 0; k < ArtObjN[a]; k++)
              if (ArtU[ArtObj[a][k]] == 0)
                break;
            if (k == ArtObjN[a])
            {
              if (fl == 0)
                Move(i), fl = 1;
              Use(a), Run = 1;
            }
          }
        for (j = 0; j < HAON[i][0]; j++)
          if (HumU[RoHum[i][j]] == 0)
          {
            a = RoHum[i][j];
            for (k = 0; k < ArtHumN[a]; k++)
              if (ArtU[ArtHum[a][k]] == 0)
                break;
            if (k == ArtHumN[a])
            {
              if (fl == 0)
                Move(i), fl = 1;
              Talk(a), Run = 1;
            }
          }
      }
  }

  /***
   * Output result
   ***/
  if (Win == 0)
    printf("dead princess");
  else
  {
    room = St;
    for (i = 0; i < WN; i++)
      switch (W[i][0])
      {
      case PICK:
        for (j = 0; j < HAON[room][2]; j++)
          printf("pick %s\n", Art[RoArt[room][j]]);
        break;
      case GOTO:
        printf("go to %s\n", Room[W[i][1]]);
        room = W[i][1];
        break;
      case OPEN:
        printf("open door to %s\n", Room[W[i][1]]);
        break;
      case SAVE:
        printf("save princess\n");
        break;
      case USE:
        printf("use ");
        Display(ArtObjN[W[i][1]], ArtObj[W[i][1]]);
        printf(" on %s\n", Obj[W[i][1]]);
        printf("take ");
        Display(ObjArtN[W[i][1]], ObjArt[W[i][1]]);
        printf(" from %s\n", Obj[W[i][1]]);
        break;
      case TALK:
        printf("talk to %s\n", Hum[W[i][1]]);
        printf("give ");
        Display(ArtHumN[W[i][1]], ArtHum[W[i][1]]);
        printf(" to %s\n", Hum[W[i][1]]);
        printf("take ");
        Display(HumArtN[W[i][1]], HumArt[W[i][1]]);
        printf(" from %s\n", Hum[W[i][1]]);
        break;
      }
  }
  return 0;
} /* End of 'main' function */
