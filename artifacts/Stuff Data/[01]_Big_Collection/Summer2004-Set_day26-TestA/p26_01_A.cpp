#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MM 10010
#define CUB(x) ((x)*(x)*(x))
#define MX 1261

FILE *fi, *fo;
int n,m;
int a[MM];
int d[MM];
int stk[10];
int i;
int mxdeep=0;

void wr(int v) {
  if(d[v]<0 ) return ;
  fprintf(fo,"%d ",d[v]);
  wr(v-CUB(d[v]));
}

int rec(int n, int deep,int mx) {
  int i,f;
  if(n<0)
    return 9;
  if(deep>8) {
    return 9;
  }
  if(n==0) {
    mxdeep=deep;
    return deep;
  }
  if(n<MM) {
    mxdeep=deep;
    return a[n]+deep;
  }
  f=9;
  for(i=mx;i>0;i--) {
    stk[deep]=i;
    f=rec(n-CUB(i),deep+1,i);
    if(f<=8)
      return f;
  }
  return 9;
}

int main() {
  int i,j;
  fi=fopen("sumcubes.in","rt");
  fo=fopen("sumcubes.out","wt");

  fscanf(fi,"%d",&n);
  if(n<MM) {
    m=n+1;
  } else {
    m=MM;
  }
  a[0]=0;
  d[0]=-1;
  for(i=1;i<m;i++) {
    a[i]=9;
    d[i]=-1;
    for(j=1;CUB(j)<=i;j++) {
//      a[i]=min(a[i],a[i-CUB(j));
      if(a[i]>a[i-CUB(j)]+1) {
        a[i]=a[i-CUB(j)]+1;
        d[i]=j;
      }
    }
  }
  if(n<MM) {
    if(d[n]<0) {
      fprintf(fo,"IMPOSSIBLE\n");
    } else {
//      fprintf(fo,"%d\n",a[n]);
      wr(n);
    }
  } else {
    i=rec(n,0,MX);
//    i=rec(n,0,1001);
    if(i>8) {
      fprintf(fo,"IMPOSSIBLE\n");
    } else {
//      fprintf(fo,"%d\n",i);
      for(i=0;i<mxdeep;i++) {
        fprintf(fo,"%d ",stk[i]);
        n-=CUB(stk[i]);
      }
      wr(n);
    }
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
