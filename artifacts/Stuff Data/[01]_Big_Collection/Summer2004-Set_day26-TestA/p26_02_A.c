#include <stdio.h>
#include <math.h>

int n, a[10], l;

int calc( int k, int n )
{
  int i;

  if ((n != 0 && l == 8) || n < 0)
    return 0;
  if (n <= (8 - l))
  {
    while (n != 0)
    {
      a[++l] = 1;
      n--;
    }
    return 1;
  }
  for (i = k; i > 1; i--)
  {
    a[++l] = i;
    if (calc(i, n - i * i * i))
      return 1;
    l--;
  }
  return 0;
}

int main( void )
{
  int i, j;

  freopen("sumcubes.in", "r", stdin);
  freopen("sumcubes.out", "w", stdout);
  scanf("%d", &n);
  l = 0;
  if (calc(cbrt(n) + 1, n))
  {
    for (i = 1; i <= l; i++)
      printf("%d ", a[i]);
    printf("\n");
  }
  else
    printf("IMPOSSIBLE\n");
  return 0;
}

