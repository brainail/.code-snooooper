#define ONLINE
#include <stdio.h>
#include <string.h>

#define N_CUBES_BOUND 8

long sum,cbrt,cubes[N_CUBES_BOUND];
int n_cubes;

int check(long rest,long max,int i_cube) {
  if (i_cube>=N_CUBES_BOUND) return(0);
  long cur,cub,mult;
  mult=(long)(N_CUBES_BOUND-i_cube);
  for (cur=max; cur>=1; cur--) {
    cub=cur*cur*cur; cubes[i_cube]=cur;
    if (cub==rest) { n_cubes=i_cube+1; return(1); }
    if (cub>rest) continue;
    if (cub*mult<rest) break;
    if (check(rest-cub,cur,i_cube+1)) return(1);
  }
  return(0);
}

int main(void) {
  #ifdef ONLINE
  freopen("sumcubes.in","rt",stdin);
  freopen("sumcubes.out","wt",stdout);
  #endif
  scanf("%ld",&sum);
  for (cbrt=1; cbrt*cbrt*cbrt<=sum; cbrt++);
  n_cubes=-1;
  check(sum,cbrt,0);
  if (n_cubes>=0) {
    int i_cube;
    for (i_cube=0; i_cube<n_cubes; i_cube++) {
      if (i_cube) printf(" ");
      printf("%d",cubes[i_cube]);
    } printf("\n");
  } else printf("IMPOSSIBLE\n");
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}



