/* 26.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))

long A[100];
long N, Num = 0;

int main( void )
{
  long i;

  freopen("sumcubes.in", "r", stdin);
  freopen("sumcubes.out", "w", stdout);
  scanf("%i", &N);
  while (N > 0)
  {
    for (i = 1; i * i * i <= N; i++)
      ;
    i--;
    A[Num++] = i;
    N -= i * i * i;
  }  
  if (Num > 8)
    printf("IMPOSSIBLE");
  else
    for (i = 0; i < Num; i++)
      printf("%li ", A[i]);
  return 0;
}