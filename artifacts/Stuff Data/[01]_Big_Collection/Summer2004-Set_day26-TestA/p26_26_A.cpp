#include <iostream>
#include <fstream>
#include <cstdio>
using namespace std;
typedef unsigned uint;
unsigned N;

inline unsigned cube(unsigned x) { return x*x*x; }

int main(void)
{
  {
    ifstream fi("sumcubes.in");
    fi >> N;
  }
  ofstream fo("sumcubes.out");
  fo << "IMPOSSIBLE";
  return 0;
}
