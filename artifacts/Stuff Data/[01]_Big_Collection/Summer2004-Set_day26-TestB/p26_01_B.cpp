#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 240000

FILE *fi, *fo;
int a[NN],b[NN];
int n,m;

void sift(int *a, int p, int q) {
  int i,j;
  int x;
  i=p;
  j=2*i+1;
  x=a[i];
  if(j<q-1) if(a[j+1]>a[j]) j++;
  if(j<q) if(a[j]<x) j=q+1;
  while(j<q) {
    a[i]=a[j];
    i=j;
    j=2*j+1;
    if(j<q-1) if(a[j+1]>a[j]) j++;
    if(j<q) if(a[j]<x) j=q+1;
  }
  a[i]=x;
}

void HeapSort(int *a, int n) {
  int i,x;
  for(i=n/2+1;i>=0;i--)
    sift(a,i,n); 
  for(i=n-1;i>0;i--) {
    x=a[i];
    a[i]=a[0];
    a[0]=x;
    sift(a,0,i);
  }
}

int main() {
  int i,j,k;
  fi=fopen("sumtwo.in","rt");
  fo=fopen("sumtwo.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++) {
    fscanf(fi,"%d",&a[i]);
  }
  HeapSort(a,n);
  i=0;j=n-1;
  k=0;
  while(i<=j) {
    b[k++]=a[j--];
    if(i<=j) 
      b[k++]=a[i++];
  }
  m=0;
  for(i=1;i<n;i++)
    if(m<b[i-1]+b[i])
      m=b[i-1]+b[i];
  fprintf(fo,"%d\n",m);
  for(i=0;i<n;i++)
    fprintf(fo,"%d ",b[i]);

  fclose(fi);
  fclose(fo);
  return 0;
}
