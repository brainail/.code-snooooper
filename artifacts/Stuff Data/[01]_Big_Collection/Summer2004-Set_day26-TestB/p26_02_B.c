#include <stdio.h>

#define m 300000

int a[m], s[m];

void down( int v, int s )
{
  int t;

  while ((v * 2 <= s && a[v] < a[v * 2])
         || (v * 2 + 1 <= s && a[v] < a[v * 2 + 1]))
    if (v * 2 + 1 <= s && a[v * 2 + 1] > a[v * 2])
    {
      t = a[v];
      a[v] = a[v * 2 + 1];
      a[v * 2 + 1] = t;
      v = v * 2 + 1;
    }
    else
    {
      t = a[v];
      a[v] = a[v * 2];
      a[v * 2] = t;
      v *= 2;
    }
}

int main( void )
{
  int n, ss, i, j, t, l = 0;

  freopen("sumtwo.in", "r", stdin);
  freopen("sumtwo.out", "w", stdout);
  scanf("%d", &n);
  for (i = 1; i <= n; i++)
    scanf("%d", &a[i]);
  for (i = n; i >= 1; i--)
    down(i, n);
  for (i = n; i > 1; )
  {
    t = a[i];
    a[i] = a[1];
    a[1] = t;
    i--;
    down(1, i);
  }
  for (i = 1, j = n; i <= j; i++, j--)
  {
    s[++l] = a[j];
    if (i != j)
      s[++l] = a[i];
  }
  for (i = 1, ss = 0; i < n; i++)
    if (s[i] + s[i + 1] > ss)
      ss = s[i] + s[i + 1];
  printf("%d\n", ss);
  for (i = 1; i <= n; i++)
    printf("%d ", s[i]);
  printf("\n");
  return 0;
}

