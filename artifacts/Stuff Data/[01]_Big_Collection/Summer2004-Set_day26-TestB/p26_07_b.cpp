#define ONLINE
#include <stdio.h>
#include <string.h>

void sort(long*,int);

int n_nums;
long*nums,*perm,max;

int main(void) {
  #ifdef ONLINE
  freopen("sumtwo.in","rt",stdin);
  freopen("sumtwo.out","wt",stdout);
  #endif
  int i_num;
  scanf("%d",&n_nums);
  nums=new long[n_nums]; perm=new long[n_nums];
  for (i_num=0; i_num<n_nums; i_num++) scanf("%ld",&nums[i_num]);
  sort(nums,n_nums);
  int i_head,i_tail;
  i_head=0; i_tail=n_nums-1;
  for (i_num=0; i_num<n_nums; i_num++) {
    if (i_num&1) perm[i_num]=nums[i_head++];
    else perm[i_num]=nums[i_tail--];
  }
  max=0;
  for (i_num=1; i_num<n_nums; i_num++) {
    if (max<perm[i_num]+perm[i_num-1]) max=perm[i_num]+perm[i_num-1];
  }
  printf("%ld\n",max);
  for (i_num=0; i_num<n_nums; i_num++) {
    if (i_num) printf(" ");
    printf("%ld",perm[i_num]);
  } printf("\n");
  delete[]nums; delete[]perm;
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void sift(long*a,int v,int w) {
  int i,j; long z;
  i=v; j=(v<<1)+1;
  z=a[i];
  while (j<=w) {
    if (j<w) if (a[j]<a[j+1]) j++;
    if (!(z<a[j])) break;
    a[i]=a[j];
    i=j; j=(j<<1)+1;
  }
  a[i]=z;
}
void sort(long*a,int n) {
  int i,j; long z;
  for (i=n>>1,j=n-1; i>0; i--) sift(a,i,j);
  for (; j>0; j--) {
    sift(a,i,j);
    z=a[i]; a[i]=a[j]; a[j]=z;
  }
}



