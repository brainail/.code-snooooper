/* 26.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))

#define MAX_N 240000L

long N, Max;
long A[MAX_N];
long B[MAX_N];

/* Quick sort */
void QSort( long l, long r )
{
  long i = l, j = r, tmp, x = A[(l + r + 1) >> 1];

  if (l >= r)
    return;
  while (i < j)
  {
    while (A[i] < x)
      i++;
    while (A[j] > x)
      j--;
    if (i < j)
      SWAP(A[i], A[j], tmp);
    if (i <= j)
      i++, j--;
  }
  QSort(l, j);
  QSort(i, r);
} /* End of 'QSort' function */

/* The main program function */
int main( void )
{
  long i;

  freopen("sumtwo.in", "r", stdin);
  freopen("sumtwo.out", "w", stdout);
  scanf("%li", &N);
  for (i = 0; i < N; i++)
    scanf("%li", &A[i]);
  QSort(0, N - 1);
  for (i = 0; i < N; i++)
    if (i & 1)
      B[i] = A[i >> 1];
    else
      B[i] = A[N - 1 - (i >> 1)];
  Max = B[0] + B[1];
  for (i = 1; i < N - 1; i++)
    if (B[i] + B[i + 1] > Max)
      Max = B[i] + B[i + 1];
  printf("%li\n", Max);
  for (i = 0; i < N; i++)
    printf("%li ", B[i]);
  return 0;
} /* End of 'main' function */

