/* ID: 26. Bukhalenkov Alexander */
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int N;
int *a;
int *r;
void read_data()
{
  static const char* finame = "sumtwo.in";
  ifstream fi(finame);
  fi >> N;
  a = new int[N];
  r = new int[N];
  for (int i=0; i<N; i++) {
    fi >> a[i];
    cout << a[i] << " ";
  }
}

const char* foname = "sumtwo.out";
ofstream fo(foname);

int intcmp(const void*pa, const void *pb)
{
 int a,b;
 a = (*(const int*)pa);
 b = (*(const int*)pb);
 if (a<b) return -1;
 if (a==b) return 0;
 return 1;
}

void solve()
{
  qsort(a,N,sizeof(int),intcmp);
  int i=0;
  int j=N-1;
  for (int cnt=0; cnt<N; ) {
    r[cnt++] = a[j--];
    r[cnt++] = a[i++];
  }
}

void write_data()
{
  int summ = reinterpret_cast<int>(1<<31);
  for (int i=0; i<N-1; i++)
    if (r[i]+r[i+1] > summ)
      summ = r[i]+r[i+1];
  fo << summ << endl;
  for (int i=0; i<N; i++)
    fo << r[i] << " ";
}
int main(void)
{
  read_data();
  solve();
  write_data();
  delete[]a;
  delete[]r;
  return 0;
}
