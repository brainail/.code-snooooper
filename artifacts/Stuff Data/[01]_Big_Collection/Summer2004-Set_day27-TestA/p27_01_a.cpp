#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 256

struct REB {
  int a;
  REB *p;
};

FILE *fi, *fo;
//int p[NN];
REB swap[1000];
int iswap=0;
REB *g[NN];
bool used[NN];
bool bused[NN];
int q[NN];
int r[NN];
int lex;
int top;

REB *getnew() {
  return &swap[iswap++];
}

void Add(int x, int y) {
  REB *t;
  t=getnew();
  t->a=y;
  t->p=g[x];
  g[x]=t;
}

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

int getlex() {
  int c;
  c=getcc();
  if(c=='-')
    c=getcc();
  return c;
}

int read_tree(int d) {
  int c;
  if(isalpha(lex)) {
    c=lex-'A';
    used[c]=true;
    lex=getlex();
    if(lex=='>') {
      lex=getlex();
      read_tree(c);
    }
    if(d>=0) {
      Add(c,d);
      Add(d,c);
    } else {
      top=c;
    }
    return c;
  }
  if(lex=='(') {
    while(lex!=')') {
      lex=getlex();
      c=read_tree(d);
    }
    if(d<0) {
      d=top;
    }
    lex=getlex();
    if(lex=='>') {
      lex=getlex();
      read_tree(c);
    }
    return c;
  }
  return -1;
}

void bfs(int st) {
  int b,e;
  int i;
  REB *t;
  b=0;
  e=1;
  q[0]=st;
  for(i=0;i<NN;i++) {
    bused[i]=false;
    r[i]=10000000;
  }
  r[st]=0;
  bused[st]=true;
  while(b<e) {
    for(t=g[q[b]];t!=NULL;t=t->p) {
      if(!bused[t->a]) {
        bused[t->a]=true;
        r[t->a]=r[q[b]]+1;
        q[e++]=t->a;
      }
    }
    b++;
  }
}

int main() {
  int i;
  fi=fopen("dist.in","rt");
  fo=fopen("dist.out","wt");

  for(i=0;i<NN;i++) {
    used[i]=false;
    g[i]=NULL;
  }
  top=-1;
  lex=getlex();
  read_tree(-1);
  bfs(top);
  for(i=0;i<NN;i++) {
    if(used[i]) {
      fprintf(fo,"%c %d\n",i+'A',r[i]);
    }
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
