#include <stdio.h>

#define m 300
#define mm 250
#define em 20000

char s[m], l, let[em];
int p, r[em], rr, next[em], h[em], n, a[250], d[250], mark[250], q[250],
    _stklen = 8 * 1024 * 1024;

void add( int x, int y )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
}

void get( void )
{
  l = s[++p];
  if (l == '-')
    p++;
}

int tree( void );

int list( int *y )
{
  int t = 1;

  y[1] = tree();
  while (l == ',')
  {
    get();
    y[++t] = tree();
  }
  return t;
}

int forest( int *y )
{
  int t;

  if (l == '(')
  {
    get();
    t = list(y);
    get();
    return t;
  }
  else
  {
    y[1] = tree();
    return 1;
  }
}

int tree( void )
{
  int k, y[mm], t, i;

  if (l == '(')
  {
    get();
    k = tree();
    get();
  }
  else
  {
    if (!a[(int)l])
    {
      let[++n] = l;
      k = a[(int)l] = n;
    }
    else
      k = a[(int)l];
    get();
  }
  if (l != '-')
    return k;
  get();
  t = forest(y);
  for (i = 1; i <= t; i++)
  {
    add(let[k], let[y[i]]);
    add(let[y[i]], let[k]);
  }
  return k;
}

int main( void )
{
  int k, l, i;

  freopen("dist.in", "r", stdin);
  freopen("dist.out", "w", stdout);
  scanf("%s", &s[1]);
  p = 0;
  get();
  k = tree();
  mark[(int)let[k]] = 1;
  q[l = 1] = let[k];
  for (p = 1; p <= l; p++)
    for (i = h[q[p]]; i != 0; i = next[i])
      if (!mark[r[i]])
      {
        mark[r[i]] = 1;
        q[++l] = r[i];
        d[r[i]] = d[q[p]] + 1;
      }
  for (i = 'A'; i <= 'Z'; i++)
    if (a[i])
      printf("%c %d\n", i, d[i]);
  return 0;
}

