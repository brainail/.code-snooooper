#define ONLINE
#include <stdio.h>
#include <string.h>

#define STR_SIZE 514
#define N_SYMS 26

#define LEX_SYM 0x01
#define LEX_CHILD 0x02
#define LEX_LIST 0x03
#define LEX_BR_OP 0x04
#define LEX_BR_CL 0x05
#define LEX_END 0x08
#define LEX_ERROR 0x09

class TParser {
  private:
    char*data,*p_data,cur;
  public:
    TParser(int);
    virtual ~TParser();
    void scan(void);
    void rewind(void);
    char getsym(void);
    char cursym(void) const { return(cur); }
    char nextsym(void) const { return(*p_data); }
};
class TExprParser: public TParser {
  private:
    int lex;
    char sym;
  public:
    TExprParser(int);
    virtual ~TExprParser(void);
    int getlex(void);
    int lastlex(void) const { return(lex); }
    char getvalue(void) const { return(sym); }
};

class TGraph {
  private:
    int n_verts,n_cells,start;
    char*matrix_cont,**matrix;
    char*exists; int*level;
    int*que; int que_len,que_pos;
    TExprParser*parser;
  protected:
    static int sym2idx(char sym);
    static char idx2sym(int idx);
    int instance(int,int,int);
    int parse(int);
    void search(int);
  public:
    TGraph(int,int);
    ~TGraph();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printdists(void);
    void finddists(void);
};

TGraph graph(N_SYMS,STR_SIZE);

int main(void) {
  #ifdef ONLINE
  freopen("dist.in","rt",stdin);
  freopen("dist.out","wt",stdout);
  #endif
  graph.scan();
  graph.finddists();
  graph.printdists();
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void TGraph::search(int cur) {
  int to;
  que_len=1; *que=cur; level[cur]=0;
  for (que_pos=0; que_pos<que_len; que_pos++) {
    cur=que[que_pos];
    for (to=0; to<n_verts; to++) if (matrix[cur][to]) {
      if (level[to]>=0) continue;
      que[que_len++]=to; level[to]=level[cur]+1;
    }
  }
}
void TGraph::finddists(void) {
  memset(level,-1,sizeof(int)*n_verts);
  search(start);
}

int TGraph::parse(int from) {
  int to=-1,to_main=-1;
  switch (parser->lastlex()) {
    case LEX_SYM:
      to=sym2idx(parser->getvalue());
      to_main=instance(from,to,to_main);
      if (parser->getlex()==LEX_CHILD) {
        parser->getlex();
        parse(to);
      }
      break;
    case LEX_BR_OP:
      do {
        parser->getlex();
        to=parse(-1);
        to_main=instance(from,to,to_main);
      } while (parser->lastlex()==LEX_LIST);
      if (parser->getlex()==LEX_CHILD) {
        parser->getlex();
        parse(to_main);
      }
      break;
  }
  return(to_main);
}
int TGraph::instance(int from,int to,int to_main) {
  exists[to]=1;
  if (from>=0) { matrix[from][to]=1; matrix[to][from]=1; }
  if (to_main<0) return(to); else return(to_main);
}

void TGraph::scan(void) {
  parser->scan();
  memset(matrix_cont,0,sizeof(char)*n_cells);
  memset(exists,0,sizeof(char)*n_verts);
  parser->getlex();
  start=parse(-1);
}
void TGraph::printdists(void) {
  int vert;
  for (vert=0; vert<n_verts; vert++) if (exists[vert]) {
    printf("%c %d\n",idx2sym(vert),level[vert]);
  }
}

int TGraph::sym2idx(char sym) {
  sym-='A';
  int idx=(int)sym;
  idx=(idx%N_SYMS+N_SYMS)%N_SYMS;
  return(idx);
}
char TGraph::idx2sym(int idx) {
  char sym=(char)idx;
  sym+='A';
  return(sym);
}

TGraph::TGraph(int n_syms,int parser_data_size) {
  n_verts=n_syms;
  n_cells=n_verts*n_verts;
  Create();
  parser=new TExprParser(parser_data_size);
}
TGraph::~TGraph() {
  delete parser;
  Delete();
}
void TGraph::Create(void) {
  int i_vert,i_cell;
  matrix_cont=new char[n_cells]; matrix=new char*[n_verts];
  for (i_vert=0,i_cell=0; i_vert<n_verts; i_vert++,i_cell+=n_verts) {
    matrix[i_vert]=&matrix_cont[i_cell];
  }
  exists=new char[n_verts]; level=new int[n_verts]; que=new int[n_verts];
}
void TGraph::Delete(void) {
  delete[]matrix_cont; delete[]matrix;
  delete[]exists; delete[]level; delete[]que;
}


int TExprParser::getlex(void) {
  sym=getsym();
  if (!sym) return(lex=LEX_END);
  switch (sym) {
    case '-': sym=getsym();
      if (sym=='>') return(lex=LEX_CHILD);
      return(lex=LEX_ERROR);
    case ',': return(lex=LEX_LIST);
    case '(': return(lex=LEX_BR_OP);
    case ')': return(lex=LEX_BR_CL);
  }
  if ((sym>='a')&&(sym<='z')) sym-='a'-'A';
  if ((sym>='A')&&(sym<='Z')) return(lex=LEX_SYM);
  return(lex=LEX_ERROR);
}

TExprParser::TExprParser(int data_size): TParser(data_size) {
}
TExprParser::~TExprParser(void) {
}
    

void TParser::scan(void) {
  scanf("%s",data);
  rewind();
}
void TParser::rewind(void) {
  for (p_data=data; (*p_data<=32)&&(*p_data); p_data++);
  getsym();
}
char TParser::getsym(void) {
  char ret=cur;
  cur=*p_data;
  if (*p_data) for (p_data++; (*p_data<=32)&&(*p_data); p_data++);
  return(ret);
}

TParser::TParser(int data_size) {
  data=new char[data_size];
}
TParser::~TParser() {
  delete[]data;
}




