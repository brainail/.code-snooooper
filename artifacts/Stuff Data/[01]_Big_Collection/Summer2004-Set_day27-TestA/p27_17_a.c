/* 27.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 256

char S[MAX_LEN + 1];
int Root;
int M[30];
int C[30][30];
int U[30], D[30];
int A1[100], A2[100];
int Size1, Size2;

/* Reads the graph */
int Read( int prev, int l, int r )
{
  int root, node, pos = l, ln = 0, rn = 0, pos1;

  if (S[pos] != '(')
  {
    root = S[pos] - 'A';
    M[root] = 1;
    pos++;
    if (pos <= r)
    {
      pos += 2;
      node = Read(root, pos, r);
      C[root][node] = C[node][root] = 1;
    }
  }
  else
  {
    pos++;
    ln = 1;
    pos1 = pos;
    while (S[pos1] != ')')
    {
      for (pos1 = pos; ln != rn + 1 ||
           (S[pos1] != ',' && S[pos1] != ')'); pos1++)
        if (S[pos1] == '(')
          ln++;
        else if (S[pos1] == ')')
          rn++;
      root = Read(root, pos, pos1 - 1);
      C[root][prev] = C[prev][root] = 1;
      pos = pos1 + 1;
    }
    if (pos <= r)
    {
      pos += 2;
      node = Read(root, pos, r);
      C[root][node] = C[node][root] = 1;
    }
  }
  return root;
} /* End of 'Read' function */

/* The main program function */
int main( void )
{
  int i, j, len;
  
  freopen("dist.in", "r", stdin);
  freopen("dist.out", "w", stdout);
  for (i = 0; i < 26; i++)
    M[i] = 0;
  for (i = 0; i < 26; i++)
    for (j = 0; j < 26; j++)
      C[i][j] = 0;
  scanf("%s", S);
  Root = Read(26, 0, strlen(S) - 1);
  for (i = 0; i < 26; i++)
    U[i] = 0, D[i] = 999;
  Size1 = 1;
  A1[0] = Root;
  U[Root] = 1;
  D[Root] = 0;
  len = 0;
  while (Size1 != 0)
  {
    len++;
    Size2 = 0;
    for (i = 0; i < Size1; i++)
      for (j = 0; j < 26; j++)
        if (C[A1[i]][j] == 1)
          if (U[j] == 0)
          {
            D[j] = len;
            U[j] = 1;
            A2[Size2++] = j;
          }
    Size1 = Size2;
    for (i = 0; i < Size1; i++)
      A1[i] = A2[i];
  }
  for (i = 0; i < 26; i++)
    if (M[i])
      printf("%c %i\n", i + 'A', D[i]);
  return 0;
} /* End of 'main' function */

