/* ID: 26 - Bukhalenkov Alexander */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;

// DON`T FORGET!!!
#define DBG if(0)

const int INF = 1000;
const int NMAX = 26;

char *names;//index2name
int idx[256];//name2index

char vmain;
int **am;
int N;

int *dist;

struct List {
  int num;
  List *next;
};

char s[256];
int n;
int cur=0;

int read_tree();

void free_list(List *x)
{
    List *y;
    while (x) {
      y = x;
      x = x->next;
      delete y;
    }
}
List *read_forest()
{
  int count = 0;
  List *r = new List;
  List *x = r;
  int oldcur = cur;
  bool wasopen = (s[cur]=='(');
  if (wasopen)
    cur++;

  while (1)
  {
    List *t = new List;
    x->next = t;
    t->num = read_tree();
    t->next = 0;
    x = t;
    count ++;
    if ((!wasopen) || s[cur] != ',')
      break;
    cur++;
  }
  DBG if (count != 1) cerr << "count == " << count << endl;
  // if we had read (tree), not (forest)
  if (count == 1)
  {
  #if 0
    List *t = new List;
    t->next = 0;
    t->num = x->num;
    free_list(r);
    if (wasopen)
      cur++;
    return t;
  #else
    free_list(r);
    List *t = new List;
    t->next = 0;
    cur = oldcur;
    t->num = read_tree();
    return t;
  #endif
  }
  
  if (wasopen)
    cur++;
  x = r->next;
  delete r;
  return x;
}

int read_tree()//returns main vertex
{
  int r;
  if (s[cur]=='(') {
    cur++;
    r = read_tree();
    cur++;//miss ')'
  } else {
    r = idx[s[cur]];
    cur++;
  }
  if (cur<n && s[cur]=='-')
  {
    cur+=2;//miss '->'
    List *l = read_forest();
    List *x = l;
    for (; l; l=l->next) {
      am[r][l->num] = am[l->num][r] = 1;
    }
    // free memory
    free_list(x);
  }
  return r;
}

void read_graph()
{
  {
    ifstream fi("dist.in");
    fi >> s;
    n = strlen(s);
  }
  // parse
  names = new char[NMAX];
  for (int i=0; i<256; i++) idx[i] = -1;
  N = 0;
  for (int i=0; i<n; i++) {
    if (s[i] >= 'A' && s[i] <= 'Z') {
      if (idx[s[i]] == -1) {
        names[N] = s[i];
        idx[s[i]] = N;
        N++;
      }
    }
  }  /*
  for (int i=0;i<n;i++) {
      if (s[i] >= 'A' && s[i] <= 'Z') {
        vmain = s[i];
        break;
      }
  }*/
  dist = new int[N];
  am = new int*[N];
  for (int i=0;i<N;i++) {
    am[i] = new int[N];
    for (int j=0; j<N; j++)
      am[i][j] = INF;
    dist[i] = -1;
  }

  vmain = read_tree();
  
}

void solve()
{
  for (int i=0; i<N; i++)
    dist[i] = INF;
  dist[vmain] = 0;
  // Ford-Belman`s sssp
  for (int k=0; k<N; k++)
  {
    for (int i=0; i<N; i++)
    {
      for (int j=0; j<N; j++)
      {
        int x = dist[j] + am[i][j];
        if (x < dist[i])
          dist[i] = x;
      }
    }
  }
}

void out_data()
{
  ofstream fo("dist.out");
  for (int c='A'; c<='Z'; c++) {
    if (idx[c] != -1) {
      fo << (char)c << ' ' << dist[idx[c]] << endl;
    }
  }
}

int main(void)
{
  read_graph();
  solve();
  out_data();
  
  delete[]names;
  for (int i=0;i<N;i++) delete[]am[i];
  delete[]am;
  delete[]dist;
  
  return 0;
}
