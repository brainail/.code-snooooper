#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 1010
#define LL 101

FILE *fi, *fo;
unsigned long long a[NN][NN];
int f[NN][LL];
int b[NN];
int n;

void sum(int *a, int *b) {
  int r;
  int i;
  r=0;
  for(i=LL-1;i>=0;i--) {
    a[i]+=b[i]+r;
    r=a[i]/10;
    a[i]%=10;
  }
}

void vich(int *a, int *b) {
  int r;
  int i;
  r=0;
  for(i=LL-1;i>=0;i--) {
    a[i]-=b[i]+r;
    r=0;
    while(a[i]<0) {
      a[i]+=10;
      r++;
    }
  }
}

void wr(int *a) {
  int i;
  for(i=0;(i<LL-1)&&(a[i]==0);i++);
  for(;i<LL;i++) {
    fprintf(fo,"%d",a[i]);
  }
  fprintf(fo,"\n");
}

int main() {
  int i,j,k,l;
  bool flag;
  unsigned long long ans;
  fi=fopen("young.in","rt");
  fo=fopen("young.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++)
    fscanf(fi,"%d",&b[i]);
    
  if((b[0]==n)&&(n>1)) {
    fprintf(fo,"0\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }

  flag=true;
  for(i=1;i<n;i++) {
    if(b[i-1]>b[i]) {
      flag=false;
      break;
    }
  }
  if(flag) {
    for(i=0;i<=n;i++) {
      for(j=0;j<LL;j++) {
        f[i][j]=0;
      }
    }
    f[0][LL-1]=1;
    for(i=1;i<=n;i++) {
      for(j=1;j*(3*j+1)/2<=i;j++) {
        if(j%2)
          sum(f[i],f[i-j*(3*j+1)/2]);
        else
          vich(f[i],f[i-j*(3*j+1)/2]);
      }
      for(j=1;j*(3*j-1)/2<=i;j++) {
        if(j%2)
          sum(f[i],f[i-j*(3*j-1)/2]);
        else
          vich(f[i],f[i-j*(3*j-1)/2]);
      }
    }
    wr(f[n]);
    fclose(fi);
    fclose(fo);
    return 0;
  }

  for(i=0;i<n;i++)
    a[0][i]=1LL;
/*  for(i=1;i<n;i++) {
    for(j=0;j<=i;j++) {
      a[i][j]=0LL;
      for(k=j;k<=n-i-1;k++) {
        if(k>1)
          if(b[n-1-i+k-1]>b[n-1-i+k])
            break;
        flag=true;
        for(l=0;l<=k;l++) {
          if(b[n-3-i-j+l]>b[n-1-i+l]) {
            flag=false;
            break;
          }
        }
        if(flag) {
          a[i][j]+=a[i-k-1][k];
        }
      }
    }
  }*/
  for(i=1;i<=n;i++) {
    for(j=1;j<=i;j++) {
      a[i][j]=0;
      flag=true;
      for(k=1;k<j;k++) {
        if(b[i-j+k-1]>b[i-j+k]) {
          flag=false;
          break;
        }
      }
      if(!flag)
        break;
      if(i==j) {
        a[i][j]=1LL;
        break;
      }
      for(k=j;k<=i-j;k++) {
        flag=true;
        if(a[i-j][k]>0) {
          for(l=0;l<j;l++) {
            if(b[i-j-k+l]>b[i-j+l]) {
              flag=false;
              break;
            }
          }
        }
        if(flag) {
          a[i][j]+=a[i-j][k];
        }
      }
    }
  }
  ans=0LL;
  for(i=1;i<=n;i++) {
    ans+=a[n][i];
  }
  fprintf(fo,"%llu\n",ans);

  fclose(fi);
  fclose(fo);
  return 0;
}
