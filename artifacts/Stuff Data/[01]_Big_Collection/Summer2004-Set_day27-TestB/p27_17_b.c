/* 27.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>

#define MIN(A, B) ((A) < (B) ? (A) : (B))

#define MAX_N 1000

int N;
int A[MAX_N];
long Count = 0;

/* Seacrh function */
void Search( int Pos, int Max, int S )
{
  int i, j, min = MIN(Max, N - Pos);

  if (Pos == N)
    Count++;
  else if (A[S] < A[Pos])
  {
    Search(Pos + 1, 1, Pos);
    for (i = 2; i <= min; i++)
    {
      if (A[S + i - 1] > A[Pos + i - 1] || A[Pos + i - 2] > A[Pos + i - 1])
        break;
      Search(Pos + i, i, Pos);
    }
  }
} /* End of 'Search' function */

/* The main program function */
int main( void )
{
  int i;

  freopen("young.in", "r", stdin);
  freopen("young.out", "w", stdout);
  scanf("%i", &N);
  for (i = 0; i < N; i++)
    scanf("%i", &A[i]);
  for (i = 1; i < N && A[i - 1] <= A[i]; i++)
    ;
  for (i = i; i >= 1; i--)
    Search(i, i, 0);
  printf("%li\n", Count);
  return 0;
} /* End of 'main' function */

