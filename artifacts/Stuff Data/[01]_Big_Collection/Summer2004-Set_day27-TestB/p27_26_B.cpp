/* ID: 26 - Bukhalenkov Alexander */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;

#define DBG if(0)

const int NMAX = 1001;
int a[NMAX];
int N;

struct Range {
  int l,r;     // == [l,r)
};
Range t[NMAX];//current table

inline bool fits_row(int row, int col, int elem)
{
  return (col==0) || (elem > a[ t[row].l + col-1 ]);
}
inline bool fits_col(int row, int col, int elem)
{
  return (row==0) || (elem > a[ t[row-1].l + col ]);
}

int solve(int row)
{
  int r = 0;
  int start =(row==0) ? (0) : (t[row-1].r);
  t[row].l = start;
  for (int i=start;
       i<N && fits_row(row,i-start,a[i]) && fits_col(row,i-start,a[i]);
       i++)
  {
    t[row].r = i+1;
    if (i+1 == N) r++;// or "return (r+1);"
    else r += solve(row+1);
  }
  return r;
}

int main(void)
{
  {
    ifstream fi("young.in");
    fi >> N;
    for (int i=0; i<N; i++)
      fi >> a[i];
  }
  int R;
  if (a[0]==N) R=0;

  else
    R = solve(0);
  {
    ofstream fo("young.out");
    fo << R;
  }
  return 0;
}

