#include <stdio.h>
#include <stdlib.h>

#define NN 110
#define MANY 10000000

struct POINT {
  int x,y;
};

FILE *fi, *fo;
bool used[NN][NN];
int r[NN][NN];
POINT d[NN][NN];
POINT q[NN*NN+10];
int pp,qq;
int n,m;

bool bfs_pos(int x, int y) {
  if(x<0) return false;
  if(y<0) return false;
  if(x>=n) return false;
  if(y>=m) return false;
  return true;
}

void bfs(POINT st) {
  int b,e;
  int i,j;
  POINT p;
  for(i=0;i<n;i++) {
    for(j=0;j<m;j++) {
      r[i][j]=MANY;
      used[i][j]=false;
      d[i][j].x=-1;
      d[i][j].y=-1;
    }
  }
  q[0]=st;
  b=0;
  e=1;
  used[st.x][st.y]=true;
  r[st.x][st.y]=0;
  while(b<e) {
    p=q[b];
    p.x+=pp;
    p.y+=qq;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x+=pp;
    p.y-=qq;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x-=pp;
    p.y+=qq;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x-=pp;
    p.y-=qq;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x+=qq;
    p.y+=pp;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x+=qq;
    p.y-=pp;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x-=qq;
    p.y+=pp;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    p=q[b];
    p.x-=qq;
    p.y-=pp;
    if(bfs_pos(p.x,p.y)) {
      if(!used[p.x][p.y]) {
        r[p.x][p.y]=r[q[b].x][q[b].y]+1;
        used[p.x][p.y]=true;
        d[p.x][p.y]=q[b];
        q[e++]=p;
      }
    }
    b++;
  }
}

void wr(POINT p) {
  if(p.x<0)
    return ;
  if(p.y<0)
    return ;
  wr(d[p.x][p.y]);
  fprintf(fo,"%d %d\n",p.x+1,p.y+1);
}

int main() {
  int x1,y1;
  int x2,y2;
  POINT p;
  fi=fopen("horse.in","rt");
  fo=fopen("horse.out","wt");

  fscanf(fi,"%d%d",&n,&m);
  fscanf(fi,"%d%d",&pp,&qq);
  fscanf(fi,"%d%d",&x1,&y1);
  x1--; y1--;
  fscanf(fi,"%d%d",&x2,&y2);
  x2--; y2--;
  p.x=x1;
  p.y=y1;
  bfs(p);
  if(!used[x2][y2]) {
    fprintf(fo,"-1\n");
  } else {
    fprintf(fo,"%d\n",r[x2][y2]);
    p.x=x2;
    p.y=y2;
    wr(p);
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
