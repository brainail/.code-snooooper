#include <stdio.h>

#define mm 102

int mark[mm][mm], d[mm][mm], q[mm * mm][2], w[mm * mm][2], prev[mm][mm];

int main( void )
{
  int i, j, t, x, y, xx, yy, p, m, n, l, a, b;

  freopen("horse.in", "r", stdin);
  freopen("horse.out", "w", stdout);
  scanf("%d%d%d%d%d%d%d%d", &m, &n, &a, &b, &x, &y, &xx, &yy);
  q[1][0] = x;
  q[1][1] = y;
  mark[x][y] = 1;
  for (p = l = 1; p <= l; p++)
  {
    if (mark[xx][yy])
      break;
    for (i = -1; i <= 1; i += 2)
      for (j = -1; j <= 1; j += 2)
        if (q[p][0] + a * i <= m && q[p][0] + a * i > 0
            && q[p][1] + b * j <= n && q[p][1] + b * j > 0
            && !mark[q[p][0] + a * i][q[p][1] + b * j])
        {
          q[++l][0] = q[p][0] + a * i;
          q[l][1] = q[p][1] + b * j;
          mark[q[p][0] + a * i][q[p][1] + b * j] = 1;
          d[q[p][0] + a * i][q[p][1] + b * j] = d[q[p][0]][q[p][1]] + 1;
          prev[q[p][0] + a * i][q[p][1] + b * j] = p;
        }
    for (i = -1; i <= 1; i += 2)
      for (j = -1; j <= 1; j += 2)
        if (q[p][0] + b * i <= m && q[p][0] + b * i > 0
            && q[p][1] + a * j <= n && q[p][1] + a * j > 0
            && !mark[q[p][0] + b * i][q[p][1] + a * j])
        {
          q[++l][0] = q[p][0] + b * i;
          q[l][1] = q[p][1] + a * j;
          mark[q[p][0] + b * i][q[p][1] + a * j] = 1;
          d[q[p][0] + b * i][q[p][1] + a * j] = d[q[p][0]][q[p][1]] + 1;
          prev[q[p][0] + b * i][q[p][1] + a * j] = p;
        }
  }
  if (!mark[xx][yy])
    printf("-1\n");
  else
  {
    printf("%d\n", d[xx][yy]);
    for (i = 0; prev[xx][yy] != 0; )
    {
      w[++i][0] = xx;
      w[i][1] = yy;
      t = prev[xx][yy];
      xx = q[t][0];
      yy = q[t][1];
    }
    printf("%d %d\n", x, y);
    for (j = i; j > 0; j--)
      printf("%d %d\n", w[j][0], w[j][1]);
  }
  return 0;
}
