#define ONLINE
#include <stdio.h>
#include <string.h>

#define DIRS_NUM 8

struct TPoint {
  short x,y;
  int valid(short n_x,short n_y) const {
    return((x>=0)&&(y>=0)&&(x<n_x)&&(y<n_y));
  }
  int index(short width) const {
    return( ((int)x)*((int)width)+((int)y) );
  }
  TPoint& set(int index,short width) {
    x=(short)(index/((int)width));
    y=(short)(index%((int)width));
    return(*this);
  };
  TPoint& operator+=(const TPoint&);
};

struct TMap {
  short xx,xy,yx,yy;
  TPoint operator*(const TPoint&arg) const {
    TPoint res;
    res.x=arg.x*xx+arg.y*xy;
    res.y=arg.x*yx+arg.y*yy;
    return(res);
  }
};

const TMap dirs[DIRS_NUM]={
  {-1,0,0,+1},{0,-1,+1,0},{0,+1,+1,0},{+1,0,0,+1},
  {+1,0,0,-1},{0,+1,-1,0},{0,-1,-1,0},{-1,0,0,-1}
};

class TGraph {
  private:
    int n_verts,start,target; short n_x,n_y;
    TPoint vect;
    int*level,*fromp;
    int*que; int que_len,que_pos;
    int*path; int path_len;
  protected:
    void search(int);
    void retain(int);
  public:
    TGraph(void);
    ~TGraph();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printpath(void);
    void findpath(void);
};

TGraph desk;

int main(void) {
  #ifdef ONLINE
  freopen("horse.in","rt",stdin);
  freopen("horse.out","wt",stdout);
  #endif
  desk.scan();
  desk.findpath();
  desk.printpath();
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}

void TGraph::search(int cur) {
  int to,i_dir; TPoint pnt_cur,pnt_to;
  memset(level,-1,sizeof(int)*n_verts);
  memset(fromp,-1,sizeof(int)*n_verts);
  que_len=1; *que=cur; level[cur]=0;
  for (que_pos=0; que_pos<que_len; que_pos++) {
    pnt_cur.set(cur=que[que_pos],n_y);
    for (i_dir=0; i_dir<DIRS_NUM; i_dir++) {
      pnt_to=pnt_cur;
      pnt_to+=dirs[i_dir]*vect;
      if (!pnt_to.valid(n_x,n_y)) continue;
      to=pnt_to.index(n_y);
      if (level[to]>=0) continue;
      que[que_len++]=to;
      level[to]=level[cur]+1;
      fromp[to]=cur;
    }
  }
}
void TGraph::retain(int cur) {
  if (level[cur]<0) { path_len=-1; return; }
  for (path_len=0; cur>=0; path_len++,cur=fromp[cur]) path[path_len]=cur;
}

void TGraph::scan(void) {
  Delete();
  scanf("%hd%hd",&n_x,&n_y);
  n_verts=((int)n_x)*((int)n_y);
  Create();
  scanf("%hd%hd",&vect.x,&vect.y);
  TPoint pnt;
  scanf("%hd%hd",&pnt.x,&pnt.y); pnt.x--; pnt.y--; start=pnt.index(n_y);
  scanf("%hd%hd",&pnt.x,&pnt.y); pnt.x--; pnt.y--; target=pnt.index(n_y);
}
void TGraph::printpath(void) {
  if (path_len>=0) {
    int path_pos; TPoint pnt;
    printf("%d\n",path_len-1);
    for (path_pos=path_len-1; path_pos>=0; path_pos--) {
      pnt.set(path[path_pos],n_y);
      printf("%hd %hd\n",pnt.x+1,pnt.y+1);
    }
  } else printf("-1\n");
}
void TGraph::findpath(void) {
  search(start);
  retain(target);
}

TGraph::TGraph(void) {
  n_verts=0; n_x=0; n_y=0;
  Create();
}
TGraph::~TGraph() {
  Delete();
}
void TGraph::Create(void) {
  level=new int[n_verts]; fromp=new int[n_verts];
  que=new int[n_verts]; path=new int[n_verts];
}
void TGraph::Delete(void) {
  delete[]level; delete[]fromp; delete[]que; delete[]path;
}


TPoint& TPoint::operator+=(const TPoint&arg) {
  x+=arg.x;
  y+=arg.y;
  return(*this);
}


