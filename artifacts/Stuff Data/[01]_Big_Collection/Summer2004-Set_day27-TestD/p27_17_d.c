/* 27.06.04 SK1 */

#include <stdio.h>

#define MAX_SIZE 200

long D[MAX_SIZE + 1][MAX_SIZE + 1];
long Prev[MAX_SIZE + 1][MAX_SIZE + 1][2];
long M, N, p, q, x1, y1, x2, y2;
long A1[MAX_SIZE * MAX_SIZE][2];
long A2[MAX_SIZE * MAX_SIZE][2];
long Size1, Size2;
long Path[MAX_SIZE * MAX_SIZE * 2][2];
long PN = 0;

/* Adds new point two the queue */
void Add( long x, long y, long i )
{
  long d;
  
  d = D[A1[i][0]][A1[i][1]];
  if (x >= 1 && x <= M && y >= 1 && y <= N)
    if (D[x][y] == -1 || D[x][y] > d + 1)
    {
      D[x][y] = d + 1;
      Prev[x][y][0] = A1[i][0];
      Prev[x][y][1] = A1[i][1];
      A2[Size2][0] = x;
      A2[Size2][1] = y;
      Size2++;
    }
} /* End of 'Add' function */

/* The main program function */
int main( void )
{
  long i, j, x, y, tx, ty;
  
  freopen("horse.in", "r", stdin);
  freopen("horse.out", "w", stdout);
  scanf("%li%li%li%li%li%li%li%li", &M, &N, &p, &q, &x1, &y1, &x2, &y2);
  for (i = 1; i <= M; i++)
    for (j = 1; j <= N; j++)
      D[i][j] = -1;
  D[x1][y1] = 0;
  Prev[x1][y1][0] = -1;
  Prev[x1][y1][1] = -1;
  Size1 = 1;
  A1[0][0] = x1;
  A1[0][1] = y1;
  while (Size1 != 0)
  {
    Size2 = 0;
    for (i = 0; i < Size1; i++)
    {
      Add(A1[i][0] + p, A1[i][1] + q, i);
      Add(A1[i][0] + p, A1[i][1] - q, i);
      Add(A1[i][0] - p, A1[i][1] + q, i);
      Add(A1[i][0] - p, A1[i][1] - q, i);
      Add(A1[i][0] + q, A1[i][1] + p, i);
      Add(A1[i][0] + q, A1[i][1] - p, i);
      Add(A1[i][0] - q, A1[i][1] + p, i);
      Add(A1[i][0] - q, A1[i][1] - p, i);
    }
    Size1 = Size2;
    for (i = 0; i < Size1; i++)
      A1[i][0] = A2[i][0], A1[i][1] = A2[i][1];
  }
  printf("%li\n", D[x2][y2]);
  if (D[x2][y2] != -1)
  {
    x = x2, y = y2;
    while (Prev[x][y][0] != -1)
    {
      Path[PN][0] = x;
      Path[PN][1] = y;
      PN++;
      tx = Prev[x][y][0];
      ty = Prev[x][y][1];
      x = tx, y = ty;
    }
    Path[PN][0] = x;
    Path[PN][1] = y;
    for (i = PN; i >= 0; i--)
      printf("%li %li\n", Path[i][0], Path[i][1]);
  }
  return 0;
} /* End of 'main' function */

