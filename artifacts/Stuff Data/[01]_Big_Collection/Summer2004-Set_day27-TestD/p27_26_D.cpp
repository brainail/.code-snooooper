/* ID: 26 - Bukhalenkov Alexander,  2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;
// doesn`t work when no way
#define DBG if(0)

ofstream fo("horse.out");

struct Point {
  Point() {}
  Point(int i,int j) : x(i), y(j) { }
  int x;
  int y;
};

const int INF = (INT_MAX/2)-12;
const int LENMAX=101;

int M,N;
int p,q;
int xs,ys,xe,ye;

inline int get_am(int i1,int j1, int i2, int j2)
{
  int di = abs(i1 - i2);
  int dj = abs(j1 - j2);
  if (di==p) {
    if (dj==q)
      return 1;
  } else if (di==q) {
    if (dj==p)
      return 1;
  }
  return INF;
}

Point prev[LENMAX][LENMAX];
bool U[LENMAX][LENMAX] = {false};
int d[LENMAX][LENMAX];

static Point Q[LENMAX*LENMAX];// queue
int Qb=0,Qe=0;//queue begin and end
inline void Qput(Point x) {
  Q[++Qe] = x;
}
inline Point Qget() {
  if (Qb<Qe)
    return Q[Qb++];
  else
    return Point(-1,-1);
}
int solve()
{

  for (int i=0; i<M; i++)
  {
    for (int j=0; j<N; j++) {
      prev[i][j].x=-1; prev[i][j].y=-1;
      d[i][j] = INF;
    }
  }
  d[xs][ys] = 0;
  U[xs][ys] = true;
  Qput(Point(xs,ys));
  while (U[xe][ye] == false)
  {
    Point p = Qget();
    if (p.x==-1)// no way
    {
      fo << "-1" << endl;
      exit(0);
    }
    U[p.x][p.y] = true;
    for (int i=0; i<M; i++)
      for (int j=0; j<N; j++)
        if (get_am(p.x, p.y, i,j)==1)
        {
          Qput(Point(i,j));
          if (d[i][j] > d[p.x][p.y]+1)
          {
            d[i][j] = d[p.x][p.y] + 1;
            prev[i][j].x = p.x;
            prev[i][j].y = p.y;
          }
        }
  }
  fo << d[xe][ye] << endl;
  static Point path[LENMAX*LENMAX];
  int x,y,n=0;
  x=xe; y=ye;
  while (1) {
    path[n].x = x;
    path[n].y = y;
    n++;
    if (x==xs && y==ys)
      break;
    int xn,yn;
    xn = prev[x][y].x;
    yn = prev[x][y].y;
    x=xn; y=yn;
  }
  DBG cerr << "n=="<<n <<endl;
  for (int i=n-1; i>=0; i--) {
    fo << path[i].x+1 << ' ' << path[i].y+1 << endl;
  }
}

int main(void)
{
  {
    ifstream fi("horse.in");
    fi >> M >> N >> p >> q >> xs>>ys >>xe>>ye;
    xs--;ys--;xe--;ye--;
  }
  solve();
  return 0;
}
