#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LL 500
#define BASE 10000

struct LLONG {
  int l;
  int a[LL];
};

FILE *fi, *fo;
LLONG res;
int n;

void umn(LLONG *a, int q) {
  int r,i;
  r=0;
  for(i=LL-1;i>=LL-a->l;i--) {
    a->a[i]=a->a[i]*q+r;
    r=a->a[i]/BASE;
    a->a[i]%=BASE;
  }
  while(r>0) {
    a->a[LL-(++(a->l))]=r%BASE;
    r/=BASE;
  }
}

void wr(LLONG *a) {
  int i;
  if(a->l==0) {
    fprintf(fo,"0\n");
    return ;
  }
  fprintf(fo,"%d",a->a[LL-a->l]);
  for(i=LL-a->l+1;i<LL;i++)
    fprintf(fo,"%04d",a->a[i]);
  fprintf(fo,"\n");
}

bool isprime(int x) {
  int i;
  for(i=2;i*i<=x;i++) {
    if(x%i==0)
      return false;
  }
  return true;
}

int inmaxpw(int x, int n) {
  int ans;
  ans=x;
  while(ans<=n)
    ans*=x;
  return ans/x;
}

int main() {
  int i,j;
  fi=fopen("lcm.in","rt");
  fo=fopen("lcm.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<LL;i++)
    res.a[i]=0;
  res.l=1;
  res.a[LL-1]=1;
  for(i=2;i<=n;i++) {
    if(isprime(i)) {
      j=inmaxpw(i,n);
      umn(&res,j);
    }
  }
  wr(&res);

  fclose(fi);
  fclose(fo);
  return 0;
}
