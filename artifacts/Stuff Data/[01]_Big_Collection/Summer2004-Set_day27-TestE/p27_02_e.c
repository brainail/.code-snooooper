#include <stdio.h>

#define o 100000
#define m 200

int d[m + 1], a, b, c, n;

int main( void )
{
  int i, j;

  freopen("lcm.in", "r", stdin);
  freopen("lcm.out", "w", stdout);
  scanf("%d", &n);
  if (n < 4)
  {
    if (n == 1)
      printf("1\n");
    else if (n == 2)
      printf("2\n");
    else if (n == 3)
      printf("6\n");
    return 0;
  }
  d[m] = 6;
  for (j = 4; j <= n; j++)
  {
    for (i = 1, c = 0; i <= m; i++)
    {
      c = c * o + d[i];
      c %= j;
    }
    a = j;
    b = c;
    while (b != 0)
    {
      c = a % b;
      a = b;
      b = c;
    }
    for (i = m, c = 0; i > 0; i--)
    {
      d[i] = d[i] * j / a + c;
      c = d[i] / o;
      d[i] %= o;
    }
  }
  for (i = 1; d[i] == 0 && i <= m; i++)
    ;
  printf("%d", d[i]);
  for (i++; i <= m; i++)
    printf("%05d", d[i]);
  printf("\n");
  return 0;
}

