#define ONLINE
//#define DEBUG
#include <stdio.h>
#include <string.h>

#define NUM_SIZE 128
#define NUM_DIG 10000

class TNum {
  private:
    long*data;
    int len,size;
  protected:
    void reset(void);
    void update(int);
  public:
    TNum(int);
    ~TNum();
    void print(void) const;
    TNum& operator=(long);
    TNum& operator*=(long);
};

short bound,*primes,*degs;
int n_primes;
TNum lcm(NUM_SIZE);

int main(void) {
  #ifdef ONLINE
  freopen("lcm.in","rt",stdin);
  freopen("lcm.out","wt",stdout);
  #endif
  scanf("%hd",&bound);
  short cur;
  int i_prime;
  primes=new short[bound];
  n_primes=0; if (bound>=2) primes[n_primes++]=2;
  for (cur=3; cur<=bound; cur+=2) {
    for (i_prime=0; i_prime<n_primes; i_prime++)
      if (!(cur%primes[i_prime])) break;
    if (i_prime<n_primes) continue;
    primes[n_primes++]=cur;
  }
  #ifdef DEBUG
  printf("%d\n",n_primes);
  for (i_prime=0; i_prime<n_primes; i_prime++) {
    if (i_prime) printf(" ");
    printf("%hd",primes[i_prime]);
  } printf("\n");
  #endif
  short buf,deg_cur;
  degs=new short[n_primes];
  memset(degs,0,sizeof(short)*n_primes);
  for (cur=1; cur<=bound; cur++) {
    buf=cur;
    for (i_prime=0; i_prime<n_primes; i_prime++) {
      for (deg_cur=0; !(buf%primes[i_prime]); deg_cur++,buf/=primes[i_prime]);
      if (degs[i_prime]<deg_cur) degs[i_prime]=deg_cur;
    }
  }
  #ifdef DEBUG
  for (i_prime=0; i_prime<n_primes; i_prime++) {
    if (i_prime) printf(" ");
    printf("%hd",degs[i_prime]);
  } printf("\n");
  #endif
  #ifdef DEBUG
  { long double lcm=1.L;
    for (i_prime=0; i_prime<n_primes; i_prime++)
     for (deg_cur=0; deg_cur<degs[i_prime]; deg_cur++)
      lcm*=(long double)primes[i_prime];
    printf("%.0Lf\n",lcm);
  }
  #endif
  lcm=1L;
  for (i_prime=0; i_prime<n_primes; i_prime++)
   for (deg_cur=0; deg_cur<degs[i_prime]; deg_cur++)
    lcm*=(long)primes[i_prime];
  lcm.print(); printf("\n");
  delete[]degs;
  delete[]primes;
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void TNum::reset(void) {
  memset(data,0,sizeof(long)*size); len=0;
}
void TNum::update(int pos) {
  for (len=pos; len>0; len--) if (data[len-1]) break;
}

void TNum::print(void) const {
  if (len>0) {
    int pos;
    printf("%ld",data[len-1]);
    for (pos=len-2; pos>=0; pos--) printf("%04ld",data[pos]);
  } else printf("0");
}

TNum& TNum::operator=(long arg) {
  reset();
  if (arg) { *data=arg; len++; }
  return(*this);
}
TNum& TNum::operator*=(long arg) {
  int pos;
  long reg=0;
  for (pos=0; ((pos<len)||reg)&&(pos<size); pos++) {
    data[pos]=data[pos]*arg+reg;
    reg=data[pos]/NUM_DIG;
    data[pos]%=NUM_DIG;
  }
  update(pos);
  return(*this);
}

TNum::TNum(int data_size) {
  data=new long[data_size];
  size=data_size;
}
TNum::~TNum() {
  delete[]data;
}


