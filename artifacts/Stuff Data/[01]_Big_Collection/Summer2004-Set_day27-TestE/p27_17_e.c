/* 27.06.04 SK1 */

#include <stdio.h>

#define MAX 1000
#define MAX_LEN 1000

long Is[MAX];
long PN = 0;
long P[MAX];
long Ans[MAX_LEN];
long N;

/* Mult */
void Mult( long k )
{
  long i, max;

  for (i = 0; i < MAX_LEN; i++)
    Ans[i] *= k;
  for (i = 0; i < MAX_LEN; i++)
    if (Ans[i] > 9)
      Ans[i + 1] += Ans[i] / 10, Ans[i] %= 10;
} /* End of 'Mult' function */

/* The main program function */
int main( void )
{
  long i, j;
  
  freopen("lcm.in", "r", stdin);
  freopen("lcm.out", "w", stdout);
  scanf("%li", &N);
  for (i = 0; i < MAX; i++)
    Is[i] = 1;
  for (i = 2; i < MAX; i++)
    if (Is[i])
      for (j = i << 1; j < MAX; j += i)
        Is[j] = 0;
  for (i = 2; i <= N; i++)
    if (Is[i])
      P[PN++] = i;
  for (i = 0; i < MAX_LEN; i++)
    Ans[i] = 0;
  Ans[0] = 1;
  for (i = 0; i < PN; i++)
  {
    for (j = P[i]; j <= N; j *= P[i])
      ;
    j /= P[i];
    Mult(j);
  }
  for (i = MAX_LEN - 1; Ans[i] == 0; i--)
    ;
  while (i >= 0)
    printf("%li", Ans[i--]);
  return 0;
} /* End of 'main' function */

