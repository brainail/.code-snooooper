/* ID: 26 - Bukhalenkov Alexander */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
using namespace std;
typedef unsigned long long int ullint;
#define DBG if(0)

ullint gcd(ullint a, ullint b)
{                          
  // a > b
  while (b)
  {
    ullint r = a%b;
    a = b;
    b = r;
  }
  return a;
}

inline ullint lcm(ullint a, ullint b)
{
  return (a*b/gcd(a,b));
}

int main(void)
{
  ifstream fi("lcm.in");
  int N;
  fi >> N;

  ullint x = 1;
  for (int i=1; i<=N; i++)
    x = lcm(x,i);

  ofstream fo("lcm.out");
  fo << x;
  return 0;
}
