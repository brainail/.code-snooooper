#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 10010
#define SS 10010

FILE *fi, *fo;
int a[NN];
int sign[NN];
int used[NN];
int b[SS];
//int d[SS];
int s;
int aa,n;
int sp;
int d;

void marksp(int v) {
  while(true) {
    if(v==0)
      return ;
    if(b[v]<=0)
      return ;
    used[b[v]]=1;
    v-=a[b[v]];
  }
}

int main() {
  int i,j;
  fi=fopen("nearest.in","rt");
  fo=fopen("nearest.out","wt");

  fscanf(fi,"%d%d",&n,&aa);
  s=0;
  for(i=0;i<n;i++) {
    fscanf(fi,"%d",&a[i]);
    if(a[i]<0)
      sign[i]=1;
    else
      sign[i]=0;
    a[i]=abs(a[i]);
    if(i>0) s+=a[i];
    used[i]=0;
  }
  for(i=0;i<SS;i++) b[i]=n+1;
  b[0]=0;
  for(i=0;i<SS;i++) {
    for(j=b[i]+1;j<n;j++) {
      if(b[i+a[j]]>j)
        b[i+a[j]]=j;
    }
  }
  if(sign[0])
    a[0]=-a[0];
  sp=0;
  d=a[0]-s;
  for(i=0;i<SS;i++) {
    if(b[i]<n) {
      j=a[0]+i-(s-i);
      if(abs(j-aa)<abs(d-aa)) {
        d=j;
        sp=i;
      }
    }
  }
  used[0]=1;
  marksp(sp);
  fprintf(fo,"%d\n",d);
  fprintf(fo,"%d",a[0]);
  for(i=1;i<n;i++) {
    if(sign[i]) {
      if(used[i]) {
        fprintf(fo,"-");
      } else {
        fprintf(fo,"+");
      }
      fprintf(fo,"-");
    } else {
      if(used[i]) {
        fprintf(fo,"+");
      } else {
        fprintf(fo,"-");
      }
    }
    fprintf(fo,"%d",a[i]);
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
