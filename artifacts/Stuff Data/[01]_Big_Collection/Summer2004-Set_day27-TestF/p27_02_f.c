#include <stdio.h>
#include <stdlib.h>

#define m 10002

char *c[m], *p[m], w[m];

int main( void )
{
  int n, s, i, j, a[20000], k, l[m], f = 0;

  freopen("nearest.in", "r", stdin);
  freopen("nearest.out", "w", stdout);
  scanf("%d%d", &n, &s);
  for (i = 1; i <= n; i++)
    scanf("%d", &a[i]);
  l[0] = 0;
  for (i = 1; i <= n; i++)
    l[i] = l[i - 1] + abs(a[i]);
  for (i = 0; i <= n; i++)
  {
    c[i] = (char *)malloc(2 * l[i] + 2);
    p[i] = (char *)malloc(2 * l[i] + 2);
  }
  for (i = 0; i <= n; i++)
    for (j = -l[i]; j <= l[i]; j++)
      c[i][j + l[i]] = 0;
  c[1][a[1] + l[1]] = 1;
  for (i = 2; i <= n; i++)
  {
    for (j = -l[i]; j <= l[i]; j++)
      if (j - a[i] >= -l[i - 1] && j - a[i] <= l[i - 1]
          && c[i - 1][j - a[i] + l[i - 1]])
      {
        c[i][j + l[i]] = 1;
        p[i][j + l[i]] = '+';
      }
      else if (j + a[i] >= -l[i - 1] && j + a[i] <= l[i - 1]
         && c[i - 1][j + a[i] + l[i - 1]])
      {
        c[i][j + l[i]] = 1;
        p[i][j + l[i]] = '-';
      }
  }
  for (k = 1000000000, i = -l[n]; i <= l[n]; i++)
    if (c[n][i + l[n]] && abs(k - s) > abs(i - s))
      k = i;
  printf("%d\n", k);
  for (i = n; i > 1; i--)
  {
    w[i] = p[i][k + l[i]];
    if (w[i] == '+')
      k -= a[i];
    else
      k += a[i];
  }
  printf("%d", a[1]);
  for (i = 2; i <= n; i++)
    printf("%c%d", w[i], a[i]);
  printf("\n");
  for (i = 0; i <= n; i++)
  {
    free(c[i]);
    free(p[i]);
  }
  return 0;
}

