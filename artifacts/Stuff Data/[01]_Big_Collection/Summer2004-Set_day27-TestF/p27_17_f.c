/* 27.06.04 SK1 */

#include <stdio.h>

#define ABS(A) ((A) > 0 ? (A) : -(A))

#define MAX_N 10000

long N, A, AA, Min = 1000000000L;
long X[MAX_N];
long Ans;
int S[MAX_N];
int Sign[MAX_N + 1];

/* The main program function */
int main( void )
{
  long i;

  freopen("nearest.in", "r", stdin);
  freopen("nearest.out", "w", stdout);
  scanf("%li%li", &N, &A);
  for (i = 0; i < N; i++)
    scanf("%li", &X[i]);
  for (i = 0; i <= N; i++)
    Sign[i] = 1;
  while (Sign[N] == 1 && Min != 0)
  {
    Ans = X[0];
    for (i = 1; i < N; i++)
      Ans += X[i] * Sign[i - 1];
    if (ABS(Ans - A) < Min)
    {
      Min = ABS(Ans - A);
      AA = Ans;
      for (i = 0; i < N; i++)
        S[i] = Sign[i];
    }
    i = 0;
    while (Sign[i] == -1)
      Sign[i++] = 1;
    Sign[i] = -1;
  }
  printf("%li\n", AA);
  printf("%li", X[0]);
  for (i = 1; i < N; i++)
    printf("%c%i", (S[i - 1] == 1 ? '+' : '-'), X[i]);
  return 0;
} /* End of 'main' funoction */

