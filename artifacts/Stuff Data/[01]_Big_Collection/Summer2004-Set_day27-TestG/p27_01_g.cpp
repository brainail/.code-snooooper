#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LL 50
#define SL 33
#define BASE 10000
#define KK 32

struct LLONG {
  int l;
  int a[LL];
};

FILE *fi, *fo;
LLONG cc[SL+1][SL+1];
LLONG res;
LLONG tmp;
int str[SL];
int sm[KK];
int length=0;

void umn(LLONG *a, int q) {
  int r,i;
  r=0;
  for(i=LL-1;i>=LL-a->l;i--) {
    a->a[i]=a->a[i]*q+r;
    r=a->a[i]/BASE;
    a->a[i]%=BASE;
  }
  while(r>0) {
    a->a[LL-(++(a->l))]=r%BASE;
    r/=BASE;
  }
}

void div(LLONG *a, int q) {
  int r,i;
  r=0;
  for(i=LL-a->l;i<LL;i++) {
    r=r*BASE+a->a[i];
    a->a[i]=r/q;
    r%=q;
  }
  while((a->a[LL-a->l]==0)&&(a->l>0)) {
    (a->l)--;
    if(a->l==0)
      break;
  }
}

void sum(LLONG *a, LLONG *b) {
  int r,i;
  r=0;
  if(a->l<b->l)
    a->l=b->l;
  for(i=LL-1;i>=LL-a->l;i--) {
    a->a[i]=a->a[i]+b->a[i]+r;
    r=a->a[i]/BASE;
    a->a[i]%=BASE;
  }
  while(r>0) {
    a->a[LL-(++(a->l))]=r%BASE;
    r/=BASE;
  }
}

void wr(LLONG *a) {
  int i;
  if(a->l==0) {
    fprintf(fo,"0\n");
    return ;
  }
  fprintf(fo,"%d",a->a[LL-a->l]);
  for(i=LL-a->l+1;i<LL;i++)
    fprintf(fo,"%04d",a->a[i]);
  fprintf(fo,"\n");
}

void set_one(LLONG *a) {
  int i;
  for(i=0;i<LL;i++)
    a->a[i]=0;
  a->a[LL-1]=1;
  a->l=1;
}

void set_zero(LLONG *a) {
  int i;
  for(i=0;i<LL;i++)
    a->a[i]=0;
  a->l=0;
}

/*void c(LLONG *a, int n, int k) {
  int i;
  set_one(a);
  for(i=n-k+1<=n;i++)
    umn(a,i);
  for(i=2;i<=k;i++)
    div(a,i);
}*/

void c2(LLONG *a, int n, int k) {
  int i;
  for(i=n-k+1;i<=n;i++)
    umn(a,i);
  for(i=2;i<=k;i++)
    div(a,i);
}

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

void getstr() {
  int c;
  int i;
  for(i=0;i<KK;i++)
    sm[i]=0;
  while((c=getcc())!=EOF) {
    str[length++]=c-'a';
    sm[c-'a']++;
  }
}

void solv(int last) {
  int i;
  set_one(&tmp);
  for(i=0;i<KK;i++) {
    if(last==0)
      return ;
    if(sm[i]>0) {
      c2(&tmp,last,sm[i]);
      last-=sm[i];
    }
  }
//  sum(&res,&tmp);
}

int main() {
  int i,j;
  fi=fopen("string.in","rt");
  fo=fopen("string.out","wt");

/*  for(i=1;i<=SL;i++) {
    for(j=1;j<=i;j++) {
      c(&cc[i][j],i,j);
    }
  }*/
  getstr();
  set_one(&res);
  for(i=0;i<length;i++) {
    for(j=0;j<str[i];j++) {
      if(sm[j]>0) {
        sm[j]--;
        solv(length-i-1);
        sum(&res,&tmp);
        sm[j]++;
      }
    }
    sm[str[i]]--;
  }
  wr(&res);

  fclose(fi);
  fclose(fo);
  return 0;
}
