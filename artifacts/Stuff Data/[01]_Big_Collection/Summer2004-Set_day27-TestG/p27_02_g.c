#include <stdio.h>

#define m 40

char s[m];
int n, let[m];
unsigned long long c[m][m];

int main( void )
{
  int i, n, j, ii, t;
  unsigned long long ss, k = 0;

  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);
  scanf("%s", &s[1]);
  n = strlen(&s[1]);
  for (i = 0; i <= n; i++)
  {
    c[i][0] = 1;
    c[i][i] = i;
  }
  for (i = 2; i <= n; i++)
    for (j = 1; j <= i; j++)
      c[i][j] = c[i - 1][j - 1] + c[i - 1][j];
  k = 0;
  for (i = 1; i <= n; i++)
    let[s[i] - 'a' + 1]++;
  for (i = 1; i <= n; i++)
  {
    for (j = 1; j < s[i] + 1 - 'a'; j++)
      if (let[j] > 0)
      {
        let[j]--;
        ss = 1;
        for (ii = 1, t = n - i; ii <= 26; ii++)
          if (let[ii] > 0)
          {
            ss *= c[t][let[ii]];
            t -= let[ii];
          }
        k += ss;
        let[j]++;
      }
    let[s[i] - 'a' + 1]--;
  }
  printf("%LU\n", k + 1);
  return 0;
}

