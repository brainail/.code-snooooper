#define ONLINE
//#define DEBUG
//#define APPROX
#include <stdio.h>
#include <string.h>

#define N_LETTERS 26
#define SEQ_SIZE 34

#define NUM_SIZE 32
#define NUM_DIG 10000

class TNum {
  private:
    long*data;
    int len,size;
  protected:
    void reset(void);
    void update(int);
    int cmp(const TNum&) const;
  public:
    TNum(int);
    ~TNum();
    void print(void) const;
    TNum& operator=(long);
    TNum& operator=(const TNum&);
    TNum& operator+=(const TNum&);
    TNum& operator*=(long);
    TNum& operator/=(long);
    int operator<(const TNum&arg) const { return(cmp(arg)<0); }
    int operator>(const TNum&arg) const { return(cmp(arg)>0); }
    int operator<=(const TNum&arg) const { return(cmp(arg)<=0); }
    int operator>=(const TNum&arg) const { return(cmp(arg)>=0); }
};

int seq[SEQ_SIZE]; int len;
int c_letter[N_LETTERS],c_total;
#ifndef APPROX
TNum idx(NUM_SIZE),total(NUM_SIZE),next(NUM_SIZE);
#else
long double idx,total,next;
#endif

int main(void) {
  #ifdef ONLINE
  freopen("string.in","rt",stdin);
  freopen("string.out","wt",stdout);
  #endif
  { char*str=new char[SEQ_SIZE+2],*p_str;
    scanf("%s",str);
    memset(c_letter,0,sizeof(int)*N_LETTERS); c_total=0; len=0;
    for (p_str=str; *p_str; p_str++) {
      seq[len]=((*p_str-'a')%N_LETTERS+N_LETTERS)%N_LETTERS;
      c_letter[seq[len++]]++; c_total++;
    }
    delete[]str;
  }
  int i_let,c_cur;
  total=1;
  for (c_cur=1; c_cur<=c_total; c_cur++) total*=(long)c_cur;
  for (i_let=0; i_let<N_LETTERS; i_let++) {
    for (c_cur=1; c_cur<=c_letter[i_let]; c_cur++) total/=(long)c_cur;
  }
  #ifdef DEBUG
  #ifndef APPROX
  total.print(); printf("\n");
  #else
  printf("%.0Lf.\n",total);
  #endif
  #endif
  int pos;
  idx=1L;
  for (pos=0; pos<len; pos++) {
    for (i_let=0; i_let<seq[pos]; i_let++) if (c_letter[i_let]>0) {
      next=total; next*=(long)c_letter[i_let]; next/=(long)c_total;
      idx+=next;
    } total*=(long)c_letter[i_let]--; total/=(long)c_total--;
  }
  #ifndef APPROX
  idx.print(); printf("\n");
  #else
  printf("%.0Lf.\n",idx);
  #endif
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void TNum::reset(void) {
  memset(data,0,sizeof(long)*size); len=0;
}
void TNum::update(int pos) {
  for (len=pos; len>0; len--) if (data[len-1]) break;
}

void TNum::print(void) const {
  if (len>0) {
    int pos;
    printf("%ld",data[len-1]);
    for (pos=len-2; pos>=0; pos--) printf("%04ld",data[pos]);
  } else printf("0");
}

TNum& TNum::operator=(long arg) {
  reset();
  if (arg) { *data=arg; len++; }
  return(*this);
}
TNum& TNum::operator=(const TNum&arg) {
  reset();
  memcpy(data,arg.data,sizeof(long)*(arg.size<=size?arg.size:size));
  len=(arg.len<=size?arg.len:size);
  return(*this);
}
TNum& TNum::operator+=(const TNum&arg) {
  int pos;
  long reg=0;
  for (pos=0; ((pos<len)||(pos<arg.len)||reg)&&(pos<size); pos++) {
    data[pos]+=arg.data[pos]+reg;
    reg=data[pos]/NUM_DIG;
    data[pos]%=NUM_DIG;
  }
  update(pos);
  return(*this);
}
TNum& TNum::operator*=(long arg) {
  int pos;
  long reg=0;
  for (pos=0; ((pos<len)||reg)&&(pos<size); pos++) {
    data[pos]=data[pos]*arg+reg;
    reg=data[pos]/NUM_DIG;
    data[pos]%=NUM_DIG;
  }
  update(pos);
  return(*this);
}
TNum& TNum::operator/=(long arg) {
  int pos;
  long reg=0;
  for (pos=len-1; pos>=0; pos--) {
    data[pos]+=reg*NUM_DIG;
    reg=data[pos]%arg;
    data[pos]/=arg;
  }
  update(len);
  return(*this);
}

int TNum::cmp(const TNum&arg) const {
  int pos=len;
  if (pos<arg.len) pos=arg.len;
  if (pos>size) return(-1);
  if (pos>arg.size) return(+1);
  for (pos--; pos>=0; pos--) {
    if (data[pos]<arg.data[pos]) return(-1);
    if (data[pos]>arg.data[pos]) return(+1);
  }
  return(0);
}

TNum::TNum(int data_size) {
  data=new long[data_size];
  size=data_size;
}
TNum::~TNum() {
  delete[]data;
}


