/* 27.06.04 SK1 */

#include <stdio.h>
#include <string.h>

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))

#define MAX_LEN1 32
#define MAX_LEN2 40

char S1[MAX_LEN1 + 1];
char S2[MAX_LEN1 + 1];
char S3[MAX_LEN1 + 1];
int L[26];
long Num[MAX_LEN2], X[MAX_LEN2];

/* Qucik sort */
void QSort( char *S, int l, int r )
{
  char x = S[(l + r + 1) >> 1], tmp;
  int i = l, j = r;

  if (l >= r)
    return;
  while (i < j)
  {
    while (S[i] < x)
      i++;
    while (S[j] > x)
      j--;
    if (i < j)
      SWAP(S[i], S[j], tmp);
    if (i <= j)
      i++, j--;
  }
  QSort(S, l, j);
  QSort(S, i, r);
} /* End of 'QSort' function */

/* Addition */
void Add( long *a, long *b )
{
  int i;

  for (i = 0; i < MAX_LEN2; i++)
    a[i] += b[i];
  for (i = 0; i < MAX_LEN2; i++)
    if (a[i] > 9)
      a[i + 1]++, a[i] -= 10;
} /* End of 'Add' function */

/* Mult */
void Mult( long *a, long k )
{
  int i;

  for (i = 0; i < MAX_LEN2; i++)
    a[i] *= k;
  for (i = 0; i < MAX_LEN2; i++)
    if (a[i] > 9)
      a[i + 1] += a[i] / 10, a[i] %= 10;
} /* End of 'Mult' function */

/* Division */
void Div( long *a, long k )
{
  int i;

  for (i = MAX_LEN2 - 1; a[i] == 0; i--)
    ;
  while (i > 0)
    a[i - 1] += (a[i] % k) * 10, a[i] /= k, i--;
  a[0] /= k;
  for (i = 0; i < MAX_LEN2; i++)
    if (a[i] > 9)
      a[i + 1] += a[i] / 10, a[i] %= 10;
} /* End of 'Div' function */

/* The main program function */
int main( void )
{
  int i, j, k, h, g, len;
  char tmp;
  
  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);
  scanf("%s", S1);
  len = strlen(S1);
  memset(Num, 0, sizeof(long) * MAX_LEN2);
  Num[0] = 1;
  for (i = 0; i < len; i++)
  {
    strcpy(S2, S1);
    QSort(S2, i, len - 1);
    for (j = i; S2[j] < S1[i]; j++)
      ;
    for (k = i; k < j; k++)
    {
      if (k != i && S2[k] == S2[k - 1])
        continue;
      strcpy(S3, S2);
      SWAP(S3[i], S3[k], tmp);
      memset(L, 0, sizeof(int) * 26);
      for (h = i + 1; h < len; h++)
        L[S3[h] - 'a']++;
      memset(X, 0, sizeof(long) * MAX_LEN2);
      X[0] = 1;
      for (h = 2; h <= len - i - 1; h++)
        Mult(X, h);
      for (h = 0; h < 26; h++)
        for (g = 2; g <= L[h]; g++)
          Div(X, g);
      Add(Num, X);
    }
  }
  for (i = MAX_LEN2 - 1; Num[i] == 0; i--)
    ;
  while (i >= 0)
    printf("%li", Num[i--]);
  return 0;
} /* End of 'main' function */

