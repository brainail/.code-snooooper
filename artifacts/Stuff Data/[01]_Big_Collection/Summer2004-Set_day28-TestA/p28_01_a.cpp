#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 4100
#define MANY 1000000000

struct REB {
  int a;
  int w;
  int ind;
  REB *p;
};

FILE *fi, *fo;
int n,m;
REB *g[NN];
REB swap[400000];
int iswap=0;
int r0[NN];
bool used[NN];
int r[NN];
REB *d[NN];
int delta[NN];
int a[100100];
int ans;

REB *getnew() {
  return &swap[iswap++];
}

void Add(int x, int y, int w, int ind) {
  REB *t=getnew();
  t->a=y;
  t->w=w;
  t->ind=ind;
  t->p=g[x];
  g[x]=t;
}

bool Dejkstr() {
  int i,k,m;
  REB *t;
  for(i=0;i<n;i++) {
    used[i]=false;
    r[i]=r0[i];
    d[i]=NULL;
    delta[i]=MANY;
  }
  if(r0[0]!=0)
    return false;
  while(true) {
    m=MANY;
    k=-1;
    for(i=0;i<n;i++) {
      if(!used[i]) {
        if(r[i]<m) {
          k=i;
          m=r[i];
        }
      }
    }
    if(k<0)
      return true;
    used[k]=true;
    if((d[k]==NULL)&&(k!=0))  {
      return false;
    }
    if(d[k]!=NULL) {
      if((d[k]->w+delta[k])<0) {
        return false;
      }
      d[k]->w-=delta[k];
      a[d[k]->ind]=d[k]->w;
      ans+=delta[k];
    }
    for(t=g[k];t!=NULL;t=t->p) {
      if(!used[t->a]) {
        if(r[k]+t->w<=r[t->a]) {
          ans+=r[t->a]-r[k]-t->w;
          t->w+=r[t->a]-r[k]-t->w;
          a[t->ind]=t->w;
          d[t->a]=t;
          delta[t->a]=0;
        } else {
          if(delta[t->a]>t->w+r[k]-r[t->a]) {
            d[t->a]=t;
            delta[t->a]=t->w+r[k]-r[t->a];
          }
        }
      }
    }
  }
}

int main() {
  int i,j;
  int x,y,z;
  fi=fopen("minister.in","rt");
  fo=fopen("minister.out","wt");

  fscanf(fi,"%d%d",&n,&m);
  for(i=0;i<n;i++) {
    fscanf(fi,"%d",&r0[i]);
    g[i]=NULL;
  }
  for(i=0;i<m;i++) {
    fscanf(fi,"%d%d%d",&x,&y,&z);
    x--; y--;
    if(z<0) {
      fprintf(fo,"-1\n");
      fclose(fi);
      fclose(fo);
      return 0;
    }
    a[i]=z;
    Add(x,y,z,i);
    Add(y,x,z,i);
  }
  if(Dejkstr()) {
    fprintf(fo,"%d\n",ans);
    for(i=0;i<m;i++) {
      fprintf(fo,"%d\n",a[i]);
    }
  } else {
    fprintf(fo,"-1\n");
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
