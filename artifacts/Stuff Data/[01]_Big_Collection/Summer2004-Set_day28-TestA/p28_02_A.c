#include <stdio.h>
#include <stdlib.h>

#define me 200005
#define mv 5000
#define inf 1000000000

int r[me], rr, next[me], h[mv], d[mv], mark[mv], c[mv], l[me], p[me],
    edge[mv], pr[mv], close[mv];

void add( int x, int y, int ll )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  l[rr] = ll;
}

int main( void )
{
  int n, m, i, x, y, ll, sum = 0, j, w, ww = 0;

  freopen("minister.in", "r", stdin);
  freopen("minister.out", "w", stdout);
  scanf("%d%d", &n, &m);
  for (i = 1; i <= n; i++)
    scanf("%d", &d[i]);
  for (i = 1; i <= m; i++)
  {
    scanf("%d%d%d", &x, &y, &ll);
    add(x, y, ll);
    p[rr] = rr + 1;
    add(y, x, ll);
    p[rr] = rr - 1;
  }
  if (d[1] != 0)
  {
    printf("-1\n");
    return 0;
  }
  for (i = 2; i <= n; i++)
  {
    for (j = h[i]; j != 0; j = next[j])
      if (d[r[j]] < d[i])
        break;
    if (j == 0)
    {
      printf("-1\n");
      return 0;
    }
  }
  for (i = 2; i <= n; i++)
    for (j = h[i]; j != 0; j = next[j])
      if (d[r[j]] + l[j] < d[i])
      {
        sum += d[i] - d[r[j]] - l[j];
        l[p[j]] = l[j] = d[i] - d[r[j]];
      }
  for (i = 2; i <= n; i++)
    c[i] = inf;
  /*
  for (i = 1; i <= n; i++)
  {
    for (w = -1, j = 1; j <= n; j++)
      if (!mark[j] && (w == -1 || c[w] > c[j]))
        w = j;
    if (w == -1 || c[w] == inf)
      break;
    mark[w] = 1;
    for (j = h[w]; j != 0; j = next[j])
      if (!mark[r[j]] && d[w] <= d[r[j]]
          && c[r[j]] > c[w] + abs(l[j] - (d[r[j]] - d[w])))
      {
        c[r[j]] = c[w] + abs(l[j] - (d[r[j]] - d[w]));
        pr[r[j]] = w;
        edge[r[j]] = j;
      }
  }
  */
  /*
  mark[1] = 1;
  for (i = h[1]; i != 0; i = next[i])
  {
    close[r[i]] = 1;
    edge[r[i]] = i;
  }
  for (i = 2; i <= n; i++)
  {
    for (w = -1, j = 1; j <= n; j++)
      if (!mark[j] && close[j] != 0
          && (w == -1 || abs(l[edge[j]] - abs(d[j] - d[close[j]])) < ww))
      {
        w = j;
        ww = abs(l[edge[j]] - abs(d[j] - d[close[j]]));
      }
    if (w == -1)
      break;
    mark[w] = 1;
    sum += ww;
    l[edge[w]] = l[p[edge[w]]] = abs(d[w] - d[close[w]]);
    for (j = h[w]; j != 0; j = next[j])
      if (d[r[j]] >= d[w] && !mark[r[j]] && (close[r[j]] == 0
          || abs(l[edge[r[j]]] - abs(d[r[j]] - d[close[r[j]]]))
          > abs(l[j] - abs(d[r[j]] - d[w]))))
      {
        close[r[j]] = w;
        edge[r[j]] = j;
      }
  }
  */
  for (i = 2; i <= n; i++)
  {
    for (w = -1, j = h[i]; j != 0; j = next[j])
      if (d[i] > d[r[j]] && (w == -1 || abs(l[j] - (d[i] - d[r[j]])) < ww))
      {
        ww = abs(l[j] - (d[i] - d[r[j]]));
        w = j;
      }
    sum += ww;
    l[w] = l[p[w]] = d[i] - d[r[w]];
  }
  if (i <= n)
    printf("-1\n");
  else
  {
    /*
    for (i = 2; i <= n; i++)
    {
      sum += abs(l[edge[i]] - (d[i] - d[pr[i]]));
      l[edge[i]] = d[i] - d[pr[i]];
    } */
    printf("%d\n", sum);
    for (i = 1; i <= m; i++)
      printf("%d\n", l[i * 2 - 1]);
  }
  return 0;
}

