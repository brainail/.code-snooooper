#define ONLINE
#include <stdio.h>
#include <string.h>

#define INF 0x7FFFFF00L

struct TEdge {
  int to;
  long*weight;
  TEdge*next;
};
class TGraph {
  private:
    int n_verts,n_edges;
    TEdge*edges_cont_0,*edges_cont_1,**edges;
    long*weights,*declars,*dists,alters;
    char*mark;
  public:
    TGraph(void);
    ~TGraph();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printweights(void);
    int repair(void);
};

TGraph graph;

int main(void) {
  #ifdef ONLINE
  freopen("minister.in","rt",stdin);
  freopen("minister.out","wt",stdout);
  #endif
  graph.scan();
  if (!graph.repair()) graph.printweights();
  else printf("-1\n");
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


int TGraph::repair(void) {
  int cur,to; TEdge*edge,*edge_best; long diff,diff_best;
  memset(mark,0,sizeof(char)*n_verts);
  for (cur=1; cur<n_verts; cur++) dists[cur]=INF;
  *dists=0; if (*declars) return(3);
  for (;;) {
    for (cur=0; cur<n_verts; cur++) if (!mark[cur]) break;
    for (to=cur; to<n_verts; to++) if (!mark[to]) {
      if (declars[cur]>declars[to]) cur=to;
      else if (declars[cur]==declars[to]) if (dists[cur]>dists[to]) cur=to;
    }
    if (cur>=n_verts) break;
    if (dists[cur]>=INF) return(1);
    if (dists[cur]>declars[cur]) {
      edge_best=0; diff_best=INF;
      for (edge=edges[cur]; edge; edge=edge->next) {
        to=edge->to; if (!mark[to]) continue;
        if (dists[to]>declars[cur]) continue;
        diff=dists[to]+*edge->weight-declars[cur];
        if (diff_best>diff) { edge_best=edge; diff_best=diff; }
      }
      if (!edge_best) return(2);
      dists[cur]=dists[edge_best->to]+(*edge_best->weight-=diff_best);
      alters+=diff_best;
    }
    mark[cur]=1;
    for (edge=edges[cur]; edge; edge=edge->next) {
      to=edge->to; if (mark[to]) continue;
      diff=declars[to]-(dists[cur]+*edge->weight);
      if (diff>0) {
        *edge->weight+=diff;
        alters+=diff;
      }
      if (dists[to]>dists[cur]+*edge->weight) {
        dists[to]=dists[cur]+*edge->weight;
      }
    }
  }
  return(0);
}

void TGraph::scan(void) {
  Delete();
  scanf("%d%d",&n_verts,&n_edges);
  Create();
  int vert,to,i_edge;
  for (vert=0; vert<n_verts; vert++) scanf("%ld",&declars[vert]);
  memset(edges,0,sizeof(TEdge*)*n_verts);
  for (i_edge=0; i_edge<n_edges; i_edge++) {
    scanf("%d%d%ld",&vert,&to,&weights[i_edge]); vert--; to--;
    edges_cont_0[i_edge].to=to;
    edges_cont_0[i_edge].weight=&weights[i_edge];
    edges_cont_0[i_edge].next=edges[vert];
    edges[vert]=&edges_cont_0[i_edge];
    edges_cont_1[i_edge].to=vert;
    edges_cont_1[i_edge].weight=&weights[i_edge];
    edges_cont_1[i_edge].next=edges[to];
    edges[to]=&edges_cont_1[i_edge];
  }
  alters=0;
}
void TGraph::printweights(void) {
  int i_edge;
  printf("%ld\n",alters);
  for (i_edge=0; i_edge<n_edges; i_edge++) printf("%ld\n",weights[i_edge]);
}

TGraph::TGraph(void) {
  n_verts=0; n_edges=0;
  Create();
}
TGraph::~TGraph() {
  Delete();
}
void TGraph::Create(void) {
  edges_cont_0=new TEdge[n_edges]; edges_cont_1=new TEdge[n_edges];
  edges=new TEdge*[n_verts]; weights=new long[n_edges];
  declars=new long[n_verts]; dists=new long[n_verts];
  mark=new char[n_verts];
}
void TGraph::Delete(void) {
  delete[]edges_cont_0; delete[]edges_cont_1; delete[]edges;
  delete[]weights; delete[]declars; delete[]dists;
  delete[]mark;
}


