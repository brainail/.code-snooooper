/* 28.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_M 100000
#define MAX_N   4000

long N, M;
long R[MAX_M][3];
long P[MAX_N], Pos[MAX_N];
long *C[MAX_N][2];
long Di[MAX_N];
long U[MAX_N];
long X[MAX_N];
long Dif = 0;
int flag = 1;

/* The main program function */
int main( void )
{
  long i, j, a, b;

  freopen("minister.in", "r", stdin);
  freopen("minister.out", "w", stdout);
  /* Read input data */
  scanf("%li%li", &N, &M);
  for (i = 0; i < N; i++)
    scanf("%li", &Di[i]);
  for (i = 0; i < M; i++)
    for (j = 0; j < 3; j++)
      scanf("%li", &R[i][j]);
  for (i = 0; i < M; i++)
    R[i][0]--, R[i][1]--;
  /* Solve */
  if (Di[0] != 0)
    flag = 0;
  else
  {
    for (i = 0; i < N; i++)
      P[i] = Pos[i] = 0;
    for (i = 0; i < M; i++)
      P[R[i][0]]++, P[R[i][1]]++;
    for (i = 0; i < N; i++)
      for (j = 0; j < 2; j++)
        C[i][j] = malloc(sizeof(long) * P[i]);
    for (i = 0; i < M; i++)
    {
      a = R[i][0], b = R[i][1];
      C[a][0][Pos[a]] = b, C[a][1][Pos[a]] = i;
      C[b][0][Pos[b]] = a, C[b][1][Pos[b]] = i;
      Pos[a]++, Pos[b]++;
    }
    for (i = 0; i < N; i++)
      X[i] = 0, U[i] = 0;
    X[0] = U[0] = 1;
    for (i = 0; i < P[0]; i++)
      U[C[0][0][i]] = 1;
    for (i = 1; i < N && flag; i++)
    {
      int MinI = -1, MinL = -1, L;

      for (j = 0; j < N; j++)
        if (U[j] == 1 && X[j] == 0 && (MinI == -1 || Di[MinI] > Di[j]))
          MinI = j;
      for (j = 0; j < P[MinI]; j++)
      {
        a = C[MinI][0][j];
        b = C[MinI][1][j];
        if (X[a] == 1)
        {
          if (Di[a] + R[b][2] < Di[MinI])
            Dif += Di[MinI] - Di[a] - R[b][2], R[b][2] = Di[MinI] - Di[a];
          if (Di[a] + R[b][2] < L || MinL == -1)
            MinL = b, L = Di[a] + R[b][2];
        }
        else
          U[a] = 1;
      }
      Dif += L - Di[MinI];
      R[MinL][2] -= L - Di[MinI];
      X[MinI] = 1;
    }
  }
  for (i = 0; i < M; i++)
    if (R[i][2] < 0)
      flag = 0;
  /* Output result */
  if (!flag)
    printf("-1\n");
  else
  {
    printf("%li\n", Dif);
    for (i = 0; i < M; i++)
      printf("%li\n", R[i][2]);
  }
  return 0;
} /* End of 'main' function */

