#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SS 30000
#define NN 210
#define MM 4010
#define MANY 1000000000

struct REB {
  bool tp;
  REB *inv;
  int ind;
  int w;
  int a;
  REB *p;
};

FILE *fi, *fo;
REB swap[SS];
REB *g[NN];
int iswap=0;
int r[NN];
REB *d[NN];
REB *g2[NN];
int f[NN];
int xx[MM];
int yy[MM];
bool used[NN];
int n,m,k;
double tm;
int b[NN];
int l;

REB *getnew() {
  return &swap[iswap++];
}

REB *Add(int x, int y, int w) {
  REB *t;
  t=getnew();
  t->a=y;
  t->w=w;
  t->tp=true;
  t->p=g[x];
  t->inv=NULL;
  g[x]=t;
  return t;
}

void Add2(int x, int y, int i) {
  REB *t;
  t=getnew();
  t->a=y;
  t->p=g2[x];
  t->ind=i;
  t->tp=true;
  g2[x]=t;
}

void Dejkstr() {
  int i,k,m;
  REB *t;
  for(i=0;i<n;i++) {
    used[i]=false;
    r[i]=MANY+f[i];
    d[i]=NULL;
  }
  r[0]=0;
  while(true) {
    k=-1;
    m=MANY;
    for(i=0;i<n;i++) {
      if(!used[i]) {
        if(r[i]-f[i]<m) {
          m=r[i]-f[i];
          k=i;
        }
      }
    }
    if(k<0)
      return ;
    used[k]=true;
    for(t=g[k];t!=NULL;t=t->p) {
      if(t->tp) {
        if(r[t->a]>r[k]+t->w) {
          r[t->a]=r[k]+t->w;
          d[t->a]=t;
        }
      }
    }
  }
}

void mark_used(int v) {
  while(d[v]!=NULL) {
    if(d[v]->inv->tp) {
      d[v]->inv->w=-d[v]->w;
      d[v]->tp=false;
    } else {
      d[v]->inv->tp=true;
      d[v]->w=-d[v]->w;
    }
    if(v!=xx[d[v]->ind]) {
      v=xx[d[v]->ind];
    } else {
      v=yy[d[v]->ind];
    }
  }
}

void dfs(int v) {
  REB *t;
  used[v]=true;
  for(t=g2[v];t!=NULL;t=t->p) {
    if(t->tp) {
      if(!used[t->a]) {
//      fprintf(fo,"%d ",t->ind+1);
        t->tp=false;
        b[l++]=t->ind;
        dfs(t->a);
        return ;
      }
    }
  }
}

int main() {
  int i,j;
  int x,y,z;
  REB *t, *p;
  REB *tt;
  fi=fopen("brides.in","rt");
  fo=fopen("brides.out","wt");

  fscanf(fi,"%d%d%d",&n,&m,&k);
  for(i=0;i<n;i++) {
    g[i]=NULL;
    g2[i]=NULL;
    f[i]=0;
  }
  for(i=0;i<m;i++) {
    fscanf(fi,"%d%d%d",&x,&y,&z);
    x--; y--;
    xx[i]=x;
    yy[i]=y;
    t=Add(x,y,z);
    p=Add(y,x,z);
    t->inv=p;
    t->ind=i;
    p->ind=i;
    p->inv=t;
  }
  for(i=0;i<k;i++) {
    Dejkstr();
    if(!used[n-1]) {
      fprintf(fo,"-1\n");
      fclose(fi);
      fclose(fo);
      return 0;
    }
    mark_used(n-1);
    for(j=0;j<n;j++)
      f[j]=r[j];
  }
  tm=0;
  for(i=0;i<n;i++) {
    for(t=g[i];t!=NULL;t=t->p) {
      if(!t->tp) {
        Add2(i,t->a,t->ind);
        tm+=(double)t->w;
      }
    }
  }
  tm/=(double)k;
  fprintf(fo,"%0.5lf\n",tm);
  for(i=0;i<n;i++)
    used[i]=false;
  for(i=0;i<k;i++) {
    l=0;
    dfs(0);
    for(j=0;j<n;j++)
      used[j]=false;
    fprintf(fo,"%d",l);
    for(j=0;j<l;j++)
      fprintf(fo," %d",b[j]+1);
    fprintf(fo,"\n");
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
