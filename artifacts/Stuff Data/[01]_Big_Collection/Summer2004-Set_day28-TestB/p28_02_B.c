#include <stdio.h>

#define mv 202
#define me 5000
#define inf 1000000000

int f[me], rr, r[me], next[me], h[mv], t[me], oldt[me], n,
    d[mv], q[me], l, mark[mv], p[me], m, k, _stklen = 8 * 1024 * 1024;

void add( int x, int y, int tt )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  t[rr] = oldt[rr] = tt;
}

int dfs( int v )
{
  int i;

  if (v == n)
    return 1;
  if (mark[v])
    return 0;
  mark[v] = 1;
  for (i = h[v]; i != 0; i = next[i])
    if (f[i] <= 0 && t[i] == 0 && dfs(r[i]))
    {
      f[i]++;
      f[p[i]] = -f[i];
      return 1;
    }
  return 0;
}

int dfs2( int v )
{
  int i;

  if (v == n)
    return 1;
  if (mark[v])
    return 0;
  mark[v] = 1;
  for (i = h[v]; i != 0; i = next[i])
    if (f[i] == 1 && dfs2(r[i]))
    {
      f[i] = f[p[i]] = 0;
      q[++l] = i;
      return 1;
    }
  return 0;
}

int main( void )
{
  int i, ii, j, x, y, tt, w;
  double s = 0;

  freopen("brides.in", "r", stdin);
  freopen("brides.out", "w", stdout);
  scanf("%d%d%d", &n, &m, &k);
  for (i = 1; i <= m; i++)
  {
    scanf("%d%d%d", &x, &y, &tt);
    add(x, y, tt);
    p[rr] = rr + 1;
    add(y, x, tt);
    p[rr] = rr - 1;
  }
  for (ii = 1; ii <= k; ii++)
  {
    for (i = 1; i <= n; i++)
    {
      mark[i] = 0;
      d[i] = inf;
    }
    d[1] = 0;
    for (i = 1; i <= n; i++)
    {
      for (w = -1, j = 1; j <= n; j++)
        if (!mark[j] && (w == -1 || d[w] > d[j]))
          w = j;
      if (w == -1 || d[w] == inf)
        break;
      mark[w] = 1;
      for (j = h[w]; j != 0; j = next[j])
        if (f[j] <= 0 && !mark[r[j]] && d[r[j]] > d[w] + t[j])
          d[r[j]] = d[w] + t[j];
    }
    for (i = 1; i <= n; i++)
      for (j = h[i]; j != 0; j = next[j])
        if (f[j] <= 0)
        {
          t[j] += d[i] - d[r[j]];
          //t[p[j]] = t[j];
        } 
    for (i = 1; i <= n; i++)
      mark[i] = 0;
    if (!dfs(1))
      break;
  }
  if (ii <= k)
    printf("-1\n");
  else
  {
    s = 0;
    for (i = 1; i <= n; i++)
      for (j = h[i]; j != 0; j = next[j])
        if (f[j] == 1)
          s += oldt[j];
    printf("%lf\n", s / k);
    for (i = 1; i <= k; i++)
    {
      l = 0;
      for (j = 1; j <= n; j++)
        mark[j] = 0;
      dfs2(1);
      printf("%d ", l);
      for (j = l; j >= 1; j--)
        printf("%d ", (q[j] + 1) / 2);
      printf("\n");
    }
  }
  return 0;
}

