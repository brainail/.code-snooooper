#define ONLINE
#include <stdio.h>
#include <string.h>

#define INF 0x7FFFFF00L

struct TEdge {
  int from,to;
  long weight;
  int index;
  char*used,*reci;
  TEdge*next;
};
class TNetwork {
  private:
    int n_verts,n_edges,n_paths;
    TEdge*edges_cont_0,*edges_cont_1,**edges;
    char*edges_mark_0,*edges_mark_1,*mark;
    long*dists,weight_total; TEdge**byp;
    int*que,*path; int que_len,que_pos,path_len,path_pos;
  protected:
    int searchchain(int,int);
    void searchpath(int);
    void retainpath(int);
  public:
    TNetwork(void);
    ~TNetwork();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printweight(void);
    int findpaths(void);
    void printpaths(void);
};

TNetwork net;

int main(void) {
  #ifdef ONLINE
  freopen("brides.in","rt",stdin);
  freopen("brides.out","wt",stdout);
  #endif
  net.scan();
  if (net.findpaths()) {
    net.printweight();
    net.printpaths();
  } else printf("-1\n");
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


int TNetwork::searchchain(int source,int target) {
  int iter,cur,to; TEdge*edge; long dist;
  memset(mark,0,sizeof(char)*n_verts);
  for (cur=0; cur<n_verts; cur++) dists[cur]=INF;
  memset(byp,0,sizeof(TEdge*)*n_verts);
  que_len=1; *que=source;
  mark[source]=1; dists[source]=0;
  for (iter=0; iter<n_verts+2; iter++) {
    for (que_pos=0; que_pos<que_len; que_pos++) {
      cur=que[que_pos];
      for (edge=edges[cur]; edge; edge=edge->next) if (!*edge->used) {
        to=edge->to;
        dist=edge->weight;
        if (*edge->reci) dist=-dist;
        dist+=dists[cur];
        if (dists[to]<=dist) continue;
        dists[to]=dist; byp[to]=edge;
        if (mark[to]) continue;
        que[que_len++]=to; mark[to]=1;
      }
    }
  }
  if (!mark[target]) return(0);
  for (edge=byp[target]; edge; edge=byp[edge->from]) {
    if (!*edge->reci) {
      *edge->used=1;
      weight_total+=edge->weight;
    } else {
      *edge->reci=0;
      weight_total-=edge->weight;
    }
  }
  return(1);
}
void TNetwork::searchpath(int cur) {
  int to; TEdge*edge;
  memset(mark,0,sizeof(char)*n_verts);
  memset(byp,0,sizeof(TEdge*)*n_verts);
  que_len=1; *que=cur; mark[cur]=1;
  for (que_pos=0; que_pos<que_len; que_pos++) {
    cur=que[que_pos];
    for (edge=edges[cur]; edge; edge=edge->next) if (*edge->used) {
      to=edge->to; if (mark[to]) continue;
      que[que_len++]=to; mark[to]=1; byp[to]=edge;
    }
  }
}
void TNetwork::retainpath(int target) {
  TEdge*edge;
  path_len=0;
  for (edge=byp[target]; edge; edge=byp[edge->from]) {
    path[path_len++]=edge->index;
    *edge->used=0;
  }
}

int TNetwork::findpaths(void) {
  int i_path;
  memset(edges_mark_0,0,sizeof(char)*n_edges);
  memset(edges_mark_1,0,sizeof(char)*n_edges);
  weight_total=0;
  for (i_path=0; i_path<n_paths; i_path++)
    if (!searchchain(0,n_verts-1)) break;
  return(i_path>=n_paths);
}
void TNetwork::printpaths(void) {
  int i_path;
  for (i_path=0; i_path<n_paths; i_path++) {
    searchpath(0); retainpath(n_verts-1);
    printf("%d",path_len);
    for (path_pos=path_len-1; path_pos>=0; path_pos--) {
      printf(" %d",path[path_pos]+1);
    } printf("\n");
  }
}

void TNetwork::scan(void) {
  Delete();
  scanf("%d%d%d",&n_verts,&n_edges,&n_paths);
  Create();
  int i_edge,vert,to; long weight;
  memset(edges,0,sizeof(TEdge*)*n_verts);
  for (i_edge=0; i_edge<n_edges; i_edge++) {
    scanf("%d%d%ld",&vert,&to,&weight); vert--; to--;
    edges_cont_0[i_edge].from=vert;
    edges_cont_0[i_edge].to=to;
    edges_cont_0[i_edge].weight=weight;
    edges_cont_0[i_edge].index=i_edge;
    edges_cont_0[i_edge].next=edges[vert];
    edges_cont_0[i_edge].used=&edges_mark_0[i_edge];
    edges_cont_0[i_edge].reci=&edges_mark_1[i_edge];
    edges[vert]=&edges_cont_0[i_edge];
    edges_cont_1[i_edge].from=to;
    edges_cont_1[i_edge].to=vert;
    edges_cont_1[i_edge].weight=weight;
    edges_cont_1[i_edge].index=i_edge;
    edges_cont_1[i_edge].next=edges[to];
    edges_cont_1[i_edge].used=&edges_mark_1[i_edge];
    edges_cont_1[i_edge].reci=&edges_mark_0[i_edge];
    edges[to]=&edges_cont_1[i_edge];
  }
}
void TNetwork::printweight(void) {
  long double aver=(long double)weight_total;
  aver/=(long double)n_paths;
  printf("%.6Lf\n",aver);
}

TNetwork::TNetwork(void) {
  n_verts=0; n_edges=0; n_paths=0;
  Create();
}
TNetwork::~TNetwork() {
  Delete();
}
void TNetwork::Create(void) {
  edges_cont_0=new TEdge[n_edges]; edges_cont_1=new TEdge[n_edges];
  edges_mark_0=new char[n_edges]; edges_mark_1=new char[n_edges];
  edges=new TEdge*[n_verts]; byp=new TEdge*[n_verts];
  mark=new char[n_verts]; dists=new long[n_verts];
  que=new int[n_verts]; path=new int[n_verts];
}
void TNetwork::Delete(void) {
  delete[]edges_cont_0; delete[]edges_cont_1;
  delete[]edges_mark_0; delete[]edges_mark_1;
  delete[]edges; delete[]byp; delete[]mark; delete[]dists;
  delete[]que; delete[]path;
}


