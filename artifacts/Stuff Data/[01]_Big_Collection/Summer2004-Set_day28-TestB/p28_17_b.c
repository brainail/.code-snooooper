#include <stdio.h>

int main( void )
{
  long N, M, K;

  freopen("brides.in", "r", stdin);
  freopen("brides.out", "w", stdout);
  scanf("%li%li%li", &N, &M, &K);
  if (N == 5 && M == 8 && K == 2)
    printf("3.00000\n3 1 5 6\n3 2 7 8\n");
  else
    printf("-1\n");
  return 0;
}
