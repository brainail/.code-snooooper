/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;

#define DBG if(0)

const int INF = (INT_MAX/2)-12;

const int NMAX=200;
const int MMAX=2000;
const int KMAX=100;

int N,M,K;
int **am;
int **road_no;

void print_float(ostream& os, double x)
{
  // 7 digits after '.'
  os << floor(x);
  os << '.';
  for (int i=0; i<7; i++) {
    x = (x-floor(x))*10;
    os << floor(x);
  }
}

void init()
{
  ifstream fi("brides.in");
  fi >> N >> M >> K;

  am = new int*[N];
  road_no = new int*[N];
  for (int i=0; i<N; i++) {
    am[i] = new int[N];
    road_no[i] = new int[N];
  }
  for (int i=0; i<N; i++)
    for (int j=0; j<M; j++)
      am[i][j] = road_no[i][j] = INF;

  int i;
  for (i=0; i<M; i++) {
    int x,y,z;
    fi >> x >> y >> z;
    x--;y--;
    am[x][y] = am[y][x] = z;
    road_no[x][y] = road_no[y][x] = i;
  }
}
void final()
{
  for (int i=0; i<N; i++) {
    delete[] am[i];
    delete[] road_no[i];
  }
  delete[]am;
  delete[]road_no;
}



template <class T, class Key>
struct PQueue {
  int msizemax;
  int msize;
  T *mval;
  Key *mkey;
  PQueue(int sizemax) {
    mval = new T[msizemax=sizemax];
    mkey = new Key[sizemax];
    msize = 0;
  }
  virtual ~PQueue() { delete[] mval; delete[] mkey; }
  void insert(const T& x, const Key k) {
    mval[msize] = x;
    mkey[msize] = k;
    msize++;
  }
  T extract_min(Key *key) {
    int imin = 0;
    for (int i=0; i<msize; i++)
      if (mkey[i] < mkey[imin])
        imin = i;
    T val = mval[imin];
    if (key) *key=mkey[imin];
    for (int i=imin; i<N-1; i++) {
      mval[i] = mval[i+1];
      mkey[i] = mkey[i+1];
    }
    msize--;
    return val;
  }
  bool is_empty() { return (msize==0); }
  void decrease_key(const T& v, Key key) {
    for (int i=0; i<msize; i++)
      if (mval[i]==v)
      { // we`ve find it
        mkey[i] = key;
        break;
      }
  }
};
  
// dijkstra`s algorithm
void sssp(const int **am, const int N, const int s,
          int *dist, int *prev)
{
  enum Color { White, Gray, Black };
  Color *color = new Color[N];
  for (int i=0; i<N; i++) { color[i] = White; }
  color[s] = Gray;
  for (int i=0; i<N; i++) {
    dist[i]=prev[i]=INF;
  }
  dist[s] = 0;
  prev[s] = INF;
  PQueue<int,int> pq(N);
  pq.insert(s,0);
  while (!pq.is_empty())
  {
    int key;
    int v = pq.extract_min(&key);
    for (int j=0; j<N; j++)
      if (am[v][j]!=INF)
      {
        // relax(v,j)
        int nd = key + am[v][j];
        if (nd < dist[j]) {
          if (color[j] == White) {
            pq.insert(j,nd);
            color[j] = Gray;
          } else /* if (color[j]==Gray) it can`t be Black */ {
            pq.decrease_key(j,nd);
          }
          dist[j] = nd;
          prev[j] = v;
        }
      }
    color[v] = Black;
  }
  delete[] color;
}

void solve()
{
  int *prev = new int[N];
  int *dist = new int[N];
  ofstream fo("brides.out");
}

int main(void)
{
  init();
  solve();
  final();
  return 0;
}
