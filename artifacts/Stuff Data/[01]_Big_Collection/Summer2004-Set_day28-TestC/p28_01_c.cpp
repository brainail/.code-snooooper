#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 10100
#define RSWAPS 1700000
#define LSWAPS 700000
#define MANY 1000000000

int _stklen = 4000000;

struct REB {
  int a;
  int b;
  REB *p;
};

struct RList {
  REB *a;
  RList *p;
};

FILE *fi, *fo;
int fNum[NN],sNum[NN];
int parent[NN];
int enter[NN], leave[NN];
REB *g[NN];
REB *g2[NN];
RList *lst[NN];
REB *tree[NN];
REB *final[NN];
bool used[NN];
int current_2num=0;
int time=0;
int a[4*NN];
int ind=2*NN-1;
int come[NN];
REB rswap[RSWAPS];
int irswap=0;
RList lswap[LSWAPS];
int ilswap=0;
int b[NN];
REB swap2[700000];
int iswap2=0;
int tt;
int ex[NN];
int n;
int sm[NN];
bool flag;

int min(int a, int b) { return (a<b)?a:b; }
int max(int a, int b) { return (a>b)?a:b; }

REB *getreb() {
  return &rswap[irswap++];
}

RList *getlst() {
  return &lswap[ilswap++];
}

REB *getreb2() {
  return &swap2[iswap2++];
}

REB *AddRebG(int x, int y) {
  REB *t;
  t=getreb();
  t->a=y;
  t->b=x;
  t->p=g[x];
  g[x]=t;
  return t;
}

void AddRebG2(int x, int y) {
  REB *t;
  t=getreb2();
  t->a=y;
  t->b=x;
  t->p=g2[x];
  g2[x]=t;
}

void AddRebTree(int x, int y) {
  REB *t;
  t=getreb();
  t->a=y;
  t->b=x;
  t->p=tree[x];
  tree[x]=t;
}

void AddRebFinal(int x, int y) {
  REB *t;
  t=getreb();
  t->a=y;
  t->b=x;
  t->p=final[x];
  final[x]=t;
}

void AddList(int x, REB *p) {
  RList *t;
  t=getlst();
  t->a=p;
  t->p=lst[x];
  lst[x]=t;
}

void dfs1(int v) {
  REB *t;
  sNum[v]=current_2num++;
  fNum[sNum[v]]=v;
  used[v]=true;
  enter[v]=time++;
  come[v]=ind;
  a[ind++]=sNum[v];
  for(t=tree[v];t!=NULL;t=t->p) {
    dfs1(t->a);
    a[ind++]=sNum[v];
  }
  leave[v]=time++;
}

int isprev(int x, int y) {
  if(x==y) return 1;
  if(enter[x]<enter[y]) {
    if(leave[x]>leave[y]) {
      return 1;
    } else {
      return 0;
    }
  } else {
    if(leave[x]<leave[y]) {
      return 2;
    } else {
      return 0;
    }
  }
  return 0;
}

int rmq_get(int x) {
  if(x>=4*NN)
    return MANY;
  return a[x];
}

void rmq_build() {
  int i;
  for(i=2*NN-2;i>=0;i--) {
    a[i]=min(rmq_get(2*i+1),rmq_get(2*i+2));
  }
}

int rmq_parent(int x) {
  return (x-1)/2;
}

int rmq(int x, int y) {
  if(x>y)
    return MANY;
  if(x==y)
    return rmq_get(x);
  return min(rmq_get(x),min(rmq_get(y),rmq(rmq_parent(x+1),rmq_parent(y-1))));
}

void build_tree() {
  int i;
  for(i=1;i<n;i++) {
    AddRebTree(parent[i],i);
  }
}

int getcc(FILE *fi) {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

int GetInt(FILE *fi) {
  int c;
  int ans;
  ans=0;
  c=getcc(fi);
  while(isdigit(c)) {
    ans=ans*10+c-'0';
    c=getc(fi);
  }
  return ans;
}

int lca(int x, int y) {
  int z;
  z=rmq(come[x],come[y]);
  if(z>n)
    z=rmq(come[y],come[x]);
  return fNum[z];
}

void build_g() {
  int i,j,x,y,m;
  REB *t;
  for(i=0;i<n;i++) {
    fscanf(fi,"%d",&m);
    for(j=0;j<m;j++) {
      x=GetInt(fi)-1;
      t=AddRebG(i,x);
//      if(i<x) {
        if(!isprev(i,x)) {
          y=lca(i,x);
          AddList(y,t);
        }
//      }
    }
  }
}

int super_get(int v, int x) {
  REB *t;
  for(t=tree[v];t!=NULL;t=t->p)
    if(isprev(t->a,x))
      return t->a;
}

void dfs2(int v) {
  REB *t;
  used[v]=true;
  for(t=g2[v];t!=NULL;t=t->p) {
    if(!used[t->a])
      dfs2(t->a);
  }
  ex[tt++]=v;
}

void relax_node(int v) {
  int x,y;
  int i,j;
  int m;
  RList *t;
  REB *p;
  iswap2=0;
  for(t=lst[v];t!=NULL;t=t->p) {
    y=t->a->a;
    x=t->a->b;
    x=super_get(v,x);
    y=super_get(v,y);
    AddRebG2(x,y);
  }
  for(p=g[v];p!=NULL;p=p->p) {
    if(parent[p->a]!=v) {
      AddRebFinal(v,p->a);
    } else {
      sm[p->a]++;
      used[p->a]=true;
    }
  }
  for(p=tree[v];p!=NULL;p=p->p) {
    if(!used[p->a]) {
      flag=false;
      return ;
    }
    used[p->a]=false;
  }
  m=0;
  tt=0;
  for(p=tree[v];p!=NULL;p=p->p) {
    if(!used[p->a]) {
      dfs2(p->a);
    }
    m++;
//    sm[p->a]++;
  }
  for(p=tree[v];p!=NULL;p=p->p) {
    used[p->a]=false;
    g2[v]=NULL;
  }
  for(i=m-1;i>=0;i--) {
    for(j=0;j<sm[ex[i]];j++)
      AddRebFinal(v,ex[i]);
  }
  iswap2=0;
}

void dfs3(int v, int from) {
  REB *t;
  if(parent[v]!=from) {
    flag=false;
    return;
  }
  used[v]=true;
  for(t=final[v];t!=NULL;t=t->p)
    if(!used[t->a])
      dfs3(t->a,v);
}

void verify() {
  int i;
  dfs3(0,-1);
  for(i=0;i<n;i++)
    if(!used[i])
      flag=false;
}

int main() {
  int i,j;
  REB *t;
  fi=fopen("dfs.in","rt");
  fo=fopen("dfs.out","wt");

  fscanf(fi,"%d",&n);
  for(i=0;i<n;i++) {
    g[i]=NULL;
    tree[i]=NULL;
    final[i]=NULL;
    fNum[i]=i;
    lst[i]=NULL;
    g2[i]=NULL;
    sm[i]=0;
  }
  for(i=1;i<n;i++) {
    fscanf(fi,"%d",&parent[i]);
    parent[i]--;
  }
  parent[0]=-1;
  for(i=0;i<4*NN;i++)
    a[i]=1000000000; //MANYYY
  flag=true;
  build_tree();
  dfs1(0);
  rmq_build();
  build_g();
  for(i=0;i<n;i++)
    used[i]=false;
  for(i=0;i<n;i++) {
    relax_node(i);
  }
  verify();
  if(flag) {
    fprintf(fo,"YES\n");
    for(i=0;i<n;i++) {
      j=0;
      for(t=final[i];t!=NULL;t=t->p)
        j++;
      fprintf(fo,"%d",j);
      for(t=final[i];t!=NULL;t=t->p)
        fprintf(fo," %d",t->a+1);
      fprintf(fo,"\n");
    }
  } else {
    fprintf(fo,"NO\n");
  }

  fclose(fi);
  fclose(fo);
  return 0;
}
