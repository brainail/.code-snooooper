#include <stdio.h>

#define mv 10010
#define me 1000002

int next[me], h[mv], nextc[mv * 2], tree[me], good[me], hc[mv], rc[mv * 2],
    mark[mv], num[mv], use[me], r[me], rr, cc, old[me], beg[mv], end[mv], t,
    l, n, p[mv], pair[me], _stklen = 8 * 1024 * 1024,
    hn[mv], nextn[me / 2], rn[me / 2], rrn, pp[mv];

void add( int x, int y, int g )
{
  r[++rr] = y;
  next[rr] = h[x];
  h[x] = rr;
  good[rr] = old[rr] = g;
}

void add2( int x, int y )
{
  rn[++rrn] = y;
  nextn[rrn] = hn[x];
  hn[x] = rrn;
}

void dfs1( int v )
{
  int i;

  if (mark[v])
    return;
  mark[v] = 1;
  beg[v] = t++;
  for (i = hc[v]; i != 0; i = nextc[i])
    dfs1(rc[i]);
  end[v] = t++;
}

int dfs2( int v )
{
  int i;

  if (mark[v] == 2)
    return 0;
  if (mark[v])
    return 1;
  mark[v] = 2;
  for (i = h[v]; i != 0; i = next[i])
    if (good[i] && !dfs2(r[i]))
      return 0;
  num[v] = n - l++;
  mark[v] = 1;
  return 1;
}

void dfs3( int v )
{
  int i;

  if (mark[v])
    return;
  mark[v] = 1;
  for (i = h[v]; i != 0; i = next[i])
    if (tree[i])
    {
      dfs3(r[i]);
      if (num[v] < num[r[i]])
        num[v] = num[r[i]];
    }
}

void dfs( int v )
{
  int i;

  if (mark[v])
    return;
  mark[v] = 1;
  for (i = hn[v]; i != 0; i = nextn[i])
    if (!mark[rn[i]])
    {
      dfs(rn[i]);
      pp[rn[i]] = v;
    }
}

int main( void )
{
  int i, j, k, x;

  freopen("dfs.in", "r", stdin);
  freopen("dfs.out", "w", stdout);
  scanf("%d", &n);
  for (i = 2; i <= n; i++)
  {
    scanf("%d", &x);
    rc[++cc] = i;
    nextc[cc] = hc[x];
    hc[x] = cc;
    p[i] = x;
  }
  for (i = 1; i <= n; i++)
  {
    scanf("%d", &k);
    for (j = 1; j <= k; j++)
    {
      scanf("%d", &x);
      add(i, x, 1);
      if (i != x)
      {
        pair[rr] = rr + 1;
        add(x, i, 0);
        pair[rr] = rr - 1;
      }
      else
        pair[rr] = rr;
    }
  }
  for (i = 1; i <= n; i++)
    for (j = h[i]; j != 0; j = next[j])
      if (i == p[r[j]] && !use[r[j]] && good[j])
        use[r[j]] = tree[j] = 1;
  dfs1(1);
  for (i = 1; i <= rr; i++)
    use[i] = 0;
  for (i = 1; i <= n; i++)
    for (j = h[i]; j != 0; j = next[j])
      if (!use[j] && !tree[j] && good[j]
           && ((beg[i] > beg[r[j]] && end[i] > end[r[j]])
            || (beg[i] < beg[r[j]] && end[i] < end[r[j]])))
      {
        use[j] = use[pair[j]] = good[pair[j]] = 1;
        good[j] = 0;
      }
      else if (tree[j])
        use[j] = use[pair[j]] = good[j] = 1;
      else if (!use[j])
      {
        use[j] = use[pair[j]] = 1;
        good[j] = good[pair[j]] = 0;
      }
  for (i = 1; i <= n; i++)
    mark[i] = 0;
  if (!dfs2(1))
  {
    printf("NO\n");
    return 0;
  }
  for (i = 1; i <= n; i++)
    mark[i] = 0;
  dfs3(1);
  //printf("YES\n");
  for (i = 1; i <= n; i++)
  {
    for (j = 1; j <= n; j++)
      mark[j] = 0;
    for (j = h[i]; j != 0; j = next[j])
      if (tree[j])
        mark[num[r[j]]] = r[j];
    for (j = h[i]; j != 0; j = next[j])
      if (!tree[j] && old[j])
        add2(i, r[j]);
    for (j = n; j >= 1; j--)
      if (mark[j])
        add2(i, mark[j]);
  }
  for (i = 1; i <= n; i++)
    mark[i] = 0;
  dfs(1);
  for (i = 2; i <= n; i++)
    if (pp[i] != p[i])
      break;
  if (i <= n)
  {
    printf("NO\n");
    return 0;
  }
  printf("YES\n");
  for (i = 1; i <= n; i++)
  {
    for (k = 0, j = hn[i]; j != 0; j = nextn[j])
      k++;
    printf("%d ", k);
    for (j = hn[i]; j != 0; j = nextn[j])
      printf("%d ", rn[j]);
    printf("\n");
  }
  return 0;
}
