#define ONLINE
#include <stdio.h>
#include <string.h>

#define N_EDGES_BOUND 524288

struct TEdge0 {
  int to;
  TEdge0*next;
};
class TGraph0 {
  private:
    int n_verts,n_edges_bound,n_edges;
    TEdge0*edges_cont,**edges;
    char*mark; int*idx;
    int*stk_vert,*seq; TEdge0**stk_edge;
    int stk_pnt,seq_len,seq_pos,time;
  protected:
    void search(int);
  public:
    TGraph0(void);
    ~TGraph0();
    void Create(void);
    void Delete(void);
    void init(int,int);
    void clear(void);
    void addedge(int,int);
    void sort(void);
    int check(void);
    void rewind(void);
    int next(void);
};

struct TEdge1 {
  int from,to;
  int index;
  char type;
  TEdge1*next;
};
class TGraph1 {
  private:
    int n_verts,n_edges_bound,n_edges;
    TEdge1*edges_cont,*edges_cont_new,**edges; int*parents,*degs;
    TGraph0 edgr;
  protected:
    void search(void);
  public:
    TGraph1(void);
    ~TGraph1();
    void Create(void);
    void Delete(void);
    void scan(void);
    void printedges(void);
    int sequence(void);
};

TGraph1 graph;

int main(void) {
  #ifdef ONLINE
  freopen("dfs.in","rt",stdin);
  freopen("dfs.out","wt",stdout);
  #endif
  graph.scan();
  if (graph.sequence()) {
    printf("YES\n");
    graph.printedges();
  } else printf("NO\n");
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}


void TGraph1::search(void) {
}

int TGraph1::sequence(void) {
  edgr.clear();
  search();
  edgr.sort();
  int i_edge,n_edges_were;
  n_edges_were=n_edges; n_edges=0;
  memset(edges,0,sizeof(TEdge1*)*n_verts);
  edgr.rewind();
  for (i_edge=edgr.next(); i_edge>=0; i_edge=edgr.next()) {
    if (i_edge>=n_edges_were) continue;
    edges_cont_new[n_edges]=edges_cont[i_edge];
    edges_cont_new[n_edges].next=edges[edges_cont[i_edge].from];
    edges[edges_cont[i_edge].from]=&edges_cont_new[n_edges++];
  }
  { TEdge1*z=edges_cont; edges_cont=edges_cont_new; edges_cont_new=z; }
  return(edgr.check());
}

void TGraph1::scan(void) {
  Delete();
  scanf("%d",&n_verts);
  n_edges=n_edges_bound=N_EDGES_BOUND;
  Create();
  int vert,to,n_edges_cur,i_edge_cur;
  *parents=-1;
  for (vert=1; vert<n_verts; vert++) {
    scanf("%d",&parents[vert]); parents[vert]--;
  }
  memset(edges,0,sizeof(TEdge1*)*n_verts);
  memset(degs,0,sizeof(int)*n_verts);
  for (n_edges=0,vert=0; vert<n_verts; vert++) {
    scanf("%d",&n_edges_cur);
    for (i_edge_cur=0; i_edge_cur<n_edges_cur; i_edge_cur++,n_edges++) {
      scanf("%d",&to); to--;
      edges_cont[n_edges].from=vert;
      edges_cont[n_edges].to=to;
      edges_cont[n_edges].index=n_edges;
      edges_cont[n_edges].type=(parents[to]==vert);
      edges_cont[n_edges].next=edges[vert];
      edges[vert]=&edges_cont[n_edges];
      degs[vert]++;
    }
  }
  edgr.init(n_edges+1,n_edges+2);
}
void TGraph1::printedges(void) {
  int vert; TEdge1*edge;
  for (vert=0; vert<n_verts; vert++) {
    printf("%d",degs[vert]);
    for (edge=edges[vert]; edge; edge=edge->next) {
      printf(" %d",edge->to+1);
    } printf("\n");
  }
}

TGraph1::TGraph1(void) {
  n_verts=0; n_edges_bound=0; n_edges=0;
  Create();
}
TGraph1::~TGraph1() {
  Delete();
}
void TGraph1::Create(void) {
  edges_cont=new TEdge1[n_edges_bound];
  edges_cont_new=new TEdge1[n_edges_bound];
  edges=new TEdge1*[n_verts];
  parents=new int[n_verts]; degs=new int[n_verts];
}
void TGraph1::Delete(void) {
  delete[]edges_cont; delete[]edges_cont_new; delete[]edges;
  delete[]parents; delete[]degs;
}


void TGraph0::search(int cur) {
  int to; TEdge0*edge;
  stk_pnt=0; *stk_vert=cur; *stk_edge=edges[cur];
  while (stk_pnt>=0) {
    cur=stk_vert[stk_pnt]; edge=stk_edge[stk_pnt];
    if (!edge) {
      idx[cur]=time++;
      seq[seq_len++]=cur;
      stk_pnt--; continue;
    }
    stk_edge[stk_pnt]=edge->next;
    to=edge->to; if (mark[edge->to]) continue;
    stk_pnt++;
    stk_vert[stk_pnt]=to; stk_edge[stk_pnt]=edges[to];
    mark[to]=1;
  }
}
void TGraph0::sort(void) {
  int vert;
  memset(mark,0,sizeof(char)*n_verts);
  memset(idx,-1,sizeof(int)*n_verts);
  time=0; seq_len=0;
  for (vert=0; vert<n_verts; vert++) if (!mark[vert]) search(vert);
}
int TGraph0::check(void) {
  int vert; TEdge0*edge;
  for (vert=0; vert<n_verts; vert++) {
    for (edge=edges[vert]; edge; edge=edge->next) {
      if (idx[edge->to]>idx[vert]) return(0);
    }
  }
  return(1);
}

void TGraph0::init(int n_verts_,int n_edges_) {
  Delete();
  n_verts=n_verts_; n_edges=n_edges_bound=n_edges_;
  Create();
  clear();
}
void TGraph0::clear(void) {
  memset(edges,0,sizeof(TEdge0*)*n_verts); n_edges=0;
}
void TGraph0::addedge(int from,int to) {
  edges_cont[n_edges].to=to;
  edges_cont[n_edges].next=edges[from];
  edges[from]=&edges_cont[n_edges++];
}
void TGraph0::rewind(void) {
  seq_pos=0;
}
int TGraph0::next(void) {
  if (seq_pos>=seq_len) return(-1);
  return(seq[seq_pos++]);
}

TGraph0::TGraph0(void) {
  n_verts=0; n_edges_bound=0; n_edges=0;
  Create();
}
TGraph0::~TGraph0() {
  Delete();
}
void TGraph0::Create(void) {
  edges_cont=new TEdge0[n_edges]; edges=new TEdge0*[n_verts];
  mark=new char[n_verts]; idx=new int[n_verts]; seq=new int[n_verts];
  stk_vert=new int[n_verts]; stk_edge=new TEdge0*[n_verts];
}
void TGraph0::Delete(void) {
  delete[]edges_cont; delete[]edges;
  delete[]mark; delete[]idx; delete[]seq;
  delete[]stk_vert; delete[]stk_edge;
}


