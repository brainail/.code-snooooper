/* 28.06.04 SK1 */

#include <stdio.h>
#include <stdlib.h>

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))

#define MAX_N  10000
#define MAX_M 500000

long N;
long Prev[MAX_N];
long TP[MAX_N], TPos[MAX_N];
long *TC[MAX_N];
long P[MAX_N];
long *C[MAX_N];
int flag = 1;
long A1[MAX_N], A2[MAX_N];
long Size1, Size2;

/* Quick sort */
void QSort( long *a, long l, long r )
{
  long i = l, j = r, x = a[(l + r + 1) >> 1], tmp;

  if (l >= r)
    return;
  while (i < j)
  {
    while (a[i] < x)
      i++;
    while (a[j] > x)
      j--;
    if (i < j)
      SWAP(a[i], a[j], tmp);
    if (i <= j)
      i++, j--;
  }
  QSort(a, l, j);
  QSort(a, i, r);
} /* End of 'QSort' function */

/* Views sub-tree */
void View( long *A, long *N, long v )
{
  long i;
  
  A[*N] = v;
  (*N)++;
  for (i = 0; i < TP[v]; i++)
    View(A, N, TC[v][i]);
} /* End of 'View' function */

/* Searhs the number in the sorted array */
int S( long *a, long n, long key )
{
  long min = 0, max = n - 1, ave;

  while (min <= max)
  {
    ave = (min + max) >> 1;
    if (a[ave] < key)
      min = ave + 1;
    else if (a[ave] > key)
      max = ave - 1;
    else
      return 1;
  }
  return 0;
} /* End of 'S' function */

/* Checks two sub-trees */
void Check( long v, long s1, long s2, long *a1, long *a2 )
{
  long i, j;

  *a1 = *a2 = 0;
  Size1 = 0;
  Size2 = 0;
  View(A1, &Size1, s1);
  View(A2, &Size2, s2);
  QSort(A1, 0, Size1 - 1);
  QSort(A2, 0, Size2 - 1);
  for (i = 0; i < Size1; i++)
    for (j = 0; j < P[A1[i]]; j++)
      if (S(A2, Size2, C[A1[i]][j]))
        *a2 = 1;
  for (i = 0; i < Size2; i++)
    for (j = 0; j < P[A2[i]]; j++)
      if (S(A1, Size1, C[A2[i]][j]))
        *a1 = 1;
} /* End of 'Check' function */

/* Compares two sub-trees */
int CmpT( long v, long s1, long s2 )
{
  long a1, a2;

  if (s1 == s2)
    return 0;
  Check(v, s1, s2, &a1, &a2);
  if (a1 == 1 && a2 == 1)
    flag = 0;
  else if (a2 == 1 || S(TC[s2], TP[s2], s1) == 1)
    return 0;
/*
  else if (S(TC[v], TP[v], s1) == 0)
    return 0;
*/
  else
    return 1;
  return -1;
} /* End of 'CmpT' function */

/* Qucik Sort (sorts sub-trees) */
void QSortT( long v, long l, long r )
{
/*
  long i = l, j = r, tmp, x = C[v][(l + r + 1) >> 1];

  if (l >= r || !flag)
    return;
  while (i < j)
  {
    while (CmpT(v, C[v][i], x) == 1)
      i++;
    if (!flag)
      return;
    while (CmpT(v, x, C[v][j]) == 1)
      j--;
    if (!flag)
      return;
    if (i < j)
      SWAP(C[v][i], C[v][j], tmp);
    if (i <= j)
      i++, j--;
  }
  QSortT(v, l, j);
  QSortT(v, i, r);
*/
  long i, j, tmp;

  for (i = l; i <= r; i++)
    for (j = i + 1; j <= r; j++)
      if (CmpT(v, C[v][i], C[v][j]) == 0)
        SWAP(C[v][i], C[v][j], tmp);
} /* End of 'QSortT' function */

/* Search function */
void Search( long v )
{
  long i;
  
  if (!flag)
    return;
  for (i = 0; i < TP[v]; i++)
    Search(TC[v][i]);
  QSortT(v, 0, P[v] - 1);
} /* End of 'Search' function */

/* The main program function */
int main( void )
{
  long i, j;
  
  freopen("dfs.in", "r", stdin);
  freopen("dfs.out", "w", stdout);
  /* Read input data */
  scanf("%li", &N);
  for (i = 1; i < N; i++)
    scanf("%li", &Prev[i]), Prev[i]--;
  for (i = 0; i < N; i++)
    TP[i] = TPos[i] = 0;
  for (i = 1; i < N; i++)
    TP[Prev[i]]++;
  for (i = 0; i < N; i++)
    TC[i] = malloc(sizeof(long) * TP[i]);
  for (i = 1; i < N; i++)
    TC[Prev[i]][TPos[Prev[i]]++] = i;
  for (i = 0; i < N; i++)
    QSort(TC[i], 0, TP[i] - 1);
  for (i = 0; i < N; i++)
  {
    scanf("%li", &P[i]);
    C[i] = malloc(sizeof(long) * P[i]);
    for (j = 0; j < P[i]; j++)
      scanf("%li", &C[i][j]), C[i][j]--;
  }
  /* Solve */
  Search(0);
  /* Output result */
  if (!flag)
    printf("NO\n");
  else
  {
    printf("YES\n");
    for (i = 0; i < N; i++)
    {
      printf("%li", P[i]);
      for (j = 0; j < P[i]; j++)
        printf(" %li", C[i][j] + 1);
      printf("\n");
    }
  }
  return 0;
} /* End of 'main' function */

