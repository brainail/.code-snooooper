#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "balls.h"

#define NN 26

int n,k;
int req[NN];
int final[NN];

int bsearch(int st, int fn) {
  int b,e;
  int c;
  int i;
  b=st;
  e=fn;
  while(e!=b) {
    c=(b+e)/2;
    for(i=0;i<n;i++)
      req[i]=0;
    for(i=st;i<=c;i++)
      req[i]=1;
    if(IsRadioactive(req)) {
      e=c;
    } else {
      b=c+1;
    }
  }
  return b;
}

void solv1() {
  int b,e;
  int c;
  int i;
  b=0;
  e=n-1;
  b=bsearch(b,e);
  for(i=0;i<n;i++)
    final[i]=0;
  final[b]=1;
  Finish(final);
}

void solvbs() {
  int x;
  int i;
  for(i=0;i<n;i++)
    final[i]=0;
  x=-1;
  for(i=0;i<k;i++) {
    x=bsearch(x+1,n-1);
    final[x]=1;
  }
  Finish(final);
}

int main() {
  int i,j;
  double fus1;
  int us1,us2;
  Init(&n,&k);
  for(i=0;i<n;i++) {
    req[i]=0;
    final[i]=0;
  }
  if(n==k) {
    for(i=0;i<n;i++)
      final[i]=1;
    Finish(final);
    return 0;
  } else {
    if(k==1) {
      solv1();
      return 0;
    }
//    us1=k*(int)(log(n)/(log(2))+1.0-0.00001);
    fus1=0.0;
    us1=0;
    for(i=0;i<k;i++) {
      fus1=log(n-i);
      fus1+=1.0;
      fus1-=0.00001;
      us1+=(int)fus1;
    }
    if(us1<n) {
      solvbs();
      return 0;
    }
    j=0;
    for(i=0;i<n;i++)
      final[i]=0;
    for(i=0;i<n;i++) {
      req[i]=1;
      if(IsRadioactive(req)) {
        final[i]=1;
        j++;
        if(j==k) {
          Finish(final);
        }
      } else {
        final[i]=0;
      }
      req[i]=0;
    }
    Finish(final);
  }
  return 0;
}
