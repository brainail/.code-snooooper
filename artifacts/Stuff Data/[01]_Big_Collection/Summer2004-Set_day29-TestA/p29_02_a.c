#include <stdio.h>

#include "balls.h"

#define m 30
#define inf 100

int mask[m], good[m], b[m][m][m], a[m][m][m], d[m][m][m], n, old,
    _stklen = 8 * 1024 * 1024;

int max2( int x, int y )
{
  if (x > y)
    return x;
  return y;
}

int calc( int n, int l, int r )
{
  int k, p, t;

  if (b[n][l][r])
    return a[n][l][r];
  b[n][l][r] = 1;
  if (r == 0 || l >= n || n == 0)
    return 0;
  a[n][l][r] = inf;
  for (k = 1; k <= n; k++)
  {
    t = calc(n - k, l, r) + 1;
    for (p = 1; p <= r; p++)
      t = max2(t, calc(k, 1, r)
                + calc(n - k, max2(0, l - p), r - p) + 1);
    if (a[n][l][r] > t)
    {
      a[n][l][r] = t;
      d[n][l][r] = k;
    }
  }
  return a[n][l][r];
}

int get( int n, int l, int r, int ll, int rr )
{
  int k = 0, i, j;

  if (r == 0 || l > r || ll > rr || n == 0)
    return 0;
  if (l == n)
  {
    for (i = ll; i <= rr; i++)
      good[i - 1] = 1;
    return n;
  }
  for (i = 1; i <= old; i++)
    mask[i - 1] = 0;
  for (i = ll, j = 1; j <= d[n][l][r]; j++, i++)
    mask[i - 1] = 1;
  if (!IsRadioactive(mask))
    return get(n - d[n][l][r], l, r, i, rr);
  k = get(d[n][l][r], 1, r, ll, i - 1);
  return k + get(n - d[n][l][r], max2(0, l - k), r - k, i, rr);
  //t = max2(t, calc(k, max2(l, 1), r) + calc(k, max2(0, l - p), r - p));
}

int main( void )
{
  int k;

  Init(&n, &k);
  old = n;
  calc(n, k, k);
  get(n, k, k, 1, n);
  Finish(good);
  return 0;
}

