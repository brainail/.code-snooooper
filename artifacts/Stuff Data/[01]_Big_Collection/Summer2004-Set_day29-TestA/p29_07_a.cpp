//#define DEBUG
#include <stdio.h>
#include <string.h>
#include "balls.h"

#define MASK_RANGE 16
#define MASK_MASK 0xFFFF
#define MASK_NUM 0x10000

int n_cells,n_balls,n_quests_bound;
int*distr,*quest,*quest_cur;
long c_vars_total,diff_best,diff_cur;
char*areas_cont,**areas; int*areas_marks,*areas_stops;
unsigned long*mask,n_masks; int*c_mask_same,*i_mask_same;
long*comps_cont,**comps;
int n_areas,c_cells_marked,c_areas_marked;

long countvars(int,int);
long countvars_0(int f_leave=0,int i_cell=0) {
  long c_vars=0;
  int i_area;
  distr[i_cell]=0;
  if (c_areas_marked<n_areas) {
    for (i_area=0; i_area<n_areas; i_area++) if (!areas_marks[i_area]) {
      if (areas_stops[i_area]<=i_cell) break;
      if (c_cells_marked>=n_balls) break;
    }
  } else i_area=n_areas;
  if (i_area>=n_areas) c_vars=countvars(f_leave,i_cell+1);
  if (!f_leave) distr[i_cell]=-1;
  return(c_vars);
}
long countvars_1(int f_leave=0,int i_cell=0) {
  if (c_cells_marked>=n_balls) return(0);
  long c_vars=0;
  int i_area;
  distr[i_cell]=1;
  for (i_area=0; i_area<n_areas; i_area++)
    if (areas[i_area][i_cell]) if (!areas_marks[i_area]++) c_areas_marked++;
  c_cells_marked++;
  c_vars=countvars(f_leave,i_cell+1);
  if (!f_leave) {
    for (i_area=0; i_area<n_areas; i_area++)
      if (areas[i_area][i_cell]) if (!--areas_marks[i_area]) c_areas_marked--;
    c_cells_marked--;
    distr[i_cell]=-1;
  }
  return(c_vars);
}
long countvars(int f_leave=0,int i_cell=0) {
  if (i_cell>=n_cells) {
    if (c_cells_marked==n_balls) if (c_areas_marked>=n_areas) return(1);
    return(0);
  }
  if (distr[i_cell]>=0) return(countvars(f_leave,i_cell+1));
  if (f_leave||(c_areas_marked<n_areas)) {
    long c_vars=0,c_vars_0=0,c_vars_1=0;
    c_vars+=c_vars_0=countvars_0(0,i_cell);
    c_vars+=c_vars_1=countvars_1(0,i_cell);
    if (f_leave) {
      if (c_vars_0<=0) countvars_1(f_leave,i_cell);
      else if (c_vars_1<=0) countvars_0(f_leave,i_cell);
    }
    return(c_vars);
  } else return(comps[n_cells-i_cell][n_balls-c_cells_marked]);
}

void findquest(int i_cell=0) {
  if (i_cell>=n_cells) return;
  if (distr[i_cell]<0) if (i_mask_same[i_cell]==c_mask_same[mask[i_cell]]) {
    long c_vars_cur;
    distr[i_cell]=0; quest_cur[i_cell]=1; c_mask_same[mask[i_cell]]++;
    c_vars_cur=countvars(0);
    diff_cur=c_vars_total-c_vars_cur;
    if (diff_cur<c_vars_cur) diff_cur=c_vars_cur;
    if (diff_best>diff_cur) {
      // || ((diff_best>diff_cur)&&(rand()&0xA00==0xA00)) 
      memcpy(quest,quest_cur,sizeof(int)*n_cells);
      diff_best=diff_cur;
    }
    if (c_vars_total-c_vars_cur<c_vars_cur) findquest(i_cell+1);
    distr[i_cell]=-1; quest_cur[i_cell]=0; c_mask_same[mask[i_cell]]--;
  }
  findquest(i_cell+1);
}

int main(void) {
  //srand(997);
  Init(&n_cells,&n_balls);
  n_quests_bound=(n_cells+1)<<1;
  #ifdef DEBUG
  printf("%d %d\n",n_cells,n_balls);
  #endif
  distr=new int[n_cells]; quest=new int[n_cells];
  quest_cur=new int[n_cells];
  areas_cont=new char[n_cells*n_quests_bound];
  areas=new char*[n_quests_bound];
  areas_marks=new int[n_quests_bound];
  areas_stops=new int[n_quests_bound];
  mask=new unsigned long[n_cells];
  c_mask_same=new int[MASK_NUM]; i_mask_same=new int[n_cells];
  comps_cont=new long[(n_cells+1)*(n_cells+1)]; comps=new long*[n_cells+1];
  { int i_quest,i_ball,i_cell;
    for (i_quest=0,i_cell=0; i_quest<n_quests_bound; i_quest++,i_cell+=n_cells)
    areas[i_quest]=&areas_cont[i_cell];
    for (i_ball=0,i_cell=0; i_ball<=n_cells; i_ball++,i_cell+=n_cells+1)
    comps[i_ball]=&comps_cont[i_cell];
  }
  memset(distr,-1,sizeof(int)*n_cells);
  memset(quest,0,sizeof(int)*n_cells);
  memset(quest_cur,0,sizeof(int)*n_cells);
  memset(areas_cont,0,sizeof(char)*(n_cells*n_quests_bound));
  memset(areas_marks,0,sizeof(int)*n_quests_bound);
  memset(areas_stops,-1,sizeof(int)*n_quests_bound);
  memset(mask,0,sizeof(unsigned long)*n_cells); n_masks=1;
  memset(c_mask_same,0,sizeof(int)*MASK_NUM);
  memset(i_mask_same,-1,sizeof(int)*n_cells);
  memset(comps_cont,0,sizeof(long)*(n_cells+1)*(n_cells+1));
  **comps=1;
  { int n,k;
    for (n=1; n<=n_cells; n++) {
      comps[n][0]=1;
      for (k=1; k<=n; k++) comps[n][k]=comps[n-1][k-1]+comps[n-1][k];
    }
  }
  n_areas=0; c_cells_marked=0; c_areas_marked=0;
  int i_cell;
  for (c_vars_total=countvars(1); c_vars_total>1; c_vars_total=countvars(1)) {
    memset(c_mask_same,0,sizeof(int)*n_masks);
    for (i_cell=0; i_cell<n_cells; i_cell++) if (distr[i_cell]<0) {
      i_mask_same[i_cell]=c_mask_same[mask[i_cell]]++;
    }
    memset(c_mask_same,0,sizeof(int)*n_masks);
    #ifdef DEBUG
    printf("%-4ld  ",c_vars_total); fflush(stdout);
    for (i_cell=0; i_cell<n_cells; i_cell++) {
      if (i_cell) printf(" ");
      if (distr[i_cell]>=0) printf("%d",distr[i_cell]);
      else printf("?");
    } printf("\n"); fflush(stdout);
    /*
    printf("%-4d  ",n_areas); fflush(stdout);
    for (i_cell=0; i_cell<n_cells; i_cell++) {
      if (i_cell) printf(" ");
      printf("%04lX(%-2d)",mask[i_cell],i_mask_same[i_cell]);
    } printf("\n"); fflush(stdout);
    */
    #endif
    diff_best=c_vars_total;
    findquest();
    #ifdef DEBUG
    printf("%-4ld  ",diff_best); fflush(stdout);
    for (i_cell=0; i_cell<n_cells; i_cell++) {
      if (i_cell) printf(" ");
      printf("%d",quest[i_cell]);
    } printf("\n"); fflush(stdout);
    #endif
    if (IsRadioactive(quest)) {
      #ifdef DEBUG
      printf("1\n"); fflush(stdout);
      #endif
      for (i_cell=0; i_cell<n_cells; i_cell++) if (quest[i_cell]) {
        areas[n_areas][i_cell]=1;
        areas_stops[n_areas]=i_cell;
        if (n_areas<MASK_RANGE) mask[i_cell]|=1L<<n_areas;
      } if (n_areas++<MASK_RANGE) n_masks<<=1;
    } else {
      #ifdef DEBUG
      printf("0\n"); fflush(stdout);
      #endif
      for (i_cell=0; i_cell<n_cells; i_cell++) if (quest[i_cell])
      distr[i_cell]=0;
    }
  }
    #ifdef DEBUG
    printf("%-4ld  ",c_vars_total); fflush(stdout);
    for (i_cell=0; i_cell<n_cells; i_cell++) {
      if (i_cell) printf(" ");
      if (distr[i_cell]>=0) printf("%d",distr[i_cell]);
      else printf("?");
    } printf("\n"); fflush(stdout);
    #endif
  Finish(distr);
  delete[]mask; delete[]c_mask_same; delete[]i_mask_same;
  delete[]areas_cont; delete[]areas;
  delete[]areas_marks; delete[]areas_stops;
  delete[]quest_cur;
  //delete[]distr; delete[]quest;
  return(0);
}



