/* 29.06.04 SK1 */

#include <stdio.h>
#include <math.h>
#include "balls.h"

#define MAX(A, B) ((A) > (B) ? (A) : (B))

#define MAX_N 25

int Mask[MAX_N];
int N, K, L, H = 0;
int AL[MAX_N];

/* Calculates the worst case */
int F( int N, int K, int K1 )
{
  int a, b;
  
  if (K * 3 >= N)
    return N - 1;
  a = F((K - 1) * N / K1, K, K1);
  b = K * (int)(log2((double)N / K1) + 0.99);
  return MAX(a, b) + K1;
} /* End of 'F' function */

/* Tests balls from L to R */
int IsR( int *M, int L, int R )
{
  int i;

  for (i = 0; i < N; i++)
    Mask[i] = 0;
  for (i = L; i <= R; i++)
    Mask[M[i]] = 1;
  return IsRadioactive(Mask);
} /* End of 'IsR' function */

/* Solve 1 */
void Solve1( int *Ans, int *M, int L, int R, int N )
{
  int i, ave = (L + R) >> 1;

  if (L == R)
    Ans[L] = 1;
  else if (IsR(M, L, ave))
  {
    for (i = ave + 1; i <= R; i++)
      Ans[i] = 0;
    Solve1(Ans, M, L, ave, N);
  }
  else
  {
    for (i = L; i <= ave; i++)
      Ans[i] = 0;
    Solve1(Ans, M, ave + 1, R, N);
  }
} /* End of 'Solve1' function */

/* Solve */
void Solve( int *Ans, int *M, int N )
{
  int i, j, k, K1 = K + 1, sum = 0;
  int Ans2[MAX_N], M2[MAX_N], N2 = 0;
  int A[MAX_N];

  if (N == K)
    for (i = 0; i < N; i++)
      Ans[i] = 1;
  else if (K == 1)
    Solve1(Ans, M, 0, N - 1, N);
  else if (K * 3 >= N)
  {
    for (i = 0; i < N - 1; i++)
      Ans[i] = IsR(M, i, i);
    sum = 0;
    for (i = 0; i < N - 1; i++)
      if (Ans[i])
        sum++;
    if (sum == K)
      Ans[N - 1] = 0;
    else
      Ans[N - 1] = 1;
  }
  else
  {
    int flag = 1, x1, x2;

    K1 = K;
    while (flag && K1 < N)
    {
      x1 = F(N, K, K1);
      x2 = F(N, K, K1 + 1);
      if (x1 >= x2)
        K1++;
      else
        flag = 0;
    }
    for (i = 0; i < K1; i++)
      A[i] = IsR(M, N * i / K1, N * (i + 1) / K1 - 1);
    for (i = 0; i < K1; i++)
      if (A[i])
        sum++;
      else
        for (j = N * i / K1; j < N * (i + 1) / K1; j++)
          Ans[j] = 0;
    if (sum == K)
    {
      for (i = 0; i < K1; i++)
        if (A[i])
          Solve1(Ans, M, N * i / K1, N * (i + 1) / K1 - 1, N);
    }
    else
    {
      for (i = 0; i < K1; i++)
        if (A[i])
          for (j = N * i / K1; j < N * (i + 1) / K1; j++)
            M2[N2++] = M[j];
      Solve(Ans2, M2, N2);
      k = 0;
      for (i = 0; i < K1; i++)
        if (A[i])
          for (j = N * i / K1; j < N * (i + 1) / K1; j++)
            Ans[j] = Ans2[k++];
    }
  }
} /* End of 'Solve' function */

/* The main program function */
int main( void )
{
  int i;
  static int Ans[MAX_N];
  static int M[MAX_N];

  Init(&N, &K);
  for (i = 0; i < N; i++)
    M[i] = i;
  if (K == 1)
    Solve1(Ans, M, 0, N - 1, N);
  else
    Solve(Ans, M, N);
  Finish(Ans);
  return 0;
} /* End of 'main' function */
