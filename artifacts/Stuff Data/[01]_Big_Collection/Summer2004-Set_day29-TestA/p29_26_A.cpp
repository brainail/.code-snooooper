/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;
#include "balls.h"

#define DBG if(0)

void die(const char*msg) { cerr<<msg<<endl; exit(1); }

enum { yes=1, no=0, unk=2 };

int N,K;
int *ra;

inline void set_mask(int *xmask, int l, int r) {
  for (int i=0; i<l; i++) xmask[i] = no;
  for (int i=l; i<r; i++) xmask[i] = yes;
  for (int i=r; i<N; i++) xmask[i] = no;
}


int call_cnt = 0;
int radioactive(int *xmask) {
	int r = IsRadioactive(xmask);
	DBG {
		call_cnt++;
		cerr << call_cnt << "`s call, with mask: ";
		for (int i=0; i<N; i++) {
			cerr << xmask[i] << ' ';
		}
		cerr << " == " << r;
		cerr << endl;
	}
	return r;
}


int *xmask;
//                     k - maximum number radioactives
int scan(int l, int r, int kmin, int kmax)
{
	if (kmax==0) {
		for (int i=l; i<r; i++)
			ra[i] = no;
		return 0;
	}
	if (kmin==r-l) {
		for (int i=l; i<r; i++)
			ra[i] = yes;
		return kmin;
	}
	/*
	if (r==l+1) {
  		set_mask(xmask,l,r);
		if (radioactive(xmask)) {
			ra[l] = yes;
			return 1;
		} else {
			ra[l] = no;
			return 0;
		}
	}
	*/
	int m = (r+l)/2;
	int left_k=0;
	int right_k=0;

	set_mask(xmask,l,m);
	if (radioactive(xmask)) {
		left_k = scan(l,m,1,min(kmax,m-l));
	} else {
		for (int i=l; i<m; i++)
		  ra[i] = no;
	}
	if ((kmax-left_k) > 0) {
		if (m+1 != r) {
			right_k = scan(m,r,max(0,kmin-left_k),min(kmax-left_k,r-m));
		} else {
			set_mask(xmask,m,r);
			if (radioactive(xmask)) {
				right_k = 1;
				ra[m] = yes;
			} else {
				right_k = 0;
				ra[m] = no;
			}
		}
	} else {
		// left_k == kmax
		for (int i=m; i<r; i++)
			ra[i] = no;
	}
	return (left_k + right_k);
}

void solve()
{
	xmask = new int[N];
	if (scan(0,N,K,K) != K)
	  die("internal error");
	delete[] xmask;
}

int main(void)
{
	Init(&N,&K);
    ra = new int[N];  
	for (int i=0; i<N; i++) ra[i] = unk;
	solve();
	DBG {
			for (int i=0; i<N; i++)
				cerr << ra[i] << ' ';
			cerr << endl;
 		}
	Finish(ra);
	delete[] ra;
	return 0;
}
