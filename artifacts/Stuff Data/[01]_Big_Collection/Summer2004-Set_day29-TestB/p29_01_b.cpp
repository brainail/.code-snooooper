#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 64
#define KK 4096

FILE *fi, *fo;
int a[NN][NN];
int b[NN][KK];
int d[NN][KK];
int r[NN][NN];
int ddx[NN][NN];
int ddy[NN][NN];
int mp[NN][NN];
int segm[NN];
int n,m,k;

int min(int a, int b) { return (a<b)?a:b; }

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

int getmisspaint(int lineid, int steps) {
  int i,j;
  int k,l;
  int s;
  k=0;
  segm[0]=1;
  if(steps==0)
    return m;
  for(i=1;i<m;i++) {
    if(a[lineid][i-1]==a[lineid][i]) {
      segm[k]++;
    } else {
      segm[++k]=1;
    }
  }
  for(i=0;i<=steps;i++) {
    r[0][i]=0;
    ddx[0][i]=-1;
  }
  r[0][0]=m;
  for(i=0;i<m;i++) {
    r[i][0]=m;
    ddx[i][0]=-1;
  }
  l=0;
  s=segm[0];
  ddx[0][i]=-1;
  for(i=1;i<m;i++) {
    if(i>=s) {
      s+=segm[++l];
    }
    for(j=1;j<=steps;j++) {
      ddx[i][j]=-1;
      r[i][j]=i+1;
      if(a[lineid][i-1]==a[lineid][i]) {
        r[i][j]=r[i-1][j];
      } else {
        if(segm[l-1]==i) {
          if(j>0)
            r[i][j]=segm[l-1];
          else
            r[i][j]=i+1;
        } else {
          r[i][j]=min(r[i][j],r[i-segm[l-1]-1][j]+segm[l-1]);
        }
        if(j>0)
          r[i][j]=min(r[i][j],r[i-1][j-1]);
      }
    }
  }
  l=m+1;
  j=-1;
  for(i=0;i<m;i++) {
    if(r[i][steps]+m-i-1<l) {
      l=r[i][steps]+m-i-1;
      j=i;
    }
  }
  return r[j][steps]+m-j-1;
}

void wrline(int ln, int v, int w) {
  int i;
  if(v<0)
    return ;
  wrline(ln,ddx[v][w],ddy[v][w]);
  for(i=ddx[v][w]+1;i<=v;i++) {
    fprintf(fo,"%d",a[ln][v]);
  }
}

void paint(int lineid, int steps) {
  int i,j;
  int k,l;
  int s;
  k=0;
  segm[0]=1;
  for(i=1;i<m;i++) {
    if(a[lineid][i-1]==a[lineid][i]) {
      segm[k]++;
    } else {
      segm[++k]=1;
    }
  }
  for(i=0;i<steps;i++) {
    r[0][i]=0;
  }
  for(i=0;i<m;i++)
    r[i][0]=m;
  r[0][0]=m;
  l=0;
  s=segm[0];
  for(i=1;i<m;i++) {
    if(i>=s) {
      s+=segm[++l];
    }
    for(j=0;j<=steps;j++) {
      r[i][j]=i+1;
      if(a[lineid][i-1]==a[lineid][i]) {
        r[i][j]=r[i-1][j];
        ddx[i][j]=ddx[i-1][j];
        ddy[i][j]=ddy[i-1][j];
      } else {
        if(segm[l-1]==i) {
          if(j>0) {
            r[i][j]=segm[l-1];
            ddx[i][j]=-1;
          } else {
            r[i][j]=i+1;
            ddx[i][j]=-1;
          }
        } else {
//          r[i][j]=min(r[i][j],r[i-segm[l-1]-1][j]);
          if(r[i][j]>r[i-segm[l-1]-1][j]+segm[l-1]) {
            ddx[i][j]=ddx[i-segm[l-1]-1][j];
            ddy[i][j]=j;
            r[i][j]=r[i-segm[l-1]-1][j]+segm[l-1];
          }
        }
        if(j>0) {
//            r[i][j]=min(r[i][j],r[i-1][j-1]);
          if(r[i][j]>r[i-1][j-1]) {
            ddx[i][j]=i-1;
            ddy[i][j]=j-1;
            r[i][j]=r[i-1][j-1];
          }
        }
      }
    }
  }
  l=m+1;
  j=-1;
  for(i=0;i<m;i++) {
    if(r[i][steps]+m-i-1<l) {
      l=r[i][steps]+m-i-1;
      j=i;
    }
  }
  if(steps==0) {
    for(i=0;i<m;i++)
      fprintf(fo,"2");
    fprintf(fo,"\n");
  } else {
    wrline(lineid,j,steps);
    for(i=j+1;i<m;i++) {
      fprintf(fo,"2");
    }
    fprintf(fo,"\n");
  }
}

void wr(int ln, int k) {
  if(ln<0)
    return;
  wr(ln-1,k-d[ln][k]);
  if(ln>0)
    paint(ln,d[ln][k]);
  else
    paint(ln,k);
//  wrline(ln,d[ln][k]);
}

int main() {
  int i,j,l;
  int f;
  fi=fopen("masterpiece.in","rt");
  fo=fopen("masterpiece.out","wt");

  fscanf(fi,"%d%d%d",&n,&m,&k);
  for(i=0;i<n;i++) {
    for(j=0;j<m;j++) {
      a[i][j]=getcc()-'0';
    }
  }

  for(i=0;i<n;i++) {
    d[i][0]=-1;
    for(j=0;j<=m;j++) {
      mp[i][j]=getmisspaint(i,j);
    }
  }
  for(i=0;i<=m;i++)
    b[0][i]=mp[0][i];
  for(i=m+1;i<=k;i++)
    b[0][i]=0;
  for(i=1;i<n;i++) {
    for(j=0;j<=k;j++) {
      d[i][j]=-1;
      b[i][j]=n*m+1;
      for(l=0;l<=min(j,m);l++) {
//        f=getmisspaint(i,l);
        f=mp[i][l];
//        b[i][j]=min(b[i-1][j-l],f);
        if(b[i][j]>b[i-1][j-l]+f) {
          b[i][j]=b[i-1][j-l]+f;
          d[i][j]=l;
        }
      }
    }
  }
  fprintf(fo,"%d\n",b[n-1][k]);
  wr(n-1,k);

  fclose(fi);
  fclose(fo);
  return 0;
}
