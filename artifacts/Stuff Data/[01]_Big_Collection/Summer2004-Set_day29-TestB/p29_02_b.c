#include <stdio.h>

#define mm 65
#define inf 10000

int n, m, a[2][mm][mm], b[mm][mm], aa[2][mm][mm], d[mm][mm * mm],
    c[mm][mm * mm], _stklen = 8 * 1024 * 1024;
char s[mm][mm];

int min2( int x, int y )
{
  if (x < y)
    return x;
  return y;
}

void string( int i )
{
  int j, l, f;

  for (j = 1; j <= m; j++)
    a[1][j][0] = a[0][j][0] = m;
  for (l = 1; l <= m; l++)
  {
    a[1][0][l] = a[0][0][l] = 0;
    for (j = 1; j <= m; j++)
    {
      a[1][j][l] = a[1][j][l - 1];
      aa[1][j][l] = aa[0][j][l] = 0;
      a[0][j][l] = a[0][j][l - 1];
      f = (s[i][j] == '1');
      if (a[f][j][l] > a[f][j - 1][l])
      {
        a[f][j][l] = a[f][j - 1][l];
        aa[f][j][l] = 1;
      }
      if (a[f][j][l] > a[!f][j - 1][l - 1])
      {
        a[f][j][l] = a[!f][j - 1][l - 1];
        aa[f][j][l] = 2;
      }
      if (a[!f][j][l] > a[!f][j - 1][l] + 1)
      {
        a[!f][j][l] = a[!f][j - 1][l] + 1;
        aa[!f][j][l] = 1;
      }
      if (a[!f][j][l] > a[f][j - 1][l - 1] + 1)
      {
        a[!f][j][l] = a[f][j - 1][l - 1] + 1;
        aa[!f][j][l] = 2;
      }
    }
  }
  b[i][0] = m;
  for (j = 1; j <= m; j++)
    b[i][j] = min2(a[0][m][j], a[1][m][j]);
}

void drawstr( int n, int m, int k, int f )
{
  int i;

  if (m == 0)
    return;
  if (k == 0)
  {
    for (i = 1; i <= m; i++)
      printf("2");
    return;
  }
  if (aa[f][m][k] == 0)
    drawstr(n, m, k - 1, f);
  else
  {
    if (aa[f][m][k] == 1)
      drawstr(n, m - 1, k, f);
    else
      drawstr(n, m - 1, k - 1, !f);
    printf("%d", f);
  }
}

void draw( int n, int k )
{
  int i;

  if (n == 0)
    return;
  draw(n - 1, k - d[n][k]);
  string(n);
  if (d[n][k] == 0)
    for (i = 1; i <= m; i++)
      printf("2");
  else
    drawstr(n, m, d[n][k], a[0][m][d[n][k]] > a[1][m][d[n][k]]);
  printf("\n");
}

int main( void )
{
  int k, i, j, l;

  freopen("masterpiece.in", "r", stdin);
  freopen("masterpiece.out", "w", stdout);
  scanf("%d%d%d", &n, &m, &k);
  for (i = 1; i <= n; i++)
    scanf("%s", &s[i][1]);
  for (i = 1; i <= n; i++)
    string(i);
  for (i = 1; i <= k; i++)
    c[0][k] = inf;
  for (i = 1; i <= n; i++)
    for (l = 0; l <= k; l++)
    {
      c[i][l] = inf;
      for (j = 0; j <= l && j <= m; j++)
        if (l - j >= 0 && c[i][l] > c[i - 1][l - j] + b[i][j])
        {
          c[i][l] = c[i - 1][l - j] + b[i][j];
          d[i][l] = j;
        }
    }
  for (i = -1, l = 0; l <= k; l++)
    if (i == -1 || c[n][l] < c[n][i])
      i = l;
  printf("%d\n", c[n][i]);
  draw(n, i);
  return 0;
}

