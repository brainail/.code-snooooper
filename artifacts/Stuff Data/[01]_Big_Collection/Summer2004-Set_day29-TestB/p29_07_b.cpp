#define ONLINE
#include <stdio.h>
#include <string.h>

int n_x,n_y,n_cells,n_days,c_painted[3];
char*wanted_cont,**wanted,*cur_cont[3],**cur[3];

void createarray(char*&cont,char**&array) {
  int i_line,i_cell;
  cont=new char[n_cells]; array=new char*[n_x];
  for (i_line=0,i_cell=0; i_line<n_x; i_line++,i_cell+=n_y)
  array[i_line]=&cont[i_cell];
}
void deletearray(char*&cont,char**&array) {
  delete[]cont; delete[]array;
}


int main(void) {
  #ifdef ONLINE
  freopen("masterpiece.in","rt",stdin);
  freopen("masterpiece.out","wt",stdout);
  #endif
  scanf("%d%d%d",&n_x,&n_y,&n_days);
  n_cells=n_x*n_y;
  createarray(wanted_cont,wanted);
  createarray(cur_cont[0],cur[0]);
  createarray(cur_cont[1],cur[1]);
  createarray(cur_cont[2],cur[2]);
  int x,y;
  { char*str=new char[n_y+2];
    for (x=0; x<n_x; x++) {
      scanf("%s",str);
      for (y=0; y<n_y; y++) wanted[x][y]=(str[y]=='1'?1:0);
    }
    delete[]str;
  }
  int i_day,y0,y1,c_add,c_sub;
  memset(cur_cont[0],-1,sizeof(char)*n_cells); c_painted[0]=0;
  memset(cur_cont[1],-1,sizeof(char)*n_cells); c_painted[1]=0;
  memset(cur_cont[2],-1,sizeof(char)*n_cells); c_painted[2]=0;
  for (i_day=0; i_day<n_days; i_day++) {
    for (x=0; x<n_x; x++) for (y0=0; y0<n_y; y0=y1) {
      for (; y0<n_y; y0++) if (cur[0][x][y0]!=wanted[x][y0]) break;
      if (y0>=n_y) break;
      c_add=0;
      for (y1=y0; y1<n_y; y1++) {
        if (cur[0][x][y1]==wanted[x][y1]) break;
        if (wanted[x][y1]==wanted[x][y0]) c_add++;
      }
        if (c_painted[2]<c_painted[0]+c_add) {
          memcpy(cur_cont[2],cur_cont[0],sizeof(char)*n_cells);
          for (y=y0; y<y1; y++) cur[2][x][y]=wanted[x][y0];
          c_painted[2]=c_painted[0]+c_add;
        }
      if ((y0<=0)||(y1>=n_y)||(cur[0][x][y0]<0)) {
        if (c_painted[1]<c_painted[0]+c_add) {
          memcpy(cur_cont[1],cur_cont[0],sizeof(char)*n_cells);
          for (y=y0; y<y1; y++) cur[1][x][y]=wanted[x][y0];
          c_painted[1]=c_painted[0]+c_add;
        }
      } else {
        c_sub=0;
        for (y=0; y<y0; y++) if (cur[0][x][y]!=wanted[x][y0]) c_sub++;
        if (c_painted[1]<c_painted[0]+c_add-c_sub) {
          memcpy(cur_cont[1],cur_cont[0],sizeof(char)*n_cells);
          for (y=0; y<y1; y++) cur[1][x][y]=wanted[x][y0];
          c_painted[1]=c_painted[0]+c_add-c_sub;
        }
        c_sub=0;
        for (y=y1; y<n_y; y++) if (cur[0][x][y]!=wanted[x][y0]) c_sub++;
        if (c_painted[1]<c_painted[0]+c_add-c_sub) {
          memcpy(cur_cont[1],cur_cont[0],sizeof(char)*n_cells);
          for (y=y0; y<n_y; y++) cur[1][x][y]=wanted[x][y0];
          c_painted[1]=c_painted[0]+c_add-c_sub;
        }
      }
    }
    { char*cont,**array; int count;
      cont=cur_cont[0]; array=cur[0]; count=c_painted[0];
      cur_cont[0]=cur_cont[1]; cur[0]=cur[1]; c_painted[0]=c_painted[1];
      cur_cont[1]=cur_cont[2]; cur[1]=cur[2]; c_painted[1]=c_painted[2];
      cur_cont[2]=cont; cur[2]=array; c_painted[2]=count;
    } memcpy(cur_cont[2],cur_cont[1],sizeof(char)*n_cells);
    c_painted[2]=c_painted[1];
  }
  printf("%d\n",n_cells-c_painted[0]);
  for (x=0; x<n_x; x++) {
    for (y=0; y<n_y; y++) switch (cur[0][x][y]) {
      case 0: printf("0"); break;
      case 1: printf("1"); break;
      default: printf("2");
    }
    printf("\n");
  }
  deletearray(cur_cont[0],cur[0]);
  deletearray(cur_cont[1],cur[1]);
  deletearray(cur_cont[2],cur[2]);
  deletearray(wanted_cont,wanted);
  #ifdef ONLINE
  fclose(stdin);
  fclose(stdout);
  #endif
  return(0);
}



