/* 29.06.04 SK1 */

#include <stdio.h>

#define MAX 200

long K, N, M;
long A[MAX][MAX];
long B[MAX][MAX];
char S[MAX + 1];

/* The main program function */
int main( void )
{
  long i, j, k, c, dif = 0, flag = 1, max, mi, mj, mk, mc;

  freopen("masterpiece.in", "r", stdin);
  freopen("masterpiece.out", "w", stdout);
  scanf("%li%li%li", &N, &M, &K);
  for (i = 0; i < N; i++)
  {
    scanf("%s", S);
    for (j = 0; j < M; j++)
      A[i][j] = S[j] - '0', B[i][j] = 2;
  }
  while (K-- > 0 && flag)
  {
    max = 0;
    for (i = 0; i < N; i++)
      for (c = 0; c <= 1; c++)
      {
        long l, d = 0, s = 0;
       
        j = k = -1;
        while (j < M && k < M)
        {
          if (s - d <= 0)
            j = k + 1, s = d = 0;
          k++;
          if (A[i][k] == c)
            s++;
          if (A[i][k] == B[i][k])
            d++;
          if (s - d > max)
            max = s - d, mi = i, mj = j, mk = k, mc = c;
        }
      }
    if (max)
      for (i = mj; i <= mk; i++)
        B[mi][i] = mc;
    else
      flag = 0;
  }
  for (i = 0; i < N; i++)
    for (j = 0; j < M; j++)
      if (A[i][j] != B[i][j])
        dif++;
  printf("%i\n", dif);
  for (i = 0; i < N; i++)
  {
    for (j = 0; j < M; j++)
      printf("%li", B[i][j]);
    printf("\n");
  }
  return 0;
} /* End of 'main' function */

