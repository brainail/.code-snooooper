/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;

#define DBG if(0)

void die(const char*msg) { cerr<<msg<<endl; exit(1); }

const int NMAX=64;
const int MMAX=64;
const int KMAX=4096;

template<typename T> 
T** alloc2d(int n, int m)
{
	char *p = new char[ sizeof(T*)*n + sizeof(T)*n*m ];
	T** r = (T**)p;
	T* arr = (T*)( p + sizeof(T*)*n );
	for (size_t i=0; i<n; i++, arr+=m)
		r[i] = arr;
	return r;
}
template <typename T>
inline void free2d(T** array)
{
	delete[] (char*)array;
}

template <typename T>
int diff_count (T** a, T** b, int N, int M)
{
	int r = 0;
	for (int i=0; i<N; i++)
		for (int j=0; j<M; j++)
			if (a[i][j] != b[i][j])
				r++;
	return r;
}

int** solve(int **a, int N, int M, int K)
{
	int **b = alloc2d<int>(N,M);
	int *nzero = new int[N];
	memset(nzero,0,N*sizeof(int));

	for (int i=0; i<N; i++)
		for (int j=0; j<M; j++)
		{	
			if (a[i][j] == 0)
				nzero[i]++;
		}
	for (int i=0; i<N; i++)
		for (int j=0; j<M; j++)
			b[i][j] = 2;
	/*
	int **cnv = new int*[N];
	for (int i=0; i<N; i++) 
		cnv[i] = b[i];
	*/                  // shit
	for (int i=0; i<N && i<K; i++)
	{
		int clr = (nzero[i]*2 > M) ? (0) : (1);
		for (int j=0; j<M; j++)
			b[i][j] = clr;
	}
	return b;
}

int main(void)
{
	int N,M,K;
	int **a;// source
	int **b;// result
	{
		ifstream fi("masterpiece.in");
		fi >> N >> M >> K;
		a = alloc2d<int>(N,M);
		char c;
		for (int i=0; i<N; i++)
			for (int j=0; j<M; j++)
			{
				char c;
				for (c='\0'; c!='1'&&c!='0'; fi>>c)
					;
				a[i][j] = c - '0';
			}
	}
	b = solve(a, N, M, K);
	{
		ofstream fo("masterpiece.out");
		fo << diff_count(a,b,N,M) << endl;
		for (int i=0; i<N; i++) {
			for (int j=0; j<M; j++)
				fo << b[i][j];
			fo << endl;
		}
	}
	free2d(a);
	free2d(b);
	return 0;
}
