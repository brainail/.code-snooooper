#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 2510
#define MM 210
#define SS 100000
#define MANY 100000000

int _stklen = 2000000;

struct REB {
  int a;
  int w;
  int tp;
  REB *p;
};

struct QW {
  bool r,l,d,u;
};

FILE *fi, *fo;
REB swap[SS];
int V;
int iswap=0;
int vw[NN];
REB *g[NN];
QW qw[NN];
//int rr[NN][MM];
//int dd[NN][MM];
int r1[NN];
int d1[NN];
int r2[NN];
int d2[NN];
int r3[NN];
int d3[NN];
int r[NN];
int d[NN];
int tmp[MM][3];
bool used[NN];
bool ishole[NN];
int h[NN];
int hs;
int b[NN];
int n,m;

REB *getnew() {
  return &swap[iswap++];
}

int GetNum(int x, int y) {
  return x*m+y;
}

void GetPoint(int v, int *x, int *y) {
  (*y)=v%m;
  v/=m;
  (*x)=v%n;
}

void siftup(int p, int q) {
  int i,j;
  int x;
  i=q;
  j=(q-1)/2;
  x=h[i];
  if(j>=p) if(r[h[j]]<r[x]) j=p-1;
  while((j>=p)&&(j<i)) {
    h[i]=h[j];
    b[h[i]]=i;
    i=j;
    j=(j-1)/2;
    if(j>=p) if(r[h[j]]<r[x]) j=p-1;
  }
  h[i]=x;
  b[h[i]]=i;
}

void siftdown(int p, int q) {
  int i,j;
  int x;
  i=p;
  j=2*p+1;
  x=h[i];
  if(j<q-1) if(r[h[j+1]]>r[h[j]]) j++;
  if(j<q) if(r[h[j]]>r[x]) j=q+1;
  while(j<q) {
    h[i]=h[j];
    b[h[i]]=i;
    i=j;
    j=2*j+1;
    if(j<q-1) if(r[h[j+1]]>r[h[j]]) j++;
    if(j<q) if(r[h[j]]>r[x]) j=q+1;
  }
  h[i]=x;
  b[h[i]]=i;
}

void Dejkstr(int st) {
  //No heap!!!!
  int i,j;
  int k,m;
  REB *t;
  for(i=0;i<V;i++) {
    r[i]=MANY;
    used[i]=false;
    d[i]=-1;
  }
  r[st]=0;
  while(true) {
    k=-1;
    m=MANY;
    for(i=0;i<V;i++) {
      if(!used[i]) {
        if(r[i]<m) {
          m=r[i];
          k=i;
        }
      }
    }
    if(k<0)
      return ;
    used[k]=true;
    for(t=g[k];t!=NULL;t=t->p) {
      if(r[k]+t->w<r[t->a]) {
        r[t->a]=r[k]+t->w;
        d[t->a]=k;
      }
    }
  }
}

int getcc() {
  int c;
  c=getc(fi);
  while((c==' ')||(c=='\n')||(c=='\r')||(c=='\t'))
    c=getc(fi);
  return c;
}

void GetNb(int v, int *nb, int *nnb) {
  int k;
  int x,y;
  int w;
  k=0;
  GetPoint(v,&x,&y);
  if(x>0) {
    if(!qw[v].l) {
      if(!qw[GetNum(x-1,y)].r) {
        nb[k++]=GetNum(x-1,y);
      }
    }
  }
  if(x<n-1) {
    if(!qw[v].r) {
      if(!qw[GetNum(x+1,y)].l) {
        nb[k++]=GetNum(x+1,y);
      }
    }
  }
  if(y>0) {
    if(!qw[v].u) {
      if(!qw[GetNum(x,y-1)].d) {
        nb[k++]=GetNum(x,y-1);
      }
    }
  }
  if(y<m-1) {
    if(!qw[v].d) {
      if(!qw[GetNum(x,y+1)].u) {
        nb[k++]=GetNum(x,y+1);
      }
    }
  }
  (*nnb)=k;
}

void Add(int x, int y, int w, int tp) {
  REB *t;
  t=getnew();
  t->a=y;
  t->p=g[x];
  g[x]=t;
  t->w=w;
  t->tp=tp;
}

void AddReb(int x, int y, int tp) {
  Add(x,y,vw[x]+vw[y],tp);
}

void BuildG() {
  int i,j,k,l,kk;
  int x,y,v;
  int xx,yy,stx,sty;
  int nb[4],nnb;
  int f;
  fscanf(fi,"%d",&k);
  for(i=0;i<k;i++) {
    fscanf(fi,"%d%d",&tmp[i][0],&tmp[i][1]);
    tmp[i][3]=getcc();
  }
  kk=k;
  fscanf(fi,"%d",&k);
  for(i=0;i<k;i++) {
    fscanf(fi,"%d%d",&x,&y);
    v=GetNum(x-1,y-1);
    j=getcc();
    switch(j) {
      case 'P':
        vw[v]+=2;
        break;
      case 'M':
        vw[v]+=4;
        j=getcc();
        break;
      case 'A':
        vw[v]+=6;
        j=getcc();
        break;
      case 'K':
        break;
      case 'B':
        j=getcc();
        if(j=='A')
          vw[v]+=8;
        else
          vw[v]+=10;
        break;
    }
  }
  fscanf(fi,"%d",&k);
  for(i=0;i<k;i++) {
    fscanf(fi,"%d%d",&x,&y);
    v=GetNum(x-1,y-1);
    j=getcc();
    switch(j) {
      case 'R':
        qw[v].r=true;
        break;
      case 'L':
        qw[v].l=true;
        break;
      case 'U':
        qw[v].u=true;
        break;
      case 'D':
        qw[v].d=true;
        break;
    }
  }
  fscanf(fi,"%d",&k);
  for(i=0;i<k;i++) {
    fscanf(fi,"%d",&l);
    fscanf(fi,"%d%d",&stx,&sty);
    if(l>1) {
      stx--; sty--;
      xx=stx;
      yy=sty;
      ishole[GetNum(xx,yy)]=true;
      for(j=1;j<l;j++) {
        fscanf(fi,"%d%d",&x,&y);
        x--; y--;
        ishole[GetNum(x,y)]=true;
        GetNb(GetNum(x,y),nb,&nnb);
        for(f=0;f<nnb;f++) {
          AddReb(GetNum(xx,yy),nb[f],1);
        }
        AddReb(GetNum(xx,yy),GetNum(x,y),2);
        xx=x;
        yy=y;
      }
      GetNb(GetNum(stx,sty),nb,&nnb);
      for(f=0;f<nnb;f++) {
        AddReb(GetNum(xx,yy),nb[f],1);
      }
      AddReb(GetNum(xx,yy),GetNum(stx,sty),2);
    }
  }
  for(i=0;i<kk;i++) {
    AddReb(V-1,GetNum(tmp[i][0]-1,tmp[i][1]-1),1);
    AddReb(GetNum(tmp[i][0]-1,tmp[i][1]-1),V-1,1);
  }
  for(i=0;i<n;i++) {
    for(j=0;j<m;j++) {
      if(!ishole[GetNum(i,j)]) {
        GetNb(GetNum(i,j),nb,&nnb);
        for(k=0;k<nnb;k++) {
          AddReb(GetNum(i,j),nb[k],1);
        }
      }
    }
  }
}

int GetRebTp(int v, int u) {
  REB *t;
  for(t=g[v];t!=NULL;t=t->p)
    if(t->a==u)
      return t->tp;
  return 0;
}

int GetNextHole(int v) {
  REB *t;
  for(t=g[v];t!=NULL;t=t->p) {
    if(t->tp==2)
      return t->a;
  }
  return V-1;
}

void WrPathFw(int *d, int v,bool ff) {
  int x,y;
  int f;
  if(v<0)
    return ;
  if(d[v]<0) {
//    GetPoint(v,&x,&y);
//    fprintf(fo,"%d %d\n",x+1,y+1);
    return ;
  }
  if(ishole[d[v]]) {
    WrPathFw(d,d[v],true);
    f=GetRebTp(d[v],v);
    if(f==2) {
      if(ff) {
        GetPoint(v,&x,&y);
        fprintf(fo,"%d %d\n",x+1,y+1);
      }
    } else {
      GetPoint(GetNextHole(v),&x,&y);
      fprintf(fo,"%d %d\n",x+1,y+1);
      if(ff) {
        GetPoint(v,&x,&y);
        fprintf(fo,"%d %d\n",x+1,y+1);
      }
    }
  } else {
    WrPathFw(d,d[v],true);
    if(ff) {
      GetPoint(v,&x,&y);
      fprintf(fo,"%d %d\n",x+1,y+1);
    }
  }
}

void MarkPath(int *d,int v) {
  if(v<0)
    return;
  used[v]=true;
  MarkPath(d,d[v]);
}

void WrPathBw(int *d, int v, bool ff) { //The peth must be marked!!!
  int x,y;
  int f;
  if(v<0)
    return ;
  if(d[v]<0)
    return ;
  if(ff) {
    GetPoint(v,&x,&y);
    fprintf(fo,"%d %d\n",x+1,y+1);
  }
  if(ishole[d[v]]) {
    f=GetNextHole(d[v]);
    if(used[f]) {
      if(!ishole[v]) {
        GetPoint(f,&x,&y);
        fprintf(fo,"%d %d\n",x+1,y+1);
      }
      v=f;
      v=GetNextHole(v);
      while(v!=f) {
        GetPoint(v,&x,&y);
        fprintf(fo,"%d %d\n",x+1,y+1);
        v=GetNextHole(v);
      }
      GetPoint(v,&x,&y);
      fprintf(fo,"%d %d\n",x+1,y+1);
      WrPathBw(d,d[v],true);
    } else {
      v=f;
      while(!used[v]) {
        GetPoint(v,&x,&y);
        fprintf(fo,"%d %d\n",x+1,y+1);
        v=GetNextHole(v);
        if(v==f)
          return ;
      }
      GetPoint(v,&x,&y);
      fprintf(fo,"%d %d\n",x+1,y+1);
      WrPathBw(d,d[v],true);
    }
  } else {
    WrPathBw(d,d[v],true);
  }
}

int main() {
  int i,j;
  int k,mm;
  int x,y;
  int xp,yp;
  fi=fopen("princess.in","rt");
  fo=fopen("princess.out","wt");

  fscanf(fi,"%d%d",&n,&m);
  V=n*m+1;
  for(i=0;i<V;i++) {
    g[i]=NULL;
    vw[i]=0;
    qw[i].l=false;
    qw[i].r=false;
    qw[i].u=false;
    qw[i].d=false;
    ishole[i]=false;
  }
  BuildG();
  Dejkstr(V-1);
  for(i=0;i<V;i++) {
    r1[i]=r[i];
    d1[i]=d[i];
  }
  fscanf(fi,"%d%d",&x,&y);
  x--; y--;
  Dejkstr(GetNum(x,y));
  for(i=0;i<V;i++) {
    r2[i]=r[i];
    d2[i]=d[i];
  }
  fscanf(fi,"%d%d",&xp,&yp);
  xp--; yp--;
  Dejkstr(GetNum(xp,yp));
  for(i=0;i<V;i++) {
    r3[i]=r[i];
    d3[i]=d[i];
  }
  mm=MANY;
  k=-1;
  for(i=0;i<V-1;i++) {
    j=r1[i]+r2[i];
    j+=2*r3[i];
    j+=vw[GetNum(x,y)];
    j+=2*vw[GetNum(xp,yp)];
    j/=2;
    if(mm>j) {
      mm=j;
      k=i;
    }
  }
  if(k<0) {
    fprintf(fo,"-1\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }
  fprintf(fo,"%d\n",mm);
  fprintf(fo,"%d %d\n",x+1,y+1);
  WrPathFw(d2,k,false);
  fprintf(fo,"%d %d\n",k%m+1,k/m+1);
  for(i=0;i<V;i++)
    used[i]=false;
  MarkPath(d3,k);
  WrPathBw(d3,k,false);
  fprintf(fo,"%d %d\n",xp+1,yp+1);
  WrPathFw(d3,k,false);
  for(i=0;i<V;i++)
    used[i]=false;
  MarkPath(d1,k);
  fprintf(fo,"%d %d\n",k%m,k/m);
  WrPathBw(d1,k,false);

  fclose(fi);
  fclose(fo);
  return 0;
}
