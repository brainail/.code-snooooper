#include <stdio.h>
#include <string.h>

#define mm 53
#define inf 1000000

typedef struct
{
  int x, y;
}
pnt;

int a[mm][mm], d[mm][mm], mark[mm][mm], m, n, ex[mm][mm], wall[mm][mm][4],
    s[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}},
    tp[6] = {2, 4, 6, 8, 10, 0}, l, k, x[mm * mm], y[mm * mm],
    _stklen = 8 * 1024 * 1024;
pnt h[mm][mm], prev[mm][mm], w[mm * mm], w1[mm * mm], w2[mm * mm],
    pp[mm * mm], prev2[mm][mm], old;
char type[6][6] = {"P", "MT", "AM", "BA", "BU", "K"};

void way( pnt mt )
{
  int x, y, i, j;
  pnt w, pp;

  for (x = 1; x <= m; x++)
    for (y = 1; y <= n; y++)
      mark[y][x] = 0;
  for (x = 1; x <= m; x++)
    for (y = 1; y <= n; y++)
    {
      d[y][x] = inf;
      prev[y][x].x = 0;
    }
  d[mt.y][mt.x] = 0;
  for (x = 1; x <= m; x++)
    for (y = 1; y <= n; y++)
    {
      w.x = -1;
      for (i = 1; i <= m; i++)
        for (j = 1; j <= n; j++)
          if (!mark[j][i] && (w.x == -1 || d[w.y][w.x] > d[j][i]))
          {
            w.x = i;
            w.y = j;
          }
      if (w.x == -1 || d[w.y][w.x] == inf)
      {
        x = m + 1;
        break;
      }
      mark[w.y][w.x] = 1;
      for (j = 0; j < 4; j++)
        if (!wall[w.y][w.x][j])
        {
          pp.x = w.x + s[j][0];
          pp.y = w.y + s[j][1];
          old.x = 0;
          if (h[pp.y][pp.x].x)
          {
            old = pp;
            pp = h[pp.y][pp.x];
          }
          if (d[pp.y][pp.x] > d[w.y][w.x] + a[w.y][w.x])
          {
            d[pp.y][pp.x] = d[w.y][w.x] + a[w.y][w.x];
            prev[pp.y][pp.x] = w;
            prev2[pp.y][pp.x] = old;
          }
        }
      if (h[w.y][w.x].x)
      {
        pp = h[w.y][w.x];
        if (d[pp.y][pp.x] > d[w.y][w.x] + a[w.y][w.x])
        {
          d[pp.y][pp.x] = d[w.y][w.x] + a[w.y][w.x];
          prev[pp.y][pp.x] = w;
          prev2[pp.y][pp.x].x = 0;
        }
      }
    }
}

void go( pnt t, pnt *w )
{
  if (prev[t.y][t.x].x != 0)
    go(prev[t.y][t.x], w);
  if (l == 0 || w[l].x != t.x || w[l].y != t.y)
  {
    w[++l] = t;
    k += a[t.y][t.x];
    a[t.y][t.x] = 0;
  }
}

int main( void )
{
  int i, j, xx, yy, z, ww, p, l1;
  char dd, t[10];
  pnt w, mt, pr, first;

  freopen("princess.in", "r", stdin);
  freopen("princess.out", "w", stdout);
  scanf("%d%d%d", &m, &n, &k);
  for (i = 1; i <= k; i++)
  {
    scanf("%d%d %c", &xx, &yy, &dd);
    ex[yy][xx] = dd;
  }
  scanf("%d", &z);
  for (i = 1; i <= z; i++)
  {
    scanf("%d%d%s", &xx, &yy, t);
    for (j = 0; j < 6; j++)
      if (strcmp(t, type[j]) == 0)
        break;
    a[yy][xx] += tp[j];
  }
  scanf("%d", &ww);
  for (i = 1; i <= ww; i++)
  {
    scanf("%d%d %c", &xx, &yy, &dd);
    if (dd == 'L')
      wall[yy][xx][0] = wall[yy][xx - 1][1] = 1;
    else if (dd == 'R')
      wall[yy][xx][1] = wall[yy][xx + 1][0] = 1;
    else if (dd == 'U')
      wall[yy][xx][2] = wall[yy - 1][xx][3] = 1;
    else
      wall[yy][xx][3] = wall[yy + 1][xx][2] = 1;
  }
  scanf("%d", &l);
  for (i = 1; i <= l; i++)
  {
    scanf("%d", &p);
    for (j = 1; j <= p; j++)
      scanf("%d%d", &pp[j].x, &pp[j].y);
    for (j = 1; j < p; j++)
      h[pp[j].y][pp[j].x] = pp[j + 1];
    h[pp[p].y][pp[p].x] = pp[1];
  }
  scanf("%d%d%d%d", &mt.x, &mt.y, &pr.x, &pr.y);
  first = mt;
  if (h[mt.y][mt.x].x)
    mt = h[mt.y][mt.x];
  for (xx = 1; xx <= m; xx++)
    wall[1][xx][2] = wall[n][xx][3] = 1;
  for (yy = 1; yy <= n; yy++)
    wall[yy][1][0] = wall[yy][m][1] = 1;
  way(mt);
  l = 0;
  k = a[mt.y][mt.x];
  go(pr, w1);
  l1 = l;
  way(pr);
  w.x = -1;
  for (i = 1; i <= m; i++)
    if (ex[1][i] && (w.x == -1 || d[1][i] < d[w.y][w.x]))
      w.x = i, w.y = 1;
  for (i = 1; i <= m; i++)
    if (ex[n][i] && (w.x == -1 || d[n][i] < d[w.y][w.x]))
      w.x = i, w.y = n;
  for (i = 1; i <= n; i++)
    if (ex[i][1] && (w.x == -1 || d[i][1] < d[w.y][w.x]))
      w.x = 1, w.y = i;
  for (i = 1; i <= n; i++)
    if (ex[i][m] && (w.x == -1 || d[i][m] < d[w.y][w.x]))
      w.x = m, w.y = i;
  l = 0;
  go(w, w2);
  printf("%d\n%d %d\n", k, first.x, first.y);
  for (i = 1; i <= l1; i++)
  {
    if (prev2[w1[i].y][w1[i].x].x != 0
       && (prev2[w1[i].y][w1[i].x].x != first.x
       || prev2[w1[i].y][w1[i].x].y != first.y))
    {
      printf("%d %d\n", prev2[w1[i].y][w1[i].x].x, prev2[w1[i].y][w1[i].x].y);
      first = prev2[w1[i].y][w1[i].x];
    }
    if (w1[i].x != first.x || w1[i].y != first.y)
      printf("%d %d\n", w1[i].x, w1[i].y);
    first = w1[i];
  }
  for (i = 1; i <= l; i++)
  {
    if (prev2[w2[i].y][w2[i].x].x != 0
       && (prev2[w2[i].y][w2[i].x].x != first.x
       || prev2[w2[i].y][w2[i].x].y != first.y))
    {
      printf("%d %d\n", prev2[w2[i].y][w2[i].x].x, prev2[w2[i].y][w2[i].x].y);
      first = prev2[w2[i].y][w2[i].x];
    }
    if (w2[i].x != first.x || w2[i].y != first.y)
      printf("%d %d\n", w2[i].x, w2[i].y);
    first = w2[i];
  }
  return 0;
}
