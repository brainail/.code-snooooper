/* 29.06.04 SK1 */

#include <stdio.h>

/* The main program function */
int main( void )
{
  int a[5], i, ans[5] = {3, 4, 1, 3, 2};

  freopen("princess.in", "r", stdin);
  freopen("princess.out", "w", stdout);
  for (i = 0; i < 5; i++)
    scanf("%i", &a[i]);
  for (i = 0; i < 5; i++)
    if (a[i] != ans[i])
      break;
  if (i != 5)
    printf("-1");
  else
    printf("2\n1 4\n1 1\n2 2\n1 4\n1 3\n1 4\n1 1\n2 2\n1 4\n2 4\n"
      "3 4\n2 3\n2 1\n3 1\n3 2\n");
  return 0;
} /* End of 'main' function */
