#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define KK 251
#define NN 51
#define LL 1010
#define MANY 100000000

FILE *fi, *fo;
int *cont;
int *a[LL];  //[KK*NN+LL];
int message[NN];
int text[LL];
int final[KK*NN+LL];
int ifinal=0;
int n,l,k;

int min(int a, int b) {
  return (a<b)?a:b;
}

int getcn() {
  int c;
  c=getc(fi);
  while(c!='\n') {
    if(c==EOF)
      break;
    c=getc(fi);
  }
  if(c!=EOF)
    c=getc(fi);
  return c;
}

void read_message() {
  int c;
  c=getcn();
  k=0;
  while((c!='\n')&&(c!=EOF)) {
    message[k++]=c;
    c=getc(fi);
  }
}

void read_text() {
  int c;
  c=getc(fi);
  l=0;
  while((c!=EOF)) {
    text[l++]=c;
    c=getc(fi);
  }
}

void wr(int x, int y) {
  bool flag;
  flag=true;
  while(true) {
    if((x==0)&&(y==0)) {
      if((text[x]!='\n')||(flag))
        final[ifinal++]=text[0];
      else
        final[ifinal++]=' ';
      return ;
    }
    if((text[x]==' ')||(text[x]=='\n')) {
      if(flag) {
        final[ifinal++]=text[x];
      } else {
        final[ifinal++]=' ';
      }
      flag=false;
      if(y>0) {
        if(x>0) {
          if(a[x][y-1]+1<a[x-1][y-1]) {
            y--;
            continue;
          } else {
            x--;
            y--;
            flag=true;
            continue;
          }
        } else {
          y--;
          continue;
        }
      }
    } else {
      flag=true;
      final[ifinal++]=text[x];
      x--;
      y--;
    }
  }
}

int main() {
  int i,j,f;
  fi=fopen("aliens.in","rt");
  fo=fopen("aliens.out","wt");

//  read_data
  fscanf(fi,"%d",&n);
  read_message();
  read_text();
  cont=new int [l*(n*k+l)];
  for(i=0;i<l*(n*k+l);i+=n*k+l) {
    a[i/(n*k+l)]=&cont[i];
  }

  for(i=0;i<l;i++) {
//    for(j=k*n+l-1;j>=0;j--) {
    for(j=0;j<k*n+l;j++) {
      a[i][j]=MANY;
      if(((j+1)%n==0)&&((j+1)/n<=k)) {
        f=(j+1)/n-1;
        if(message[f]==' ') {
          if((text[i]!=' ')&&(text[i]!='\n'))
            continue;
        } else {
          if(message[f]!=text[i])
            continue;
        }
      }
      if((text[i]==' ')||(text[i]=='\n')) {
        if(j>0) a[i][j]=min(a[i][j-1]+1,a[i][j]);
        if(i>0) {
          if(j>0) a[i][j]=min(a[i][j],a[i-1][j-1]);
        } else {
          if(j==0)
            a[i][j]=0;
        }
      } else {
        if(i>0) {
          if(j>0) a[i][j]=min(a[i][j],a[i-1][j-1]);
        } else {
          if(j==0)
            a[i][j]=0;
        }
      }
    }
  }

  j=-1;
  f=MANY;
  for(i=n*k-1;i<n*k+l-1;i++) {
    if(a[l-1][i]<f) {
      f=a[l-1][i];
      j=i;
    }
  }
  if(j<0) {
    fprintf(fo,"IMPOSSIBLE\n");
  } else {
    fprintf(fo,"%d\n",f);
    wr(l-1,j);
    for(i=ifinal-1;i>=0;i--) {
      fputc(final[i],fo);
    }
  }
  delete(cont);

  fclose(fi);
  fclose(fo);
  return 0;
}
