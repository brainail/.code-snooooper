#include <stdio.h>

#define mm 2010
#define inf 10000000

int a[2005][60], d[2005][60], n, k, l, good[1005][1005],
    _stklen = 8 * 1024 * 1024;
unsigned char ss[2005], s[60], old[2010];

int space( unsigned char c )
{
  return (c == ' ' || c == '\n' || c == '\r');
}

void write( int l, int k )
{
  int i, f, j;

  if (l == 0 || k == 0)
    return;
  if (d[l][k] == -1)
  {
    write(l, k - 1);
    for (i = 1; i <= n; i++)
      printf(" ");
  }
  else
  {
    f = 0;
    write(d[l][k], k - 1);
    if (space(ss[d[l][k]]))
    {
      for (j = 1; j <= n - (l - d[l][k]); j++)
        printf(" ");
      f = 1;
    }
    for (i = d[l][k] + 1; i <= l; i++)
      if (!space(ss[i]) || f)
        printf("%c", ss[i]);
      else
      {
        f = 1;
        printf(" ");
        for (j = 1; j <= n - (l - d[l][k]); j++)
          printf(" ");
      }
  }
}

int main( void )
{
  int i, j, t, c;

  freopen("aliens.in", "r", stdin);
  freopen("aliens.out", "w", stdout);
  scanf("%d", &n);
  while (getc(stdin) != '\n')
    ;
  //scanf("%c", &c);
  c = getc(stdin);
  for (k = 1; c != '\n' && c != '\r'; k++)
  {
    s[k] = c;
    //scanf("%c", &c);
    c = getc(stdin);
  }
  s[k] = 0;
  k--;
  //fgetc(stdin);
  //fgets(&old[1], mm, stdin);
  for (l = 1; (c = getc(stdin)) > 0; l++)
    old[l] = c;
  //fputc(old[1], stdout);
  old[l] = 0;
  for (i = 1, l = 0; old[i] != 0; i++)
    if (l == 0 || ss[l] != 13)
      ss[++l] = old[i];
  for (i = 1; i <= l; i++)
    for (j = 1; j <= k; j++)
      a[i][j] = inf;
  for (i = 1; i <= l; i++)
    for (j = i; j <= l; j++)
      if (i == j && space(ss[i]))
        good[i][j] = i;
      else if (i != j)
      {
        if (good[i][j - 1])
          good[i][j] = good[i][j - 1];
        else if (space(ss[j]))
          good[i][j] = j;
      }
  for (i = 1; i <= n; i++)
    if (space(s[1]))
    {
      if (space(ss[i]))
        a[i][1] = n - i;
      else
        a[i][1] = inf;
    }
    else
    {
      if (ss[i] == s[1] && (n == i || good[1][i]))
        a[i][1] = n - i;
      else
        a[i][1] = inf;
    }
  for (i = 1; i <= l; i++)
    for (j = 2; j <= k; j++)
    {
      if ((space(s[j]) && space(ss[i])) || s[j] == ss[i])
        for (t = i - 1; t > 0 && i - t <= n; t--)
        {
          if (a[i][j] > a[t][j - 1] + (n - (i - t))
              && (n == (i - t) || good[t][i]))
          {
            a[i][j] = a[t][j - 1] + (n - (i - t));
            d[i][j] = t;
          }
        }
      if (space(s[j]) && space(ss[i]) && a[i][j] > a[i][j - 1] + n)
      {
        a[i][j] = a[i][j - 1] + n;
        d[i][j] = -1;
      }
    }
  for (j = 1, i = 2; i <= l; i++)
    if (a[i][k] < a[j][k])
      j = i;
  if (a[j][k] >= inf)
    printf("IMPOSSIBLE\n");
  else
  {
    printf("%d\n", a[j][k]);
    write(j, k);
    for (j++; j <= l; j++)
      printf("%c", ss[j]);
  }
  return 0;
}

