/* 30.06.04 SK1 */

#include <stdio.h>
#include <string.h>

#define MAX_LEN1 100
#define MAX_LEN2 2000
#define MAX_SIZE 40000

#define SPACE ' '

int N;
char S[MAX_LEN1 + 1];
int P[MAX_LEN1];
char T[MAX_LEN2 + 1];
int X[MAX_LEN2][MAX_LEN1][2];
int SL, TL = 0;
char Nul[11];
int A1[MAX_SIZE][3];
int A2[MAX_SIZE][3];
long Size1, Size2;

/* The main program function */
int main( void )
{
  int i, j, k, l, Pos, flag, Ch;
  long Min, MinI;
  char C;
  
  freopen("aliens.in", "r", stdin);
  freopen("aliens.out", "w", stdout);
  scanf("%i", &N);
  gets(S);
  gets(S);
  SL = strlen(S);
  while (scanf("%c", &C) == 1)
    if (C != 13)
      if (C == 10)
        T[TL++] = 32;
      else
        T[TL++] = C;
  T[TL] = 0;
  for (i = 0; i < TL; i++)
    for (j = 0; j < SL; j++)
      X[i][j][0] = X[i][j][1] = -1;
  Size1 = 1;
  A1[0][0] = -1;
  A1[0][1] = -1;
  A1[0][2] = 0;
  while (Size1 != 0)
  {
    Size2 = 0;
    for (i = 0; i < Size1; i++)
      if (A1[i][0] != SL - 1)
      {
        Ch = A1[i][0] + 1;
        flag = 0;
        for (Pos = A1[i][1] + 1; Pos <= A1[i][1] + N; Pos++)
        {
          if (T[Pos] == ' ')
            flag = 1;
          if (T[Pos] == S[Ch] && flag)
            if (X[Pos][Ch][0] == -1 ||
                X[Pos][Ch][0] > A1[i][2] + (A1[i][1] + N - Pos))
            {
              X[Pos][Ch][0] = A1[i][2] + (A1[i][1] + N - Pos);
              X[Pos][Ch][1] = A1[i][1];
              A2[Size2][0] = Ch;
              A2[Size2][1] = Pos;
              A2[Size2][2] = X[Pos][Ch][0];
              Size2++;
            }
        }
      }
    Size1 = Size2;
    for (i = 0; i < Size1; i++)
      for (j = 0; j < 3; j++)
        A1[i][j] = A2[i][j];
  }
  Min = 1000000000, MinI = -1;
  for (i = 0; i < TL; i++)
    if (X[i][SL - 1][0] != -1 && X[i][SL - 1][0] < Min)
      Min = X[i][SL - 1][0], MinI = i;
  if (MinI == -1)
    printf("IMPOSSIBLE");
  else
  {
    printf("%i\n", Min);
    i = MinI, j = SL - 1;
    while (i != -1)
      P[j + 1] = i, i = X[i][j][1], j--;
    P[0] = -1;
    for (i = 0; i < SL; i++)
    {
      k = P[i] + N - P[i + 1];
      for (j = P[i] + 1; T[j] != ' '; j++)
        printf("%c", T[j]);
      for (l = 0; l < k; l++)
        printf("%c", SPACE);
      for (j = j; j <= P[i + 1]; j++)
        printf("%c", T[j]);
    }
    for (j = P[i] + 1; j < TL; j++)
      printf("%c", T[j]);
  }
  return 0;
} /* End of 'main' function */

