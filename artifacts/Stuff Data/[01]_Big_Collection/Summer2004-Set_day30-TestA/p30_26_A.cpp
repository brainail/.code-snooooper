/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;

#define DBG if(0)
// ========================================================================
void die(const char*msg) { cerr<<msg<<endl; exit(1); }

// ========================================================================
int N;
char *msg;
char *song;
char *text;

int SL;
int K;
int *pos;
//		SongPos		MessagePos
int find(int sp, int mp)
{
	if (mp>=K) {
		return 0;// got it
		DBG cout << "got it" << endl;
	}
	bool was_space = false;
	int r = (-1);
	int M = min(sp+N, SL);
	DBG cout << "M==" << M << endl;
	for (int i=sp; i<M; i++)
	{
		if (song[i] == ' ') {
			was_space = true;
		} 
		if ( song[i] == msg[mp] && (was_space || (i-sp==sp+N)) )
		{
			int nextr = find(i+1, mp+1);
			if (nextr != (-1))
			{		// ok, we have find it
				pos[mp] = i-sp;
				return 0;
			}
		}
	}
	return r;
}

bool solve()
{
	// init
	pos = new int[K];
	K = strlen(msg);
	SL = strlen(song);
	DBG cout << "SL==" << SL << endl;
	// solve
	if (find(0,0)==(-1)) {
		delete[] pos;
		return false;
	}
	DBG {
		for (int i=0; i<K; i++)
			cout << "pos[" << i << "] == " << pos[i] << endl;
	}
	// make text
	int sp=0;//song pos
	int tp=0;//text pos
	for (int i=0; i<K; i++) 
	{
		if (pos[i] == N-1)
		{
			for (int j=0; j<N; j++)
				text[tp++] = song[tp++];
		}
		else // as usual, pos[i] < N-1
		{
			int cnt = 0;
			while (song[sp]!=' ') {
				text[tp++] = song[sp++];
				cnt++;
			}
			for (int j=0; j < ((N-1)-pos[i]); j++) {
				text[tp++] = ' ';
				cnt++;
			}
			for (; cnt<N; cnt++) {
				text[tp++] = song[sp++];
			}
		}
	}
	while (sp<SL) {
		text[tp++] = song[sp++];
	}
	text[tp] = '\0';
	// final
	delete[] pos;
	return true;
}
// ========================================================================

int get_c(FILE * f) {
	int c = fgetc(f);
	while (c=='\xD')
		c = fgetc(f);
	return c;
}

int main(void)
{
	{
		msg = new char[51];
		song = new char[1001];
        FILE *fi = fopen("aliens.in","rb");
		fscanf(fi,"%d",&N);
		char c;
		int cnt=0;
		while ( (c=get_c(fi)) != '\xA' )
			cnt++;
		DBG cout << "cnt==" << cnt << endl;
		int i;
		c = get_c(fi);
		for (i=0; (c != '\xA'); c=get_c(fi), i++)
		{
			msg[i] = c;
		}
		msg[i] = '\0';
		c = get_c(fi);
		for (i=0;  c!=EOF;  c=get_c(fi), i++)
		{
			if (c=='\xA') c=' ';
			song[i] = c;
		}
		song[i] = '\0';
		fclose(fi);
	}
	DBG cout << "N==" << N << endl;
	DBG cout << "msg ==\"" << msg  << "\"" << endl;
	DBG cout << "song==\"" << song << "\"" << endl;
	DBG cout << "strlen(song)==" << strlen(song) << endl;
	text = new char[100000];
	text[0] = '\0';
	bool r = false;
	r = solve();
	DBG cout << "text==\"" << text << "\"" << endl;
	DBG cout << "strlen(text)==" << strlen(text) << endl;
	DBG {
		
		for (int i=N-1; i<strlen(text); i+=N)
			cout << "text[" << i << "] == " << text[i] << endl;
	}
	{
		FILE *fo = fopen("aliens.out","wb");
		if (r) {
			int sl = strlen(song);
			int tl = strlen(text);
			fprintf(fo,"%d\n",tl-sl);
			for (int i=0; i<tl; i++) {
				fputc(text[i],fo);
			}
		} else {
			fprintf(fo,"IMPOSSIBLE");
		}
		fclose(fo);
	}
	delete[] msg;
	delete[] song;
	delete[] text;
	return 0;
}
