  #include <stdlib.h>
  #include <stdio.h>
  #include <string.h>


  int N,K;
  char P[51] , S[1001];
  int lS , lP;
  int Min=1000000,Tec;
  int rA[50] , rB[51];


  bool GetData( char *Path )
  {
      FILE *DatFile;
      if ( ( DatFile = fopen( Path , "r" ) ) == NULL ) return false;

      fscanf ( DatFile , "%d\n" , &N );
      
      int I=0 , tmp;
      for (;;)
      {
          tmp = fgetc ( DatFile );
          if ( (char) tmp == '\n' ) break;
          P[I] = tmp;
          I++;
      }
      P[I] = '\0';
      I=0;
      for (;;)
      {
          tmp = fgetc ( DatFile );
          if ( (char) tmp == '\n' ) tmp = ' ';
          else if ( tmp == EOF ) break;
          S[I] = tmp;
          I++;
      }
      S[I] = '\0';

      fclose ( DatFile );
      return true;
  }


  bool OutputResult ( char *Path )
  {
      FILE *OutFile;
      if ( ( OutFile = fopen( Path , "w" ) ) == NULL ) return false;

      int Is,Ip,t;
      bool f;
      if ( Min < 1000000 )
      {
           fprintf ( OutFile , "%d\n" , Min );
           for ( Ip=0 ; Ip<lP ; Ip++ )
           {
               f = true;
               for ( Is = rB[Ip] ; Is<rB[Ip+1] ; Is++ )
                   if ( ( f ) && ( S[Is] == ' ' ) )
                   {
                        for ( t=0 ; t<=rA[Ip] ; t++ ) fputc ( ' ' , OutFile );
                        f = false;
                   } else fputc ( S[Is] , OutFile );
           }
           for ( ; Is<lS ; Is++ ) fputc ( S[Is] , OutFile );
      }
      else fprintf ( OutFile , "IMPOSSIBLE" );

      fclose ( OutFile );
      return true;
  }


  void Run( int Is , int Ip , int tec , short A[50] , short B[51] )
  {
      int Js,Jp,t1,t2,t3,t4;
      bool f;
      if ( Min == 0 ) return;
      for (;;Is++)
      {
      if ( Ip == lP )
          {
              if ( tec < Min )
              {
                  Min = tec;
                  for ( t1=0 ; t1<lP ; t1++ )
                  {
                      rA[t1] = A[t1];
                      rB[t1] = B[t1];
                  }
                  rB[t1] = B[t1];
              }
              Run ( Is , 0 , 0 , A , B );
              return;
          }
          if ( Is == lS ) return;

          if ( S[Is] == P[Ip] )
          {
               if ( Is - B[Ip] + 1 > N ) return;
               if ( Is - B[Ip] + 1 < N )
               {
                   f = false;
                   for ( Js = B[Ip] ; Js < Is ; Js++ ) if ( S[Js] == ' ' )
                   {
                       f = true;
                       break;
                   }
               } else f = true;
               if ( f )
               {
                   A[Ip] = B[Ip] + N - Is - 1;
                   tec += A[Ip];
                   if ( tec >= Min ) return;
                   Ip++;
                   B[Ip] = Is+1;
               }
               continue;
          }

          for ( Jp=0 ; Jp<Ip ; Jp++ ) if ( S[Is] == P[Jp] )
          {
               if ( Is - B[Jp] + 1 > N ) continue;
               if ( Is - B[Jp] + 1 < N )
               {
                   f = false;
                   for ( Js = B[Jp] ; Js < Is ; Js++ ) if ( S[Js] == ' ' ) f = true;
               } else f = true;
               if ( f )
               {
                   t1 = A[Jp];
                   t2 = tec;
                   t3 = B[Jp+1];
                   
                   A[Jp] = B[Jp] + N - Is - 1;
                   tec = 0;
                   for ( t4=0 ; t4<=Jp ; t4++ ) tec += A[t4];
                   B[Jp+1] = Is+1;

                   Run ( Is+1 , Jp+1 , tec , A , B );

                   A[Jp] = t1;
                   tec = t2;
                   B[Jp+1] = t3;
               }
               break;
          }
      }
  }


  int main(void)
  {
       if ( ! GetData( "aliens.in" ) )
       {
            printf(" Could not open ""aliens.in"" ! ");
            return 1;
       }
       lS=strlen(S);
       lP=strlen(P);

       short A[50], B[51];
       B[0] = 0;
       Run( 0 , 0 , 0 , A , B );

       if ( ! OutputResult ( "aliens.out" ) )
       {
            printf(" Could not open ""aleans.out"" ! ");
            return 1;
       }       
       return 0;
  }
