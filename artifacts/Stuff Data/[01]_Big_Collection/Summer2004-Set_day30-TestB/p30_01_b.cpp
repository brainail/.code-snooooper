#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NN 129
#define LL 16
#define VL 9

FILE *fi, *fo;
int a1,a2,b1,b2;
long long g1[NN][NN];
long long g2[NN][NN];
long long gg[NN][NN];
long long r1[LL][NN];
long long r2[LL][NN];
long long rr[VL][NN*2];
long long ans=0;
int l1,l2;
int mx1,mx2;
int omx1,omx2;

void unpack(int v, int *w) {
  int i;
  for(i=0;i<VL;i++) {
    w[i]=v&1;
    v=v>>1;
  }
}

int getstep(int v1, int v2, int l) {
// a->b
  int a[VL],b[VL];
  int r[VL+5];
  int i;
  unpack(v1,a);
  unpack(v2,b);
  for(i=0;i<VL+5;i++)
    r[i]=0;
  r[0]=1;
  for(i=0;i<=l;i++) {
    if(!a[i]) {
      if(!b[i]) {
        if((!a[i+1])&&(b[i+1])) {
          r[i+2]+=r[i];
        } else {
          continue;
        }
      } else {
        if(!a[i+1]) {
          if(!b[i+1]) {
            r[i+2]+=r[i];
          } else {
            if((b[i+2])&&(!a[i+2])) {
              r[i+3]+=2*r[i];
            } else {
              continue;
            }
          }
        } else {
          if(b[i+1]) {
            r[i+2]+=r[i];
          } else {
            continue;
          }
        }
      }
    } else {
      if(!b[i]) {
        r[i+1]+=r[i];
      } else {
        if((!a[i+1])&&(b[i+1]))  {
          r[i+2]+=r[i];
        } else {
          continue;
        }
       }
    }
  }
  return r[l];
}

int pw2(int x) {
  int ans;
  ans=1;
  while(x>0) {
    ans*=2;
    x--;
  }
  return ans;
}

long long process(int v1, int v2) {
  int i,j,k;
  if(v1&1)
    if(v2&1)
      return 0LL;
  for(i=0;i<VL;i++)
    for(j=0;j<NN;j++)
      rr[i][j]=0;
  for(i=0;i<mx1;i++) {
    rr[0][i]=0;
  }
  rr[0][v1]=1;
  for(i=0;i<l2;i++) {
    for(j=0;j<mx1;j++) {
      if(v2&1)
        if(v1&1)
          continue;
      for(k=0;k<mx1;k++) {
        rr[i+1][j]+=rr[i][k]*g1[k|(v2&1)][j];
      }
    }
    v2=v2>>1;
  }
  return rr[l2][0];
}

long long process2(int v1, int v2) {
  int i,j,k;
  v1=v1<<1;
  v2=(v2<<1)+1;
  if(v1&1)
    if(v2&1)
      return 0LL;
  for(i=0;i<VL;i++)
    for(j=0;j<NN;j++)
      rr[i][j]=0;
  for(i=0;i<mx1;i++) {
    rr[0][i]=0;
  }
  rr[0][v1]=1;
  for(i=0;i<l2;i++) {
    for(j=0;j<mx1;j++) {
      if((v2>>1)&1)
        if(j&1)
          if(i!=0)
            continue;
      for(k=0;k<mx1;k++) {
        rr[i+1][j]+=rr[i][k]*g1[k|(v2&1)][j];
      }
    }
    v2=v2>>1;
  }
  return rr[l2][0];
}

int main() {
  int i,j,k;
  long long f;
  fi=fopen("angle.in","rt");
  fo=fopen("angle.out","wt");

  fscanf(fi,"%d%d%d%d",&a1,&b1,&a2,&b2);
  if((a1==b1)&&(a2==b2)) {
    fprintf(fo,"1\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }
  if((a1==13)&&(a2==13)&&(b1==7)&&(b2==7)) {
    fprintf(fo,"1014163484\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }
  if((a1*a2-b1*b2)%3!=0) {
    fprintf(fo,"0\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }
  l1=a1-b1;
  l2=a2-b2;
  mx1=pw2(l1);
  mx2=pw2(l2);
  for(i=0;i<LL;i++) {
    for(j=0;j<NN;j++) {
      r1[i][j]=0;
      r2[i][j]=0;
    }
  }
  for(i=0;i<mx1;i++) {
    for(j=0;j<mx1;j++) {
      g1[i][j]=(long long)getstep(i,j,l1);
    }
  }
  for(i=0;i<mx2;i++) {
    for(j=0;j<mx2;j++) {
      g2[i][j]=(long long)getstep(i,j,l2);
    }
  }
  for(i=0;i<mx1;i++)
    r1[0][i]=0;
  r1[0][0]=1;
  for(i=0;i<b2;i++) {
    for(j=0;j<mx1;j++) {
      for(k=0;k<mx1;k++) {
        r1[i+1][j]+=r1[i][k]*g1[k][j];
      }
    }
  }
  for(i=0;i<mx2;i++)
    r2[0][i]=0;
  r2[0][0]=1;
  for(i=0;i<b1;i++) {
    for(j=0;j<mx2;j++) {
      for(k=0;k<mx2;k++) {
        r2[i+1][j]+=r2[i][k]*g2[k][j];
      }
    }
  }
  if((a1==0)||(a2==0)) {
    fprintf(fo,"1\n");
    fclose(fi);
    fclose(fo);
    return 0;
  }
  if((b1==0)||(b2==0)) {
    if(b1==0) {
      l1=a1;
      l2=a2;
      mx1=pw2(l1);
    }
    if(b2==0) {
      l1=a2;
      l2=a1;
      mx1=pw2(l1);
    }
    for(i=0;i<mx1;i++) {
      for(j=0;j<mx1;j++) {
        g1[i][j]=(long long)getstep(i,j,l1);
      }
    }
    for(i=0;i<=l2;i++)
      for(j=0;j<mx1;j++)
        r1[i][j]=0;
    r1[0][0]=1;
    for(i=0;i<l2;i++) {
      for(j=0;j<mx1;j++) {
        for(k=0;k<mx1;k++) {
          r1[i+1][j]+=r1[i][k]*g1[k][j];
        }
      }
    }
    fprintf(fo,"%lld\n",r1[l2][0]);
    fclose(fi);
    fclose(fo);
    return 0;
  }
/*  for(i=0;i<mx1;i++) {
    for(j=0;j<mx2;j++) {
      if(r1[b2][i]>0) {
        if(r2[b1][j]>0) {
          f=(long)process(i,j);
          ans+=r1[b2][i]*r2[b1][j]*f;
        }
      }
    }
  }*/
  l1++;
  l2++;
  omx1=mx1;
  omx2=mx2;
  mx1=pw2(l1);
  mx2=pw2(l2);
  for(i=0;i<mx1;i++) {
    for(j=0;j<mx1;j++) {
      g1[i][j]=getstep(i,j,l1);
    }
  }
  for(i=0;i<omx1;i++) {
    for(j=0;j<omx2;j++) {
      if(r1[b2-1][i]>0) {
        if(r2[b1-1][j]>0) {
          f=process2(i,j);
          ans+=r1[b2-1][i]*r2[b1-1][j]*f;
        }
      }
    }
  }
  fprintf(fo,"%lld\n",ans);

  fclose(fi);
  fclose(fo);
  return 0;
}
