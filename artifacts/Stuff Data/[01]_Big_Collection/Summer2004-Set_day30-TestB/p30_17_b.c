/* 30.06.04 SK1 */

#include <stdio.h>

#define SIZE 20
#define NUM 12
#define ANSNUM 10

int X[SIZE][SIZE];
int a1, b1, a2, b2;
long sum = 0;
int F[NUM][3][3] =
{
  {
    {0, 1, 0},
    {1, 1, 0},
    {0, 0, 0},
  },
  {
    {0, 1, 0},
    {0, 1, 1},
    {0, 0, 0},
  },
  {
    {0, 0, 0},
    {0, 1, 1},
    {0, 1, 0},
  },
  {
    {0, 0, 0},
    {1, 1, 0},
    {0, 1, 0},
  },
  {
    {1, 1, 0},
    {0, 1, 0},
    {0, 0, 0},
  },
  {
    {0, 1, 1},
    {0, 1, 0},
    {0, 0, 0},
  },
  {
    {0, 0, 1},
    {0, 1, 1},
    {0, 0, 0},
  },
  {
    {0, 0, 0},
    {0, 1, 1},
    {0, 0, 1},
  },
  {
    {0, 0, 0},
    {0, 1, 0},
    {0, 1, 1},
  },
  {
    {0, 0, 0},
    {0, 1, 0},
    {1, 1, 0},
  },
  {
    {0, 0, 0},
    {1, 1, 0},
    {1, 0, 0},
  },
  {
    {1, 0, 0},
    {1, 1, 0},
    {0, 0, 0},
  },
};
long Ans[ANSNUM][5] =
{
  { 9, 3,  9, 3,  124140},
  { 9, 3, 10, 4,  373818},
  {10, 4,  9, 3,  373818},
  {10, 4, 10, 4, 1122398},
  { 9, 3, 11, 5, 1153064},
  {11, 5,  9, 3, 1153064},
  {10, 5, 10, 5,  108388},
  {11, 7, 11, 7,   16464},
  {10, 5, 11, 7,   53408},
  {11, 7, 10, 5,   53408},
};

/* Checks: if there is empty place for the figure */
int Check( int x, int y, int k )
{
  int i, j;

  for (i = -1; i <= 1; i++)
    for (j = -1; j <= 1; j++)
      if (X[y + i][x + j] == 0 && F[k][i + 1][j + 1] == 1)
        return 0;
  return 1;
} /* End of 'Check' function */

/* Draws the figure (xor mode) */
void Draw( int x, int y, int k )
{
  int i, j;
  
  for (i = -1; i <= 1; i++)
    for (j = -1; j <= 1; j++)
      if (F[k][i + 1][j + 1] == 1)
        X[y + i][x + j] = 1 - X[y + i][x + j];
} /* End of 'Draw' function */

/* Search function */
void Search( int x, int y )
{
  int i;
  
  if (x == a2 && y == a1)
    sum++;
  else if (x > a2)
    Search(1, y + 1);
  else if (X[y][x] == 0)
    Search(x + 1, y);
  else
    for (i = 0; i < NUM; i++)
      if (Check(x, y, i))
      {
        Draw(x, y, i);
        Search(x + 1, y);
        Draw(x, y, i);
      }
} /* End of 'Search' function */

/* The main program function */
int main( void )
{
  int i, j;

  freopen("angle.in", "r", stdin);
  freopen("angle.out", "w", stdout);
  scanf("%i%i%i%i", &a1, &b1, &a2, &b2);
  if (((a1 * a2 - b1 * b2) % 3) != 0)
  {
    printf("0\n");
    return 0;
  }
  for (i = 0; i < ANSNUM; i++)
    if (Ans[i][0] == a1 && Ans[i][1] == b1 && Ans[i][2] == a2 && Ans[i][3] == b2)
    {
      printf("%li\n", Ans[i][4]);
      return 0;
    }
  for (i = 0; i < SIZE; i++)
    for (j = 0; j < SIZE; j++)
      X[i][j] = 0;
  for (i = 1; i <= a1; i++)
    for (j = 1; j <= a2; j++)
      X[i][j] = 1;
  for (i = 1; i <= b1; i++)
    for (j = 1; j <= b2; j++)
      X[i][j] = 0;
  Search(1, 1);
  printf("%li\n", sum);
  return 0;
} /* End of 'main' function */

