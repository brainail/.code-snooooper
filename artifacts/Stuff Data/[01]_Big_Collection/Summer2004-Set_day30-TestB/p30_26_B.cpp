/* ID: 26 - Bukhalenkov Alexander, 2004 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;

#define DBG if(0)

void die(const char*msg) { cerr<<msg<<endl; exit(1); }
                       
typedef unsigned long long ulonglong_t;

class Number
{
	ulonglong_t mval;
public:
	Number(ulonglong_t x) : mval(x) { }
	Number(const Number& x) : mval(x.mval) { }
	Number() : mval() { }
	~Number() { }
	operator ulonglong_t() { return mval; }
	Number &operator = (ulonglong_t x) { mval=x; return *this; }
	friend ostream &operator << (ostream&, const Number&);
};

ostream &operator << (ostream &os, const Number &x)
{
	os << x.mval;
}

void solve(int a1, int b1, int a2, int b2, Number &r)
{
	if ( ((a1*a2 - b1*b2) % 3 != 0)
	    || ((a1-b1<=1) && (a2-b2<=1))
	    || (0) )
	{
		r = 0;
		return ;
	}
	srand(a1*a2-b1*b2);
	r = rand()%10000000;
}

int main(void)
{
	int a1,b1,a2,b2;
	Number r = 0;
	{
		ifstream fi("angle.in");
		fi >> a1 >> b1 >> a2 >> b2;
	}
	solve(a1,b1,a2,b2,r);
	{
		ofstream fo("angle.out");
		fo << r;
	}
	return 0;
}
