/*
 * FILE NAME : A.C.
 * PURPOSE   : Task 'A' solution.
 * PROGRAMMER: Michael Rybalkin, 
 *             Anatoly Nalimov,
 *             Denis Burkov.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

/* Types defenitions */
typedef int INT;
typedef int BOOL;
typedef unsigned int UINT;
typedef long LONG;
typedef unsigned long ULONG;
typedef char CHAR;
typedef unsigned char BYTE;
typedef double DBL;
typedef long double LDBL;
typedef void VOID;

#define TRUE  1
#define FALSE 0

typedef struct tagWAY
{
  LONG x1, y1, x2, y2;  
} WAY;

/* Swaping two variables macro */
#define COM_SWAP(a, b, t) ((t) = (a), (a) = (b), (b) = (t)) 

LONG Num, Res;
WAY *Ways;

/* Checking vertice function */
VOID CheckVert( LONG x, LONG y, LONG n )
{
  LONG i;
  BOOL F = TRUE;

  for (i = n; i < Num; i++)
  {
    if (x == Ways[i].x1 || x == Ways[i].x2) 
      if (y >= Ways[i].y1 && y <= Ways[i].y2)
        continue;
    if (y == Ways[i].y1 || y == Ways[i].y2) 
      if (x >= Ways[i].x1 && x <= Ways[i].x2)
        continue;
    F = FALSE;
    break;
  }

  if (F)
    Res++;
} /* End of 'CheckVert' function */

/* Checking horisontal edge function */
VOID CheckHEdge( LONG x1, LONG x2, LONG y, LONG n )
{
  LONG X1, X2;

  if (n == Num)
    Res += x2 - x1 + 1;

  if (Ways[n].y1 == y || Ways[n].y2 == y)  
  {
    /* X1 */
    if (Ways[n].x1 <= x1)
      X1 = x1;
    else if (Ways[n].x1 > x2)
      return;
    else
      X1 = Ways[n].x1;
    /* X2 */
    if (Ways[n].x2 >= x2)
      X2 = x2;
    else if (Ways[n].x2 < x1)
      return;
    else
      X2 = Ways[n].x2;
    CheckHEdge(X1, X2, y, n + 1);
  }
  else
  {
    if (y >= Ways[n].y1 && y <= Ways[n].y2)
    {
      if (Ways[n].x1 >= x1 && Ways[n].x1 <= x2)
        CheckVert(Ways[n].x1, y, n + 1);
      if (Ways[n].x2 >= x1 && Ways[n].x2 <= x2 && Ways[n].x1 != Ways[n].x2)
        CheckVert(Ways[n].x2, y, n + 1);
    }
  }
} /* End of 'CheckHEdge' function */ 

/* Checking vertical edge function */
VOID CheckVEdge( LONG x, LONG y1, LONG y2, LONG n )
{
  LONG Y1, Y2;

  if (n == Num)
    Res += y2 - y1 + 1;

  if (Ways[n].x1 == x || Ways[n].x2 == x)  
  {
    /* X1 */
    if (Ways[n].y1 <= y1)
      Y1 = y1;
    else if (Ways[n].y1 > y2)
      return;
    else
      Y1 = Ways[n].y1;
    /* X2 */
    if (Ways[n].y2 >= y2)
      Y2 = y2;
    else if (Ways[n].y2 < y1)
      return;
    else
      Y2 = Ways[n].y2;
    CheckVEdge(x, Y1, Y2, n + 1);
  }
  else
  {
    if (x >= Ways[n].x1 && x <= Ways[n].x2)
    {
      if (Ways[n].y1 >= y1 && Ways[n].y1 <= y2)
        CheckVert(x, Ways[n].y1, n + 1);
      if (Ways[n].y2 >= y1 && Ways[n].y2 <= y2 && Ways[n].y1 != Ways[n].y2)
        CheckVert(x, Ways[n].y2, n + 1);
    }
  }
} /* End of 'CheckVEdge' function */ 

/* The main function */
INT main( VOID )
{
  LONG i, tmp;
  FILE *In, *Out;

  In = fopen("buses.in", "rt");
  Out = fopen("buses.out", "wt");
  if (In == NULL || Out == NULL)
  {
    if (In != NULL)
      fclose(In);
    if (Out != NULL)
      fclose(Out);
    return 1;
  }

  /* Readnig data */
  fscanf(In, "%ld", &Num);
  Ways = malloc(Num * sizeof(WAY));
  for (i = 0; i < Num; i++)
  {
    fscanf(In, "%ld%ld%ld%ld", &Ways[i].x1, &Ways[i].y1, 
      &Ways[i].x2, &Ways[i].y2);
    if (Ways[i].x2 < Ways[i].x1)
      COM_SWAP(Ways[i].x1, Ways[i].x2, tmp);
    if (Ways[i].y2 < Ways[i].y1)
      COM_SWAP(Ways[i].y1, Ways[i].y2, tmp);
  }

  CheckVEdge(Ways[0].x1, Ways[0].y1, Ways[0].y2, 1);
  CheckVEdge(Ways[0].x2, Ways[0].y1, Ways[0].y2, 1);
  if (Ways[0].x2 - Ways[0].x1 > 1)
  {
    CheckHEdge(Ways[0].x1 + 1, Ways[0].x2 - 1, Ways[0].y1, 1);
    CheckHEdge(Ways[0].x1 + 1, Ways[0].x2 - 1, Ways[0].y2, 1);
  }

  fprintf(Out, "%ld", Res);
  
  fclose(In);
  fclose(Out);
  return 0;
} /* End of 'main' function */

/* END OF 'A.C' FILE */
