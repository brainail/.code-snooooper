/* Copyright (c) by PML #30-3 (02.11.2003) */

#include <stdio.h>

#define IN_FILE "buses.in"
#define OUT_FILE "buses.out"
 
#define MAX(A, B) (((A) > (B)) ? (A) : (B))
#define MIN(A, B) (((A) < (B)) ? (A) : (B))

#define NUM 128

long Coord[1000][NUM][5];
long C[60][2];
int n;

/* Finds intersection */
void Del( void )
{
  int i, j, num = 0;
	long x[NUM][5], x1, y1, x2, y2, x3, y3, x4, y4;

	for (i = 0; i < NUM; i++)
		x[i][4] = 0;
	for (i = 0; i < NUM; i++)
		if (Coord[n - 1][i][4])
		{
			x1 = Coord[n - 1][i][0]; 
			y1 = Coord[n - 1][i][1]; 
			x2 = Coord[n - 1][i][2]; 
			y2 = Coord[n - 1][i][3]; 

			for (j = 0; j < NUM; j++)
				if (Coord[n - 2][j][4])
				{
					x3 = Coord[n - 2][j][0]; 
					y3 = Coord[n - 2][j][1]; 
					x4 = Coord[n - 2][j][2]; 
					y4 = Coord[n - 2][j][3]; 
					
					if (x1 == x2 && x2 == x3 && x3 == x4)
					{
						if (MAX(y1, y2) >= MIN(y3, y4) && MAX(y3, y4) >= MIN(y1, y2))
						{	
							x[num][0] = x1;
							x[num][1] = MAX(MIN(y1, y2), MIN(y3, y4));
							x[num][2] = x1;
							x[num][3] = MIN(MAX(y1, y2), MAX(y3, y4));
							x[num][4] = 1;
							num++;
						}
					}
					else if (y1 == y2 && y2 == y3 && y3 == y4)
					{
						if (MAX(x1, x2) >= MIN(x3, x4) && MAX(x3, x4) >= MIN(x1, x2))
						{
							x[num][0] = MAX(MIN(x1, x2), MIN(x3, x4));
							x[num][1] = y1;
							x[num][2] = MIN(MAX(x1, x2), MAX(x3, x4));
							x[num][3] = y1;
							x[num][4] = 1;
							num++;
						}
					}
					else
					{
						if (x1 == x2)
						{
							if (MIN(x3, x4) <= x1 && MAX(x3, x4) >= x1 && MIN(y1, y2) <= y3 && MAX(y1, y2) >= y3)
							{
								x[num][0] = x1;
								x[num][1] = y3;
								x[num][2] = x1;
								x[num][3] = y3;
								x[num][4] = 1;
								num++;
							}
						}
						else
						{
							if (MIN(y3, y4) <= y1 && MAX(y3, y4) >= y1 && MIN(x1, x2) <= x3 && MAX(x1, x2) >= x3)
							{
								x[num][0] = x3;
								x[num][1] = y1;
								x[num][2] = x3;
								x[num][3] = y1;
								x[num][4] = 1;
								num++;
							}
						}
					}
				}
		}
	for (i = 0; i < NUM; i++)
		for (j = 0; j < 5; j++)
			Coord[n - 2][i][j] = x[i][j];
} /* End of 'Del' function */

/* The main program function */
int main( void )
{
  FILE *InF, *OutF;
	int i, j;
	long x1, y1, x2, y2;
	__int64 sum, num;
	int flag;

	InF = fopen(IN_FILE, "rt");
	OutF = fopen(OUT_FILE, "wt");

	fscanf(InF, "%i", &n);
	for (i = 0; i < n; i++)
	{
		fscanf(InF, "%i%i%i%i", &x1, &y1, &x2, &y2);

		Coord[i][0][0] = x1;
		Coord[i][0][1] = y1;
		Coord[i][0][2] = x1;
		Coord[i][0][3] = y2;
		Coord[i][0][4] = 1;

		Coord[i][1][0] = x2;
		Coord[i][1][1] = y1;
		Coord[i][1][2] = x2;
		Coord[i][1][3] = y2;
		Coord[i][1][4] = 1;

		Coord[i][2][0] = x1;
		Coord[i][2][1] = y1;
		Coord[i][2][2] = x2;
		Coord[i][2][3] = y1;
		Coord[i][2][4] = 1;

		Coord[i][3][0] = x1;
		Coord[i][3][1] = y2;
		Coord[i][3][2] = x2;
		Coord[i][3][3] = y2;
		Coord[i][3][4] = 1;

		for (j = 4; j < NUM; j++)
		  Coord[i][j][0] = 0;
	}
	while (n > 1)
	{
		Del();
		if (Coord[n - 2][0][4] == 0 && Coord[n - 2][3][4] == 0 && 
			  Coord[n - 2][3][4] == 0 && Coord[n - 2][3][4] == 0)
			fprintf(OutF, "0"), n = 0;
		n--;
	}
	if (n > 0)
	{
		sum = 0;
		num = 0;
		for (i = 0; i < NUM; i++)
			if (Coord[0][i][4])
			{
				x1 = Coord[0][i][0];
				y1 = Coord[0][i][1];
				x2 = Coord[0][i][2];
				y2 = Coord[0][i][3];
				flag = 1;
				for (j = 0; j < num; j++)
					if (C[j][0] == x1 && C[j][1] == y1)
						flag = 0;
				if (flag)
				{
					C[num][0] = x1;
					C[num][1] = y1;	
					num++;
				}
				flag = 1;
				for (j = 0; j < num; j++)
					if (C[j][0] == x2 && C[j][1] == y2)
						flag = 0;
				if (flag)
				{
					C[num][0] = x2;
					C[num][1] = y2;	
					num++;
				}
			}
		sum += num;
		for (i = 0; i < NUM; i++)
		{
			x1 = Coord[0][i][0];
			y1 = Coord[0][i][1];
			x2 = Coord[0][i][2];
			y2 = Coord[0][i][3];
			if (x1 == x2)
			{
				if (MAX(y1, y2) - MIN(y1, y2) > 1)
					sum += MAX(y1, y2) - MIN(y1, y2) - 1;
			}
			else
			{
				if (MAX(x1, x2) - MIN(x1, x2) > 1)
					sum += MAX(x1, x2) - MIN(x1, x2) - 1;
			}
		}
		fprintf(OutF, "%li", sum);
	}

	fclose(InF);
	fclose(OutF);

	return 0;
} /* End of 'main' function */

/* END OF 'A.CPP' FILE */
