#include <stdio.h>

void sort2( int *x, int *y )
{
  if (*x > *y)
  {
    int t = *x;

    *x = *y;
    *y = t;
  }
}

void sort( int *x1, int *y1, int *x2, int *y2 )
{
  sort2(x1, x2);
  sort2(y1, y2);
}

int main( void )
{
  int i, j, k, m, n, s;
  int xx1, yy1, xx2, yy2;
  int x1[4], y1[4], x2[4], y2[4];
  int nx1[4], ny1[4], nx2[4], ny2[4];

  freopen("buses.in", "r", stdin);
  freopen("buses.out", "w", stdout);
  scanf("%d%d%d%d%d", &n, &xx1, &yy1, &xx2, &yy2);
  sort(&xx1, &yy1, &xx2, &yy2);
  
  x1[0] = xx1;
  y1[0] = yy1;
  x2[0] = xx2 - 1;
  y2[0] = yy1;
  
  x1[1] = xx2;
  y1[1] = yy1;
  x2[1] = xx2;
  y2[1] = yy2 - 1;
  
  x1[2] = xx1 + 1;
  y1[2] = yy2;
  x2[2] = xx2;
  y2[2] = yy2;
  
  x1[3] = xx1;
  y1[3] = yy1 + 1;
  x2[3] = xx1;
  y2[3] = yy2;

  m = 4;
  
  for (i = 1; i < n; i++)
  {
    scanf("%d%d%d%d", &xx1, &yy1, &xx2, &yy2);
    sort(&xx1, &yy1, &xx2, &yy2);
    k = 0;
    for (j = 0; j < m; j++)
    {
      if (x1[j] < xx1)
        x1[j] = xx1;
      if (x2[j] > xx2)
        x2[j] = xx2;
      if (y1[j] < yy1)
        y1[j] = yy1;
      if (y2[j] > yy2)
        y2[j] = yy2;
      if (x1[j] <= x2[j] && y1[j] <= y2[j])
      {
        xx1++;
        yy1++;
        xx2--;
        yy2--;
        if (xx1 <= xx2 && yy1 <= yy2)
          if (x2[j] < xx1 || xx2 < x1[j] || y2[j] < yy1 || yy2 < y1[j])
          {
            nx1[k] = x1[j];
            ny1[k] = y1[j];
            nx2[k] = x2[j];
            ny2[k] = y2[j];
            k++;
          }
          else
            if (x1[j] == x2[j])
            {
              if (y1[j] < yy1 && yy1 <= y2[j])
              {
                nx1[k] = x1[j];
                ny1[k] = y1[j];
                nx2[k] = x1[j];
                ny2[k] = yy1 - 1;
                k++;
              }
              if (y1[j] <= yy2 && yy2 < y2[j])
              {
                nx1[k] = x1[j];
                ny1[k] = yy2 + 1;
                nx2[k] = x1[j];
                ny2[k] = y2[j];
                k++;
              }
            }
            else
            {
              if (x1[j] < xx1 && xx1 <= x2[j])
              {
                nx1[k] = x1[j];
                ny1[k] = y1[j];
                nx2[k] = xx1 - 1;
                ny2[k] = y1[j];
                k++;
              }
              if (x1[j] <= xx2 && xx2 < x2[j])
              {
                nx1[k] = xx2 + 1;
                ny1[k] = y1[j];
                nx2[k] = x2[j];
                ny2[k] = y1[j];
                k++;
              }
            }
        else
        {
          nx1[k] = x1[j];
          ny1[k] = y1[j];
          nx2[k] = x2[j];
          ny2[k] = y2[j];
          k++;
        }
        xx1--;
        yy1--;
        xx2++;
        yy2++;
      }
    }

    for (j = 0; j < k; j++)
    {
      x1[j] = nx1[j];
      y1[j] = ny1[j];
      x2[j] = nx2[j];
      y2[j] = ny2[j];
    }
    m = k;
  }
  s = 0;
  for (j = 0; j < m; j++)
    s += (x2[j] - x1[j] + 1) * (y2[j] - y1[j] + 1);
  printf("%d\n", s);
  return 0;
}
