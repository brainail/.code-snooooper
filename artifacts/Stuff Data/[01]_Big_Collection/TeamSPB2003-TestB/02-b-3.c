/*
 * FILE NAME : B.C.
 * PURPOSE   : Task 'B' solution.
 * PROGRAMMER: Michael Rybalkin, 
 *             Anatoly Nalimov,
 *             Denis Burkov.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

/* Types defenitions */
typedef int INT;
typedef unsigned int UINT;
typedef long LONG;
typedef unsigned long ULONG;
typedef char CHAR;
typedef unsigned char BYTE;
typedef double DBL;
typedef long double LDBL;

LONG A, B, C;
INT N;

INT NOD( INT A, INT B )
{
  INT C;

  C = A % B;
  
  if (C == 0)
    return B;
  else 
    return NOD(B, C);
}

/* The main function */
INT main( VOID )
{
  FILE *In, *Out;
  INT C, L, K;

  In = fopen("clock.in", "rt");
  Out = fopen("clock.out", "wt");
  if (In == NULL || Out == NULL)
  {
    if (In != NULL)
      fclose(In);
    if (Out != NULL)
      fclose(Out);
    return 1;
  }

  fscanf(In, "%i", &K);

  while (K-- > 0)
  {
    fscanf(In, "%i %ld %ld %ld", &N, &A, &B, &C);
    L = N - 1;

    if (B == 0)
      A = (A + 1) % N;
    else if (A <= (N - 1) * B / C)
      A = (A + 1) % N;


    if (A != 0)
    {
      if (A == L)
        fprintf(Out, "%i %i %i", 0, 0, 1);
      else
      {
        C = NOD(L, A);
        if (C != 0)
        {
          L /= C;
          A /= C;
        }
        fprintf(Out, "%i %i %i", A, A, L);
      }
    }
    else
      fprintf(Out, "%i %i %i", 0, 0, 1);

    fprintf(Out, "\n");
  }

  fclose(In);
  fclose(Out);
  return 0;
} /* End of 'main' function */

/* END OF 'B.C' FILE */
