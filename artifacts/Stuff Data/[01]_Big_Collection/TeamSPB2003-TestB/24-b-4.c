#include<stdio.h>
#include<stdlib.h>

int gcd(int a, int b)
{
  int r=1;
  
  while(r != 0)
  {
    r = a%b;
    a = b;
    b = r;
  }
  return a;
  
}

int main()
{
  FILE *in=fopen("clock.in","r"),*out=fopen("clock.out","w");
  __int64 a,b,c,N,K,i,tmp;
  fscanf(in,"%I64d",&K);
  //printf("%d",gcd(4,8));
  for (i=0;i<K;i++)
  {
    fscanf(in,"%I64d%I64d%I64d%I64d",&N,&a,&b,&c);
    if(a==N-1)
    {
       fprintf(out,"0 0 1");
    }else
    {
      if(a*c>b*(N-1))
      { 
        
        tmp=gcd(a,N-1);
        
        a=a/tmp;
        N=(N-1)/tmp;

        fprintf(out,"%I64d %I64d %I64d",a*tmp,a,N);          
      }else
      {
        if (a==N-2)
        {
          fprintf(out,"0 0 1");
        }else{

        
        a=a+1;    
        

        tmp=gcd(a,N-1);
        a=a/tmp;
        N=(N-1)/tmp;

        fprintf(out,"%I64d %I64d %I64d",a*tmp,a,N);

      }}

    }
    fprintf(out,"\n");
  }
  
 // fprintf(out,"gopa");
  fcloseall();
  return 0;
}
