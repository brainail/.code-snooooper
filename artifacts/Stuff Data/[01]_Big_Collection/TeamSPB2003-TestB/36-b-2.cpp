/* Copyright (c) by PML #30-3 (02.11.2003) */

#include <stdio.h>

#define IN_FILE "clock.in"
#define OUT_FILE "clock.out"
 
/* NOD */
long Nod( long a, long b )
{
	long c;

	while (b != 0)
	{
		c = a % b;
		a = b;
		b = c;
	}
	return a;
} /* End of 'Nod' function */

/* The main program function */
int main( void )
{
  FILE *InF, *OutF;
	__int64 A, B, C, N, K, i, j;
	__int64 Ar, Br, Cr, Nd;

	InF = fopen(IN_FILE, "rt");
	OutF = fopen(OUT_FILE, "wt");

	fscanf(InF, "%I64i", &K);
	for (i = 0; i < K; i++)
	{
		fscanf(InF, "%I64i%I64i%I64i%I64i", &N, &A, &B, &C);
		if ((A) * C > (B) * (N - 1))
		{
  		Ar = A;
			Br = Ar;
			Cr = N - 1;
		}
		else
		{
  		Ar = A + 1;
			Br = Ar;
			Cr = N - 1;
		}
		if (Br == 0)
			Cr = 1;
		if (Cr == Br)
			Ar++, Cr = 1, Br = 0;
		Nd = Nod(Br, Cr);
		Br /= Nd;
		Cr /= Nd;
    Ar %= N;
		fprintf(OutF, "%I64i %I64i %I64i\n", Ar, Br, Cr);
	}

	fclose(InF);
	fclose(OutF);
	return 0;
} /* End of 'main' function */

/* END OF 'A.CPP' FILE */
