#include <stdio.h>

int main()
{
 FILE *f, *f1;
 f=fopen("clock.in", "rt");
 f1=fopen("clock.out", "wt");
 long k,n,a,b,c,a1,b1,c1;
 int aa1,aa2;
 fscanf(f,"%ld",&k);
 for(int i = 1; i <= k; i++)
 {
  fscanf(f,"%ld",&n);
  fscanf(f,"%ld",&a);
  fscanf(f,"%ld",&b);
  fscanf(f,"%ld",&c);
  aa1 = 360*(a + b/c)/n;
  aa2 = 360*b/c;
  aa2 = (a == n - 1 ? 360 : aa2);
  if(aa1 > aa2) a1 = b1 = b = a;
  else a1 = b1 = b = a + 1;
  c1 = c = n - 1;
  b = (b >= n ? b % n : b);
  b1 = (b1 >= n ? b1 % n : b1);
  while((b != 0) && (c != 0))
  {
   if(c >= b) c %= b;
   else b %= c;
  }
  if(b1 == c1)
  {
   b1 = 0;
   c1 = 1;
   a1++;
  }
  b1 /= (c == 0 ? b : c);
  c1 /= (c == 0 ? b : c);
  a1 = (a1 == n ? 0 : a1);
  fprintf(f1, "%ld %ld %ld\n", a1, b1, c1);
 }
 fclose(f1);
 fclose(f);
 return 0;
}
