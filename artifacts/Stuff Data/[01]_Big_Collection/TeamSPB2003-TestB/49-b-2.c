#include <stdio.h>

int main( void )
{
  int m;
  
  freopen("clock.in", "r", stdin);
  freopen("clock.out", "w", stdout);
  scanf("%d", &m);
  while (m-- > 0)
  {
    int n, a, b, c, u, v;

    scanf("%d%d%d%d", &n, &a, &b, &c);
    if ((__int64)a * c <= (__int64)b * (n - 1))
    {
      a++;
      if (a == n)
        a = 0;
    }
    if (a == n - 1)
      a = 0;
    u = a;
    v = n - 1;
    while (v != 0)
    {
      int w = u % v;

      u = v;
      v = w;
    }
    printf("%d %d %d\n", a, a / u, (n - 1) / u);
  }
  return 0;
}
