#include <stdio.h>
__int64 nod(__int64 a,__int64 b){
	while((a!=0)&&(b!=0)){
		if(a>b) 
			a=a % b;
		else
			b=b % a;
	}
	return a+b;
}
int main () {
	__int64 k,n,a,b,c,a1,b1,c1,q,i;
    FILE *f1=fopen("clock.in","r");
    FILE *f2=fopen("clock.out","w");
    fscanf(f1,"%I64i",&k);
	for(i=0;i<k;i++){
		  fscanf(f1,"%I64i",&n);
		  fscanf(f1,"%I64i",&a);
		  fscanf(f1,"%I64i",&b);
		  fscanf(f1,"%I64i",&c);
		  q=nod(b,c);
		  c/=q;
		  b/=q;
		  if((a*c+b>b*n)&&(a<(n-1))){
			  a1=a;
		  }else{
			  a1=(a+1)%n;
		  }
		  b1=a1;
		  c1=n-1;
		  if(c1==0){
			   c1=1;
		  }
		  a1+=b1/c1;
		  b1=b1 % c1;
		  a1=a1%n;
		  q=nod(c1,b1);
		  c1/=q;
		  b1/=q;	  
		  fprintf(f2,"%I64i %I64i %I64i\n",a1,b1,c1);
	}
	fclose(f1);
    fclose(f2);
    return 0;
}