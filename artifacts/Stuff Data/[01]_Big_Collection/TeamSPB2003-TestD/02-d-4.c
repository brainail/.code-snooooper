/*
 * FILE NAME : D.C.
 * PURPOSE   : Task 'D' solution.
 * PROGRAMMER: Michael Rybalkin, 
 *             Anatoly Nalimov,
 *             Denis Burkov.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

/* Types defenitions */
typedef int INT;
typedef unsigned int UINT;
typedef long LONG;
typedef unsigned long ULONG;
typedef char CHAR;
typedef unsigned char BYTE;
typedef double DBL;
typedef long double LDBL;

typedef struct tagBONUS 
{
  LONG gt, pt;
  INT Is;
} OBJ;

OBJ Obj[100000];
INT Inter[100000];
INT Num, L, N;

int compare( const void *elem1, const void *elem2 ) 
{
  OBJ *O1, *O2;

  O1 = (OBJ *)elem1;
  O2 = (OBJ *)elem2;

  return O1->gt > O2->gt;
}

/* The main function */
INT main( VOID )
{
  FILE *In, *Out;
  INT i, j, C, Max, Min, MaxC = 0, t;

  In = fopen("customs.in", "rt");
  Out = fopen("customs.out", "wt");
  if (In == NULL || Out == NULL)
  {
    if (In != NULL)
      fclose(In);
    if (Out != NULL)
      fclose(Out);
    return 1;
  }


  fscanf(In, "%i", &Num);

  for (i = 0; i < Num; i++)
  {
    fscanf(In, "%ld %ld", &Obj[i].gt, &Obj[i].pt);
    Obj[i].Is = 1;
  }


  /*
  qsort(Obj, Num, sizeof(OBJ), compare);
  */
  Max = 0;
  Min = 0;
  for (i = 0; i < Num; i++)
  {
    if (Obj[i].gt  + Obj[i].pt > Obj[Max].gt + Obj[Max].pt)
      Max = i;
    if (Obj[i].gt < Obj[Min].gt)
      Min = i;
  }
  for (t = Obj[Min].gt; t <= Obj[Max].gt + Obj[Max].pt; t++)
  {
    C = 0;
    for (i = 0; i < Num; i++)
      if (t >= Obj[i].gt && t < Obj[i].gt + Obj[i].pt)
        C++;
    if (C > MaxC)
      MaxC = C;
  }
  fprintf(Out, "%d", MaxC);
  /*
  for (i = 0; i < Num; i++)
  {
    C = 0;
    for (j = 0; j < Num; j++)
    {
      if (i == j)
        continue;

      if ((Obj[j].gt > Obj[i].gt && Obj[j].gt < Obj[i].gt + Obj[i].pt) ||
          (Obj[j].gt + Obj[j].pt > Obj[i].gt && 
           Obj[j].gt + Obj[j].pt < Obj[i].gt + Obj[i].pt))
        C++;
      if (Obj[i].gt == Obj[j].gt)
        C++;
    }
    Inter[i] = C;
  }
  */

  fclose(In);
  fclose(Out);
  return 0;
} /* End of 'main' function */

/* END OF 'D' FILE */
