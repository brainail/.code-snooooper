#include <stdio.h>
#define m 100003
int a[m],b[m],n;
void read(void)
{
  int i,t,l;
  scanf("%d",&n);
  for(i=1;i<=n;i++)
  {
    scanf("%d%d",&t,&l);
    a[i*2-1]=t;b[i*2-1]=1;
    a[i*2]=t+l;b[i*2]=0;
  }
}
void swap(int i,int j)
{
  int t;
  t=a[i];a[i]=a[j];a[j]=t;
  t=b[i];b[i]=b[j];b[j]=t;
}
void down(int i,int s)
{
  while((i*2<=s&&a[i*2]>a[i])||(i*2+1<=s&&a[i*2+1]>a[i]))
  {
    if(i*2+1<=s&&a[i*2+1]>a[i*2])
    {
      swap(i*2+1,i);i=i*2+1;
    }
    else
    {
      swap(i*2,i);i*=2;
    }
  }
}
void solve(void)
{
  int i,s,c,best;
  for(i=n*2;i>0;i--)
    down(i,n*2);
  for(i=1,s=n*2;i<n*2&&s>1;i++)
  {
    swap(1,s);
    down(1,s-1);
    s--;
  }
  for(i=1,c=best=0;i<=n*2;i++)
  {
    if(b[i])c++;else c--;
    while(i<n*2&&a[i+1]==a[i])
    {
      if(b[i+1])c++;else c--;
      i++;
    }
    if(c>best)best=c;
  }
  printf("%d\n",best);
}
int main(void)
{
  freopen("customs.in","r",stdin);
  freopen("customs.out","w",stdout);
  read();
  solve();
  return 0;
}
