#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

/* H task*/

typedef int INT;
typedef __int64 INT64;
typedef long LONG;

typedef struct tagEL
{
  LONG T, L;
  INT Count;
} El;

INT Test( El Ar1, El Ar2 )
{
  if (Ar1.T + Ar1.L >= Ar2.T)
		return 1;
  if (Ar2.T + Ar2.L >= Ar1.T)
		return 1;
  return 0;
} /* End of 'Test' function */

INT main( void )
{
  FILE *Fin, *Fout;
  LONG Max = 0, N, i, j, MM = 0, st = 4000000, o;
  INT *Time;
  INT T, Cur = 0;
  INT L;
  El *Ar;
  
	Fin = fopen("customs.in", "rt");
  fscanf(Fin, "%i", &N);
  Ar = malloc(sizeof(El) * N);
  for (i = 0; i < N; i++)
  {  
		fscanf(Fin, "%i%i", &Ar[i].T, &Ar[i].L);
	  Ar[i].Count = 0;
	}
	fclose(Fin);
	for (i = 0; i < N; i++)
	{	
		for (j = Cur; j < N; j++)
      if (Test(Ar[i], Ar[j]) == 1)
			{	
				Ar[i].Count++;
			  if (Ar[i].Count > Max)
					Max = Ar[i].Count;
			}

	  Cur++;
	}
	Fout = fopen("customs.out", "wt");
  fprintf(Fout, "%i", Max ); 
  free(Ar);
	fclose(Fout);
#if 0
  Fin = fopen("customs.in", "wt");
	fprintf(Fin, "2000000 \n");;
	for (i = 0; i < 2000000ul; i++)
	{
	  fprintf(Fin, "2 1000000 \n");
	}
	fclose(Fin);
  Time = malloc(sizeof(INT) * 2000000ul);
  memset(Time, 0, sizeof(INT) * 2000000ul);
  Fin = fopen("customs.in", "rt");
  fscanf(Fin, "%i", &N);
  fscanf(Fin, "%i%i", &T, &L);
  st = T;
  MM = T + L;
  for (j = 0; j < L; j++)
    Time[T + j]++;
  
	for (i = 0; i < N - 1; i++)
  {
    fscanf(Fin, "%i%i", &T, &L);
    o = T + L;
		
		if ( T < st)
      st = T;
    else
      if ( o > MM)
          MM = o;
		
		for (j = T; j < o; j++)
			Time[j]++;
	
	}	
  fclose(Fin);
  
	 for (i = st; i < MM; i++)
     if (Time[i] > Max)
        Max = Time[i];
	Fout = fopen("customs.out", "wt");
  fprintf(Fout, "%i", Max ); 
  fclose(Fout);
  free(Time);
#endif /* 0 */
	return 0;
}

