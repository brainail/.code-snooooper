#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct cus
{
  int t, f;
};

int compare(const void *a, const void *b)
{
  //struct cus *x, *y;
  //int c;
  /*x = (struct cus*)malloc(sizeof(struct cus));
  y = (struct cus*)malloc(sizeof(struct cus));
  x = (struct cus *)a;
  y = (struct cus *)b;
  c = x->t - y->t;
  free(x);
  free(y);*/
  if(((struct cus*)a)->t - ((struct cus*)b)->t == 0)
  {
    return (((struct cus*)a)->f - ((struct cus*)b)->f);
  }
  else
  {
    return (((struct cus*)a)->t - ((struct cus*)b)->t);
  }
}

int main()
{
  FILE *in = fopen("customs.in", "r"), *out = fopen("customs.out", "w");
  int N, i, max, tmp, j, k, l;
  struct cus *S;
  char *a, *b;
  a = (char*)malloc(1000*sizeof(char));
  b = (char*)malloc(1000*sizeof(char));
  fscanf(in, "%d\n", &N);
  S = (struct cus*)malloc(2*N*sizeof(struct cus));
  for(i = 0; i < N; i++)
  {
    //fscanf(in, "%d %d", &S[i].t, &S[i+N].t);
    fgets(a, 1000, in);
    l = strlen(a);
    for(j = 0; a[j] != ' '; j++)
    for(k = j+1; k < l; k++)
    {
      b[k-j-1] = a[k];
    }
    b[l-j-1] = ' ';
    S[i].t = atoi(a);
    S[i+N].t = atoi(b);
    S[i].f = 1;
    S[i+N].f = -1;
    S[i+N].t += S[i].t;
  }
  //printf("a\n");
  qsort((void*)S, 2*N, sizeof(struct cus), compare);  
  //printf("b\n");

  /*for(i = 0; i < 2*N; i++)
  {
    printf("%d %d\n", S[i].t, S[i].f);
  }*/

  max = 0;
  tmp = 0;
  for(i = 0; i < 2*N; i++)
  {
    tmp += S[i].f;
    if(tmp > max)
    {
      max = tmp;
    }
    //printf("max=%d tmp = %d\n", max, tmp);
  }
  fprintf(out, "%d\n", max);
  fcloseall();
  return 0;
}