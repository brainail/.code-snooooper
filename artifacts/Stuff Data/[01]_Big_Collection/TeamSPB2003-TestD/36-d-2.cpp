/* Copyright (c) by PML #30-3 (02.11.2003) */

#include <stdio.h>

#define IN_FILE "customs.in"
#define OUT_FILE "customs.out"

#define SWAP(A, B, C) ((C) = (A), (A) = (B), (B) = (C))
 
long T[50001UL], L[50001UL], S[50001UL]; 

/* Merge1 */
void M( long *a, long p, long q, long r )
{
	long i = p, j = q + 1, k = 0;

	while ((i <= q) || (j <= r))
		if (((a[i] < a[j]) && (i <= q)) || (j > r))
		  S[k++] = a[i++];
		else
		  S[k++] = a[j++];
	for (i = p; i <= r; i++)
		a[i] = S[i - p];
} /* End of 'M' function */

/* Merge main */
void Merge( long *a, long p, long r )
{
	long q;

	if (p < r)
	{
		q = (p + r) / 2;
		Merge(a, p, q);
		Merge(a, q + 1, r);
		M(a, p, q, r);
	}
} /* End of 'Merge' function */

/* The main program function */
int main( void )
{
  FILE *InF, *OutF;
	long n, i, j, x, max = 1, num;
	long pos;

	InF = fopen(IN_FILE, "rt");
	OutF = fopen(OUT_FILE, "wt");
	
	fscanf(InF, "%li", &n);	
	for (i = 0; i < n; i++)
		fscanf(InF, "%li%li", &T[i], &L[i]), L[i] += T[i];
	Merge(T, 0, n - 1);
	Merge(L, 0, n - 1);
/*
	for (i = n; i > 0; i--)
		for (j = 1; j < n; j++)
			if (T[j] < T[j - 1])
				SWAP(T[j], T[j - 1], x);
	for (i = n; i > 0; i--)
		for (j = 1; j < n; j++)
			if (L[j] < L[j - 1])
				SWAP(L[j], L[j - 1], x);
*/
	T[n] = L[n] = 10000000UL;
	pos = 0, num = 1;
	for (i = 1; i < n; i++)
	{
		num++;
		while (L[pos] <= T[i])
			pos++, num--;
		if (num > max)
			max = num;
	}
	fprintf(OutF, "%li", max);

	fclose(InF);
	fclose(OutF);
	return 0;
} /* End of 'main' function */

/* END OF 'A.CPP' FILE */
