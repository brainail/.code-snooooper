#include <stdio.h>

#define maxsize 50000

int a[2 * maxsize], b[2 * maxsize];

int main( void )
{
  int cur = 0, max = 0;
  int i, m, n;
  
  freopen("customs.in", "r", stdin);
  freopen("customs.out", "w", stdout);
  scanf("%d", &n);
  for (i = 0; i < n; i++)
  {
    int T, L;

    scanf("%d%d", &T, &L);
    a[i << 1] = (T << 1) | 1;
    a[(i << 1) | 1] = (T + L) << 1;
  }
  m = n << 1;
  for (i = 0; i < 30; i++)
  {
    int j, k1 = 0, k2 = 0;
    
    for (j = 0; j < m; j++)
      if (a[j] & (1 << i))
        b[k2++] = a[j];
      else
        a[k1++] = a[j];
    for (j = 0; j < k2; j++)
      a[k1++] = b[j];
  }
  for (i = 0; i < m; i++)
    if (a[i] & 1)
    {
      cur++;
      if (max < cur)
        max = cur;
    }
    else
      cur--;
  printf("%d\n", max);
  return 0;
}
