/*
 * FILE NAME : E.C.
 * PURPOSE   : Task 'E' solution.
 * PROGRAMMER: Michael Rybalkin, 
 *             Anatoly Nalimov,
 *             Denis Burkov.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

/* Types defenitions */
typedef int INT;
typedef unsigned int UINT;
typedef long LONG;
typedef unsigned long ULONG;
typedef char CHAR;
typedef unsigned char BYTE;
typedef double DBL;
typedef long double LDBL;

/* Chars */
CHAR Chars[] = "abcdefghijklmnopqrstuvwxyz0123456789";

#define MAX_S (sizeof(Chars) / sizeof(Chars[0]))

INT CLen;

__int64 GetLen( INT N )
{
  if (N < 0)
    return 0;
  if (N == 0)
    return 1;
  return GetLen(N - 1) * 2 + 1;
} /* End of 'GetLen' functin */

/* Checking string function */
INT Check( INT N, CHAR *S, INT *Pos, INT MaxPos )
{
  if (*Pos >= MaxPos)
    return 1;
  if (N == 0)
    if (S[*Pos] == Chars[0])
    {
      *Pos = *Pos + 1;
      return 1;
    }
    else
      return 0;
  if (Check(N - 1, S, Pos, MaxPos))
  {
    if (*Pos >= MaxPos)
      return 1;
    if (S[*Pos] == Chars[N])
    {
      *Pos = *Pos + 1;
      if (Check(N - 1, S, Pos, MaxPos))
        return 1;
    }
  }
  return 0;
} /* End of 'Check' function */

/* Finding char function */
INT Find( CHAR C )
{
  INT i;

  for (i = 0; i < MAX_S; i++)
    if (Chars[i] == C)
      break;
  if (i == MAX_S)
    return -1;
  return i;
} /* End of 'Find' functin */

/* The main function */
INT main( VOID )
{
  FILE *In, *Out;
  CHAR Din[200], DinInv[200];
  INT i, ci, mi;
  INT Max, N, Pos;
  __int64 Pos2;


  In = fopen("dincode.in", "rt");
  Out = fopen("dincode.out", "wt");
  if (In == NULL || Out == NULL)
  {
    if (In != NULL)
      fclose(In);
    if (Out != NULL)
      fclose(Out);
    return 1;
  }

  fscanf(In, "%s", Din);
  CLen = strlen(Din);
  Max = 0;
  for (i = 0; i < CLen; i++)
  {
    ci = Find(Din[i]);
    mi = Find(Din[Max]);
    if (ci < 0 || mi < 0)
    {
      fprintf(Out, "0");
      return 0;
    }
    if (mi < ci)
      Max = i;
  }
  if ((N = Find(Din[Max])) == -1)
    fprintf(Out, "0");
  else
  {
    Pos = 0;
    if (Check(N - 1, Din + Max + 1, &Pos, CLen - Max - 1))
    {
      for (i = 0; i < Max; i++) 
        DinInv[i] = Din[Max - i - 1];
      Pos = 0;
      if (Check(N - 1, DinInv, &Pos, Max))
      {
        Pos2 = GetLen(N - 1);
        Pos2 = Pos2 - Max;
        fprintf(Out, "%d %I64d", N + 1, Pos2);
      }
      else
        fprintf(Out, "0");
    }
    else
      fprintf(Out, "0");
  }
  fclose(In);
  fclose(Out);
  return 0;
} /* End of 'main' function */

/* END OF 'E.C' FILE */
