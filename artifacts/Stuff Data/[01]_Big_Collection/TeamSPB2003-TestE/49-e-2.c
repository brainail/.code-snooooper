#include <stdio.h>
#include <string.h>

int main( void )
{
  char s[101];
  int t[100];
  const char *c = "abcdefghijklmnopqrstuvwxyz0123456789";
  int i, j, n;
  long long offset;
  
  freopen("dinocode.in", "r", stdin);
  freopen("dinocode.out", "w", stdout);
  scanf("%s", s);
  n = strlen(s);
  for (i = 0; i < n; i++)
    t[i] = strchr(c, s[i]) - c;
  j = 0;
  for (i = 1; i < n; i++)
    if (t[j] < t[i])
      j = i;
  offset = ((__int64)1 << t[j]) - j;
  if (offset > 0)
  {
    for (i = 0; i < n; i++)
    {
      long long j = i + offset;
      int k = 0;

      while ((j & 1) == 0)
      {
        j >>= 1;
        k++;
      }
      if (t[i] != k)
        break;
    }
    if (i == n)
      printf("%d %Ld\n", t[j] + 1, offset - 1);
    else
      printf("0\n");
  }
  else
    printf("0\n");
  return 0;
}
