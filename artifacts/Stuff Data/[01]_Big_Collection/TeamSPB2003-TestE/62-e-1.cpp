#include <stdio.h>

char trans (char c) {
	if ((c>='a')&&(c<='z')) return c-'a';
	return (c-'0')+26;
}

__int64 st2[37];
char s[100],s2[100], pattern[100];
__int64 l,i;

int recpr(int mb, char ss) {
	if (ss>0) {
		__int64 sp=st2[mb]-st2[mb-1];
		if (mb<0) return recpr(mb+sp,ss-1);
		if (mb>=l) return recpr(mb-sp,ss-1);
		if (s2[mb]==ss) 
			return (recpr(mb-sp,ss-1) || recpr(mb+sp,ss-1));
		return 0;
	} 
	return (s2[mb]==ss);
}

char getnum(__int64 arg) {
  int ix;
  ix = 0;
  while (arg % st2[ix + 1] == 0)
    ix++;
  return ix;
}

int main () {
	__int64 mb, itisright;
    __int64 shift; 

    FILE *f=fopen("dinocode.in","r");
	fgets(s,100,f);
    for (i=0;i<100;i++) if ((s[i]=='\0')||(s[i]=='\n')||(s[i]=='\r')) break;
	l=i;
	for (i=0;i<l;i++) s2[i]=trans(s[i]);
    fclose(f);
	mb=0;
	for (i=1;i<l;i++) if (s2[i]>s2[mb]) mb=i;
	st2[0]=1;
	for (i=1;i<37;i++) st2[i]=st2[i-1]*2;	

    //Make pattern
	shift = st2[s2[mb]] - mb - 1;
    for (i = 0; i < 100; i++) {
       pattern[i] = getnum(i + shift + 1);
    }
    
	itisright = 1;
	for (i = 0; i < l; i++) {
       itisright = itisright & (pattern[i] == s2[i]);
	}

    f=fopen("dinocode.out","w");
	//if (recpr(mb,s2[mb])) {
	//	fprintf(f,"%ld %ld\n", mb+1, st2[s2[mb]]-mb-1);
	//} else fprintf(f,"0\n");
    if (itisright) {
		fprintf(f,"%ld %I64i\n", s2[mb] + 1, shift);
	} else fprintf(f,"0\n");
	fclose(f);
    return 0;
}