#include <stdio.h>
#include <math.h>
#define mx 1005
double x[mx],y[mx],xx[mx],yy[mx],xc,yc;
int n;
int ans = 0;
double d(double x1,double y1,double x2,double y2)
{
  double r=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
  if(r==2)
    return sqrt(2);
  return sqrt(r);
}
void read(void)
{
  int i;
  scanf("%d%lf%lf",&n,&xc,&yc);
  for(i=1;i<=n;i++)
    scanf("%lf%lf",&x[i],&y[i]);
}
void solve(void)
{
  int i,j;
  double xm=10000,ym=10000,x1,y1,c,cc,bx,by;
  for(i=1;i<=n;i++)
  {
    if(y[i]<ym)
    {
      ym=y[i];xm=x[i];
    }
    else if(y[i]==ym&&x[i]<xm)
    {
      xm=x[i];
    }
  }
  x1=xm-1;y1=ym;
  xx[1]=xm;yy[1]=ym;
  for(i=2;1;i++)
  {
    c=100;
    for(j=1;j<=n;j++)
    {
      if(x[j]==xm&&y[j]==ym)continue;
      cc=((x[j]-xm)*(x1-xm)+(y[j]-ym)*(y1-ym))/d(xm,ym,x[j],y[j])/
      d(xm,ym,x1,y1);
      if(cc<c)
      {
	c=cc;bx=x[j];by=y[j];
      }
      else if(cc==c&&d(xm,ym,bx,by)<d(xm,ym,x[j],y[j]))
      {
	c=cc;bx=x[j];by=y[j];
      }
    }
    if(i>1&&bx==xx[1]&&by==yy[1])break;
    xx[i]=bx;yy[i]=by;
    x1=xm;y1=ym;
    xm=bx;ym=by;
  }
  n = --i;
  xx[n+1]=xx[1];
  yy[n+1]=yy[1];
  for (i = 1; i <= n; i++){
	double a, b, c;
	double dd;
	double l, l1;
 	a = yy[i] - yy[i+1];
	b = - xx[i] + xx[i+1];
	c = -xx[i]*a-yy[i]*b;
	dd = fabs((a*xc + b*yc + c)/sqrt(a*a+b*b));
	l = sqrt(d(xc, yc, xx[i], yy[i])*d(xc, yc, xx[i], yy[i]) - dd*dd);
	l1 = sqrt(d(xc, yc, xx[i+1], yy[i+1])*d(xc, yc, xx[i+1], yy[i+1]) - dd*dd);
	if ((l1 < d(xx[i+1], yy[i+1], xx[i], yy[i]) - 0.0000000001)&&
	    (l < d(xx[i+1], yy[i+1], xx[i], yy[i]) - 0.0000000001)){
	    ans++;
	}
  }
  printf("%d\n",ans);
}
int main(void)
{
  freopen("dragon.in","r",stdin);
  freopen("dragon.out","w",stdout);
  read();
  solve();
  return 0;
}
