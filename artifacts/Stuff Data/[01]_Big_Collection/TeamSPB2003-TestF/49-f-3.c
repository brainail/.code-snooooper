#include <stdio.h>

#define max 1000

int x[max + 1], y[max + 1], xx[max + 1], yy[max + 1], s[max + 1];

int main( void )
{
  int n, xc, yc, i, j, c, t;
  
  freopen("dragon.in", "r", stdin);
  freopen("dragon.out", "w", stdout);
  scanf("%d%d%d", &n, &xc, &yc);
  for (i = 0; i < n; i++)
    scanf("%d%d", &x[i], &y[i]);

  j = 0;
  for (i = 0; i < n; i++)
    if (y[i] < y[j] || (y[i] == y[j] && x[i] < x[j]))
      j = i;
  for (i = j; i < n; i++)
  {
    xx[i - j] = x[i];
    yy[i - j] = y[i];
  }
  for (i = 0; i < j; i++)
  {
    xx[n - j + i] = x[i];
    yy[n - j + i] = y[i];
  }
  for (i = 0; i < n; i++)
  {
    x[i] = xx[i];
    y[i] = yy[i];
  }


  x[n] = x[0];
  y[n] = y[0];
  t = 0;
  for (c = 0; c <= n; c++)
  {
    while (t > 1)
    {
      int a = s[t - 2], b = s[t - 1];

      if ((x[b] - x[a]) * (y[c] - y[b]) - (y[b] - y[a]) * (x[c] - x[b]) > 0)
        break;
      t--;
    }
    s[t] = c;
    t++;
  }
  c = 0;
  for (i = 0; i < t - 1; i++)
  {
    int a = s[i], b = s[i + 1];
    int dx = x[a] - x[b], dy = y[a] - y[b];

    if (dx * (xc - x[a]) + dy * (yc - y[a]) < 0
        && dx * (xc - x[b]) + dy * (yc - y[b]) > 0)
      c++;
  }
  printf("%d\n", c);
  return 0;
}
