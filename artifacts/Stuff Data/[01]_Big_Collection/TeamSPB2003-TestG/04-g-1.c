#include <stdio.h>
#define m 2000
int c[m],n,a,b;
int mx(int x,int y)
{
  if(x>y)return x;
  return y;
}
void read(void)
{
  scanf("%d%d%d",&n,&a,&b);
}
void solve(void)
{
  int i,j;
  c[1]=c[0]=0;
  for(i=2;i<=n;i++)
  {
    c[i]=-1;
    for(j=1;j<i;j++)
      if(c[i]==-1||c[i]>mx(c[j]+a,c[i-j]+b))
	c[i]=mx(c[j]+a,c[i-j]+b);
  }
  printf("%d\n",c[n]);
}
int main(void)
{
  freopen("game.in","r",stdin);
  freopen("game.out","w",stdout);
  read();
  solve();
  return 0;
}
