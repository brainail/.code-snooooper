#include <stdio.h>
#include <math.h>
#include <malloc.h>

/* H task*/

typedef int INT;
typedef __int64 INT64;
typedef long LONG;

INT *ResAr;

void Rec( INT CurPos )
{
  if (ResAr[CurPos] + 1 > 9)
  {  
    ResAr[CurPos] = ((ResAr[CurPos] + 1) % 10);
    ResAr[CurPos + 1] += 1;
  }
  return;
}

INT main( void )
{
  FILE *Fin, *Fout;
  LONG n, k, i, j, pos; 
  INT64 V1, Value = 0;
  INT *NumAr;
  LONG Cur = 0;
  INT Now, Now2;
  
  Fin = fopen("ones.in", "rt");
  fscanf(Fin, "%li", &n);
  NumAr = malloc(sizeof(INT) * n);
  ResAr = malloc(sizeof(INT) * (n + n + n));
  memset(ResAr, 0, sizeof(INT) *  3 * n);
  /*
  for (i = 0; i < n + n - 1; i++)
    ResAr[i] = 0;
  ResAr[0] = -1;
  */
  for (i = 0; i < n; i++)
    NumAr[i] = 1;
  fclose(Fin);
  for (i = 0; i < n; i++)
  {
    for (j = Cur; j < Cur + n; j++)
    {
      /*
      if (ResAr[j] == -1)
      {
        ResAr[j] = 0;
        ResAr[j + 1] = -1;
      }
      */
      Now = ResAr[j];
      if ((Now + 1) > 9)
      {
        Rec(j);
      }
      else
        ResAr[j] += 1;
    }
    Cur++;  
  }
  Fout = fopen("ones.out", "wt");
  /*
  for (i = 0; i < 3 * n; i++)
    if(ResAr[i] == -1)
      pos = i;
  
  for (i = pos - 1; i >= 0; i--)
    fprintf(Fout, "%i", ResAr[i] ); 
  */
  for (i = 2 * n - 2; i >= 0; i--)
    fprintf(Fout, "%i", ResAr[i] ); 
  fclose(Fout);
  
  free(NumAr);
  return 0;
}

