#include<stdio.h>
#include<stdlib.h>

int main(){
  __int64 N,a,b;
  FILE *in=fopen("game.in","r"),*out=fopen("game.out","w");
  __int64 k,base,j;
  __int64 max,max2;

  fscanf(in,"%I64d%I64d%I64d",&N,&a,&b);
  if (a==0)
  {
    fprintf(out,"%I64d",b);
  }else if(b==0){
    fprintf(out, "%I64d",a);
  }
  else if (a==b) {
    for(k=1,base=0;k<N;k*=2,base++);
    fprintf(out,"%I64d",(a*base));   
  }else{
      for(k=1,base=0,j=1;k<N*j;k*=(a+b),j*=a,base++);  
      max=base*N*a;
    
      for(k=1,base=0,j=1;k<N*j;k*=(a+b),j*=b,base++);  
      max2=base*N*b;
    
    if (max>max2)
    {
      fprintf(out,"%I64d",max);
    }else
    {
      fprintf(out,"%I64d",max2);  
    }
  }

  fcloseall();
  return 0;
}