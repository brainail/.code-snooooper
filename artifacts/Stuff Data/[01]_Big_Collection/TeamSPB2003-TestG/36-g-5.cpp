/* Copyright (c) by PML #30-3 (02.11.2003) */

#include <stdio.h>
#include <math.h>

#define IN_FILE "game.in"
#define OUT_FILE "game.out"

#define MAX(A, B) (((A) > (B)) ? (A) : (B))
#define EPS 1e-10 

/* The main program function */
int main( void )
{
  FILE *InF, *OutF;
	long A, B, N, c, sum = 0;

	InF = fopen(IN_FILE, "rt");
	OutF = fopen(OUT_FILE, "wt");

	fscanf(InF, "%li%li%li", &N, &A, &B);

	if (N == 1)
		fprintf(OutF, "0");
	else if (A == 0)
		fprintf(OutF, "%li", B);
	else if (B == 0)
		fprintf(OutF, "%li", A);
	else
	{
		c = MAX(A, B);
		while (N != 1)
		{
      if (N % 2 == 0)
				N /= 2;
			else
				N = N / 2 + 1;
			sum += c;
		}
		fprintf(OutF, "%li", sum);
	}

	fclose(InF);
	fclose(OutF);

	return 0;
} /* End of 'main' function */

/* END OF 'G.CPP' FILE */
