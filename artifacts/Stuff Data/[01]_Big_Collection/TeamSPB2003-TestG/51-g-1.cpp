#include <stdio.h>

int min(int x, int y)
{
	return (x < y) ? x : y;
}

int main()
{
	int n;
	int a, b;
	
	FILE *fIn, *fOut;
	fIn = fopen("game.in", "r");
	fOut = fopen("game.out", "w");

	fscanf(fIn, "%d", &n);
	fscanf(fIn, "%d%d", &a, &b);

	int sum = 0;
	int nMin = min(a, b);
	while(n != 2)
	{
		if(n & 1)
			n = n / 2 + 1;
		else
			n = n / 2;
		sum += nMin;
	}

	sum += a + b - nMin;
	fprintf(fOut, "%d", sum);

	fclose(fOut);
	fclose(fIn);
	return 0;
}