/*
 * FILE NAME : H.C.
 * PURPOSE   : Task 'H' solution.
 * PROGRAMMER: Michael Rybalkin, 
 *             Anatoly Nalimov,
 *             Denis Burkov.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

/* Types defenitions */
typedef int INT;
typedef unsigned int UINT;
typedef long LONG;
typedef unsigned long ULONG;
typedef char CHAR;
typedef unsigned char BYTE;
typedef double DBL;
typedef long double LDBL;
typedef void VOID;

BYTE Mas[210000];

/* The main function */
INT main( VOID )
{
  FILE *In, *Out;
  INT N, O, i, n, pos = 0;

  In = fopen("ones.in", "rt");
  Out = fopen("ones.out", "wt");
  if (In == NULL || Out == NULL)
  {
    if (In != NULL)
      fclose(In);
    if (Out != NULL)
      fclose(Out);
    return 1;
  }

  fscanf(In, "%d", &N);
  O = 0;
  for (i = 1; i <= N; i++)
  {
    n = O + i;
    O = n / 10;
    n = n % 10;
    Mas[pos++] = n;
  }
  for (i = N - 1; i >= 1; i--)
  {
    n = O + i;
    O = n / 10;
    n = n % 10;
    Mas[pos++] = n;
  }
  for (i = pos - 1; i >= 0; i--)
    fprintf(Out, "%d", Mas[i]);

  fclose(In);
  fclose(Out);
  return 0;
} /* End of 'main' function */

/* END OF 'H' FILE */
