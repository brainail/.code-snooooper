#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <string.h>


/* H task*/

typedef int INT;
typedef __int64 INT64;
typedef long LONG;

INT *ResAr;

void Rec( INT CurPos )
{
  if (ResAr[CurPos] + 1 > 9)
  {  
    ResAr[CurPos] = ((ResAr[CurPos] + 1) % 10);
    ResAr[CurPos + 1] += 1;
  }
  return;
}

INT main( void )
{
  FILE *Fin, *Fout;
  LONG n, k, half, i, j, pos; 
  LONG Cur = 0;
  INT Now, Now2;
  INT Ost = 0, Raz = 0, Curpl = 1;
  
  Fin = fopen("ones.in", "rt");
  fscanf(Fin, "%li", &n);
  ResAr = malloc(sizeof(INT) * (2 * n - 1));
  memset(ResAr, 0, sizeof(INT) *  (2 * n - 1));
  /*
  for (i = 0; i < n + n - 1; i++)
    ResAr[i] = 0;
  ResAr[0] = -1;
  */
  fclose(Fin);
#if 0
  for (i = 0; i < n; i++)
  {
    for (j = Cur; j < Cur + n; j++)
    {
      /*
      if (ResAr[j] == -1)
      {
        ResAr[j] = 0;
        ResAr[j + 1] = -1;
      }
      */
      /*
      Now = ResAr[j];
      */
      if ((ResAr[j] + 1) > 9)
      {
        ResAr[j] = ((ResAr[j] + 1) % 10);
        ResAr[j + 1] += 1;
        /*
        Rec(j);
        */     
      }
      else
        ResAr[j] += 1;
    }
    Cur++;  
  }
#endif /* 0 */
  half = (2 * n - 1) / 2;
  for (i = 0; i < 2 * n - 1; i++)
  {
    ResAr[i] += Raz;
    Ost = (ResAr[i] + Curpl) % 10;
    Raz = (ResAr[i] + Curpl) / 10;
    ResAr[i] = Ost ; 
    if (i < half)
      Curpl++;
    else
      Curpl--;
  }
  
  Fout = fopen("ones.out", "wt");
  /*
  for (i = 0; i < 3 * n; i++)
    if(ResAr[i] == -1)
      pos = i;
  
  for (i = pos - 1; i >= 0; i--)
    fprintf(Fout, "%i", ResAr[i] ); 
  */
  
  for (i = 2 * n - 2; i >= 0; i--)
    fprintf(Fout, "%i", ResAr[i]); 
  
  fclose(Fout);
  
  free(ResAr);
  
  return 0;
}

