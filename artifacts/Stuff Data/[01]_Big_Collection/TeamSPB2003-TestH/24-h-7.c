#include <stdio.h>
#include <stdlib.h>

int main()
{
  FILE *in = fopen("ones.in", "r"), *out = fopen("ones.out", "w");
  int N, i, last, k, l;
  char *a;
  char t[9],s[9];
  fscanf(in, "%d", &N);
  if(N<=10)
  {
    a = (char*)malloc(2*N*sizeof(char));
    a[0]=1;
    last = 0;
    for(i = 1; i < N; i++)
    {
      a[last+1]=0;
      a[last+2]=1;
      last+=2;
      for (k=last-1;k>=last-i;k--)
      {
        a[k]+=2;
        for(l=k;a[l]==10;l--)
        {
          a[l]=0;
          a[l-1]++;
        }
      }
    }
    for (i=0;i<2*N-1;i++){
      fprintf(out,"%d",a[i]);
    }
  }
  else
  {
    
    t[0]=1;
    t[1]=2;
    t[2]=3;
    t[3]=4;
    t[4]=5;
    t[5]=6;
    t[6]=7;
    t[7]=8;
    t[8]=9;
    
    for(i=0;i<((N-1)/9);i++)
    {
      fprintf(out,"123456790");
    }
    
    for(i=0;i<N-9*((N-1)/9)-1;i++)
    {
      fprintf(out,"%d",t[i]);
    }
    
    s[0]=2;
    s[1]=3;
    s[2]=4;
    s[3]=5;
    s[4]=6;
    s[5]=7;
    s[6]=8;
    s[7]=9;
    s[8]=0;
  
    for(i=N-9*((N-1)/9)-2;i>-1;i--)
    {
      fprintf(out, "%d", s[i]);
    }


    for(i=0;i<((N-1)/9);i++)
    {
      fprintf(out,"098765432");
    }
    if(N != 1)
    {
      fprintf(out,"1");
    }
  }

  fcloseall();
  return 0;
}
