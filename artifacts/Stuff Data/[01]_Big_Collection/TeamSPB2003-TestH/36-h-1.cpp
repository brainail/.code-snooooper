/* Copyright (c) by PML #30-3 (02.11.2003) */

#include <stdio.h>

#define IN_FILE "ones.in"
#define OUT_FILE "ones.out"
 
long n, i, len;
long a[200000UL];

/* The main program function */
int main( void )
{
  FILE *InF, *OutF;

	InF = fopen(IN_FILE, "rt");
	OutF = fopen(OUT_FILE, "wt");
  
	fscanf(InF, "%li", &n);
	len = 2 * n - 1; 
	for (i = 0; i < n; i++)
		a[i] = i + 1;
	for (i = n; i < len; i++)
		a[i] = 2 * n - i - 1;
	for (i = 0; i < len - 1; i++)
		if (a[i] > 9)
			a[i + 1] += a[i] / 10, a[i] %= 10;
	for (i = len - 1; i >= 0; i--)
		fprintf(OutF, "%li", a[i]);
	fclose(InF);
	fclose(OutF);
} /* End of 'main' function */

/* END OF 'H.CPP' FILE */
