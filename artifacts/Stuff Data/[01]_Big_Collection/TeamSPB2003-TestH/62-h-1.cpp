#include <stdio.h>

long t[300000];
long st[300000];
long top=0;
int main () {
	long i,n,j,tt;
	FILE *f=fopen("ones.in","r");
	for (i=0;i<30000;i++) t[i]=0;
	fscanf(f,"%ld",&n);
    fclose(f);
	f=fopen("ones.out","w");
	for (i=0;i<n;i++) t[i]=i+1;
	for (i=n-1,j=n;i>0;i--,j++) t[j]=i;
	tt=0;
	for (i=0;i<2*n-1;i++) {
		tt+=t[i];
		st[top]=tt%10;
		top++;
		tt/=10;
	}
	for (i=top-1;i>=0;i--) fprintf(f,"%ld",st[i]);
    fclose(f);
    return 0;
}