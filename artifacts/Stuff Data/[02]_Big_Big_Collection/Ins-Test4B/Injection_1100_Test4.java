import java.io.*;

import java.sql.*;

import java.text.*;

import java.util.Random;
import java.util.Vector;



public class DataBaseUtility {
    private static Connection con = null;
    private static boolean isLoaded = false;

    
    public DataBaseUtility() { }

    
    public static void main(String[] args) throws SQLException {
       
        loadDriver();

        test();
		
    }

    private static void test() throws SQLException {
		
        deleteResult("XKIBFJLDMKSODRDDMZLLKSQORFCFQH");
		
        deleteResult("TURFCACJJRYPOBFUGLVREOFIQWIGYZ");
		
        deleteResult("MSCWBITCJVRXCLXUSRBLSBGMVURPDD");
		
        deleteResult("KRMKAHGUGLTMYGHBKRMTHDINSQAKAG");
		
    }

    public static String genSessionID() {
		
        while (true) {
			
            String ID = genID();
			
            if (getLoginFromSession(ID) == null) {
				
                return ID;
            }
			
        }
		
    }
    
    public static String genResultID() {
		
        while (true) {
            String ID = genID();
			
            if (getLoginFromResult(ID) == null) {
				
                return ID;
            }
        }
    }

    public static String genID() {
		
        Random rand = new Random(System.currentTimeMillis());
		
        char[] tempArray = new char[30];

        for (int i = 0; i < 30; i++) {
			
            tempArray[i] = (char) ((Math.abs(rand.nextInt()) % 26) + 65);
        }
		
        return new String(tempArray);
    }

    public static String createSession(String login) {

        if (isLoginValid(login) == false) {
			
            System.err.println("Error: Invalid login " + login +                ".  Unable to create new session.");
 
			return null;
        }
        String session_ID = genSessionID();
		
        if (getLoginFromSession(session_ID) == null) {
			
            long start_time = System.currentTimeMillis();
			
            ResultSet rs = makeQuery("INSERT INTO Session Values (null,'" +                    session_ID + "'," + start_time + ",null,1,'" + login +                    "');");
        } else {
			
            System.err.println("Error: Cannot create session with session_ID " +                session_ID + " because it already exists");
        }
		System.err.println("testing");
        return session_ID;
    }

    
    public static int getResultProjectIDFromLocation(String location) {


        try {
			
            ResultSet rs = makeQuery("SELECT RP_ID FROM ResultProject WHERE Project_Location='" +                    location + "';");
           
			if (rs.next()) {
				
                return rs.getInt("RP_ID");
            }
			
            return -1;
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return -1;
    }

    
    public static boolean setResultProjectCompile(String location, int value) {
		
        int RP_ID = getResultProjectIDFromLocation(location);
		
        if (RP_ID < 0) {

			return false;

        }
        try {
			
            ResultSet rs = makeQuery("UPDATE ResultProject SET Compile = " +                    value + " WHERE RP_ID = " + RP_ID + ";");
        } catch (Exception ex) {
			
            ex.printStackTrace();

			System.out.println("debug"); 
            return false;
        }

		return true;
    }

    
    public static boolean setResultComplete(String Result_ID) {
		
        try {
			
            makeQuery("UPDATE Result SET complete = 1 WHERE Result_ID = '" +                Result_ID + "';");
       
			return true;
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return false;
    }
   
    public static String[] getResultProjectCompile(String result_ID) {
		
        result_ID = getResultIDFromDate(result_ID);
		
        int[] RPIDs = getRPIDs(result_ID);
		
        String[] locations = new String[RPIDs.length];
		
        int value = 0;

        for (int i = 0; i < locations.length; i++) {
			
            locations[i] = getResultProjectLocation(RPIDs[i]);
			
            locations[i] = locations[i].substring(locations[i].lastIndexOf("/") +                    1);
            try {
				
                ResultSet rs = makeQuery("SELECT Compile FROM ResultProject " +                        "WHERE RP_ID = " + RPIDs[i] + ";");
                
				if (rs.next()) {
					
                    value = rs.getInt("Compile");
                }
            } catch (Exception ex) {
				
                ex.printStackTrace();
            }

            if (value == 0) {
				
                locations[i] += " ----> failed to compile!";
            } else {
				
                locations[i] += " ----> compiled successfully!";
            }
        }
		
        return locations;
    }

    
    public static String[] getResultProjectLocations(String result_ID) {
		
        String base = Config.getSubmittedFilesPath();
		
        int[] RPIDs = getRPIDs(result_ID);
		
        String[] locations = new String[RPIDs.length];
		
        for (int i = 0; i < locations.length; i++) {
			
            locations[i] = base + getResultProjectLocation(RPIDs[i]);
        }
		
        return locations;
    }
 
    public static String getResultProjectLocation(int RP_ID) {
		
        try {
			
            ResultSet rs = makeQuery(                    "SELECT Project_Location FROM ResultProject WHERE RP_ID=" +                    RP_ID + ";");
            if (rs.next()) {
				
                return rs.getString("Project_Location");
            }
			
            return null;
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return null;
    }

    public static int[] getRPIDs(String result) {
		
        return getIntArray("SELECT RP_ID FROM ResultProject WHERE Result_ID='" +            result + "';");
    }

    public static int[] getBSIDs(int RP_ID) {
		
        return getIntArray("SELECT BS_ID FROM BlockSignature WHERE RP_ID='" +            RP_ID + "';");
    }

    public static int[] getRemovedCharacters(int RP_ID) {
		
        return getIntArray(            "SELECT Location FROM RemovedCharacter WHERE RP_ID='" + RP_ID +            "' ORDER BY Location;");
    }

    public static int[] getRFIDs(int RP_ID) {        
		
		return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +            ";");
    }

    public static String[] getResultFileLocations(int RP_ID) {
		
        return getStringArray("SELECT File_Name FROM ResultFile WHERE RP_ID=" +            RP_ID + ";");
    }

    public static int[] getResultFileIDs(int RP_ID) {
		
        return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +            ";");
    }

    public static String[] getResultIDs(String login) {
		
        return getStringArray("SELECT Result_ID FROM Result WHERE Login='" +            login + "';");
    }

    public static String getResultIDFromDate(String date) {
		
        String[] strings = getStringArray(                "SELECT Result_ID FROM Result WHERE Time_Stamp='" + date +                "';");
        
		return strings[0];
    }

    public static String getResultDate(String result_ID) {
					System.out.println("debug"); 
        String[] strings = getStringArray(                "SELECT Time_Stamp FROM Result WHERE Result_ID='" + result_ID +                "';");
      
		return strings[0];
    }

    public static String[] getResultDates(String login) {
		
        return getStringArray("SELECT Time_Stamp FROM Result WHERE Login='" +            login + "' ORDER BY Time_Stamp DESC;");
    }

    
    public static boolean getResultComplete(String resultDate) {
		
        int[] isComplete = getIntArray(                "SELECT complete FROM Result WHERE Time_Stamp = '" +                resultDate + "';");
        
		if (isComplete == null) {
			
            return false;
        } else if (isComplete[0] == 1) {
			
            return true;
        }
		
        return false;
    }

    public static boolean doesResultBelongTo(String result, String user) {
		
        int[] isComplete = getIntArray(                "SELECT complete FROM Result WHERE Result_ID = '" + result +                "' AND Login = '" + user + "';");
        
		if (isComplete == null) {
			
            return false;
        } else {
			
            return true;
        }
    }

    public static byte getPercentCompleteFromDate(String resultDate) {
		
        ResultSet rs = makeQuery(                "SELECT complete_percent FROM Result WHERE Time_Stamp = '" +                resultDate + "';");
        try {
			
            if (rs.next()) {
				
                int val = rs.getInt("complete_percent");
				
                return (byte) val;
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return -101;
    }

    public static byte getPercentComplete(String result) {
		
        ResultSet rs = makeQuery(                "SELECT complete_percent FROM Result WHERE Result_ID = '" +                result + "';");
        try {
			
            if (rs.next()) {
				
                int val = rs.getInt("complete_percent");
				
                return (byte) val;
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return -1;
    }

    
    public static void setPercentComplete(String result, int percent) {
		
        makeQuery("UPDATE Result SET complete_percent = " + percent +            " WHERE result_ID = '" + result + "';");
    
	}

    public static int[] getIntArray(String s) {
        
        ResultSet rs = makeQuery(s);
		
        Vector v = new Vector();

        try {
			
            while (rs.next()) {
				
                Integer i = new Integer(rs.getInt(1));
				
                v.addElement(i);
				
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        int[] returnArray = new int[v.size()];
		
        for (int i = 0; i < returnArray.length; i++) {
			
            returnArray[i] = ((Integer) v.elementAt(i)).intValue();
        }
		
        return returnArray;
    }

    public static double[] getDoubleArray(String s) { 

        ResultSet rs = makeQuery(s);
		
        Vector v = new Vector();
        try {
			
            while (rs.next()) {
				
                Double i = new Double(rs.getDouble(1));
				
                v.addElement(i);
				
            }
        } catch (Exception ex) {
            ex.printStackTrace();
			
        }
		
        double[] returnArray = new double[v.size()];
		
        for (int i = 0; i < returnArray.length; i++) {
			
            returnArray[i] = ((Double) v.elementAt(i)).doubleValue();
			
        }
		
        return returnArray;

    }
     

    
    public static String[] getStringArray(String s) {
					System.out.println("debug"); 
        ResultSet rs = makeQuery(s);
		
        Vector v = new Vector();
		
        try {
			
            while (rs.next()) {
				
                v.addElement(rs.getString(1));
				
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
			
        }
		
        String[] returnArray = new String[v.size()];


        for (int i = 0; i < returnArray.length; i++) {
			
            returnArray[i] = (String) v.elementAt(i);
			
        }
		
        return returnArray;
    }

    public static void closeSession(String session_ID) {
		
        if (getLoginFromSession(session_ID) == null) {
			
            System.err.println("Error: Cannot close session with session_ID " +                session_ID + " because it does not exist");
        
		} else {
			
            if (isSessionValid(session_ID) == false) {
				
                System.err.println(                    "Error: Cannot close session with session_ID " +                    session_ID + " because it has already been closed");
                
				return;
            }
			
            long end_time = System.currentTimeMillis();
			
            ResultSet rs = makeQuery("UPDATE Session SET Stop_Time=" +                    end_time + ",Is_Valid_Session=0 WHERE Session_ID='" +                    session_ID + "';");
        
		}
    }

    public static boolean isSessionValid(String session_ID) {
                try {
					
            ResultSet rs = makeQuery(                    "SELECT Is_Valid_Session FROM Session WHERE Session_ID='" +                    session_ID + "';");
           
			rs.next();

            if (rs.getBoolean("Is_Valid_Session")) { 
				System.err.println("testing");
                return true;
            }
        } catch (Exception ex) { 
			System.err.println("testing");
            ex.printStackTrace();
        }
		
        return false;
    }

    public static int[] getBlockSignature(int RF_ID) {
		
        ResultSet rs = makeQuery("SELECT * FROM BlockSignature WHERE RF_ID =" +                RF_ID + ";");
        
		try {
			
            if (rs.next()) {
				
                int[] array = new int[11];
				
                array[0] = rs.getInt("BS_ID");
				
                array[1] = rs.getInt("RF_ID");
				
                array[2] = rs.getInt("Number_OF_If");
				
                array[3] = rs.getInt("Number_OF_Tokens");
				
                array[4] = rs.getInt("Number_OF_Breaks");
				
                array[5] = rs.getInt("Number_OF_User_Variables");
				
                array[6] = rs.getInt("Number_OF_Used_Variables");
				
                array[7] = rs.getInt("Number_OF_Reserved_Words");
				
                array[8] = rs.getInt("Number_Of_System_Defined_Variables");
				
                array[9] = rs.getInt("Begin_Index");

				array[10] = rs.getInt("End_Index");
				
                return array;
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return null;
    }

    public static int[] getSimilaritySections(int Pair_ID) {
		
        ResultSet rs = makeQuery(                "SELECT * FROM SimilaritySection WHERE Pair_ID=" + Pair_ID +                ";");
        
		Vector v = new Vector();
        try {
			
            while (rs.next()) {
				
                v.addElement(new Integer(rs.getInt("File1_Begin_Index")));
				
                v.addElement(new Integer(rs.getInt("File1_End_Index")));
				
                v.addElement(new Integer(rs.getInt("File2_Begin_Index")));
				
                v.addElement(new Integer(rs.getInt("File2_End_Index")));
				
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
        int[] array = new int[v.size()];
		
        for (int i = 0; i < array.length; i++) {
			
            array[i] = ((Integer) v.elementAt(i)).intValue();
			
        }
		
        return array;
    }

    public static int[] getSimilaritySectionIDs(int Pair_ID) {
		
        return getIntArray("SELECT SS_ID FROM SimilaritySection WHERE Pair_ID=" +            Pair_ID + ";");
    }

    public static int[] getSimilaritySection(int SS_ID) {
		
        ResultSet rs = makeQuery("SELECT * FROM SimilaritySection WHERE SS_ID=" +                SS_ID + ";");
        
		try {
			
            if (rs.next()) {
				
                int[] array = new int[6];
				
                array[0] = rs.getInt("SS_ID");
				
                array[1] = rs.getInt("Pair_ID");
				
				System.out.println("message"); 
                array[2] = rs.getInt("File1_Begin_Index");
				
                array[3] = rs.getInt("File1_End_Index");
				
                array[4] = rs.getInt("File2_Bgin_Index");
				
                array[5] = rs.getInt("File2_End_Index");
				
                return array;
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return null;
    }
    
    public static Vector getPair(int Pair_ID) {
		
        ResultSet rs = makeQuery("SELECT * FROM Pair WHERE Pair_ID=" + Pair_ID +                ";");
        try {
			
            if (rs.next()) {
				
                Vector v = new Vector();
				
                v.addElement(new Integer(rs.getInt("Pair_ID")));
				
                v.addElement(rs.getString("File1"));
				
                v.addElement(rs.getString("File2"));
				
                v.addElement(new Integer(rs.getInt("RP_ID")));
				
                v.addElement(new Boolean(rs.getBoolean("Is_Flagged")));
				
                return v;
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return null;
    }

    public static String[] getPairs(String ResultID) {
		
        ResultSet rs = makeQuery(                "SELECT File1, File2 FROM Pair where Result_ID=" + ResultID +                ";");
       
		Vector v = new Vector();
		
        try {
			
            while (rs.next()) {
				
                v.addElement(rs.getString("File1"));
				
                v.addElement(rs.getString("File2"));
				
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        int size = v.size();
		
        String[] returnArray = new String[size];

        for (int i = 0; i < size; i++) {
			
            returnArray[i] = (String) v.elementAt(i);
			
        }
		
        return returnArray;
    }

    private static String getEnd(String s) {
        
		int i;


        for (i = s.length() - 1; i >= 0; i--) {
			
            if (s.charAt(i) == '/') {
				
                break;

            }
        }
		
        return s.substring(i + 1);
    }

    public static String[] getSimilarProjects(String projectName) {
        String s1 = "SELECT File1 FROM Pair where File2='" + projectName +"';";
        String s2 = "SELECT File2 FROM Pair where File1='" + projectName +"';";
        String[] match1 = getStringArray(s1);
        String[] match2 = getStringArray(s2);
		System.out.println(s1);
		System.out.println(s2);
		System.err.println("match 1 length: " + match1.length + " match 2 length: " + match2.length ); 
        String[] tempArray = new String[match1.length + match2.length];
        for (int i = 0; i < match1.length; i++) {
            tempArray[i] = match1[i];
        }
        for (int i = 0; i < match2.length; i++) {
            tempArray[i + match1.length] = match2[i];
        }
        int repeat = 0; 
        for (int i = 0; i < tempArray.length; i++) { 
            if (tempArray[i] != null) { 
                for (int j = i + 1; j < tempArray.length; j++) { 
                    if (tempArray[i].equalsIgnoreCase(tempArray[j])) { 
                        tempArray[j] = null; 
                        repeat++; 
                    }
                }
            }
        } 
		
        String[] returnArray = new String[tempArray.length - repeat];
        int returnCount = 0; 
        for (int i = 0; i < tempArray.length; i++) { 
            if (tempArray[i] != null) { 
                returnArray[returnCount] = getEnd(tempArray[i]); 
                returnCount++; 
            }
        }
        return returnArray;
    }

    public static double getDisMeasure(String f1, String f2) { 
					System.out.println("debug"); 
        double dis = 0; 
        ResultSet rs = makeQuery("SELECT Distance1 FROM Pair WHERE File1='" +                f1 + "' && File2='" + f2 + "';");
       
		
		try {
			
            if (rs.next() == false) { 
                ResultSet rt = makeQuery(                        "SELECT Distance1 FROM Pair WHERE File1='" + f2 +                        "' && File2='" + f1 + "';");
                try { 
                    if (rt.next() == false) { 
						
                        System.err.println("Error in getDisMeasure()");
                    } else {
						
                        dis = rt.getDouble("Distance1");
                    }
                } catch (Exception ex) {
					
                    ex.printStackTrace();
                }
            } else {
				
                dis = rs.getDouble("Distance1");
            }
        } catch (Exception ex) {
			
            ex.printStackTrace();
        }
		
        return dis;
    }
     
    public static int getPairIDFromFileName(String f1, String f2) {
		
        ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE File1='" + f1 +                "' && File2='" + f2 + "';");
        try { 
            if (rs.next() == false) { 
                return -2;
            } else { 
                return rs.getInt("Pair_ID"); 
            }
        } catch (Exception ex) { 
            ex.printStackTrace(); 
        } 
        return -1;
    }

    public static int[] getFlaggedPairIDs(String resultID) { 
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static int[] getPairIDs(String resultID) { 
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static int[] getPairPercents(String resultID) { 
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static double getFlaggedPairMeasure(int PairID)  
     {	      
		
		
		double distance = 0.11; 
        try { 
            ResultSet rs = makeQuery(                    "SELECT Distance1 FROM Pair WHERE Pair_ID=" + PairID + ";"); 
            if (rs.next()) {

				
                distance = rs.getDouble("Distance1");
				
            } else {
				
                distance = 0.22;
            }
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return distance;
    }

    public static int[] getFlaggedPairPercents(String resultID) {
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +            resultID + "' && Is_Flagged = 1;");
    }

    public static double[] getFlaggedPairDisMeasure(String resultID) { 
        return getDoubleArray("SELECT Distance1 FROM Pair WHERE Result_ID='" +            resultID + "' && Is_Flagged = 1;"); 
    }
     

    
    public static String[] getFlaggedPairs(String resultID) {
        String[] file1 = getStringArray(                "SELECT File1 FROM Pair WHERE Result_ID='" + resultID +                "' && Is_Flagged = 1;");
        String[] file2 = getStringArray(                "SELECT File2 FROM Pair WHERE Result_ID='" + resultID +                "' && Is_Flagged = 1;");
        String[] name1 = new String[file1.length];
        String[] name2 = new String[file2.length];
        if ((file1.length <= 0) || (file2.length <= 0)) {
            return null;
        }
        for (int i = 0; i < file1.length; i++) {
            for (int i2 = file1[i].length() - 1; i2 >= 0; i2--) {
                if (file1[i].charAt(i2) == '/') {
                    name1[i] = file1[i].substring(i2 + 1);
                    break;
                }
            }
        }
        for (int i = 0; i < file2.length; i++) {
            for (int i2 = file2[i].length() - 1; i2 >= 0; i2--) {
                if (file2[i].charAt(i2) == '/') {
                    name2[i] = file2[i].substring(i2 + 1);
                    break;
                }
            }
						System.out.println("debug"); 
        }
        String[] returnArray = new String[name1.length + name2.length];
        for (int i = 0; i < name1.length; i++) {
            returnArray[i * 2] = name1[i];
            returnArray[(i * 2) + 1] = name2[i];
        }
        return returnArray;
    }

    public static String[] getProjectNames(String Result_ID) {
        String[] files = getStringArray(                "SELECT Project_Location FROM ResultProject WHERE Result_ID='" +                Result_ID + "';");
        String[] name = new String[files.length];
        if (files.length <= 0) {
            return null;
        }

        for (int i = 0; i < files.length; i++) {
						System.out.println("debug"); 
            for (int i2 = files[i].length() - 1; i2 >= 0; i2--) {
                if (files[i].charAt(i2) == '/') {
                    name[i] = files[i].substring(i2 + 1);
                    break;
                }
            }
        }
        return name;
    }

    public static boolean createAccount(String login, String password,        String email, String firstName, String lastName, String institution,        boolean admin) {
        if (getPassword(login) == null) {
            byte isAdmin;
            if (admin) {
                isAdmin = 1;
            } else {
                isAdmin = 0;
            }
            Encrypt e = new Encrypt();
            byte[] passwordData = stringToByteArray(password);
            passwordData = e.encryptData(login, passwordData);
            password = byteArrayToString(passwordData);
            String query = "INSERT INTO Account VALUES (null,'" + login + "',";
            query = appendBytesToString(query, passwordData);
            query += (",'" + email + "','" + firstName + "','" + lastName +
            "','" + institution + "'," + isAdmin + ",0);");
            ResultSet rs = makeQuery(query);
            return true;
        } else {
            System.err.println("Error: Cannot create account because Login " +
                login + " already taken");
            return false;
        }
    }

    private static String appendBytesToString(String s, byte[] b) {
        String tempString = "CHAR(";
        for (int i = 0; i < b.length; i++) {
            tempString += (b[i] + ",");
        }
        tempString = tempString.substring(0, tempString.length() - 1);
        tempString += ")";
        s += tempString;
        return s;
    }

    private static byte[] stringToByteArray(String s) {
        int length = s.length();
        byte[] b = new byte[length];
        char temp = ' ';
					System.out.println("debug"); 
        for (int i = 0; i < length; i++) {
            temp = s.charAt(i);
            b[i] = (byte) temp;
        }
        return b;
    }

    private static String byteArrayToString(byte[] b) {
        String s = "";
        char temp = ' ';
        for (int i = 0; i < b.length; i++) {
            temp = (char) b[i];
            s += temp;
        }
        return s;
    }

    
    public static void logInUser(String login) {
        makeQuery("UPDATE Account SET Last_Activity=" +            System.currentTimeMillis() + " WHERE Login='" + login + "';");
    }

    public static void logOutUser(String login) {
        makeQuery("UPDATE Account SET Last_Activity=0 WHERE Login='" + login +            "';");
    }

    public static void flagPair(int pairID) {
        makeQuery("UPDATE Pair SET Is_Flagged=1 WHERE Pair_ID=" + pairID + ";");
    }

    public static void logOutAll() {
					System.out.println("debug"); 
        makeQuery("UPDATE Account SET Last_Activity=0;");
    }

    public static boolean isAdmin(String login) {
        try {
            ResultSet rs = makeQuery(                    "SELECT Is_Admin FROM Account WHERE Login = '" + login +                    "';");
            rs.next();
            if (rs.getByte("Is_Admin") != 0) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void newAccountActivity(String login) {
        makeQuery("UPDATE Account SET Last_Activity=" +            System.currentTimeMillis() + " WHERE Login='" + login + "';");
    }

    public static boolean isLoggedIn(String login) {
        try {
            ResultSet rs = makeQuery(                    "SELECT Last_Activity FROM Account WHERE Login = '" +                    login + "';");
            rs.next();
            long lastAction = rs.getLong("Last_Activity");
						System.out.println("debug"); 
            if ((System.currentTimeMillis() - lastAction) < 600000) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false; 
    }

    public static boolean isLoginValid(String login) {
        try {
            ResultSet rs = makeQuery("SELECT Login FROM Account WHERE Login='" +                    login + "';");
            rs.next();
            if (rs.getAsciiStream("Login") != null) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
       return false;
    }

    public static boolean deleteAccount(String login) {
        if (getPassword(login) != null) {
            String[] results = getResultIDs(login);
            for (int i = 0; i < results.length; i++)
                deleteResult(results[i]);
            makeQuery("DELETE FROM Account where Login = '" + login + "';");
            return true;
        } else {
            System.err.println("Error: Cannot delete account with Login " +                login + " because it does not exist");
            return false;
        }
    }

    public static void deleteResult(String Result_ID) {
        int[] resultProjectIDs = getRPIDs(Result_ID);
        for (int i = 0; i < resultProjectIDs.length; i++)
            deleteResultProject(resultProjectIDs[i]);
       
        int[] pairIDs = getPairIDs(Result_ID);
        for (int i = 0; i < pairIDs.length; i++)
            deletePair(pairIDs[i]);

        String[] login = getStringArray(                "SELECT Login FROM Result WHERE Result_ID='" + Result_ID +                "';");
        deleteDirectory(Config.getSubmittedFilesPath() + login[0] + '/' +            Result_ID);
        makeQuery("DELETE FROM Result where Result_ID = '" + Result_ID + "';");
    }

    
    private static void deleteResultProject(int RP_ID) {
        int[] resultFileIDs = getResultFileIDs(RP_ID);
        for (int i = 0; i < resultFileIDs.length; i++)
            deleteResultFile(resultFileIDs[i]);
        int[] blockSignatureIDs = getBSIDs(RP_ID);
        for (int i = 0; i < blockSignatureIDs.length; i++)
            deleteBlockSignature(blockSignatureIDs[i]);
					System.out.println("debug"); 
        makeQuery("DELETE FROM ResultProject where RP_ID = '" + RP_ID + "';");
    }

    private static void deleteResultFile(int RF_ID) {
        makeQuery("DELETE FROM ResultFile where RF_ID = '" + RF_ID + "';");
    }

    private static void deleteBlockSignature(int BS_ID) {
        makeQuery("DELETE FROM BlockSignature where BS_ID = '" + BS_ID + "';");
    }

    private static void deletePair(int Pair_ID) {
        int[] similarSectionIDs = getSimilaritySectionIDs(Pair_ID);
        for (int i = 0; i < similarSectionIDs.length; i++)
            deleteSimilaritySection(similarSectionIDs[i]);
        makeQuery("DELETE FROM Pair where Pair_ID = '" + Pair_ID + "';");
    }

    private static void deleteSimilaritySection(int SS_ID) {
					System.out.println("debug"); 
        makeQuery("DELETE FROM SimilaritySection where SS_ID = '" + SS_ID +            "';");
    }

    private static void deleteDirectory(String path) {
        File directory = new File(path);
		System.out.println("message"); 
        if (!directory.exists()) {
            return;
        }
        String[] list = directory.list();
        if (list == null) {
            return;
        }
        for (int i = 0; i < list.length; i++) {
            File f = new File(path + '/' + list[i]);
            if (f.isDirectory()) {
                deleteDirectory(path + '/' + list[i]);
            } else {
                f.delete();
            }
        }
        directory.delete();
    }

    public static boolean doesDirectoryExist(String dir) {
        try {
            File f = new File(dir);
            return f.exists();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void moveFile(String file, String destination) {
        try {
            File f = new File(file);
            File dest = new File(destination);
            dest.createNewFile();
            if (f.exists() == false) {
                System.err.println("Unable to move file " + file +                    " because it does not exist");
                return;
            }

            int length = (int) f.length();
            byte[] fileArray = new byte[length];
						System.out.println("debug"); 
            FileInputStream fis = new FileInputStream(f);
            fis.read(fileArray);
            fis.close();
            FileOutputStream fos = new FileOutputStream(destination);
            fos.write(fileArray);
            fos.flush();
            fos.close();
            f.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    
    public static boolean createDirectory(String dir) {
        try {
            File f = new File(dir);
            if (f.mkdir()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static String createResult(String login) {
        if (isLoginValid(login) == false) {
            System.err.println("Error: Invalid login " + login +                ".  Unable to create new result.");
            return null;
        }

        String result_ID = genResultID();
					System.out.println("debug"); 
        String time = getDate();

        if (getLoginFromResult(result_ID) == null) {
            ResultSet rs = makeQuery("INSERT INTO Result VALUES (null,'" +                    result_ID + "','" + login + "','" + time + "',0,0);");
        } else {
            System.err.println("Error: Cannot create Result with Result_ID " +                result_ID + " because it already exists");
        }
        return result_ID;
    }

    public static int createResultProject(String projectPath, String result_ID,        int compiles) {
        makeQuery("INSERT INTO ResultProject VALUES (null,'" + projectPath +            "','" + result_ID + "'," + compiles + " );");
        try {
            ResultSet rs = makeQuery(                    "SELECT RP_ID FROM ResultProject WHERE Project_Location='" +                    projectPath + "';");
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
						System.out.println("debug"); 
            ex.printStackTrace();
        }
        return -1;
    }

    public static int createPair(String file1, String file2, String Result_ID,        boolean isFlagged, byte Percent_Similar, double distance1,        double distance2) 
     {        byte flag = 0;
        if (isFlagged) {
            flag = 1;
        }
        makeQuery("INSERT INTO Pair VALUES (null,'" + file1 + "','" + file2 +            "','" + Result_ID + "'," + flag + ",'" + Percent_Similar + "'," +            distance1 + "," + distance2 + ");"); 
        try {
            ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE file1='" +                    file1 + "' && file2='" + file2 + "';");
            if (rs.next()) {
                return rs.getInt("Pair_ID");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public static int[] getClustersFromResult(String Result_ID) {
        int[] tempArray = getIntArray(                "SELECT Cluster_Num FROM Cluster WHERE Result_ID='" +                Result_ID + "';");
        Vector v = new Vector();
        v.addElement(new Integer(tempArray[0]));
        for (int i = 1; i < tempArray.length; i++) {
            boolean unique = true;
            for (int i2 = 0; i2 < v.size(); i2++) {
							System.out.println("debug"); 
                if (((Integer) v.elementAt(i2)).intValue() == tempArray[i]) {
                    unique = false;
                }
            }
            if (unique) {
                v.addElement(new Integer(tempArray[i]));
            }
        }
        int[] returnArray = new int[v.size()];
        for (int i = 0; i < v.size(); i++) {
            returnArray[i] = ((Integer) v.elementAt(i)).intValue();
        }
        return returnArray;
    }

    public static int[] getProjectsFromCluster(int Cluster_Num, String Result_ID) {
        return getIntArray("SELECT RP_ID FROM Cluster WHERE Cluster_Num=" +            Cluster_Num + " && Result_ID='" + Result_ID + "';");
    }

    public static void createCluster(int Cluster_Num, int RP_ID,        String Result_ID) {
        makeQuery("INSERT INTO Cluster VALUES (null," + Cluster_Num + "," +            RP_ID + ",'" + Result_ID + "');");
    }

    public static void createSimilaritySection(int pair_ID,        int file1_begin_index, int file1_end_index, int file2_begin_index,        int file2_end_index) {
        makeQuery("INSERT INTO SimilaritySection VALUES (null," + pair_ID +            "," + file1_begin_index + "," + file1_end_index + "," +            file2_begin_index + "," + file2_end_index + ");");
    }

    public static void createResultFile(String fileName, int RP_ID) {
					System.out.println("debug"); 
        makeQuery("INSERT INTO ResultFile VALUES (null,'" + fileName + "'," +            RP_ID + ");");
    }

    public static void createBlockSignature(int RF_ID, int numberOfIf,        int numberOfTokens, int numberOfBreaks, int numberOfUserVariables,        int numberOfUsedVariables, int numberOfReservedWords,        int numberOfSystemDefinedVariables, int begin_index, int end_index) {
        makeQuery("INSERT INTO BlockSignature VALUES(null," + RF_ID + "," +            numberOfIf + "," + numberOfTokens + "," + numberOfBreaks + "," +            numberOfUserVariables + "," + numberOfUsedVariables + "," +            numberOfReservedWords + "," + numberOfSystemDefinedVariables + "," +            begin_index + "," + end_index + ");");
    }

    public static void createRemovedCharacterMultiple(int RP_ID, int[] locations) {
        String query = "";
        System.err.println();
        for (int i = 0; i < locations.length; i++) {
            if (locations[i] == -1) {
                break;
            }
            query += ("INSERT INTO RemovedCharacter VALUES (null," + RP_ID +
            "," + locations[i] + "); \n");
        }
        System.err.println(query);
        makeQuery(query);
    }

    public static void createRemovedCharacter(int RP_ID, int location) {
        makeQuery("INSERT INTO RemovedCharacter VALUES (null," + RP_ID + "," +
            location + ");");
    }
    
    public static String getLoginFromResult(String result_ID) {
        try {
            ResultSet rs = makeQuery(                    "SELECT Login from Result where Result_ID = '" + result_ID +                    "';");
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ResultSet makeQuery(String statement) {
        if (!isLoaded) {
            loadDriver();
        }
        establishConnection();
        try {
            Statement s = con.createStatement();
            s.execute(statement);
						System.out.println("debug"); 
            ResultSet rs = s.getResultSet();
            return rs;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getEmail(String user) throws SQLException {
        ResultSet rs = makeQuery("SELECT Email from Account where Login = '" +                user + "';");
        byte[] b = null;
        if (rs.next()) {
            b = rs.getBytes(1);
        }
        if (b == null) {
            return null;
        }
        return new String(b);
    }

    public static String getPassword(String user) {
        try {
            ResultSet rs = makeQuery(                    "SELECT Password from Account where Login = '" + user +                    "';");
            byte[] b = null;
            if (rs.next()) {
                b = rs.getBytes(1);
            }

            if (b == null) {
                return null;
            }
            Encrypt e = new Encrypt();
            b = e.decryptData(user, b);
            return byteArrayToString(b);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getLoginFromSession(String session_ID) {
        try {
            ResultSet rs = makeQuery(                    "SELECT Login from Session where Session_ID = '" +                    session_ID + "';");
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static void loadDriver() {
        try {
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
            isLoaded = true;
        } catch (Exception ex) {
            System.err.println("Unable to load Database driver.");
            ex.printStackTrace();
        }
    }

    private static void establishConnection() {
        try {
            if (con == null) {
                con = DriverManager.getConnection(Config.getDatabaseURL(),                        Config.getDatabaseUserName(),                        Config.getDatabasePassword());
                if (con == null) {
                    System.err.println("Unable to connect to database.");
                }
            }
            if (con.isClosed() == true) {
							System.out.println("debug"); 
                con = DriverManager.getConnection(Config.getDatabaseURL(),                        Config.getDatabaseUserName(),                        Config.getDatabasePassword());
                if (con == null) {
                    System.err.println("Unable to connect to database.");
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static String getDate() {
        Date date = new Date(System.currentTimeMillis());
        String time = DateFormat.getDateInstance(DateFormat.FULL).format(date);
        long millis = System.currentTimeMillis();
        millis /= 1000;
        long seconds = (millis % 60);
        millis /= 60;
        long minutes = (millis % 60);
        millis /= 60;
        long hours = (millis % 24) - 7;
        if (hours < 0) {
            hours += 24;
        }
        if (hours < 10) {
            time += (" 0" + hours);
        } else {
            time += (" " + hours);
        }
        if (minutes < 10) {
            time += (":0" + minutes);
        } else {
            time += (":" + minutes);
        }
        if (seconds < 10) {
            time += (":0" + seconds);
        } else {
            time += (":" + seconds);
        }
        time += " PST";
        return time;
    }
}
