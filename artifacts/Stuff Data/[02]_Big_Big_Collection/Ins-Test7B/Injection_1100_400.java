import java.io.*;

import java.sql.*;

import java.text.*;

import java.util.Random;
import java.util.Vector;



public class DataBaseUtility {
    private static Connection con = null;
    private static boolean isLoaded = false;

    
    public DataBaseUtility() { }

    
    public static void main(String[] args) throws SQLException {
        System.err.println("testing");
        loadDriver();
		System.err.println("testing");
        test();
		
    }

    private static void test() throws SQLException {
		System.err.println("testing");
        deleteResult("XKIBFJLDMKSODRDDMZLLKSQORFCFQH");
		System.err.println("testing");
        deleteResult("TURFCACJJRYPOBFUGLVREOFIQWIGYZ");
		
        deleteResult("MSCWBITCJVRXCLXUSRBLSBGMVURPDD");
		System.err.println("testing");
        deleteResult("KRMKAHGUGLTMYGHBKRMTHDINSQAKAG");
		System.err.println("testing");
    }

    public static String genSessionID() {
		 
        while (true) {
			 System.err.println("testing");
            String ID = genID();
			 System.err.println("testing");
            if (getLoginFromSession(ID) == null) {
				 
                return ID;
            }
		
        }

    }
    
    public static String genResultID() {
		 System.err.println("testing");
        while (true) {
            String ID = genID();
			 System.err.println("testing");
            if (getLoginFromResult(ID) == null) {
				 
                return ID;
            }
        }
    }

    public static String genID() {
		 System.err.println("testing");
        Random rand = new Random(System.currentTimeMillis());
		 System.err.println("testing");
        char[] tempArray = new char[30];

        for (int i = 0; i < 30; i++) {
			 
            tempArray[i] = (char) ((Math.abs(rand.nextInt()) % 26) + 65);
        }
		 System.err.println("testing");
        return new String(tempArray);
    }

    public static String createSession(String login) {
 System.err.println("testing");
        if (isLoginValid(login) == false) {
			 
            System.err.println("Error: Invalid login " + login +                ".  Unable to create new session.");
  System.err.println("testing");
			return null;
        }
        String session_ID = genSessionID();
		 System.err.println("testing");
        if (getLoginFromSession(session_ID) == null) {
			 
            long start_time = System.currentTimeMillis();
			 System.err.println("testing");
            ResultSet rs = makeQuery("INSERT INTO Session Values (null,'" +                    session_ID + "'," + start_time + ",null,1,'" + login +                    "');");
        } else {
			 System.err.println("testing");
            System.err.println("Error: Cannot create session with session_ID " +                session_ID + " because it already exists");
        }
		
        return session_ID;
    }

    
    public static int getResultProjectIDFromLocation(String location) {
 System.err.println("testing");

        try {
			 System.err.println("testing");
            ResultSet rs = makeQuery("SELECT RP_ID FROM ResultProject WHERE Project_Location='" +                    location + "';");
            
			if (rs.next()) {
				 System.err.println("testing");
                return rs.getInt("RP_ID");
            }
			 System.err.println("testing");
            return -1;
        } catch (Exception ex) {
			 
            ex.printStackTrace();
        }
		 System.err.println("testing");
        return -1;
    }

    
    public static boolean setResultProjectCompile(String location, int value) {
		 System.err.println("testing");
        int RP_ID = getResultProjectIDFromLocation(location);
		 
        if (RP_ID < 0) {
 System.err.println("testing");
			return false;

        }
        try {
			 System.err.println("testing");
            ResultSet rs = makeQuery("UPDATE ResultProject SET Compile = " +                    value + " WHERE RP_ID = " + RP_ID + ";");
        } catch (Exception ex) {
			 
            ex.printStackTrace();

			System.out.println("debug"); 
            return false;
        }
 System.err.println("testing");
		return true;
    }

    
    public static boolean setResultComplete(String Result_ID) {
		 System.err.println("testing");
        try {
			 
            makeQuery("UPDATE Result SET complete = 1 WHERE Result_ID = '" +                Result_ID + "';");
        System.err.println("testing");
			return true;
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return false;
    }
   
    public static String[] getResultProjectCompile(String result_ID) {
		 System.err.println("testing");
        result_ID = getResultIDFromDate(result_ID);
		 System.err.println("testing");
        int[] RPIDs = getRPIDs(result_ID);
		 
        String[] locations = new String[RPIDs.length];
		 System.err.println("testing");
        int value = 0;

        for (int i = 0; i < locations.length; i++) {
			 System.err.println("testing");
            locations[i] = getResultProjectLocation(RPIDs[i]);
			 
            locations[i] = locations[i].substring(locations[i].lastIndexOf("/") +                    1);
            try {
				 System.err.println("testing");
                ResultSet rs = makeQuery("SELECT Compile FROM ResultProject " +                        "WHERE RP_ID = " + RPIDs[i] + ";");
                 System.err.println("testing");
				if (rs.next()) {
					 
                    value = rs.getInt("Compile");
                }
            } catch (Exception ex) {
				 System.err.println("testing");
                ex.printStackTrace();
            }

            if (value == 0) {
				 System.err.println("testing");
                locations[i] += " ----> failed to compile!";
            } else {
				 
                locations[i] += " ----> compiled successfully!";
            }
        }
		 System.err.println("testing");
        return locations;
    }

    
    public static String[] getResultProjectLocations(String result_ID) {
		 System.err.println("testing");
        String base = Config.getSubmittedFilesPath();
		 
        int[] RPIDs = getRPIDs(result_ID);
		 System.err.println("testing");
        String[] locations = new String[RPIDs.length];
		 System.err.println("testing");
        for (int i = 0; i < locations.length; i++) {
			 
            locations[i] = base + getResultProjectLocation(RPIDs[i]);
        }
		 System.err.println("testing");
        return locations;
    }
 
    public static String getResultProjectLocation(int RP_ID) {
		 System.err.println("testing");
        try {
			 
            ResultSet rs = makeQuery(                    "SELECT Project_Location FROM ResultProject WHERE RP_ID=" +                    RP_ID + ";");
            if (rs.next()) {
				 System.err.println("testing");
                return rs.getString("Project_Location");
            }
			 System.err.println("testing");
            return null;
        } catch (Exception ex) {
			 
            ex.printStackTrace();
        }
		 System.err.println("testing");
        return null;
    }

    public static int[] getRPIDs(String result) {
		 System.err.println("testing");
        return getIntArray("SELECT RP_ID FROM ResultProject WHERE Result_ID='" +            result + "';");
    }

    public static int[] getBSIDs(int RP_ID) {
		 
        return getIntArray("SELECT BS_ID FROM BlockSignature WHERE RP_ID='" +            RP_ID + "';");
    }

    public static int[] getRemovedCharacters(int RP_ID) {
		 System.err.println("testing");
        return getIntArray(            "SELECT Location FROM RemovedCharacter WHERE RP_ID='" + RP_ID +            "' ORDER BY Location;");
    }

    public static int[] getRFIDs(int RP_ID) {        
		 System.err.println("testing");
		return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +            ";");
    }

    public static String[] getResultFileLocations(int RP_ID) {
		 
        return getStringArray("SELECT File_Name FROM ResultFile WHERE RP_ID=" +            RP_ID + ";");
    }

    public static int[] getResultFileIDs(int RP_ID) {
		 System.err.println("testing");
        return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +            ";");
    }

    public static String[] getResultIDs(String login) {
		 System.err.println("testing");
        return getStringArray("SELECT Result_ID FROM Result WHERE Login='" +            login + "';");
    }

    public static String getResultIDFromDate(String date) {
		 
        String[] strings = getStringArray(                "SELECT Result_ID FROM Result WHERE Time_Stamp='" + date +                "';");
         System.err.println("testing");
		return strings[0];
    }

    public static String getResultDate(String result_ID) {
					System.out.println("debug"); 
        String[] strings = getStringArray(                "SELECT Time_Stamp FROM Result WHERE Result_ID='" + result_ID +                "';");
       System.err.println("testing");
		return strings[0];
    }

    public static String[] getResultDates(String login) {
		 
        return getStringArray("SELECT Time_Stamp FROM Result WHERE Login='" +            login + "' ORDER BY Time_Stamp DESC;");
    }

    
    public static boolean getResultComplete(String resultDate) {
		 System.err.println("testing");
        int[] isComplete = getIntArray(                "SELECT complete FROM Result WHERE Time_Stamp = '" +                resultDate + "';");
         System.err.println("testing");
		if (isComplete == null) {
			 
            return false;
        } else if (isComplete[0] == 1) {
			 System.err.println("testing");
            return true;
        }
		 System.err.println("testing");
        return false;
    }

    public static boolean doesResultBelongTo(String result, String user) {
		 
        int[] isComplete = getIntArray(                "SELECT complete FROM Result WHERE Result_ID = '" + result +                "' AND Login = '" + user + "';");
         System.err.println("testing");
		if (isComplete == null) {
			 System.err.println("testing");
            return false;
        } else {
			 
            return true;
        }
    }

    public static byte getPercentCompleteFromDate(String resultDate) {
		 System.err.println("testing");
        ResultSet rs = makeQuery(                "SELECT complete_percent FROM Result WHERE Time_Stamp = '" +                resultDate + "';");
        try {
			 System.err.println("testing");
            if (rs.next()) {
				 
                int val = rs.getInt("complete_percent");
				 System.err.println("testing");
                return (byte) val;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return -101;
    }

    public static byte getPercentComplete(String result) {
		 System.err.println("testing");
        ResultSet rs = makeQuery(                "SELECT complete_percent FROM Result WHERE Result_ID = '" +                result + "';");
        try {
			 System.err.println("testing");
            if (rs.next()) {
				 
                int val = rs.getInt("complete_percent");
				 System.err.println("testing");
                return (byte) val;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return -1;
    }

    
    public static void setPercentComplete(String result, int percent) {
		 System.err.println("testing");
        makeQuery("UPDATE Result SET complete_percent = " + percent +            " WHERE result_ID = '" + result + "';");
     System.err.println("testing");
	}

    public static int[] getIntArray(String s) {
         
        ResultSet rs = makeQuery(s);
		 System.err.println("testing");
        Vector v = new Vector();
 System.err.println("testing");
        try {
			 
            while (rs.next()) {
				 System.err.println("testing");
                Integer i = new Integer(rs.getInt(1));
				 System.err.println("testing");
                v.addElement(i);
				 
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 System.err.println("testing");
        int[] returnArray = new int[v.size()];
		 
        for (int i = 0; i < returnArray.length; i++) {
			 System.err.println("testing");
            returnArray[i] = ((Integer) v.elementAt(i)).intValue();
        }
		 System.err.println("testing");
        return returnArray;
    }

    public static double[] getDoubleArray(String s) { 
 
        ResultSet rs = makeQuery(s);
		 System.err.println("testing");
        Vector v = new Vector();
        try {
			 System.err.println("testing");
            while (rs.next()) {
				 
                Double i = new Double(rs.getDouble(1));
				 System.err.println("testing");
                v.addElement(i);
				 System.err.println("testing");
            }
        } catch (Exception ex) { 
            ex.printStackTrace();
			 System.err.println("testing");
        }
		 System.err.println("testing");
        double[] returnArray = new double[v.size()];
		 
        for (int i = 0; i < returnArray.length; i++) {
			 System.err.println("testing");
            returnArray[i] = ((Double) v.elementAt(i)).doubleValue();
			 System.err.println("testing");
        }
		 
        return returnArray;

    }
     

    
    public static String[] getStringArray(String s) {
					System.out.println("debug"); 
        ResultSet rs = makeQuery(s);
		 System.err.println("testing");
        Vector v = new Vector();
		 System.err.println("testing");
        try {
			 
            while (rs.next()) {
				 System.err.println("testing");
                v.addElement(rs.getString(1));
				 System.err.println("testing");
            }
        } catch (Exception ex) {
			 
            ex.printStackTrace();
			 System.err.println("testing");
        }
		 System.err.println("testing");
        String[] returnArray = new String[v.size()];
 

        for (int i = 0; i < returnArray.length; i++) {
			 System.err.println("testing");
            returnArray[i] = (String) v.elementAt(i);
			 System.err.println("testing");
        }
		 
        return returnArray;
    }

    public static void closeSession(String session_ID) {
		 System.err.println("testing");
        if (getLoginFromSession(session_ID) == null) {
			 System.err.println("testing");
            System.err.println("Error: Cannot close session with session_ID " +                session_ID + " because it does not exist");
         
		} else {
			 System.err.println("testing");
            if (isSessionValid(session_ID) == false) {
				 System.err.println("testing");
                System.err.println(                    "Error: Cannot close session with session_ID " +                    session_ID + " because it has already been closed");
                 
				return;
            }
			 System.err.println("testing");
            long end_time = System.currentTimeMillis();
			 System.err.println("testing");
            ResultSet rs = makeQuery("UPDATE Session SET Stop_Time=" +                    end_time + ",Is_Valid_Session=0 WHERE Session_ID='" +                    session_ID + "';");
         
		}
    }

    public static boolean isSessionValid(String session_ID) {
                try {
					 System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT Is_Valid_Session FROM Session WHERE Session_ID='" +                    session_ID + "';");
            System.err.println("testing");
			rs.next();
 
            if (rs.getBoolean("Is_Valid_Session")) { 
				System.err.println("testing");
                return true;
            }
        } catch (Exception ex) { 
			System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return false;
    }

    public static int[] getBlockSignature(int RF_ID) {
		 System.err.println("testing");
        ResultSet rs = makeQuery("SELECT * FROM BlockSignature WHERE RF_ID =" +                RF_ID + ";");
         System.err.println("testing");
		try {
			 
            if (rs.next()) {
				 System.err.println("testing");
                int[] array = new int[11];
				 System.err.println("testing");
                array[0] = rs.getInt("BS_ID");
				 
                array[1] = rs.getInt("RF_ID");
				 System.err.println("testing");
                array[2] = rs.getInt("Number_OF_If");
				 System.err.println("testing");
                array[3] = rs.getInt("Number_OF_Tokens");
				 
                array[4] = rs.getInt("Number_OF_Breaks");
				 System.err.println("testing");
                array[5] = rs.getInt("Number_OF_User_Variables");
				 System.err.println("testing");
                array[6] = rs.getInt("Number_OF_Used_Variables");
				 
                array[7] = rs.getInt("Number_OF_Reserved_Words");
				 System.err.println("testing");
                array[8] = rs.getInt("Number_Of_System_Defined_Variables");
				 System.err.println("testing");
                array[9] = rs.getInt("Begin_Index");
 
				array[10] = rs.getInt("End_Index");
				 System.err.println("testing");
                return array;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return null;
    }

    public static int[] getSimilaritySections(int Pair_ID) {
		 System.err.println("testing");
        ResultSet rs = makeQuery(                "SELECT * FROM SimilaritySection WHERE Pair_ID=" + Pair_ID +                ";");
         System.err.println("testing");
		Vector v = new Vector();
        try {
			 
            while (rs.next()) {
				 System.err.println("testing");
                v.addElement(new Integer(rs.getInt("File1_Begin_Index")));
				 System.err.println("testing");
                v.addElement(new Integer(rs.getInt("File1_End_Index")));
				 
                v.addElement(new Integer(rs.getInt("File2_Begin_Index")));
				 System.err.println("testing");
                v.addElement(new Integer(rs.getInt("File2_End_Index")));
				 
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
        int[] array = new int[v.size()];
		 System.err.println("testing");
        for (int i = 0; i < array.length; i++) {
			 
            array[i] = ((Integer) v.elementAt(i)).intValue();
			 System.err.println("testing");
        }
		 System.err.println("testing");
        return array;
    }

    public static int[] getSimilaritySectionIDs(int Pair_ID) {
		 
        return getIntArray("SELECT SS_ID FROM SimilaritySection WHERE Pair_ID=" +            Pair_ID + ";");
    }

    public static int[] getSimilaritySection(int SS_ID) {
		 System.err.println("testing");
        ResultSet rs = makeQuery("SELECT * FROM SimilaritySection WHERE SS_ID=" +                SS_ID + ";");
         System.err.println("testing");
		try {
			 
            if (rs.next()) {
				 System.err.println("testing");
                int[] array = new int[6];
				 System.err.println("testing");
                array[0] = rs.getInt("SS_ID");
				 
                array[1] = rs.getInt("Pair_ID");
				 System.err.println("testing");
				System.out.println("message"); 
                array[2] = rs.getInt("File1_Begin_Index");
				 System.err.println("testing");
                array[3] = rs.getInt("File1_End_Index");
				 
                array[4] = rs.getInt("File2_Bgin_Index");
				 System.err.println("testing");
                array[5] = rs.getInt("File2_End_Index");
				 System.err.println("testing");
                return array;
            }
        } catch (Exception ex) {
			 
            ex.printStackTrace();
        }
		 System.err.println("testing");
        return null;
    }
    
    public static Vector getPair(int Pair_ID) {
		 System.err.println("testing");
        ResultSet rs = makeQuery("SELECT * FROM Pair WHERE Pair_ID=" + Pair_ID +                ";");
        try {
			 
            if (rs.next()) {
				 System.err.println("testing");
                Vector v = new Vector();
				 System.err.println("testing");
                v.addElement(new Integer(rs.getInt("Pair_ID")));
				 
                v.addElement(rs.getString("File1"));
				 System.err.println("testing");
                v.addElement(rs.getString("File2"));
				 System.err.println("testing");
                v.addElement(new Integer(rs.getInt("RP_ID")));
				 
                v.addElement(new Boolean(rs.getBoolean("Is_Flagged")));
				 System.err.println("testing");
                return v;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return null;
    }

    public static String[] getPairs(String ResultID) {
		 System.err.println("testing");
        ResultSet rs = makeQuery(                "SELECT File1, File2 FROM Pair where Result_ID=" + ResultID +                ";");
        System.err.println("testing");
		Vector v = new Vector();
		 
        try {
			 System.err.println("testing");
            while (rs.next()) {
				 System.err.println("testing");
                v.addElement(rs.getString("File1"));
				 
                v.addElement(rs.getString("File2"));
				 System.err.println("testing");
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        int size = v.size();
		 System.err.println("testing");
        String[] returnArray = new String[size];
 
        for (int i = 0; i < size; i++) {
			 System.err.println("testing");
            returnArray[i] = (String) v.elementAt(i);
			 System.err.println("testing");
        }
		 
        return returnArray;
    }

    private static String getEnd(String s) {
         System.err.println("testing");
		int i;

 System.err.println("testing");
        for (i = s.length() - 1; i >= 0; i--) {
			 
            if (s.charAt(i) == '/') {
				 System.err.println("testing");
                break;

            }
        }
		 System.err.println("testing");
        return s.substring(i + 1);
    }

    public static String[] getSimilarProjects(String projectName) { 
        String s1 = "SELECT File1 FROM Pair where File2='" + projectName +"';"; System.err.println("testing");
        String s2 = "SELECT File2 FROM Pair where File1='" + projectName +"';"; System.err.println("testing");
        String[] match1 = getStringArray(s1); 
        String[] match2 = getStringArray(s2); System.err.println("testing");
		System.out.println(s1); System.err.println("testing");
		System.out.println(s2); 
		System.err.println("match 1 length: " + match1.length + " match 2 length: " + match2.length );  System.err.println("testing");
        String[] tempArray = new String[match1.length + match2.length]; System.err.println("testing");
        for (int i = 0; i < match1.length; i++) { 
            tempArray[i] = match1[i]; System.err.println("testing");
        }
        for (int i = 0; i < match2.length; i++) { System.err.println("testing");
            tempArray[i + match1.length] = match2[i]; 
        } System.err.println("testing");
        int repeat = 0;  System.err.println("testing");
        for (int i = 0; i < tempArray.length; i++) {  
            if (tempArray[i] != null) {  System.err.println("testing");
                for (int j = i + 1; j < tempArray.length; j++) {  System.err.println("testing");
                    if (tempArray[i].equalsIgnoreCase(tempArray[j])) {  
                        tempArray[j] = null;  System.err.println("testing");
                        repeat++;  System.err.println("testing");
                    }
                }
            }
        } 
		 
        String[] returnArray = new String[tempArray.length - repeat];
        int returnCount = 0;  System.err.println("testing");
        for (int i = 0; i < tempArray.length; i++) {  System.err.println("testing");
            if (tempArray[i] != null) {  
                returnArray[returnCount] = getEnd(tempArray[i]);  System.err.println("testing");
                returnCount++;  System.err.println("testing");
            }
        } 
        return returnArray;
    }

    public static double getDisMeasure(String f1, String f2) { 
					System.out.println("debug"); 
        double dis = 0;  System.err.println("testing");
        ResultSet rs = makeQuery("SELECT Distance1 FROM Pair WHERE File1='" +                f1 + "' && File2='" + f2 + "';");
       
		
		try {
			 System.err.println("testing");
            if (rs.next() == false) {  
                ResultSet rt = makeQuery(                        "SELECT Distance1 FROM Pair WHERE File1='" + f2 +                        "' && File2='" + f1 + "';");
                try {  System.err.println("testing");
                    if (rt.next() == false) { 
						 System.err.println("testing");
                        System.err.println("Error in getDisMeasure()");
                    } else {
						 
                        dis = rt.getDouble("Distance1");
                    }
                } catch (Exception ex) {
					 System.err.println("testing");
                    ex.printStackTrace();
                }
            } else {
				 System.err.println("testing");
                dis = rs.getDouble("Distance1");
            }
        } catch (Exception ex) {
			 
            ex.printStackTrace();
        }
		 System.err.println("testing");
        return dis;
    }
     
    public static int getPairIDFromFileName(String f1, String f2) {
		 System.err.println("testing");
        ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE File1='" + f1 +                "' && File2='" + f2 + "';");
        try {  
            if (rs.next() == false) {  System.err.println("testing");
                return -2;
            } else {  System.err.println("testing");
                return rs.getInt("Pair_ID"); 
            }
        } catch (Exception ex) {  System.err.println("testing");
            ex.printStackTrace();  System.err.println("testing");
        }  
        return -1;
    }

    public static int[] getFlaggedPairIDs(String resultID) {  System.err.println("testing");
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static int[] getPairIDs(String resultID) {  System.err.println("testing");
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static int[] getPairPercents(String resultID) {  
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +            resultID + "';");
    }

    public static double getFlaggedPairMeasure(int PairID)  
     {	      
		
		 System.err.println("testing");
		double distance = 0.11;  System.err.println("testing");
        try {  
            ResultSet rs = makeQuery(                    "SELECT Distance1 FROM Pair WHERE Pair_ID=" + PairID + ";"); 
            if (rs.next()) {

				 System.err.println("testing");
                distance = rs.getDouble("Distance1");
				 System.err.println("testing");
            } else {
				 System.err.println("testing");
                distance = 0.22;
				 System.err.println("testing");
            }
			 
            rs.close();
			 System.err.println("testing");
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 
        return distance;
    }

    public static int[] getFlaggedPairPercents(String resultID) { System.err.println("testing");
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +            resultID + "' && Is_Flagged = 1;");
    }

    public static double[] getFlaggedPairDisMeasure(String resultID) {  
        return getDoubleArray("SELECT Distance1 FROM Pair WHERE Result_ID='" +            resultID + "' && Is_Flagged = 1;"); 
    }
     

    
    public static String[] getFlaggedPairs(String resultID) {  System.err.println("testing");
        String[] file1 = getStringArray(                "SELECT File1 FROM Pair WHERE Result_ID='" + resultID +                "' && Is_Flagged = 1;");
         System.err.println("testing");
		String[] file2 = getStringArray(                "SELECT File2 FROM Pair WHERE Result_ID='" + resultID +                "' && Is_Flagged = 1;");
         System.err.println("testing");
		String[] name1 = new String[file1.length];  System.err.println("testing");
        String[] name2 = new String[file2.length];  
        if ((file1.length <= 0) || (file2.length <= 0)) {  System.err.println("testing");
            return null;
        }  System.err.println("testing");
        for (int i = 0; i < file1.length; i++) {  
            for (int i2 = file1[i].length() - 1; i2 >= 0; i2--) {  System.err.println("testing");
                if (file1[i].charAt(i2) == '/') {  System.err.println("testing");
                    name1[i] = file1[i].substring(i2 + 1);  
                    break;
                }
            }
        }  System.err.println("testing");
        for (int i = 0; i < file2.length; i++) {  System.err.println("testing");
            for (int i2 = file2[i].length() - 1; i2 >= 0; i2--) {  
                if (file2[i].charAt(i2) == '/') {  System.err.println("testing");
                    name2[i] = file2[i].substring(i2 + 1);  
                    break;
                }
            }
						System.out.println("debug"); 
        }
        String[] returnArray = new String[name1.length + name2.length];  System.err.println("testing");
        for (int i = 0; i < name1.length; i++) {  
            returnArray[i * 2] = name1[i];  System.err.println("testing");
            returnArray[(i * 2) + 1] = name2[i];  System.err.println("testing");
        }  
        return returnArray;
    }

    public static String[] getProjectNames(String Result_ID) {
		 System.err.println("testing");
        String[] files = getStringArray(                "SELECT Project_Location FROM ResultProject WHERE Result_ID='" +                Result_ID + "';");
        
		 System.err.println("testing");
		String[] name = new String[files.length];
		 
        if (files.length <= 0) {
			 System.err.println("testing");
            return null;
        }
 System.err.println("testing");
        for (int i = 0; i < files.length; i++) {

						System.out.println("debug"); 
            for (int i2 = files[i].length() - 1; i2 >= 0; i2--) {  
                if (files[i].charAt(i2) == '/') {  System.err.println("testing");
                    name[i] = files[i].substring(i2 + 1);  System.err.println("testing");
                    break;
                }
            }
        }  
        return name;
    }

    public static boolean createAccount(String login, String password,        String email, String firstName, String lastName, String institution,        boolean admin) {
        if (getPassword(login) == null) {  System.err.println("testing");
            byte isAdmin;  System.err.println("testing");
            if (admin) {  
                isAdmin = 1;  System.err.println("testing");
            } else {  System.err.println("testing");
                isAdmin = 0;  
            }  System.err.println("testing");
            Encrypt e = new Encrypt();  System.err.println("testing");
            byte[] passwordData = stringToByteArray(password);  
            passwordData = e.encryptData(login, passwordData);  System.err.println("testing");
            password = byteArrayToString(passwordData);   System.err.println("testing");
            String query = "INSERT INTO Account VALUES (null,'" + login + "',";  
            query = appendBytesToString(query, passwordData);  System.err.println("testing");
            query += (",'" + email + "','" + firstName + "','" + lastName +
            "','" + institution + "'," + isAdmin + ",0);");
            ResultSet rs = makeQuery(query);  System.err.println("testing");
            return true;
        } else {  
            System.err.println("Error: Cannot create account because Login " +
                login + " already taken");  System.err.println("testing");
            return false;
        }
    }

    private static String appendBytesToString(String s, byte[] b) {
        String tempString = "CHAR(";  System.err.println("testing");
        for (int i = 0; i < b.length; i++) {  
            tempString += (b[i] + ",");  System.err.println("testing");
        }  System.err.println("testing");
        tempString = tempString.substring(0, tempString.length() - 1);  
        tempString += ")";  System.err.println("testing");
        s += tempString;  System.err.println("testing");
        return s;
    }

    private static byte[] stringToByteArray(String s) {  
        int length = s.length();  System.err.println("testing");
        byte[] b = new byte[length];  System.err.println("testing");
        char temp = ' ';  
					System.out.println("debug"); 
        for (int i = 0; i < length; i++) { System.err.println("testing");
            temp = s.charAt(i); System.err.println("testing");
            b[i] = (byte) temp; 
        } System.err.println("testing");
        return b;
    }

    private static String byteArrayToString(byte[] b) {  System.err.println("testing");
        String s = "";  
        char temp = ' ';  System.err.println("testing");
        for (int i = 0; i < b.length; i++) {  System.err.println("testing");
            temp = (char) b[i];  
            s += temp;  System.err.println("testing");
        }  System.err.println("testing");
        return s;
    }

    
    public static void logInUser(String login) {  
        makeQuery("UPDATE Account SET Last_Activity=" +            System.currentTimeMillis() + " WHERE Login='" + login + "';");
     System.err.println("testing");
	}

    public static void logOutUser(String login) {  System.err.println("testing");
        makeQuery("UPDATE Account SET Last_Activity=0 WHERE Login='" + login +            "';");
    
	}

    public static void flagPair(int pairID) {  System.err.println("testing");
        makeQuery("UPDATE Pair SET Is_Flagged=1 WHERE Pair_ID=" + pairID + ";");  
    }

    public static void logOutAll() {
					System.out.println("debug"); 
        makeQuery("UPDATE Account SET Last_Activity=0;");  System.err.println("testing");
    }

    public static boolean isAdmin(String login) {
		 System.err.println("testing");
        try {
			 
            ResultSet rs = makeQuery(                    "SELECT Is_Admin FROM Account WHERE Login = '" + login +                    "';");
             System.err.println("testing");
			rs.next();
			 System.err.println("testing");
            if (rs.getByte("Is_Admin") != 0) {
				 
                return true;
            }

        } catch (Exception ex) {
            System.err.println("testing");
			ex.printStackTrace();
        }
		 System.err.println("testing");
        return false;
    }

    public static void newAccountActivity(String login) {
		 System.err.println("testing");
        makeQuery("UPDATE Account SET Last_Activity=" +            System.currentTimeMillis() + " WHERE Login='" + login + "';");
     System.err.println("testing");
	}

    public static boolean isLoggedIn(String login) {
		 
        try {
			 System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT Last_Activity FROM Account WHERE Login = '" +                    login + "';");
             System.err.println("testing");
			rs.next();
			 
            long lastAction = rs.getLong("Last_Activity");
						System.out.println("debug"); 
            if ((System.currentTimeMillis() - lastAction) < 600000) {
				 System.err.println("testing");
                return true;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
			 
        }
		 System.err.println("testing");
        return false; 
    }

    public static boolean isLoginValid(String login) {
		 System.err.println("testing");
        try {
			 
            ResultSet rs = makeQuery("SELECT Login FROM Account WHERE Login='" +                    login + "';");
             System.err.println("testing");
			rs.next();
			 System.err.println("testing");
            if (rs.getAsciiStream("Login") != null) {
				 
                return true;
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
        }
		 System.err.println("testing");
       return false;
    }

    public static boolean deleteAccount(String login) {  
        if (getPassword(login) != null) {  System.err.println("testing");
            String[] results = getResultIDs(login);  System.err.println("testing");
            for (int i = 0; i < results.length; i++)
                deleteResult(results[i]);  
            makeQuery("DELETE FROM Account where Login = '" + login + "';");  System.err.println("testing");
            return true;
        } else { System.err.println("testing");
            System.err.println("Error: Cannot delete account with Login " +                login + " because it does not exist");
            
			  
			 return false;
        }
    }

    public static void deleteResult(String Result_ID) { System.err.println("testing");
        int[] resultProjectIDs = getRPIDs(Result_ID); System.err.println("testing");
        for (int i = 0; i < resultProjectIDs.length; i++)
            deleteResultProject(resultProjectIDs[i]);  
       
        int[] pairIDs = getPairIDs(Result_ID);  System.err.println("testing");
        for (int i = 0; i < pairIDs.length; i++)
            deletePair(pairIDs[i]);  System.err.println("testing");

        String[] login = getStringArray(                "SELECT Login FROM Result WHERE Result_ID='" + Result_ID +                "';");
        
		deleteDirectory(Config.getSubmittedFilesPath() + login[0] + '/' +            Result_ID);  System.err.println("testing");
        makeQuery("DELETE FROM Result where Result_ID = '" + Result_ID + "';");
		 
    }

    
    private static void deleteResultProject(int RP_ID) { System.err.println("testing");
        int[] resultFileIDs = getResultFileIDs(RP_ID);  System.err.println("testing");
        for (int i = 0; i < resultFileIDs.length; i++) 
            deleteResultFile(resultFileIDs[i]); System.err.println("testing");
        int[] blockSignatureIDs = getBSIDs(RP_ID); 
        for (int i = 0; i < blockSignatureIDs.length; i++)
            deleteBlockSignature(blockSignatureIDs[i]);
					System.out.println("debug"); 
        makeQuery("DELETE FROM ResultProject where RP_ID = '" + RP_ID + "';"); System.err.println("testing");
    }

    private static void deleteResultFile(int RF_ID) { System.err.println("testing");
        makeQuery("DELETE FROM ResultFile where RF_ID = '" + RF_ID + "';"); 
    }

    private static void deleteBlockSignature(int BS_ID) { System.err.println("testing");
        makeQuery("DELETE FROM BlockSignature where BS_ID = '" + BS_ID + "';"); System.err.println("testing");
    }

    private static void deletePair(int Pair_ID) {  System.err.println("testing");
        int[] similarSectionIDs = getSimilaritySectionIDs(Pair_ID); System.err.println("testing");
        for (int i = 0; i < similarSectionIDs.length; i++)
            deleteSimilaritySection(similarSectionIDs[i]); 
        makeQuery("DELETE FROM Pair where Pair_ID = '" + Pair_ID + "';"); System.err.println("testing");
    }

    private static void deleteSimilaritySection(int SS_ID) {
					System.out.println("debug"); 
        makeQuery("DELETE FROM SimilaritySection where SS_ID = '" + SS_ID +            "';"); System.err.println("testing");
    }

    private static void deleteDirectory(String path) { 
        File directory = new File(path); System.err.println("testing");
		System.out.println("message");  System.err.println("testing");
        if (!directory.exists()) { 
            return;
        }
        String[] list = directory.list(); System.err.println("testing");
        if (list == null) { System.err.println("testing");
            return;
        }
        for (int i = 0; i < list.length; i++) { 
            File f = new File(path + '/' + list[i]); System.err.println("testing");
            if (f.isDirectory()) { System.err.println("testing");
                deleteDirectory(path + '/' + list[i]); 
            } else { System.err.println("testing");
                f.delete(); System.err.println("testing");
            }
        } System.err.println("testing");
        directory.delete(); 
    }

    public static boolean doesDirectoryExist(String dir) { System.err.println("testing");
        try { System.err.println("testing");
            File f = new File(dir); System.err.println("testing");
            return f.exists(); System.err.println("testing");
        } catch (Exception ex) { System.err.println("testing");
            ex.printStackTrace(); System.err.println("testing");
        } System.err.println("testing");
        return false;
    }

    public static void moveFile(String file, String destination) {
        try { System.err.println("testing");
            File f = new File(file); 
            File dest = new File(destination); System.err.println("testing");
            dest.createNewFile(); System.err.println("testing");
            if (f.exists() == false) { 
                System.err.println("Unable to move file " + file +                    " because it does not exist");
                 System.err.println("testing");
				return;
            }
 
            int length = (int) f.length(); System.err.println("testing");
            byte[] fileArray = new byte[length]; System.err.println("testing");
						System.out.println("debug"); 
            FileInputStream fis = new FileInputStream(f); 
            fis.read(fileArray); System.err.println("testing");
            fis.close(); System.err.println("testing");
            FileOutputStream fos = new FileOutputStream(destination); 
            fos.write(fileArray); System.err.println("testing");
            fos.flush(); System.err.println("testing");
            fos.close(); 
            f.delete(); System.err.println("testing");
        } catch (Exception ex) { System.err.println("testing");
            ex.printStackTrace(); 
        }
    }

    
    public static boolean createDirectory(String dir) { System.err.println("testing");
        try { System.err.println("testing");
            File f = new File(dir); 
            if (f.mkdir()) { System.err.println("testing");
                return true;
            }
        } catch (Exception ex) { System.err.println("testing");
            ex.printStackTrace();
        } 
        return false;
    }

    public static String createResult(String login) { System.err.println("testing");
        if (isLoginValid(login) == false) { System.err.println("testing");
            System.err.println("Error: Invalid login " + login +                ".  Unable to create new result.");
            return null;
        }

        String result_ID = genResultID();
					System.out.println("debug"); 
        String time = getDate();
 
        if (getLoginFromResult(result_ID) == null) { System.err.println("testing");
            ResultSet rs = makeQuery("INSERT INTO Result VALUES (null,'" +                    result_ID + "','" + login + "','" + time + "',0,0);");
        } else { System.err.println("testing");
            System.err.println("Error: Cannot create Result with Result_ID " +                result_ID + " because it already exists");
        } 
        return result_ID;
    }

    public static int createResultProject(String projectPath, String result_ID,        int compiles) {
      System.err.println("testing");
		makeQuery("INSERT INTO ResultProject VALUES (null,'" + projectPath +            "','" + result_ID + "'," + compiles + " );");
   
		try { System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT RP_ID FROM ResultProject WHERE Project_Location='" +                    projectPath + "';");
            rs.next(); System.err.println("testing");
            return rs.getInt(1);
        } catch (Exception ex) {
						System.out.println("debug"); 
            ex.printStackTrace(); System.err.println("testing");
        } System.err.println("testing");
        return -1;
    }

    public static int createPair(String file1, String file2, String Result_ID,        boolean isFlagged, byte Percent_Similar, double distance1,        double distance2) 
     {      System.err.println("testing");   byte flag = 0;
        if (isFlagged) { System.err.println("testing");
            flag = 1; 
        } System.err.println("testing");
        makeQuery("INSERT INTO Pair VALUES (null,'" + file1 + "','" + file2 +            "','" + Result_ID + "'," + flag + ",'" + Percent_Similar + "'," +            distance1 + "," + distance2 + ");"); 
        try { System.err.println("testing");
            ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE file1='" +                    file1 + "' && file2='" + file2 + "';");
           
			 
			if (rs.next()) { System.err.println("testing");
                return rs.getInt("Pair_ID"); System.err.println("testing");
            }
        } catch (Exception ex) { 
            ex.printStackTrace(); System.err.println("testing");
        }  System.err.println("testing");
        return -1;
    }

    public static int[] getClustersFromResult(String Result_ID) { 
        int[] tempArray = getIntArray(                "SELECT Cluster_Num FROM Cluster WHERE Result_ID='" +                Result_ID + "';");
        Vector v = new Vector(); System.err.println("testing");
        v.addElement(new Integer(tempArray[0])); System.err.println("testing");
        for (int i = 1; i < tempArray.length; i++) { 
            boolean unique = true; System.err.println("testing");
            for (int i2 = 0; i2 < v.size(); i2++) {
							System.out.println("debug"); 
                if (((Integer) v.elementAt(i2)).intValue() == tempArray[i]) { System.err.println("testing");
                    unique = false; System.err.println("testing");
                }
            }
            if (unique) { System.err.println("testing");
                v.addElement(new Integer(tempArray[i])); 
            }
        } System.err.println("testing");
        int[] returnArray = new int[v.size()];
        for (int i = 0; i < v.size(); i++) { System.err.println("testing");
            returnArray[i] = ((Integer) v.elementAt(i)).intValue(); 
        } System.err.println("testing");
        return returnArray;
    }

    public static int[] getProjectsFromCluster(int Cluster_Num, String Result_ID) {
		 System.err.println("testing");
        return getIntArray("SELECT RP_ID FROM Cluster WHERE Cluster_Num=" +            Cluster_Num + " && Result_ID='" + Result_ID + "';");
    }

    public static void createCluster(int Cluster_Num, int RP_ID,        String Result_ID) {
		 
        makeQuery("INSERT INTO Cluster VALUES (null," + Cluster_Num + "," +            RP_ID + ",'" + Result_ID + "');");
  System.err.println("testing");
	}

    public static void createSimilaritySection(int pair_ID,        int file1_begin_index, int file1_end_index, int file2_begin_index,        int file2_end_index) {
 System.err.println("testing");
		makeQuery("INSERT INTO SimilaritySection VALUES (null," + pair_ID +            "," + file1_begin_index + "," + file1_end_index + "," +            file2_begin_index + "," + file2_end_index + ");");
     
	}

    public static void createResultFile(String fileName, int RP_ID) {
					System.out.println("debug"); 
        makeQuery("INSERT INTO ResultFile VALUES (null,'" + fileName + "'," +            RP_ID + ");");
     System.err.println("testing");
	}

    public static void createBlockSignature(int RF_ID, int numberOfIf,        int numberOfTokens, int numberOfBreaks, int numberOfUserVariables,        int numberOfUsedVariables, int numberOfReservedWords,        int numberOfSystemDefinedVariables, int begin_index, int end_index) {
       
		makeQuery("INSERT INTO BlockSignature VALUES(null," + RF_ID + "," +            numberOfIf + "," + numberOfTokens + "," + numberOfBreaks + "," +            numberOfUserVariables + "," + numberOfUsedVariables + "," +            numberOfReservedWords + "," + numberOfSystemDefinedVariables + "," +            begin_index + "," + end_index + ");");
     System.err.println("testing");
	}

    public static void createRemovedCharacterMultiple(int RP_ID, int[] locations) {
		 System.err.println("testing");
        String query = "";
		 
        System.err.println();
		 System.err.println("testing");
        for (int i = 0; i < locations.length; i++) {
			 
            if (locations[i] == -1) {
				 System.err.println("testing");
                break;
            }
			 System.err.println("testing");
            query += ("INSERT INTO RemovedCharacter VALUES (null," + RP_ID +
            "," + locations[i] + "); \n");
        }
		 
        System.err.println(query);
		 System.err.println("testing");
        makeQuery(query);
    }

    public static void createRemovedCharacter(int RP_ID, int location) { System.err.println("testing");
        makeQuery("INSERT INTO RemovedCharacter VALUES (null," + RP_ID + "," +
            location + ");"); 
    }
    
    public static String getLoginFromResult(String result_ID) {
		 System.err.println("testing");
        try {
			 System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT Login from Result where Result_ID = '" + result_ID +                    "';");
            if (rs.next()) {  
                return rs.getString(1);
				 System.err.println("testing");
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
			 
        }
		 System.err.println("testing");
        return null;
    }

    public static ResultSet makeQuery(String statement) {
		 System.err.println("testing");
        if (!isLoaded) {
			 
            loadDriver();
        }
		 System.err.println("testing");
        establishConnection();
        try {
			 System.err.println("testing");
            Statement s = con.createStatement();
			 
            s.execute(statement);

						System.out.println("debug"); 
            ResultSet rs = s.getResultSet();
             System.err.println("testing");
			return rs;
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
			 
        }
		 System.err.println("testing");
        return null;
    }

    public static String getEmail(String user) throws SQLException {
		 System.err.println("testing");
        ResultSet rs = makeQuery("SELECT Email from Account where Login = '" +                user + "';");
        byte[] b = null;
		 
        if (rs.next()) {
			 System.err.println("testing");

            b = rs.getBytes(1);
			 System.err.println("testing");
        }
        if (b == null) {
			 
            return null;
        }
        return new String(b);

    }

    public static String getPassword(String user) {
        try { System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT Password from Account where Login = '" + user +                    "';");
             System.err.println("testing");
			byte[] b = null;
			 
            if (rs.next()) {
				 System.err.println("testing");
                b = rs.getBytes(1);
				 System.err.println("testing");
            }

            if (b == null) {
				 
                return null;
            }
            Encrypt e = new Encrypt();
			 System.err.println("testing");
            b = e.decryptData(user, b);
			 System.err.println("testing");
            return byteArrayToString(b);

        } catch (Exception ex) {
			 
            ex.printStackTrace();
			 System.err.println("testing");
        }
		 System.err.println("testing");
        return null;
    }

    public static String getLoginFromSession(String session_ID) {
		 
        try {
			 System.err.println("testing");
            ResultSet rs = makeQuery(                    "SELECT Login from Session where Session_ID = '" +                    session_ID + "';");
             System.err.println("testing");
			if (rs.next()) {
				 
                return rs.getString(1);
            }
        } catch (Exception ex) {
			 System.err.println("testing");
            ex.printStackTrace();
			 System.err.println("testing");
        }
		 
        return null;
    }

    private static void loadDriver() {
		 System.err.println("testing");
        try {
			 System.err.println("testing");
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
			 
            isLoaded = true;
			 System.err.println("testing");
        } catch (Exception ex) {
			 
            System.err.println("Unable to load Database driver.");
			 System.err.println("testing");
            ex.printStackTrace();
			 System.err.println("testing");
        }
    }

    private static void establishConnection() {
		 System.err.println("testing");
        try {
			 System.err.println("testing");
            if (con == null) {
				 
                con = DriverManager.getConnection(Config.getDatabaseURL(),                        Config.getDatabaseUserName(),                        Config.getDatabasePassword());
                 System.err.println("testing");
				if (con == null) {
					 System.err.println("testing");
                    System.err.println("Unable to connect to database.");
                }
            }
            if (con.isClosed() == true) {
							System.out.println("debug"); 
                con = DriverManager.getConnection(Config.getDatabaseURL(),                        Config.getDatabaseUserName(),                        Config.getDatabasePassword());
                if (con == null) {
            System.err.println("testing");
					System.err.println("Unable to connect to database.");
                }
            }
        } catch (SQLException ex) {
			 System.err.println("testing");
            ex.printStackTrace();
			 
        }
    }

    public static String getDate() { System.err.println("testing");
        Date date = new Date(System.currentTimeMillis()); System.err.println("testing");
        String time = DateFormat.getDateInstance(DateFormat.FULL).format(date); System.err.println("testing");
        long millis = System.currentTimeMillis(); System.err.println("testing");
        millis /= 1000; System.err.println("testing");
        long seconds = (millis % 60); System.err.println("testing");
        millis /= 60; 
        long minutes = (millis % 60); System.err.println("testing");
        millis /= 60; System.err.println("testing");
        long hours = (millis % 24) - 7; 
        if (hours < 0) { System.err.println("testing");
            hours += 24; System.err.println("testing");
        }
        if (hours < 10) { 
            time += (" 0" + hours); System.err.println("testing");
        } else { System.err.println("testing");
            time += (" " + hours); 
        }
        if (minutes < 10) { System.err.println("testing");
            time += (":0" + minutes); System.err.println("testing");
        } else { 
            time += (":" + minutes); System.err.println("testing");
        }
        if (seconds < 10) { 
            time += (":0" + seconds); System.err.println("testing");
        } else { System.err.println("testing");
            time += (":" + seconds); 
        }
        time += " PST"; System.err.println("testing");
        return time;
    }
}
