import java.io.*;

import java.sql.*;

import java.text.*;

import java.util.Random;
import java.util.Vector;


/**  
 * A class that contains methods to provide database access required by any
 * of the classes in the SID PDS.
 *
 * @author Brian McKinnon
 * @version 1.0, 08/23/01
 */
public class DataBaseUtility {
    private static Connection con = null;
    private static boolean isLoaded = false;


    /** -- 1 -- 1 line
     * Default constructor.
     */
    public DataBaseUtility() {
        //loadDriver();
        //establishConnection();
    }

    /** -- 2 -- 4 lines 
     * FOR DEBUG AND TESTING PURPOSES.
     */
    public static void main(String[] args) throws SQLException {
        System.err.println("testing");
        loadDriver();

        //establishConnection();
        test();
    }

    /** -- 3 -- 5 lines
     * FOR DEBUG AND TESTING PURPOSES.
     */
    private static void test() throws SQLException {
        deleteResult("XKIBFJLDMKSODRDDMZLLKSQORFCFQH");
        deleteResult("TURFCACJJRYPOBFUGLVREOFIQWIGYZ");
        deleteResult("MSCWBITCJVRXCLXUSRBLSBGMVURPDD");
        deleteResult("KRMKAHGUGLTMYGHBKRMTHDINSQAKAG");

        //deleteResult("CVAPKLAMMVZYZBNDJCGGYXCPBDNAIW");
    }

    /** -- 4 -- 7 lines
     * Generates a new Session ID from genID() and checks to make sure
     * the ID generated does not exist in the database with
     * getLoginFromSession().  IDs will be generated until an unused one
     * is created.
     *
     * @return a unique(to the system) session ID value.
     */
    public static String genSessionID() {
        while (true) {
            String ID = genID();

            if (getLoginFromSession(ID) == null) {
                return ID;
            }
        }
    }

    /** -- 5 -- 7 lines
     * Generates a new Result ID from genID() and checks to make sure
     * the ID generated does not exist in the database with
     * getLoginFromResult().  IDs will be generated until an unused one
     * is created.
     *
     * @return a unique(to the system) result ID value.
     */
    public static String genResultID() {
        while (true) {
            String ID = genID();

            if (getLoginFromResult(ID) == null) {
                return ID;
            }
        }
    }

    /** -- 6 -- 7 lines
     * Generates a random 30 character long String of uppercase letters.
     *
     * @return A random 30 character long String.
     */
    public static String genID() {
        Random rand = new Random(System.currentTimeMillis());
        char[] tempArray = new char[30];

        for (int i = 0; i < 30; i++) {
            tempArray[i] = (char) ((Math.abs(rand.nextInt()) % 26) + 65);
        }

        return new String(tempArray);
    }

    /** -- 7 -- 14 lines
     * Creates a new session in the database for the for the user with
     * login name login.
     *
     * @param login The login name of the user who is having a new session
     * created
     * @return The sessionID of the created session if session was created
     * successfully otherwise null.
     */
    public static String createSession(String login) {
        //establishConnection();
        if (isLoginValid(login) == false) {
            System.err.println("Error: Invalid login " + login +
                ".  Unable to create new session.");

            return null;
        }

        String session_ID = genSessionID();

        if (getLoginFromSession(session_ID) == null) {
            long start_time = System.currentTimeMillis();
            ResultSet rs = makeQuery("INSERT INTO Session Values (null,'" +
                    session_ID + "'," + start_time + ",null,1,'" + login +
                    "');");
        } else {
            System.err.println("Error: Cannot create session with session_ID " +
                session_ID + " because it already exists");
        }

        return session_ID;
    }

    /** -- 8 -- 12 lines
     * Get the ResultProjectID from the path to the project in the database.
     *
     * @param location The path to the project.
     * @return The ResultProjectID for the specified project.
     */
    public static int getResultProjectIDFromLocation(String location) {
        try {
            ResultSet rs = makeQuery(
                    "SELECT RP_ID FROM ResultProject WHERE Project_Location='" +
                    location + "';");

            if (rs.next()) {
                return rs.getInt("RP_ID");
            }

            return -1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    /** -- 9 -- 13 lines
     * Set the ResultProject Compile based on the path to the project in the database.
     *
     * @param location The path to the project.
     * @param value the value to set compile to.
     * @return true if success else false.
     */
    public static boolean setResultProjectCompile(String location, int value) {
        int RP_ID = getResultProjectIDFromLocation(location);

        if (RP_ID < 0) {
            return false;
        }

        try {
            ResultSet rs = makeQuery("UPDATE ResultProject SET Compile = " +
                    value + " WHERE RP_ID = " + RP_ID + ";");
        } catch (Exception ex) {
            ex.printStackTrace();

            return false;
        }

        return true;
    }

    

    /** -- 11 -- 
     * DOCUMENT ME!
     *
     * @param result_ID DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String[] getResultProjectCompile(String result_ID) {
        result_ID = getResultIDFromDate(result_ID);

        int[] RPIDs = getRPIDs(result_ID);
        String[] locations = new String[RPIDs.length];
        int value = 0;

        for (int i = 0; i < locations.length; i++) {
            locations[i] = getResultProjectLocation(RPIDs[i]);
            locations[i] = locations[i].substring(locations[i].lastIndexOf("/") +
                    1);

            try {
                ResultSet rs = makeQuery("SELECT Compile FROM ResultProject " +
                        "WHERE RP_ID = " + RPIDs[i] + ";");

                if (rs.next()) {
                    value = rs.getInt("Compile");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (value == 0) {
                locations[i] += " ----> failed to compile!";
            } else {
                locations[i] += " ----> compiled successfully!";
            }
        }

        return locations;
    }

    /** -- 12 -- 
     * Get the paths to all of the projects in a specified result from the database.
     *
     * @param result_ID The ID of the result to get information from.
     * @return An array of paths to all the projects in the result.
     */
    public static String[] getResultProjectLocations(String result_ID) {
        String base = Config.getSubmittedFilesPath();
        int[] RPIDs = getRPIDs(result_ID);
        String[] locations = new String[RPIDs.length];

        for (int i = 0; i < locations.length; i++) {
            locations[i] = base + getResultProjectLocation(RPIDs[i]);
        }

        return locations;
    }

    /** -- 13 -- 
     * Get the path to a ResultProject from its ID value in the database.
     *
     * @param RP_ID The ID of the ResultProject to get information from.
     * @result The path to the ResultProject.
     */
    public static String getResultProjectLocation(int RP_ID) {
        try {
            ResultSet rs = makeQuery(
                    "SELECT Project_Location FROM ResultProject WHERE RP_ID=" +
                    RP_ID + ";");

            if (rs.next()) {
                return rs.getString("Project_Location");
            }

            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 14 -- 
     * Get all the ResultProject IDs from a specified result in the database.
     *
     * @param result The String ID value of the result to get information from.
     * @return An int[] containing all the ResultProject IDs from the result.
     */
    public static int[] getRPIDs(String result) {
        return getIntArray("SELECT RP_ID FROM ResultProject WHERE Result_ID='" +
            result + "';");
    }

    /** -- 15 -- 
     * Gets all BlockSignature IDs from a specified ResultFile in the database
     *
     * @param RF_ID The ResultFileID used to get information from the BlockSignature table.
     * @return An int[] of all BlockSignature IDs that are from the specified ResultFile.
     */
    public static int[] getBSIDs(int RP_ID) {
        return getIntArray("SELECT BS_ID FROM BlockSignature WHERE RP_ID='" +
            RP_ID + "';");
    }

    /** -- 16 --
     * Get all RemovedCharacter IDs from a specified ResultProject in the database.
     *
     * @param RP_ID The ResultProject ID to get RemovedCharacter IDs from.
     * @return An int[] of all RemovedCharacter IDs from the specified ResultProject.
     */
    public static int[] getRemovedCharacters(int RP_ID) {
        return getIntArray(
            "SELECT Location FROM RemovedCharacter WHERE RP_ID='" + RP_ID +
            "' ORDER BY Location;");
    }

    /** -- 17 -- 
     * Get all ResultFile IDs from a specified ResultProject in the database
     *
     * @param RP_ID The ResultProject ID used to query the ResultFile table for matches.
     * @return An int[] of all ResultFile IDs from the specified ResultProject
     */
    public static int[] getRFIDs(int RP_ID) {
        return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +
            ";");
    }

    /** -- 18 -- 
     * Get the Path to all the ResultFiles that are from a specified ResultProject in
     * the database.
     *
     * @param RP_ID The ResultProject ID to which the ResultFiles belong to.
     * @return A String[] containing paths to all the files in the specified ResultProject
     */
    public static String[] getResultFileLocations(int RP_ID) {
        return getStringArray("SELECT File_Name FROM ResultFile WHERE RP_ID=" +
            RP_ID + ";");
    }

    /** -- 19 -- 
     * Get the IDs of all the ResultFiles from a specified ResultProject in
     * the database.
     *
     * @param RP_ID The ResultProject ID to which the ResultFiles belong to.
     * @return An int[] containing IDs of all the ResultFiles in the specified ResultProject
     */
    public static int[] getResultFileIDs(int RP_ID) {
        return getIntArray("SELECT RF_ID FROM ResultFile WHERE RP_ID=" + RP_ID +
            ";");
    }

   

    /** -- 21 -- 
     * Get the Result ID of the Result created on a specified date frome the database.
     *
     * @param date A string reresenting the Date the Result that is wanted was created.
     * @return The Result ID of the Result created on the specified date.
     */
    public static String getResultIDFromDate(String date) {
        String[] strings = getStringArray(
                "SELECT Result_ID FROM Result WHERE Time_Stamp='" + date +
                "';");

        return strings[0];
    }

    /** -- 22 -- 
     * Get the date a specified Result was created on from the database.
     *
     * @param result_ID The ID of the Result to get the creation date from.
     * @return The date the specified Result was created on.
     */
    public static String getResultDate(String result_ID) {
        String[] strings = getStringArray(
                "SELECT Time_Stamp FROM Result WHERE Result_ID='" + result_ID +
                "';");

        return strings[0];
    }

    /** -- 23 -- 
     * Get all the dates for all Results for a given account from the database.
     *
     * @param login The login of the Account to get information from.
     * @return A String[] of all the Dates of all the Results from the specified account.
     */
    public static String[] getResultDates(String login) {
        return getStringArray("SELECT Time_Stamp FROM Result WHERE Login='" +
            login + "' ORDER BY Time_Stamp DESC;");
    }

    /** -- 24 -- 
     * Checks if a submission has finished processing or not
     * @param resultDate The date of the Result to check.
     * @return true if processing has finished otherwise false.
     */
    public static boolean getResultComplete(String resultDate) {
        int[] isComplete = getIntArray(
                "SELECT complete FROM Result WHERE Time_Stamp = '" +
                resultDate + "';");

        if (isComplete == null) {
            return false;
        } else if (isComplete[0] == 1) {
            return true;
        }

        return false;
    }

    /** -- 25 -- 
     * DOCUMENT ME!
     *
     * @param result DOCUMENT ME!
     * @param user DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static boolean doesResultBelongTo(String result, String user) {
        int[] isComplete = getIntArray(
                "SELECT complete FROM Result WHERE Result_ID = '" + result +
                "' AND Login = '" + user + "';");

        if (isComplete == null) {
            return false;
        } else {
            return true;
        }
    }

    /** -- 26 --
     * DOCUMENT ME!
     *
     * @param resultDate DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static byte getPercentCompleteFromDate(String resultDate) {
        ResultSet rs = makeQuery(
                "SELECT complete_percent FROM Result WHERE Time_Stamp = '" +
                resultDate + "';");

        try {
            if (rs.next()) {
                int val = rs.getInt("complete_percent");

                return (byte) val;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -101;
    }

    /** -- 27 -- 
     * DOCUMENT ME!
     *
     * @param result DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static byte getPercentComplete(String result) {
        ResultSet rs = makeQuery(
                "SELECT complete_percent FROM Result WHERE Result_ID = '" +
                result + "';");

        try {
            if (rs.next()) {
                int val = rs.getInt("complete_percent");

                return (byte) val;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    /** -- 28 -- 
     * DOCUMENT ME!
     *
     * @param result DOCUMENT ME!
     * @param percent DOCUMENT ME!
     */
    public static void setPercentComplete(String result, int percent) {
        makeQuery("UPDATE Result SET complete_percent = " + percent +
            " WHERE result_ID = '" + result + "';");
    }

    /** -- 29 -- 
     * Executes a database query and processes all data as ints.  This method is used
     * as a generic sub method for any method that makes database call that returns a
     * list of ints.
     *
     * @param s The database query to execute.
     * @return The results of the database query in an int[].
     */
    public static int[] getIntArray(String s) {
        //establishConnection();
        ResultSet rs = makeQuery(s);
        Vector v = new Vector();

        try {
            while (rs.next()) {
                Integer i = new Integer(rs.getInt(1));
                v.addElement(i);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int[] returnArray = new int[v.size()];

        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = ((Integer) v.elementAt(i)).intValue();
        }

        return returnArray;
    }

   

    /** -- 31 --
     * Executes a database query and processes all data as Strings.  This method is used
     * as a generic sub method for any method that makes database call that returns a
     * list of Strings.
     *
     * @param s The database query to execute.
     * @return The results of the database query in a String[].
     */
    public static String[] getStringArray(String s) {
        //establishConnection();
        ResultSet rs = makeQuery(s);
        Vector v = new Vector();

        try {
            while (rs.next()) {
                v.addElement(rs.getString(1));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        String[] returnArray = new String[v.size()];

        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = (String) v.elementAt(i);
        }

        return returnArray;
    }

    /** -- 32 --
     * Closes a specified session in the database.
     *
     * @param session_ID The ID of the session to close.
     */
    public static void closeSession(String session_ID) {
        //establishConnection();
        if (getLoginFromSession(session_ID) == null) {
            System.err.println("Error: Cannot close session with session_ID " +
                session_ID + " because it does not exist");
        } else {
            if (isSessionValid(session_ID) == false) {
                System.err.println(
                    "Error: Cannot close session with session_ID " +
                    session_ID + " because it has already been closed");

                return;
            }

            long end_time = System.currentTimeMillis();
            ResultSet rs = makeQuery("UPDATE Session SET Stop_Time=" +
                    end_time + ",Is_Valid_Session=0 WHERE Session_ID='" +
                    session_ID + "';");
        }
    }

    /** -- 33 -- 
     * Checks to see if a given session is still valid or if it has expired in the
     * database.
     *
     * @param session_ID The ID of the session to check.
     * @return true if session is valid otherwise false.
     */
    public static boolean isSessionValid(String session_ID) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Is_Valid_Session FROM Session WHERE Session_ID='" +
                    session_ID + "';");
            rs.next();

            if (rs.getBoolean("Is_Valid_Session")) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /** -- 34 -- 
     * Gets the BlockSignature information from a specified ResultFile in the database.
     *
     * @param The ResultFile ID  of which to obtain information about.
     * @return An int[] containing all the BlockSignature information as follows.<br>
     *  int[0] = The BlockSignature ID for the block.<br>
     *  int[1] = The ResultFile ID of the file the block is in.<br>
     *  int[2] = Number of if statements in the block.<br>
     *  int[3] = Number of tokens in the block.<br>
     *  int[4] = Number of break statements in the block.<br>
     *  int[5] = Number of user variables in the block.<br>
     *  int[6] = Number of used variables in the block.<br>
     *  int[7] = Number of reserved words used in the block.<br>
     *  int[8] = Number of system defined variables in the block.<br>
     *  int[9] = The Begining Index for the block.<br>
     *  int[10] = The End Index for the block.
     */
    public static int[] getBlockSignature(int RF_ID) {
        ResultSet rs = makeQuery("SELECT * FROM BlockSignature WHERE RF_ID =" +
                RF_ID + ";");

        try {
            if (rs.next()) {
                int[] array = new int[11];
                array[0] = rs.getInt("BS_ID");
                array[1] = rs.getInt("RF_ID");
                array[2] = rs.getInt("Number_OF_If");
                array[3] = rs.getInt("Number_OF_Tokens");
                array[4] = rs.getInt("Number_OF_Breaks");
                array[5] = rs.getInt("Number_OF_User_Variables");
                array[6] = rs.getInt("Number_OF_Used_Variables");
                array[7] = rs.getInt("Number_OF_Reserved_Words");
                array[8] = rs.getInt("Number_Of_System_Defined_Variables");
                array[9] = rs.getInt("Begin_Index");
                array[10] = rs.getInt("End_Index");

                return array;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 35 -- 
     * Get all SimilaritySections from a specified Pair in the database.
     *
     * @param Pair_ID The ID of the Pair to get information from.
     * @return An int[] with all similar sections in the given pair with the following
     * format.<br>
     * int[0] = File 1's Beginning index for first similarity.<br>
     * int[1] = File 1's Ending index for first similarity.<br>
     * int[2] = File 2's Beginning index for first similarity.<br>
     * int[3] = File 2's Ending index for first similarity.<br>
     * int[4] = File 1's Beginning index for second similarity.<br>
     * int[5] = File 1's Ending index for second similarity.<br>
     * int[6] = File 2's Beginning index for second similarity.<br>
     * int[7] = File 2's Ending index for second similarity.<br>
     * ...
     */
    public static int[] getSimilaritySections(int Pair_ID) {
        ResultSet rs = makeQuery(
                "SELECT * FROM SimilaritySection WHERE Pair_ID=" + Pair_ID +
                ";");
        Vector v = new Vector();

        try {
            while (rs.next()) {
                v.addElement(new Integer(rs.getInt("File1_Begin_Index")));
                v.addElement(new Integer(rs.getInt("File1_End_Index")));
                v.addElement(new Integer(rs.getInt("File2_Begin_Index")));
                v.addElement(new Integer(rs.getInt("File2_End_Index")));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int[] array = new int[v.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = ((Integer) v.elementAt(i)).intValue();
        }

        return array;
    }

    /** -- 36 -- 
     * Get the SimilaritySection IDs from a specified Pair in the database.
     *
     * @param Pair_ID The pair to get information from.
     * @return An int[] with all the SimilaritySection IDs
     */
    public static int[] getSimilaritySectionIDs(int Pair_ID) {
        return getIntArray("SELECT SS_ID FROM SimilaritySection WHERE Pair_ID=" + Pair_ID + ";");
    }

    /** -- 37 -- 
     * Get a specific SimilaritySection from it's ID in the database.
     *
     * @param SS_ID The SimilaritySection ID to get information from.
     * @result An int[] array with information about the specified SimilaritySection<br>
     * in the following format if it exists else null.<br>
     * int[0] = SimilaritySection ID.<br>
     * int[1] = Pair ID that the SimilaritySection is from.<br>
     * int[2] = File 1's Beginning Index.<br>
     * int[3] = File 1's Ending Index.<br>
     * int[4] = File 2's Beginning Index.<br>
     * int[5] = File 2's Ending Index.
     */
    public static int[] getSimilaritySection(int SS_ID) {
        ResultSet rs = makeQuery("SELECT * FROM SimilaritySection WHERE SS_ID=" +
                SS_ID + ";");

        try {
            if (rs.next()) {
                int[] array = new int[6];
                array[0] = rs.getInt("SS_ID");
                array[1] = rs.getInt("Pair_ID");
                array[2] = rs.getInt("File1_Begin_Index");
                array[3] = rs.getInt("File1_End_Index");
                array[4] = rs.getInt("File2_Bgin_Index");
                array[5] = rs.getInt("File2_End_Index");

                return array;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 38 -- 
     * Get the Pair information on a specified pair from the database.
     *
     * @param Pair_ID The ID of the pair to get information about.
     * @return A Vector with the following format.<br>
     *  vector.elementAt(0) = (Integer)The Pair ID.<br>
     *  vector.elementAt(1) = (String)The path to File1.<br>
     *  vector.elementAt(2) = (String)The path to File2.<br>
     *  vector.elementAt(3) = (Integer)The ResultProject ID that the Pair is from.<br>
     *  vector.elementAt(4) = (Boolean)The Is_Flagged boolean value.
     */
    public static Vector getPair(int Pair_ID) {
        ResultSet rs = makeQuery("SELECT * FROM Pair WHERE Pair_ID=" + Pair_ID +
                ";");

        try {
            if (rs.next()) {
                Vector v = new Vector();
                v.addElement(new Integer(rs.getInt("Pair_ID")));
                v.addElement(rs.getString("File1"));
                v.addElement(rs.getString("File2"));
                v.addElement(new Integer(rs.getInt("RP_ID")));
                v.addElement(new Boolean(rs.getBoolean("Is_Flagged")));

                return v;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 39 -- 
     * Gets a list of all Pair's paths for a specified Result from the database.
     *
     * @param ResultID The ID of the Result to get information from.
     * @return A String[] in the following format.<br>
     *  String[0] = The path to File1 from Pair1.<br>
     *  String[1] = The path to File2 from Pair1.<br>
     *  String[2] = The path to File1 from Pair2.<br>
     *  String[3] = The path to File2 from Pair2.<br>
     *  ...
     */
    public static String[] getPairs(String ResultID) {
        ResultSet rs = makeQuery(
                "SELECT File1, File2 FROM Pair where Result_ID=" + ResultID +
                ";");
        Vector v = new Vector();

        try {
            while (rs.next()) {
                v.addElement(rs.getString("File1"));
                v.addElement(rs.getString("File2"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int size = v.size();
        String[] returnArray = new String[size];

        for (int i = 0; i < size; i++) {
            returnArray[i] = (String) v.elementAt(i);
        }

        return returnArray;
    }


    /** -- 41 -- 
     * Get all similar Projects to a specified Project in the database.
     *
     * @param projectName The path to the Project to get information from.
     * @return A String[] with all Projects similar to the specified Project.
     */
    public static String[] getSimilarProjects(String projectName) {
        String s1 = "SELECT File1 FROM Pair where File2='" + projectName +"';";
        String s2 = "SELECT File2 FROM Pair where File1='" + projectName +"';";
        String[] match1 = getStringArray(s1);
        String[] match2 = getStringArray(s2);
		System.out.println(s1);
		System.out.println(s2);
		System.err.println("match 1 length: " + match1.length + " match 2 length: " + match2.length ); 
        String[] tempArray = new String[match1.length + match2.length];

        for (int i = 0; i < match1.length; i++) {
            tempArray[i] = match1[i];
        }

        for (int i = 0; i < match2.length; i++) {
            tempArray[i + match1.length] = match2[i];
        }

        int repeat = 0;

        for (int i = 0; i < tempArray.length; i++) {
            if (tempArray[i] != null) {
                for (int j = i + 1; j < tempArray.length; j++) {
                    if (tempArray[i].equalsIgnoreCase(tempArray[j])) {
                        tempArray[j] = null;
                        repeat++;
                    }
                }
            }
        }

        String[] returnArray = new String[tempArray.length - repeat];
        int returnCount = 0;

        for (int i = 0; i < tempArray.length; i++) {
            if (tempArray[i] != null) {
                returnArray[returnCount] = getEnd(tempArray[i]);
                returnCount++;
            }
        }

        return returnArray;
    }

    /** -- 42 -- 
     * Get distance measure of two files from the Pair database.
     * @param f1 The path to the project to get information from.
     * @param f2 The path to the project to get information from.
     * @return A integer value of distance measure.
     */
    public static double getDisMeasure(String f1, String f2) { // xin

        double dis = 0;
        ResultSet rs = makeQuery("SELECT Distance1 FROM Pair WHERE File1='" +
                f1 + "' && File2='" + f2 + "';");

        try {
            if (rs.next() == false) {
                ResultSet rt = makeQuery(
                        "SELECT Distance1 FROM Pair WHERE File1='" + f2 +
                        "' && File2='" + f1 + "';");

                try {
                    if (rt.next() == false) {
                        System.err.println("Error in getDisMeasure()");
                    } else {
                        dis = rt.getDouble("Distance1");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                dis = rs.getDouble("Distance1");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dis;
    }
     // xin

    /** -- 43 -- 
     * Get the PairID from the database for two Projects with the specified FileNames.
     *
     * @param f1 The path to the first Project.
     * @param f2 The path to the second Project.
     * @return The PairID for the two Projects if it exists otherwise -1.
     */
    public static int getPairIDFromFileName(String f1, String f2) {
        ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE File1='" + f1 +
                "' && File2='" + f2 + "';");

        try {
            if (rs.next() == false) {
                //rs = makeQuery("SELECT Pair_ID FROM Pair WHERE File1='" + f2 + "' && File2='" + f1 + "';");
                //if(rs.next())
                //    return rs.getInt("Pair_ID");
                return -2;
            } else {
                return rs.getInt("Pair_ID");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    /** -- 44 -- 
     * Get all FlaggedPair IDs from a specified Result in the database.
     *
     * @param resultID The ID of the Result to get information from.
     * @return An int[] of all the FlaggedPair IDs from the Result.
     */
    public static int[] getFlaggedPairIDs(String resultID) {
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +
            resultID + "';");
    }

    /** -- 45 -- 
     * Get all Pair IDs from a specified Result in the database.
     *
     * @param resultID The ID of the Result to get information from.
     * @return An int[] of all the Pair IDs from the Result.
     */
    public static int[] getPairIDs(String resultID) {
        return getIntArray("SELECT Pair_ID FROM Pair WHERE Result_ID='" +
            resultID + "';");
    }

    /** -- 46 -- 
     * Get a list of percentages of similarity for all pairs from a specified Result
     * in the database.
     *
     * @param resultID The ID of the result to get information from.
     * @return An int[] with the percentages of similarity for all pairs in the
     * specified Result.  The indexes of the array will correspond with the indexes
     * of the getPairs() return value.
     */
    public static int[] getPairPercents(String resultID) {
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +
            resultID + "';");
    }


	// -- 47 -- 
    // added by Xin Chen
    public static double getFlaggedPairMeasure(int PairID) // xin
     {
        //establishConnection();
        double distance = 0.11;

        try {
            ResultSet rs = makeQuery(
                    "SELECT Distance1 FROM Pair WHERE Pair_ID=" + PairID + ";"); // xin

            if (rs.next()) {
                distance = rs.getDouble("Distance1");
            } else {
                distance = 0.22;
            }

            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return distance;
    }
     // xin



    /** -- 48 -- 
     * Gets a list of percents for the FlaggedPairs from a specified Result in the database.
     *
     * @param resultID The ID of the Result to get information from.
     * @return An int[] with the percerntages of similarity for all flagged pairs in
     * the specified Result.  The indexes of the array will correspond with the indexes
     * of the getFlaggedPairs() return value.
     */
    public static int[] getFlaggedPairPercents(String resultID) {
        return getIntArray("SELECT Percent_Similar FROM Pair WHERE Result_ID='" +
            resultID + "' && Is_Flagged = 1;");
    }


    /** -- 49 -- 
     * DOCUMENT ME!
     *
     * @param resultID DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static double[] getFlaggedPairDisMeasure(String resultID) { // xin

        return getDoubleArray("SELECT Distance1 FROM Pair WHERE Result_ID='" +
            resultID + "' && Is_Flagged = 1;"); // xin
    }
     // xin



  


    /** -- 51 -- 
     * Get a list of all ProjectNames(A ProjectName is the path to the project) for
     * a specified Result from the database.
     *
     * @param Result_ID The ID of the Result to get information from.
     * @return A String[] of all the ProjectNames in the specified Result.
     */
    public static String[] getProjectNames(String Result_ID) {
        String[] files = getStringArray(
                "SELECT Project_Location FROM ResultProject WHERE Result_ID='" +
                Result_ID + "';");
        String[] name = new String[files.length];

        if (files.length <= 0) {
            return null;
        }

        for (int i = 0; i < files.length; i++) {
            for (int i2 = files[i].length() - 1; i2 >= 0; i2--) {
                if (files[i].charAt(i2) == '/') {
                    name[i] = files[i].substring(i2 + 1);

                    break;
                }
            }
        }

        return name;
    }


    /** -- 52 -- 
     * Creates a new user Account in the database.
     *
     * @param login The login name for the account.
     * @param password The password to access the system.
     * @param email The email that will recieve messages when a submission is done being processed.
     * @param firstName No longer used.
     * @param lastName No longer used.
     * @param institution No longer used.
     * @param admin If true allows this account to access administration functions in the SID PDS.
     * @return True if account has been succesfully created else false.
     */
    public static boolean createAccount(String login, String password,
        String email, String firstName, String lastName, String institution,
        boolean admin) {
        //establishConnection();
        if (getPassword(login) == null) {
            byte isAdmin;

            if (admin) {
                isAdmin = 1;
            } else {
                isAdmin = 0;
            }

            Encrypt e = new Encrypt();
            byte[] passwordData = stringToByteArray(password);
            passwordData = e.encryptData(login, passwordData);
            password = byteArrayToString(passwordData);

            String query = "INSERT INTO Account VALUES (null,'" + login + "',";
            query = appendBytesToString(query, passwordData);
            query += (",'" + email + "','" + firstName + "','" + lastName +
            "','" + institution + "'," + isAdmin + ",0);");

            ResultSet rs = makeQuery(query);

            return true;
        } else {
            System.err.println("Error: Cannot create account because Login " +
                login + " already taken");

            return false;
        }
    }


    /** -- 53 -- 
     *
     */
    private static String appendBytesToString(String s, byte[] b) {
        String tempString = "CHAR(";

        for (int i = 0; i < b.length; i++) {
            tempString += (b[i] + ",");
        }

        tempString = tempString.substring(0, tempString.length() - 1);
        tempString += ")";
        s += tempString;

        return s;
    }

    /** -- 54 -- 
     *
     */
    private static byte[] stringToByteArray(String s) {
        int length = s.length();
        byte[] b = new byte[length];
        char temp = ' ';

        for (int i = 0; i < length; i++) {
            temp = s.charAt(i);
            b[i] = (byte) temp;
        }

        return b;
    }

    /** -- 55 -- 
     *
     */
    private static String byteArrayToString(byte[] b) {
        String s = "";
        char temp = ' ';

        for (int i = 0; i < b.length; i++) {
            temp = (char) b[i];
            s += temp;
        }

        return s;
    }

    /** -- 56 -- 
     * Logs a user into the system with the specified login by setting
     * the Last_Activity value for the specified account in the database
     * to the current time.
     *
     * @param login The login name of the account to log in.
     */
    public static void logInUser(String login) {
        //establishConnection();
        makeQuery("UPDATE Account SET Last_Activity=" +
            System.currentTimeMillis() + " WHERE Login='" + login + "';");
    }

    /** -- 57 --
     * Logs a user out of the system with the specified login by setting
     * the Last_Activity value for the specified account in the database
     * to 0.
     *
     * @param login The login name of the account to log out.
     */
    public static void logOutUser(String login) {
        //establishConnection();
        makeQuery("UPDATE Account SET Last_Activity=0 WHERE Login='" + login +
            "';");
    }

    /** -- 58 -- 
     * Sets the Is_Flagged variable in the specified Pair from the database
     * to the value 1.
     *
     * @param pairID The ID of the Pair in the database to flag.
     */
    public static void flagPair(int pairID) {
        makeQuery("UPDATE Pair SET Is_Flagged=1 WHERE Pair_ID=" + pairID + ";");
    }

    /** -- 59 -- 
     * Logs out all users from the system by setting their account's
     * Last_Activity values to 0.
     */
    public static void logOutAll() {
        //establishConnection();
        makeQuery("UPDATE Account SET Last_Activity=0;");
    }

    

    /** -- 61 -- 
     * Sets the specified Account's Last_Activity value to the current time
     * so the account does not timeout.
     *
     * @param login The login of the Account to modify.
     */
    public static void newAccountActivity(String login) {
        makeQuery("UPDATE Account SET Last_Activity=" +
            System.currentTimeMillis() + " WHERE Login='" + login + "';");
    }

    /** -- 62 --
     * Checks to see if a specified Account is logged in by checking the
     * Account's Last_Activity value.  If the value is within 10 minutes of
     * the current time the Account is logged in otherwise it is not.
     *
     * @param login The login of the Account to check.
     * @return True if the Account is logged in otherwise false.
     */
    public static boolean isLoggedIn(String login) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Last_Activity FROM Account WHERE Login = '" +
                    login + "';");
            rs.next();

            long lastAction = rs.getLong("Last_Activity");

            if ((System.currentTimeMillis() - lastAction) < 600000) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false; // xin

        //return true;  // xin
    }

    /** -- 63 --
     * Checks to see if a given login name exists as a registered Account.
     *
     * @param login The login name to check.
     * @return True if login exists otherwise false.
     */
    public static boolean isLoginValid(String login) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery("SELECT Login FROM Account WHERE Login='" +
                    login + "';");
            rs.next();

            if (rs.getAsciiStream("Login") != null) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /** -- 64 -- 
     * Deletes the specified Account and all its information from the
     * database as well as all its files from the server.  So be careful
     * becuase this is very final and can't be reversed.
     *
     * @param login The login of the Account to delete.
     */
    public static boolean deleteAccount(String login) {
        if (getPassword(login) != null) {
            String[] results = getResultIDs(login);

            for (int i = 0; i < results.length; i++)
                deleteResult(results[i]);

            makeQuery("DELETE FROM Account where Login = '" + login + "';");

            return true;
        } else {
            System.err.println("Error: Cannot delete account with Login " +
                login + " because it does not exist");

            return false;
        }
    }

    /** -- 65 -- 
     * Deletes the specified Result from the database and all information
     * associated with it including files and database entries.
     *
     * @param Result_ID the result to delete.
     */
    public static void deleteResult(String Result_ID) {
        //delete Result Project
        int[] resultProjectIDs = getRPIDs(Result_ID);

        for (int i = 0; i < resultProjectIDs.length; i++)
            deleteResultProject(resultProjectIDs[i]);

        //delete Pair
        int[] pairIDs = getPairIDs(Result_ID);

        for (int i = 0; i < pairIDs.length; i++)
            deletePair(pairIDs[i]);

        //Delete the files associated with the Result
        String[] login = getStringArray(
                "SELECT Login FROM Result WHERE Result_ID='" + Result_ID +
                "';");
        deleteDirectory(Config.getSubmittedFilesPath() + login[0] + '/' +
            Result_ID);

        //delete itself
        makeQuery("DELETE FROM Result where Result_ID = '" + Result_ID + "';");
    }

    /** -- 66 -- 
     * Deletes the specified ResultProject from the database and all information
     * associated with it in the database.
     *
     * @param RP_ID the ResultProject to delete.
     */
    private static void deleteResultProject(int RP_ID) {
        //delete ResultFiles
        int[] resultFileIDs = getResultFileIDs(RP_ID);

        for (int i = 0; i < resultFileIDs.length; i++)
            deleteResultFile(resultFileIDs[i]);

        //delete Block Signatures
        int[] blockSignatureIDs = getBSIDs(RP_ID);

        for (int i = 0; i < blockSignatureIDs.length; i++)
            deleteBlockSignature(blockSignatureIDs[i]);

        //delete itself
        makeQuery("DELETE FROM ResultProject where RP_ID = '" + RP_ID + "';");
    }

    /** -- 67 -- 
     * Deletes the specified ResultFile from the database.
     *
     * @param RF_ID the ResultFile to delete.
     */
    private static void deleteResultFile(int RF_ID) {
        makeQuery("DELETE FROM ResultFile where RF_ID = '" + RF_ID + "';");
    }

    /** -- 68 -- 
     * Deletes the specified BlockSignature from the database.
     *
     * @param BS_ID the BlockSignature to delete.
     */
    private static void deleteBlockSignature(int BS_ID) {
        makeQuery("DELETE FROM BlockSignature where BS_ID = '" + BS_ID + "';");
    }

    /** -- 69 -- 
     * Deletes the specified Pair from the database and all information
     * associated with it in the database.
     *
     * @param Pair_ID the Pair to delete.
     */
    private static void deletePair(int Pair_ID) {
        //delete Similarity Sections
        int[] similarSectionIDs = getSimilaritySectionIDs(Pair_ID);

        for (int i = 0; i < similarSectionIDs.length; i++)
            deleteSimilaritySection(similarSectionIDs[i]);

        //delete itself
        makeQuery("DELETE FROM Pair where Pair_ID = '" + Pair_ID + "';");
    }


    /** -- 71 -- 
     * DOCUMENT ME!
     *
     * @param path DOCUMENT ME!
     */
    private static void deleteDirectory(String path) {
        File directory = new File(path);

        if (!directory.exists()) {
            return;
        }

        String[] list = directory.list();

        if (list == null) {
            return;
        }

        for (int i = 0; i < list.length; i++) {
            File f = new File(path + '/' + list[i]);

            if (f.isDirectory()) {
                deleteDirectory(path + '/' + list[i]);
            } else {
                f.delete();
            }
        }

        directory.delete();
    }

    /** -- 72 -- 
     * Checks to see if a specified directory exists.
     *
     * @param dir The directory path to check.
     * @return True if the specified directory exists otherwise false.
     */
    public static boolean doesDirectoryExist(String dir) {
        try {
            File f = new File(dir);

            return f.exists();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /** -- 73 -- 
     * Moves a specified file from its original location to a new
     * specified location.
     *
     * @param file The path to the file to move.
     * @param destination The path to where the file will be moved.
     */
    public static void moveFile(String file, String destination) {
        try {
            File f = new File(file);
            File dest = new File(destination);
            dest.createNewFile();

            if (f.exists() == false) {
                System.err.println("Unable to move file " + file +
                    " because it does not exist");

                return;
            }

            /*if(dest.exists() == false)
               {
                   System.err.println("Unable to move file " + file + " because destination " + destination + " does not exist");
                   return;
               }
               if(dest.isDirectory() == false)
               {
                   System.err.println("Unable to move destination " + destination + " is a directory not a file");
                   return;
               }*/
            int length = (int) f.length();
            byte[] fileArray = new byte[length];
            FileInputStream fis = new FileInputStream(f);
            fis.read(fileArray);
            fis.close();

            FileOutputStream fos = new FileOutputStream(destination);
            fos.write(fileArray);
            fos.flush();
            fos.close();
            f.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** -- 74 -- 
     * Creates a new directory from the specified path.
     *
     * @param dir The path of the directory to create.
     * @return True if directory is created otherwise false.
     */
    public static boolean createDirectory(String dir) {
        try {
            File f = new File(dir);

            if (f.mkdir()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /** -- 75 -- 
     * Creates a new Result record in the database for a specified Account.
     *
     * @param login The login of the Account to create a new Result record in.
     * @return The new Result's ID.
     */
    public static String createResult(String login) {
        //establishConnection();
        if (isLoginValid(login) == false) {
            System.err.println("Error: Invalid login " + login +
                ".  Unable to create new result.");

            return null;
        }

        //String time = date.toString() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        String result_ID = genResultID();
        String time = getDate();

        if (getLoginFromResult(result_ID) == null) {
            ResultSet rs = makeQuery("INSERT INTO Result VALUES (null,'" +
                    result_ID + "','" + login + "','" + time + "',0,0);");
        } else {
            System.err.println("Error: Cannot create Result with Result_ID " +
                result_ID + " because it already exists");
        }

        return result_ID;
    }

    /** -- 76 -- 
     * Creates a new ResultProject for a specified path and Result.
     *
     * @param projectPath The path to where the project is stored on the server.
     * @param result_ID The ID of the Result that the ResultProject belongs to.
     * @return The ResultProject's ID if it is successfully added to the
     * database otherwise -1.
     */
    public static int createResultProject(String projectPath, String result_ID,
        int compiles) {
        //establishConnection();
        makeQuery("INSERT INTO ResultProject VALUES (null,'" + projectPath +
            "','" + result_ID + "'," + compiles + " );");

        try {
            ResultSet rs = makeQuery(
                    "SELECT RP_ID FROM ResultProject WHERE Project_Location='" +
                    projectPath + "';");
            rs.next();

            return rs.getInt(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    /** -- 77 -- 
     * Creates a new Pair in the database from Two Projects and a specified
     * ResultID.
     *
     * @param file1 The path to the first Project of the Pair.
     * @param file2 The path to the second Project of the Pair.
     * @param Result_ID The ResultID that the pair belongs to.
     * @param isFlagged True if the pair is flagged otherwise false.
     * @param Percent_Similar The percentage of similarity between the
     * projects.
     * @return The PairID of the new Pair.
     */

    //    public static int createPair(String file1, String file2, String Result_ID, boolean isFlagged, byte Percent_Similar)
    public static int createPair(String file1, String file2, String Result_ID,
        boolean isFlagged, byte Percent_Similar, double distance1,
        double distance2) // xin
     {
        byte flag = 0;

        if (isFlagged) {
            flag = 1;
        }

        makeQuery("INSERT INTO Pair VALUES (null,'" + file1 + "','" + file2 +
            "','" + Result_ID + "'," + flag + ",'" + Percent_Similar + "'," +
            distance1 + "," + distance2 + ");"); // xin   

        try {
            ResultSet rs = makeQuery("SELECT Pair_ID FROM Pair WHERE file1='" +
                    file1 + "' && file2='" + file2 + "';");

            if (rs.next()) {
                return rs.getInt("Pair_ID");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    /** -- 78 -- 
     * Gets all Clusters from a specified Result.
     *
     * @param Result_ID The ID of the Result to get information from.
     * @return An int[] containg all the Cluster IDs from the specified
     * Result.
     */
    public static int[] getClustersFromResult(String Result_ID) {
        int[] tempArray = getIntArray(
                "SELECT Cluster_Num FROM Cluster WHERE Result_ID='" +
                Result_ID + "';");
        Vector v = new Vector();
        v.addElement(new Integer(tempArray[0]));

        for (int i = 1; i < tempArray.length; i++) {
            boolean unique = true;

            for (int i2 = 0; i2 < v.size(); i2++) {
                if (((Integer) v.elementAt(i2)).intValue() == tempArray[i]) {
                    unique = false;
                }
            }

            if (unique) {
                v.addElement(new Integer(tempArray[i]));
            }
        }

        int[] returnArray = new int[v.size()];

        for (int i = 0; i < v.size(); i++) {
            returnArray[i] = ((Integer) v.elementAt(i)).intValue();
        }

        return returnArray;
    }

    /** -- 79 -- 
     * Get all projects from a specified Cluster.
     *
     * @param Cluster_Num The cluster number to get projects from.
     * @param Result_ID The Result_ID that the cluster belongs to.
     * @return An int[] with all the ResultProject IDs from the specified
     * cluster.
     */
    public static int[] getProjectsFromCluster(int Cluster_Num, String Result_ID) {
        return getIntArray("SELECT RP_ID FROM Cluster WHERE Cluster_Num=" +
            Cluster_Num + " && Result_ID='" + Result_ID + "';");
    }

   

    /** -- 81 -- 
     * Creates a new SimilaritySection in the database.
     *
     * @param pair_ID The PairID of the pair that the SimilaritySection belongs to.
     * @param file1_begin_index The starting index of the similar section for the
     * first file in the pair.
     * @param file1_end_index The ending index of the similar section for the
     * first file in the pair.
     * @param file2_being_index The starting index of the similar section for the
     * second file in the pair.
     * @param file2_end_index The ending index of the similar section for the
     * second file in the pair.
     */
    public static void createSimilaritySection(int pair_ID,
        int file1_begin_index, int file1_end_index, int file2_begin_index,
        int file2_end_index) {
        //establishConnection();
        makeQuery("INSERT INTO SimilaritySection VALUES (null," + pair_ID +
            "," + file1_begin_index + "," + file1_end_index + "," +
            file2_begin_index + "," + file2_end_index + ");");
    }

    /** -- 82 -- 
     * Creates a new ResultFile in the database.
     *
     * @param fileName The path to the file that is being represented by the
     * ResultFile entry in the database.
     * @param RP_ID The ResultProjectID that the file represented by the ResultFile
     * entry in the database belongs to.
     */
    public static void createResultFile(String fileName, int RP_ID) {
        //establishConnection();
        makeQuery("INSERT INTO ResultFile VALUES (null,'" + fileName + "'," +
            RP_ID + ");");
    }

    /** -- 83 -- 
     * Creates a new BlockSignature in the database.
     *
     * @param RF_ID The ResultFileID that the BlockSignature belongs to.
     * @param numberOfIf The number of if statements in the block.
     * @param numberOfTokens The number of tokens in the block.
     * @param numberOfBreaks The number of break statements in the block.
     * @param numberOfUserVariables The number of user defined variables in the block.
     * @param numberOfUsedVariables The number of used user defined variables in the block.
     * @param numberOfReservedWords The number of reserved words used in the block.
     * @param numberOfSystemDefinedVariables The number of system defined variables in the block.
     * @param begin_index The starting index in the ResultFile of the block.
     * @param end_index The ending index in the ResultFile of the block.
     */
    public static void createBlockSignature(int RF_ID, int numberOfIf,
        int numberOfTokens, int numberOfBreaks, int numberOfUserVariables,
        int numberOfUsedVariables, int numberOfReservedWords,
        int numberOfSystemDefinedVariables, int begin_index, int end_index) {
        //establishConnection();
        makeQuery("INSERT INTO BlockSignature VALUES(null," + RF_ID + "," +
            numberOfIf + "," + numberOfTokens + "," + numberOfBreaks + "," +
            numberOfUserVariables + "," + numberOfUsedVariables + "," +
            numberOfReservedWords + "," + numberOfSystemDefinedVariables + "," +
            begin_index + "," + end_index + ");");
    }

    /** -- 84 --
     * Creates a group of new RemovedCharacters in the database.  This is no longer
     * used.  This was slow and RemovedCharacters are now reported in a file where
     * the Project that it belongs to is located.
     */
    public static void createRemovedCharacterMultiple(int RP_ID, int[] locations) {
        String query = "";
        System.err.println();

        for (int i = 0; i < locations.length; i++) {
            if (locations[i] == -1) {
                break;
            }

            query += ("INSERT INTO RemovedCharacter VALUES (null," + RP_ID +
            "," + locations[i] + "); \n");
        }

        System.err.println(query);
        makeQuery(query);
    }

    /** -- 85 -- 
     * Creates a new RemovedCharacter in the database.  This is no longer
     * used.  This was slow and RemovedCharacters are now reported in a file where
     * the Project that it belongs to is located.
     */
    public static void createRemovedCharacter(int RP_ID, int location) {
        //establishConnection();
        makeQuery("INSERT INTO RemovedCharacter VALUES (null," + RP_ID + "," +
            location + ");");
    }

    /** -- 86 -- 
     * Gets the login name that is associated with a specified Result.
     *
     * @param result_ID The ResultID of the Result to get information from in the
     * database.
     * @return The login name of the user who owns the specified result.
     */
    public static String getLoginFromResult(String result_ID) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Login from Result where Result_ID = '" + result_ID +
                    "';");

            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 87 -- 
     * Makes a query to the database and returns the results.
     *
     * @param statement The SQL query to make in the database.
     * @return The result object containing all the results from the
     * given SQL statement.
     */
    public static ResultSet makeQuery(String statement) {
        if (!isLoaded) {
            loadDriver();
        }

        establishConnection();

        try {
            Statement s = con.createStatement();
            s.execute(statement);

            ResultSet rs = s.getResultSet();

            return rs;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /** -- 88 -- 
     * Gets a user's email.
     *
     * @param user The login name of the user to get information from in the database.
     * @return The user's email.
     */
    public static String getEmail(String user) throws SQLException {
        ResultSet rs = makeQuery("SELECT Email from Account where Login = '" +
                user + "';");
        byte[] b = null;

        if (rs.next()) {
            b = rs.getBytes(1);
        }

        if (b == null) {
            return null;
        }

        return new String(b);
    }

    /** -- 89 -- 
     * Gets a user's password.
     *
     * @param user The login name of the user to get information from in the database.
     * @return The user's password.
     */
    public static String getPassword(String user) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Password from Account where Login = '" + user +
                    "';");
            byte[] b = null;

            if (rs.next()) {
                b = rs.getBytes(1);
            }

            if (b == null) {
                return null;
            }

            Encrypt e = new Encrypt();
            b = e.decryptData(user, b);

            return byteArrayToString(b);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }


    /** -- 91 -- 
     * Loads the driver that interfaces with the MySQL database.  This can
     * be changed to support other databases, but the SQL code in this class
     * may also need modification depending on the database.
     */
    private static void loadDriver() {
        try {
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
            isLoaded = true;
        } catch (Exception ex) {
            System.err.println("Unable to load Database driver.");
            ex.printStackTrace();
        }
    }

    /** -- 92 -- 
     * Creates an active connection to the database to be used.
     */
    private static void establishConnection() {
        try {
            if (con == null) {
                con = DriverManager.getConnection(Config.getDatabaseURL(),
                        Config.getDatabaseUserName(),
                        Config.getDatabasePassword());

                if (con == null) {
                    System.err.println("Unable to connect to database.");
                }
            }

            if (con.isClosed() == true) {
                con = DriverManager.getConnection(Config.getDatabaseURL(),
                        Config.getDatabaseUserName(),
                        Config.getDatabasePassword());

                if (con == null) {
                    System.err.println("Unable to connect to database.");
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /** -- 93 -- 
     * Calculates the current date and time from System.currentTimeMillis() and
     * DateFormat.getDateInstance().format().
     *
     * @return The date in the following format
     * "<day>, <month> <date>, <year> <hour>:<minute>:<second> PST" if you are
     * in a different region change the PST to your region.
     */
    public static String getDate() {
        Date date = new Date(System.currentTimeMillis());
        String time = DateFormat.getDateInstance(DateFormat.FULL).format(date);
        long millis = System.currentTimeMillis();
        millis /= 1000;

        long seconds = (millis % 60);
        millis /= 60;

        long minutes = (millis % 60);
        millis /= 60;

        long hours = (millis % 24) - 7;

        if (hours < 0) {
            hours += 24;
        }

        if (hours < 10) {
            time += (" 0" + hours);
        } else {
            time += (" " + hours);
        }

        if (minutes < 10) {
            time += (":0" + minutes);
        } else {
            time += (":" + minutes);
        }

        if (seconds < 10) {
            time += (":0" + seconds);
        } else {
            time += (":" + seconds);
        }

        time += " PST";

        return time;
    }

/** -- 10 -- 9 lines
     * DOCUMENT ME!
     *
     * @param Result_ID DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static boolean setResultComplete(String Result_ID) {
        try {
            makeQuery("UPDATE Result SET complete = 1 WHERE Result_ID = '" +
                Result_ID + "';");

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

 /** -- 20 -- 
     * Get all Result IDs for a given account from the database.
     *
     * @param login The login name of the account to get Result IDs from.
     * @return A String[] containing all the Result IDs from the specified account.
     */
    public static String[] getResultIDs(String login) {
        return getStringArray("SELECT Result_ID FROM Result WHERE Login='" +
            login + "';");
    }


 //  added by Xin Chen

    /** -- 30 -- 
     * Executes a database query and processes all data as doubles. This mehtod is used
     * as a generic sub method for any method that makes database call that returns a
     * list of doubles.
     *
     * @param s The database query to execute.
     * @return The results of the database query in an int[].
     */
    public static double[] getDoubleArray(String s) { // xin

        //establishConnection();
        ResultSet rs = makeQuery(s);
        Vector v = new Vector();

        try {
            while (rs.next()) {
                Double i = new Double(rs.getDouble(1));
                v.addElement(i);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        double[] returnArray = new double[v.size()];

        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = ((Double) v.elementAt(i)).doubleValue();
        }

        return returnArray;
    }
     // xin


    /** -- 40 -- 
     * Gets the last token from a specified path.  This method scans the input String
     * from the end towards the beginning until it finds a '/' character then returns
     * all characters as a String after the '/' character.
     *
     * @param s The path to get the last token from.
     * @return The last token in the specified path.
     */
    private static String getEnd(String s) {
        int i;

        for (i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == '/') {
                break;
            }
        }

        return s.substring(i + 1);
    }
  /** -- 50 -- 
     * Gets the list of all FlaggedPairs for a specified Result from the database.
     *
     * @param resultID The ID of the Result to get information from.
     * @return A String[] of the FlaggedPairs for the specified Result.
     */
    public static String[] getFlaggedPairs(String resultID) {
        String[] file1 = getStringArray(
                "SELECT File1 FROM Pair WHERE Result_ID='" + resultID +
                "' && Is_Flagged = 1;");
        String[] file2 = getStringArray(
                "SELECT File2 FROM Pair WHERE Result_ID='" + resultID +
                "' && Is_Flagged = 1;");
        String[] name1 = new String[file1.length];
        String[] name2 = new String[file2.length];

        if ((file1.length <= 0) || (file2.length <= 0)) {
            return null;
        }

        for (int i = 0; i < file1.length; i++) {
            for (int i2 = file1[i].length() - 1; i2 >= 0; i2--) {
                if (file1[i].charAt(i2) == '/') {
                    name1[i] = file1[i].substring(i2 + 1);

                    break;
                }
            }
        }

        for (int i = 0; i < file2.length; i++) {
            for (int i2 = file2[i].length() - 1; i2 >= 0; i2--) {
                if (file2[i].charAt(i2) == '/') {
                    name2[i] = file2[i].substring(i2 + 1);

                    break;
                }
            }
        }

        String[] returnArray = new String[name1.length + name2.length];

        for (int i = 0; i < name1.length; i++) {
            returnArray[i * 2] = name1[i];
            returnArray[(i * 2) + 1] = name2[i];
        }

        return returnArray;
    }

/** -- 60 -- 
     * Checks to see if a specified account has administration access.
     *
     * @param login The login of the Account to check.
     * @return True if account has administration access otherwise false.
     */
    public static boolean isAdmin(String login) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Is_Admin FROM Account WHERE Login = '" + login +
                    "';");
            rs.next();

            if (rs.getByte("Is_Admin") != 0) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /** -- 70 -- 
     * Deletes the specified SimilaritySection from the database.
     *
     * @param SS_ID the SimilaritySection to delete.
     */
    private static void deleteSimilaritySection(int SS_ID) {
        makeQuery("DELETE FROM SimilaritySection where SS_ID = '" + SS_ID +
            "';");
    }

 /** -- 80 -- 
     * Creates a new cluster in the database.
     *
     * @param Cluster_Num The cluster number to associate with this database entry.
     * @param RP_ID The ResultProjectID that the Cluster belongs to.
     * @param Result_ID The ResultID of the Result that is being added to this cluster.
     */
    public static void createCluster(int Cluster_Num, int RP_ID,
        String Result_ID) {
        //establishConnection();
        makeQuery("INSERT INTO Cluster VALUES (null," + Cluster_Num + "," +
            RP_ID + ",'" + Result_ID + "');");
    }


    /** -- 90 -- 
     * Get the login name of the user from a specified SessionID.
     *
     * @param session_ID The SessionID of the Session to get information about
     * from the database.
     * @return The login name of the user from the specified SessionID.
     */
    public static String getLoginFromSession(String session_ID) {
        //establishConnection();
        try {
            ResultSet rs = makeQuery(
                    "SELECT Login from Session where Session_ID = '" +
                    session_ID + "';");

            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
