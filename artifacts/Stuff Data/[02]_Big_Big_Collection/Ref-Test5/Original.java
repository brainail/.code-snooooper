import java.io.*;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision$
 */
public class NewFileTransferServlet extends HttpServlet {
    public static int FALSE = 0;
    public static int TRUE = 1;
    public static int EMAIL = 2;
    public static int PREGROUPED = 3;
    public static int NUM_SIMILAR_FLAG = 4;
    public static int NUM_LINES_SIMILAR = 5;
    public static int GROUP_SIZE = 6;
    String baseDir;
    String compareDir;
    int toughness = 5;
    int flagAmmount = 75;
    int groupSize = 7;
    boolean email = true;
    boolean pregrouped = true;
    String sourceEmail;
    String subjectEmail = "Processing of your submission complete";
    String RESULT_ID;

    /**
     * Creates a new NewFileTransferServlet object.
     */
    public NewFileTransferServlet() {
        baseDir = Config.getSubmittedFilesPath();
        compareDir = Config.getNewCompareBinPath();
        sourceEmail = Config.getSourceEmail();
    }

    /**
     * DOCUMENT ME!
     *
     * @param req DOCUMENT ME!
     * @param resp DOCUMENT ME!
     *
     * @throws ServletException DOCUMENT ME!
     * @throws IOException DOCUMENT ME!
     */
    public void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        boolean isAuthentic = false;
        HttpSession session = req.getSession(true);
        String user = (String) session.getValue("login");
        ObjectInputStream objectIn = new ObjectInputStream(req.getInputStream());
        resp.setContentType("application/octet-stream");

        DataOutputStream percentOut = new DataOutputStream(resp.getOutputStream());

        /******************
         **if user == null then user is calling from
         **file transfer application and login needs
         **to be validated.
         *******************/
        if (user == null) {
            String password = "";

            try {
                user = objectIn.readUTF();
                password = objectIn.readUTF();

                String tempPass = DataBaseUtility.getPassword(user);

                if (tempPass == null) {
                    percentOut.writeByte(-2);
                    percentOut.flush();
                    percentOut.close();

                    return;
                }

                if (tempPass.startsWith(password) &&
                        tempPass.endsWith(password)) {
                    try {
                        DataBaseUtility.logInUser(user);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    percentOut.writeByte(-2);
                    percentOut.flush();
                    percentOut.close();
                    objectIn.close();

                    return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (DataBaseUtility.isLoggedIn(user)) {
            isAuthentic = true;
        }

        if (isAuthentic == false) {
            try {
                session.putValue("message",
                    "Your Session Has Expired. Please Login Again.");
                resp.sendRedirect(Config.getBaseServletURL() + "LoginPage");

                return;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        long time_begin = System.currentTimeMillis();

        DataBaseUtility.newAccountActivity(user);

        String errorLocation = "NewFileTranferServlet.java ";

        try {
            System.err.println(errorLocation + "File Transfer Called");

            //            ObjectInputStream objectIn = new ObjectInputStream(req.getInputStream());
            //            resp.setContentType("application/octet-stream");
            Submission submission = new Submission();
            System.err.println(errorLocation + "Begin Reading Object");

            try {
                submission = (Submission) objectIn.readObject();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }

            System.err.println(errorLocation + "End Reading Object");

            int[] options = submission.getOptions();
            Vector projects = submission.getProjects();

            ////process initial header info here
            System.err.println(errorLocation + "Options: " + options.length);

            for (int i = 0; i < (options.length / 2); i++) {
                switch (options[i * 2]) {
                case 2:
                    System.err.println("2 " + options[(i * 2) + 1]);

                    if (options[(i * 2) + 1] == TRUE) {
                        email = true;
                    } else {
                        email = false;
                    }

                    break;

                case 3:
                    System.err.println("3 " + options[(i * 2) + 1]);

                    if (options[(i * 2) + 1] == TRUE) {
                        pregrouped = true;
                    } else {
                        pregrouped = false;
                    }

                    break;

                case 4:
                    System.err.println("4 " + options[(i * 2) + 1]);
                    flagAmmount = options[(i * 2) + 1];

                    break;

                case 5:
                    System.err.println("5 " + options[(i * 2) + 1]);
                    toughness = options[(i * 2) + 1];

                    break;

                case 6:
                    System.err.println("6 " + options[(i * 2) + 1]);
                    groupSize = options[(i * 2) + 1];

                    break;
                }
            }

            System.err.println(errorLocation + "End Options");

            int numOfProjects = projects.size();
            System.err.println(errorLocation + "NumOfProjects:" +
                numOfProjects);

            if (numOfProjects < 1) {
                percentOut.writeByte(-3);
                percentOut.flush();
                percentOut.close();

                return;
            }

            if (DataBaseUtility.doesDirectoryExist(baseDir + user) == false) {
                DataBaseUtility.createDirectory(baseDir + user);
            }

            String result_ID = DataBaseUtility.createResult(user);
            RESULT_ID = result_ID;
            DataBaseUtility.setPercentComplete(result_ID, -33);
            percentOut.writeByte(-1);
            percentOut.writeUTF(result_ID);
            percentOut.flush();
            percentOut.close();

            if (DataBaseUtility.doesDirectoryExist(baseDir + user + "/" +
                        result_ID) == false) {
                DataBaseUtility.createDirectory(baseDir + user + "/" +
                    result_ID);
            }

            session.putValue("Result_ID", result_ID);

            String resultDir = baseDir + user + "/" + result_ID + "/";

            String[] projectNames = new String[numOfProjects];

            //projectLengths now refers to the number of files in a project
            int[] projectLengths = new int[numOfProjects];
            int[] resultProjectIDs = new int[numOfProjects];

            System.err.println(errorLocation + "Reading project names");
            DataBaseUtility.setPercentComplete(result_ID, -30);

            for (int i = 0; i < numOfProjects; i++) {
                projectNames[i] = ((Project) projects.elementAt(i)).getName();

                //??                projectLengths[i] = in.readInt();
                if (DataBaseUtility.doesDirectoryExist(resultDir +
                            projectNames[i]) == false) {
                    DataBaseUtility.createDirectory(resultDir +
                        projectNames[i]);
                }

                //store RPID in resultProjectIDs for use later in creating result files
                resultProjectIDs[i] = DataBaseUtility.createResultProject(user +
                        "/" + result_ID + "/" + projectNames[i], result_ID, 0);

                //no longer need to create a result file concat.cmp
                //DataBaseUtility.createResultFile(resultDir + projectNames[i] + "/" + "/concat.cmp", ResultProjectIDs[i]);
            }

            boolean[] parseOk = new boolean[numOfProjects];
            JavaParserMain javaParser;
            CPPParserMain cppParser;
            System.err.println(errorLocation + "Reading files");

            int numOfValidProjects = numOfProjects; // xin (June 6)

            DataOutputStream compHeadOutputStream = new DataOutputStream(new BufferedOutputStream(
                        new FileOutputStream(resultDir + "/compHeadFile")));

            // write db_name
            compHeadOutputStream.writeBytes(
                "jdbc:mysql://dna.cs.ucsb.edu/test\n");

            // write result_ID
            compHeadOutputStream.writeBytes(result_ID + "\n");
            DataBaseUtility.setPercentComplete(result_ID, -35);

            for (int i = 0; i < numOfProjects; i++) {
                ((Project) projects.elementAt(i)).writeFiles(resultDir +
                    projectNames[i] + "/", "concat.cmp");

                /////////////////////////////////////////////////////////////////////
                /////////run parser on files
                /////////////////////////////////////////////////////////////////////
                File f = new File(resultDir + projectNames[i] +
                        "/concat.cmp.prs");
                File f2 = new File(resultDir + projectNames[i] +
                        "/tokenposfile.txt");
                FileOutputStream concatFOut = new FileOutputStream(f);
                FileOutputStream tokenFOut = new FileOutputStream(f2);
                DataOutputStream concatOut = new DataOutputStream(concatFOut);
                DataOutputStream tokenOut = new DataOutputStream(tokenFOut);

                //String[] arguments = new String[3];
                //arguments[0] = resultDir + projectNames[i] + "/concat.cmp";
                //arguments[1] = resultDir + projectNames[i] + "/concat.cmp.prs";
                //arguments[2] = resultDir + projectNames[i] + "/tokenposfile.txt";
                boolean parseOK = true;
                Project p = ((Project) projects.elementAt(i));

                for (int j = 0; j < p.length(); j++) {
                    try {
                        System.err.println(" beginning parsing: " +
                            projectNames[i]);

                        if (pregrouped) {
                            cppParser = new CPPParserMain(resultDir +
                                    projectNames[i] + "/file" + j, concatOut,
                                    tokenOut, j);

                            if (!cppParser.isParseOK()) {
                                parseOK = false;

                                break;
                            }
                        } else {
                            javaParser = new JavaParserMain(resultDir +
                                    projectNames[i] + "/file" + j, concatOut,
                                    tokenOut, j);

                            if (!javaParser.isParseOK()) {
                                System.err.println(projectNames[i] +
                                    " can't be successfully parsed!");
                                parseOK = false;

                                break;
                            }
                        }

                        //System.err.println(" ending parsing: " + projectNames[i]);
                        //parseOk[i] = DataBaseUtility.setResultProjectCompile(user + "/" + result_ID + "/" + projectNames[i], 1);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                 //end for

                if (parseOK) {
                    parseOk[i] = DataBaseUtility.setResultProjectCompile(user +
                            "/" + result_ID + "/" + projectNames[i], 1);
                } else {
                    parseOk[i] = DataBaseUtility.setResultProjectCompile(user +
                            "/" + result_ID + "/" + projectNames[i], 0);
                    parseOk[i] = false; // xin
                    numOfValidProjects--; // xin (June 6)
                }

                concatOut.flush();
                concatOut.close();
                tokenOut.flush();
                tokenOut.close();

                //calculates the aprox. percentage of parsing and saving complete
                float percentParsed = (float) 0.0;
                percentParsed = (percentParsed + i + 1) / numOfProjects;
                percentParsed = 65 * percentParsed;
                DataBaseUtility.setPercentComplete(result_ID,
                    (-35 - ((int) percentParsed)));
            }
             //end for

            // write numOfValidProjects
            compHeadOutputStream.writeBytes(Integer.toString(numOfValidProjects)); // xin (June 6)
            compHeadOutputStream.writeBytes("\n");

            System.err.println(errorLocation +
                "Finished saving and parsing files");
            DataBaseUtility.setPercentComplete(result_ID, -100);

            if (numOfValidProjects < 2) {
                DataBaseUtility.setPercentComplete(result_ID, 100);
            } else {
                CommandPrompt commandPrompt = new CommandPrompt();

                // write filenames
                String[] projectLocations = new String[numOfProjects]; // xin (June 6)

                for (int i = 0; i < numOfProjects; i++) {
                    if (parseOk[i]) {
                        projectLocations[i] = resultDir + projectNames[i];
                        compHeadOutputStream.writeBytes(projectLocations[i] +
                            "\n"); // xin (June 6)
                    }
                }

                compHeadOutputStream.flush();
                compHeadOutputStream.close();

                // run comparison
                System.err.println(errorLocation + "Starting comparison");
                System.err.println(compareDir + " " + resultDir +
                    "/compHeadFile" + " -m " + toughness);
                DataBaseUtility.setPercentComplete(result_ID, 0);
                commandPrompt.run(compareDir + " " + resultDir +
                    "/compHeadFile" + " -m " + toughness);
            }

            System.err.println(errorLocation + "Finished comparison");
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataBaseUtility.setResultComplete(RESULT_ID);
        System.err.println(errorLocation + "End file comparison");

        long time_end = System.currentTimeMillis();
        long time_duration = time_end - time_begin;
        System.err.println("time cost: " + time_duration + " milliseconds.");

        try {
            if (email) {
                String tempString = DataBaseUtility.getResultDate(RESULT_ID);
                String encodedString = "";

                for (int i = 0; i < tempString.length(); i++) {
                    if (tempString.charAt(i) == ' ') {
                        encodedString += "%20";
                    } else {
                        encodedString += tempString.charAt(i);
                    }
                }

                encodedString += ("&login=" + user);

                String body =
                    "Your submission has been processed and you may view the results by clicking on the following " +
                    "link <a HREF=\"" + Config.getBaseServletURL() +
                    "NewResultViewerServlet?ResultID=" + encodedString +
                    "\" target=\"_top\">results</a> or by logging into your account and " +
                    "selecting the result by its date.  Thank you for using our plagerism dection " +
                    "software based on SID technology";
                EmailSender es = new EmailSender();
                es.sendEmail(DataBaseUtility.getEmail(user), sourceEmail,
                    subjectEmail, body);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    synchronized void timeOut(long delay) {
        try {
            wait(delay);
        } catch (InterruptedException e) {
        }
    }
}
