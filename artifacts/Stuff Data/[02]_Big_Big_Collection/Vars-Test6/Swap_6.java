import java.io.*;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;



public class NewFileTransferServlet extends HttpServlet {
    public static int FALSE = 0;               //1
    public static int TRUE = 1;                //2
    public static int EMAIL = 2;               //3
    public static int PREGROUPED = 3;          //4
    public static int NUM_SIMILAR_FLAG = 4;    //5
    public static int NUM_LINES_SIMILAR = 5;   //6
    public static int GROUP_SIZE = 6;          //7 
    String compareDir;                         //9 ------------------------------------
    
	String baseDir;                            //8
    int toughness = 5;                         //10
    int flagAmmount = 75;                      //11
    int groupSize = 7;                         //12
    boolean email = true;                      //13
    boolean pregrouped = true;                 //14
    String sourceEmail;                        //15
    String subjectEmail = "Processing of your submission complete";  //16
    String RESULT_ID;                                                //17

    
    public NewFileTransferServlet() {
        baseDir = Config.getSubmittedFilesPath();
        compareDir = Config.getNewCompareBinPath();
        sourceEmail = Config.getSourceEmail();
    }

    
    public void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        boolean isAuthentic = false;              //18
        HttpSession session = req.getSession(true);  //19
        String user = (String) session.getValue("login");   //20
        ObjectInputStream objectIn = new ObjectInputStream(req.getInputStream());  //21
        resp.setContentType("application/octet-stream");

        DataOutputStream percentOut = new DataOutputStream(resp.getOutputStream());  //22

        
        if (user == null) {
            String password = "";   //23

            try {
                user = objectIn.readUTF();
                password = objectIn.readUTF();
                String tempPass = DataBaseUtility.getPassword(user);  //24

                if (tempPass == null) {
                    percentOut.writeByte(-2);
                    percentOut.flush();
                    percentOut.close();
                    return;
                }

                if (tempPass.startsWith(password) &&
                        tempPass.endsWith(password)) {
                    try {
                        DataBaseUtility.logInUser(user);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    percentOut.writeByte(-2);
                    percentOut.flush();
                    percentOut.close();
                    objectIn.close();
                    return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (DataBaseUtility.isLoggedIn(user)) {
            isAuthentic = true;
        }

        if (isAuthentic == false) {
            try {
                session.putValue("message", "Your Session Has Expired. Please Login Again.");
                resp.sendRedirect(Config.getBaseServletURL() + "LoginPage");
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        long time_begin = System.currentTimeMillis();  //25

        DataBaseUtility.newAccountActivity(user);

        String errorLocation = "NewFileTranferServlet.java ";  //26

        try {
            System.err.println(errorLocation + "File Transfer Called");
            Submission submission = new Submission();       //27
            System.err.println(errorLocation + "Begin Reading Object");

            try {
                submission = (Submission) objectIn.readObject();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }

            System.err.println(errorLocation + "End Reading Object");

            Vector projects = submission.getProjects();   //29 ------------------------
            int[] options = submission.getOptions();      //28

            
            System.err.println(errorLocation + "Options: " + options.length);

            for (int i = 0; i < (options.length / 2); i++) {
                switch (options[i * 2]) {
                case 2:
                    System.err.println("2 " + options[(i * 2) + 1]);
                    if (options[(i * 2) + 1] == TRUE) {
                        email = true;
                    } else {
                        email = false;
                    }
                    break;
                case 3:
                    System.err.println("3 " + options[(i * 2) + 1]);
                    if (options[(i * 2) + 1] == TRUE) {
                        pregrouped = true;
                    } else {
                        pregrouped = false;
                    }
                    break;
                case 4:
                    System.err.println("4 " + options[(i * 2) + 1]);
                    flagAmmount = options[(i * 2) + 1];
                    break;
                case 5:
                    System.err.println("5 " + options[(i * 2) + 1]);
                    toughness = options[(i * 2) + 1];
                    break;
                case 6:
                    System.err.println("6 " + options[(i * 2) + 1]);
                    groupSize = options[(i * 2) + 1];
                    break;
                }
            }

            System.err.println(errorLocation + "End Options");
            int numOfProjects = projects.size();   //30
            System.err.println(errorLocation + "NumOfProjects:" + numOfProjects);

            if (numOfProjects < 1) {
                percentOut.writeByte(-3);
                percentOut.flush();
                percentOut.close();
                return;
            }

            if (DataBaseUtility.doesDirectoryExist(baseDir + user) == false) {
                DataBaseUtility.createDirectory(baseDir + user);
            }

            String result_ID = DataBaseUtility.createResult(user);    //31
            RESULT_ID = result_ID;
            DataBaseUtility.setPercentComplete(result_ID, -33);
            percentOut.writeByte(-1);
            percentOut.writeUTF(result_ID);
            percentOut.flush();
            percentOut.close();

            if (DataBaseUtility.doesDirectoryExist(baseDir + user + "/" + result_ID) == false) {
                DataBaseUtility.createDirectory(baseDir + user + "/" + result_ID);
            }

            session.putValue("Result_ID", result_ID);
            String[] projectNames = new String[numOfProjects];           //33 ------------------
            
            String resultDir = baseDir + user + "/" + result_ID + "/";   //32

            int[] projectLengths = new int[numOfProjects];               //34
            int[] resultProjectIDs = new int[numOfProjects];

            System.err.println(errorLocation + "Reading project names");
            DataBaseUtility.setPercentComplete(result_ID, -30);

            for (int i = 0; i < numOfProjects; i++) {
                projectNames[i] = ((Project) projects.elementAt(i)).getName();

                
                if (DataBaseUtility.doesDirectoryExist(resultDir +
                            projectNames[i]) == false) {
                    DataBaseUtility.createDirectory(resultDir +
                        projectNames[i]);
                }

                
                resultProjectIDs[i] = DataBaseUtility.createResultProject(user +
                        "/" + result_ID + "/" + projectNames[i], result_ID, 0);

                
                
            }

            boolean[] parseOk = new boolean[numOfProjects];   //35
            CPPParserMain cppParser;      //37  ------------------------------
			JavaParserMain javaParser;    //36
          
            System.err.println(errorLocation + "Reading files");

            int numOfValidProjects = numOfProjects;     //38

            DataOutputStream compHeadOutputStream = new DataOutputStream(new BufferedOutputStream(
                        new FileOutputStream(resultDir + "/compHeadFile")));   //39

            compHeadOutputStream.writeBytes("jdbc:mysql://dna.cs.ucsb.edu/test\n");
            compHeadOutputStream.writeBytes(result_ID + "\n");
            DataBaseUtility.setPercentComplete(result_ID, -35);

            for (int i = 0; i < numOfProjects; i++) {
                ((Project) projects.elementAt(i)).writeFiles(resultDir +
                    projectNames[i] + "/", "concat.cmp");

				File f2 = new File(resultDir + projectNames[i] +                        "/tokenposfile.txt");  //41
                File f = new File(resultDir + projectNames[i] +                        "/concat.cmp.prs");   //40  ----------------
              
				FileOutputStream concatFOut = new FileOutputStream(f);  //42
                FileOutputStream tokenFOut = new FileOutputStream(f2);  //43
                DataOutputStream tokenOut = new DataOutputStream(tokenFOut);    //45 ------------------

				DataOutputStream concatOut = new DataOutputStream(concatFOut);  //44


                Project p = ((Project) projects.elementAt(i));  //47 ------------------------------
                boolean parseOK = true;    //46

                for (int j = 0; j < p.length(); j++) {
                    try {
                        System.err.println(" beginning parsing: " +                            projectNames[i]);
                        if (pregrouped) {
                            cppParser = new CPPParserMain(resultDir + projectNames[i] + "/file" + j, concatOut, tokenOut, j);
                            if (!cppParser.isParseOK()) {
                                parseOK = false;
                                break;
                            }
                        } else {
                            javaParser = new JavaParserMain(resultDir + projectNames[i] + "/file" + j, concatOut, tokenOut, j);
                            if (!javaParser.isParseOK()) {
                                System.err.println(projectNames[i] +                                     " can't be successfully parsed!");
                                parseOK = false;
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                 
                if (parseOK) {
                    parseOk[i] = DataBaseUtility.setResultProjectCompile(user + "/" + result_ID + "/" + projectNames[i], 1);
                } else {
                    parseOk[i] = DataBaseUtility.setResultProjectCompile(user + "/" + result_ID + "/" + projectNames[i], 0);
                    parseOk[i] = false; 
                    numOfValidProjects--; 
                }
                concatOut.flush();
                concatOut.close();
                tokenOut.flush();
                tokenOut.close();
                
                float percentParsed = (float) 0.0;  //48
                percentParsed = (percentParsed + i + 1) / numOfProjects;
                percentParsed = 65 * percentParsed;
                DataBaseUtility.setPercentComplete(result_ID,
                    (-35 - ((int) percentParsed)));
            }
             
            compHeadOutputStream.writeBytes(Integer.toString(numOfValidProjects)); 
            compHeadOutputStream.writeBytes("\n");

            System.err.println(errorLocation +
                "Finished saving and parsing files");
            DataBaseUtility.setPercentComplete(result_ID, -100);

            if (numOfValidProjects < 2) {
                DataBaseUtility.setPercentComplete(result_ID, 100);
            } else {
                
				
                String[] projectLocations = new String[numOfProjects];   //50 ------------------------
				CommandPrompt commandPrompt = new CommandPrompt();   //49

                

                for (int i = 0; i < numOfProjects; i++) {
                    if (parseOk[i]) {
                        projectLocations[i] = resultDir + projectNames[i];
                        compHeadOutputStream.writeBytes(projectLocations[i] +
                            "\n"); 
                    }
                }

                compHeadOutputStream.flush();
                compHeadOutputStream.close();

                
                System.err.println(errorLocation + "Starting comparison");
                System.err.println(compareDir + " " + resultDir +
                    "/compHeadFile" + " -m " + toughness);
                DataBaseUtility.setPercentComplete(result_ID, 0);
                commandPrompt.run(compareDir + " " + resultDir +
                    "/compHeadFile" + " -m " + toughness);
            }

            System.err.println(errorLocation + "Finished comparison");
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataBaseUtility.setResultComplete(RESULT_ID);
        System.err.println(errorLocation + "End file comparison");

        long time_end = System.currentTimeMillis();   //51
        long time_duration = time_end - time_begin;   //52
        System.err.println("time cost: " + time_duration + " milliseconds.");

        try {
            if (email) {

				String encodedString = "";                                     //54 ----------------
				String tempString = DataBaseUtility.getResultDate(RESULT_ID);  //53

                for (int i = 0; i < tempString.length(); i++) {
                    if (tempString.charAt(i) == ' ') {
                        encodedString += "%20";
                    } else {
                        encodedString += tempString.charAt(i);
                    }
                }

                encodedString += ("&login=" + user);
 EmailSender es = new EmailSender();  //56 -----------------------------------
                String body =
                    "Your submission has been processed and you may view the results by clicking on the following " +
                    "link <a HREF=\"" + Config.getBaseServletURL() +
                    "NewResultViewerServlet?ResultID=" + encodedString +
                    "\" target=\"_top\">results</a> or by logging into your account and " +
                    "selecting the result by its date.  Thank you for using our plagerism dection " +
                    "software based on SID technology";   //55
               
                es.sendEmail(DataBaseUtility.getEmail(user), sourceEmail, subjectEmail, body);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    synchronized void timeOut(long delay) {
        try {
            wait(delay);
        } catch (InterruptedException e) {
        }
    }
}
