/**
 * BSD-style license; for more info see http://pmd.sourceforge.net/license.html
 */
package net.sourceforge.pmd.cpd;

import com.alee.extended.breadcrumb.WebFileBreadcrumb;
import com.alee.extended.button.WebSwitch;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.splitpane.WebSplitPane;
import com.alee.laf.text.WebTextField;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.utils.FileUtils;

import net.sourceforge.pmd.cpd.LanguageStuff.LanguageConfig;

import org.apache.commons.io.IOUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class GUI implements CPDListener {

    // TODO#somewhen: It's created More for work then for competitions
    // TODO#somewhen: compare (for instance) java with c++

    // http://pmd.sourceforge.net/pmd-5.2.3/usage/cpd-usage.html
    // TODO: analyze Bsu.IRunner (database, result) & compare with this one

    //
    // +------------------------------------------------------------+
    // | UI stuff/components                                        |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private WebSpinner mCpdMinLength = new WebSpinner();
    private JTextField mFileEncoding = new JTextField(System.getProperty("file.encoding"));
    private WebFileBreadcrumb mSourceDir = new WebFileBreadcrumb(FileUtils.getUserHomePath());

    private JTextField mTimer = new JTextField(6);
    private JLabel mPhase = new JLabel();
    private WebProgressBar mTokenizingProgress = new WebProgressBar();
    private JButton mRunCpdBtn;
    private JButton mCancelCpdBtn;
    private JPanel mProgressPanel;

    private WebSwitch mSubDirsFlag = new WebSwitch(true);
    private WebSwitch mIgnoreIdentifiersFlag = new WebSwitch(false);
    private WebSwitch mIgnoreLiteralsFlag = new WebSwitch(false);
    private WebSwitch mIgnoreAnnotationsFlag = new WebSwitch(false);
    private WebSwitch mSkipLexicalErrorsFlag = new WebSwitch(false);
    private WebSwitch mSkipSubmatchesFlag = new WebSwitch(false);
    private boolean mTrimLeadingWhitespaceMenuFlag = false;

    private JComboBox mLanguages = new JComboBox();
    private JTextField mExtensions = new JTextField();
    private JLabel mExtensionsLabel = new JLabel("Extension: ", SwingConstants.RIGHT);

    private JTable mResultsTable = new JTable() {
        @Override
        public Dimension getPreferredScrollableViewportSize() {
            int headerHeight = mResultsTable.getTableHeader().getPreferredSize().height;
            int height = headerHeight + (10 * getRowHeight());
            int width = getPreferredSize().width;
            return new Dimension(width, height);
        }
    };

    private JFrame mFrame;
    private List<Match> mCpdMatches = new ArrayList<Match>();

    //
    // +------------------------------------------------------------+
    // | Action listeners                                           |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private static class CancelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    private class GoListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            new Thread(new Runnable() {
                public void run() {
                    // mTokenizingProgress.setValue(0);
                    mTokenizingProgress.setIndeterminate(true);
                    mTokenizingProgress.setStringPainted(true);
                    mTokenizingProgress.setString("Please wait ...");
                    mLeftCodePanel.setBorder(BorderFactory.createEmptyBorder());
                    TooltipManager.removeTooltips(mLeftCodePanel);
                    mLeftCodeText.removeAllLineHighlights();
                    mLeftCodeText.setText("");
                    mRightCodePanel.setBorder(BorderFactory.createEmptyBorder());
                    TooltipManager.removeTooltips(mRightCodePanel);
                    mRightCodeText.removeAllLineHighlights();
                    mRightCodeText.setText("");
                    mPhase.setText("");
                    mTimer.setText("");
                    go();
                }
            }).start();
        }
    }

    private class SaveListener implements ActionListener {

        final Renderer renderer;

        public SaveListener(Renderer theRenderer) {
            renderer = theRenderer;
        }

        public void actionPerformed(ActionEvent evt) {
            JFileChooser fcSave = new JFileChooser();
            int ret = fcSave.showSaveDialog(GUI.this.mFrame);
            File f = fcSave.getSelectedFile();
            if (f == null || ret != JFileChooser.APPROVE_OPTION) {
                return;
            }

            if (!f.canWrite()) {
                PrintWriter pw = null;
                try {
                    pw = new PrintWriter(new FileOutputStream(f));
                    pw.write(renderer.render(mCpdMatches.iterator()));
                    pw.flush();
                    JOptionPane.showMessageDialog(mFrame, "Saved " + mCpdMatches.size() + " matches");
                } catch (IOException e) {
                    error("Couldn't save file" + f.getAbsolutePath(), e);
                } finally {
                    IOUtils.closeQuietly(pw);
                }
            } else {
                error("Could not write to file " + f.getAbsolutePath(), null);
            }
        }

        private void error(String message, Exception e) {
            if (e != null) {
                e.printStackTrace();
            }

            JOptionPane.showMessageDialog(GUI.this.mFrame, message);
        }

    }

    private class BrowseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser(mSourceDir.getCurrentFile().getAbsolutePath());
            fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            fc.showDialog(mFrame, "Select");
            if (fc.getSelectedFile() != null) {
                mSourceDir.setCurrentFile(fc.getSelectedFile().getAbsolutePath());
            }
        }
    }

    //
    // +------------------------------------------------------------+
    // | Save as ... option                                         |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private void addSaveOptionsTo(JMenu menu) {
        JMenuItem saveItem;

        for (int i = 0; i < GUIStuff.RENDERER_SETS.length; i++) {
            saveItem = new JMenuItem("Save as " + GUIStuff.RENDERER_SETS[i][0]);
            saveItem.addActionListener(new SaveListener((Renderer) GUIStuff.RENDERER_SETS[i][1]));
            menu.add(saveItem);
        }
    }

    //
    // +------------------------------------------------------------+
    // | Main GUI enter                                             |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public GUI() {
        mFrame = new JFrame("✗✗✗ Code Snooper ✓✓✓");

        final Toolkit defToolkit = Toolkit.getDefaultToolkit();
        mFrame.setSize((int) defToolkit.getScreenSize().getWidth(), (int) defToolkit.getScreenSize().getHeight());
        mFrame.setExtendedState(Frame.MAXIMIZED_BOTH);

        mTimer.setEditable(false);

        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic('f');

        addSaveOptionsTo(fileMenu);

        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic('x');
        exitItem.addActionListener(new CancelListener());
        fileMenu.add(exitItem);
        JMenu viewMenu = new JMenu("View");
        fileMenu.setMnemonic('v');

        JMenuItem trimItem = new JCheckBoxMenuItem("Trim leading whitespace");
        trimItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                AbstractButton button = (AbstractButton) e.getItem();
                // GUI.this.mTrimLeadingWhitespaceMenuFlag = button.isSelected();
            }
        });
        // viewMenu.add(trimItem);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        // menuBar.add(viewMenu);
        mFrame.setJMenuBar(menuBar);

        // first make all the buttons
        JButton browseButton = new JButton("Browse");
        browseButton.setMnemonic('b');
        browseButton.addActionListener(new BrowseListener());
        mRunCpdBtn = new JButton("Go");
        mRunCpdBtn.setMnemonic('g');
        mRunCpdBtn.addActionListener(new GoListener());
        mCancelCpdBtn = new JButton("Cancel");
        mCancelCpdBtn.addActionListener(new CancelListener());

        JPanel settingsPanel = makeSettingsPanel(browseButton, mRunCpdBtn, mCancelCpdBtn);
        mProgressPanel = makeProgressPanel();
        JPanel resultsPanel = initResultsPanel();

        adjustLanguageControlsFor((LanguageConfig) LanguageStuff.LANGUAGE_SETS [0][1]);

        mFrame.getContentPane().setLayout(new BorderLayout());
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());

        JPanel mixPanel = new JPanel();
        GridBagHelper helper = new GridBagHelper(mixPanel, new double [] {0.05, 0.7});
        helper.add(settingsPanel);
        helper.add(makeMatchList());

        topPanel.add(mixPanel, BorderLayout.NORTH);
        topPanel.add(mProgressPanel, BorderLayout.CENTER);

        setProgressControls(false);

        mFrame.getContentPane().add(topPanel, BorderLayout.NORTH);
        mFrame.getContentPane().add(resultsPanel, BorderLayout.CENTER);

        mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mFrame.pack();
        mFrame.setVisible(true);
    }

    private void adjustLanguageControlsFor(LanguageConfig current) {
        mIgnoreIdentifiersFlag.setEnabled(current.canIgnoreIdentifiers());
        mIgnoreLiteralsFlag.setEnabled(current.canIgnoreLiterals());
        mIgnoreAnnotationsFlag.setEnabled(current.canIgnoreAnnotations());
        mExtensions.setText(current.extensions()[0]);
        boolean enableExtension = current.extensions()[0].length() == 0;
        mExtensions.setEnabled(enableExtension);
        mExtensionsLabel.setEnabled(enableExtension);
    }

    //
    // +------------------------------------------------------------+
    // | Construct settings panel                                   |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private void checkTokenLimit() {
        try {
            final int value = Integer.parseInt(mCpdMinLength.getValue().toString());
            if (value < Stuff.MINIMUM_CPD_MINIMUM_LENGTH)
                mCpdMinLength.setValue(Stuff.MINIMUM_CPD_MINIMUM_LENGTH);
        } catch (Exception exception) {
            mCpdMinLength.setValue(Stuff.MINIMUM_CPD_MINIMUM_LENGTH);
        }
    }

    private JPanel makeSettingsPanel(JButton browseBtn, JButton goBtn, JButton cancelBtn) {
        JPanel settingsPanel = new JPanel();
        GridBagHelper helper = new GridBagHelper(settingsPanel, new double [] {0.05, 0.35, 0.1, 0.1});

        helper.addLabel("Root source directory: ");

        mSourceDir.setDisplayFileTip(false);
        mSourceDir.setUndecorated(true);
        final WebScrollPane pane = new WebScrollPane(mSourceDir, true, false);
        pane.getWebHorizontalScrollBar().setPreferredSize(new Dimension(0, 7));
        pane.getWebHorizontalScrollBar().setPaintTrack(false);
        pane.getWebHorizontalScrollBar().setPaintButtons(false);
        pane.getWebHorizontalScrollBar().setBorder(null);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        pane.setPreferredWidth(100);
        pane.setPreferredHeight(35);
        helper.add(pane);

        helper.add(browseBtn, 2);
        helper.nextRow();

        // TIP
        GUIStuff.tip(
                mSourceDir,
                "Source path that can include <font color=\"yellow\">sub-directories</font> ...",
                true
        );

        helper.addLabel("Minimum duplicated chunks: ");
        mCpdMinLength.setValue(Stuff.DEFAULT_CPD_MINIMUM_LENGTH);
        mCpdMinLength.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                checkTokenLimit();
            }
        });
        mCpdMinLength.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                checkTokenLimit();
            }

            @Override
            public void focusLost(FocusEvent e) {
                checkTokenLimit();
            }
        });
        helper.add(mCpdMinLength);

        // TIP
        GUIStuff.tip(
                mCpdMinLength,
                "A positive integer indicating the <font color=\"red\">minimum duplicate</font> size.",
                true
        );

        helper.addLabel("Language: ");

        for (int i = 0; i < LanguageStuff.LANGUAGE_SETS.length; i++) {
            mLanguages.addItem(LanguageStuff.LANGUAGE_SETS[i][0]);
        }

        mLanguages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adjustLanguageControlsFor(
                        LanguageStuff.languageConfigFor((String) mLanguages.getSelectedItem())
                );
            }
        });

        helper.add(mLanguages);
        helper.nextRow();

        helper.addLabel("Subdirectories: ");
        helper.add(mSubDirsFlag);

        // TIP
        GUIStuff.tip(
                mSubDirsFlag,
                "If you specify a source directory and want to scan the <font color=\"yellow\">sub-directories</font>.",
                true
        );

        helper.add(mExtensionsLabel);
        helper.add(mExtensions);

        helper.nextRow();
        helper.addLabel("Ignore literals: ");
        helper.add(mIgnoreLiteralsFlag);

        // helper.addLabel("");
        // helper.addLabel("");
        helper.addLabel("Skip lexical errors: ");
        helper.add(mSkipLexicalErrorsFlag);

        helper.nextRow();

        // TIP
        GUIStuff.tip(
                mIgnoreLiteralsFlag,
                "Ignore <font color=\"yellow\">literal value</font> differences when evaluating a duplicate block.",
                true
        );

        helper.nextRow();
        helper.addLabel("Ignore identifiers: ");
        helper.add(mIgnoreIdentifiersFlag);

        // helper.addLabel("");
        // helper.addLabel("");
        helper.addLabel("Don't show submatches: ");
        helper.add(mSkipSubmatchesFlag);

        helper.nextRow();

        // TIP
        GUIStuff.tip(
                mIgnoreIdentifiersFlag,
                "Similar to \"<font color=\"yellow\">ignore literals</font>\" but for <font color=\"yellow\">identifiers</font>; " +
                "i.e., " + "variable names, methods names, and so forth ...",
                true
        );

        helper.nextRow();
        helper.addLabel("Ignore annotations: ");
        helper.add(mIgnoreAnnotationsFlag);

        // helper.addLabel("");
        // helper.addLabel("");

        helper.addLabel("File encoding: ");
        mFileEncoding.setColumns(1);
        helper.add(mFileEncoding);

        helper.nextRow();

        // TIP
        GUIStuff.tip(
                mIgnoreAnnotationsFlag,
                "Ignore <font color=\"yellow\">redundant annotations</font> such as in Java and so forth ...",
                true
        );

        // TIP
        GUIStuff.tip(
                mSkipLexicalErrorsFlag,
                "Skip files which can't be tokenized due to " +
                "<font color=\"red\">invalid characters</font> instead of aborting application.",
                true
        );

        // TIP
        GUIStuff.tip(
                mSkipSubmatchesFlag,
                "Ignore <font color=\"yellow\">submatches</font> in table ...",
                true
        );

        helper.addLabel("");
        helper.addLabel("");
        helper.add(goBtn);
        helper.add(cancelBtn);

        helper.nextRow();

        // TIP
        GUIStuff.tip(
                mFileEncoding,
                "The character set encoding (<font color=\"yellow\">e.g., UTF-8</font>) to use when reading the source code files, " +
                "but also when producing the report." + "<br>If not specified, app uses the <font color=\"yellow\">system default encoding</font>.",
                true
        );

        return settingsPanel;
    }

    //
    // +------------------------------------------------------------+
    // | Construct progress panel                                   |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private JPanel makeProgressPanel() {
        JPanel progressPanel = new JPanel();
        final double[] weights = {0.0, 0.8, 0.4, 0.2};
        GridBagHelper helper = new GridBagHelper(progressPanel, weights);
        helper.addLabel("Tokenizing files:");
        helper.add(mTokenizingProgress, 3);
        helper.nextRow();
        helper.addLabel("Phase:");
        helper.add(mPhase);
        helper.addLabel("Time elapsed:");
        helper.add(mTimer);
        helper.nextRow();
        // progressPanel.setBorder(BorderFactory.createTitledBorder("Progress"));
        return progressPanel;
    }

    //
    // +------------------------------------------------------------+
    // | Construct result panel                                     |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public RSyntaxTextArea mLeftCodeText;
    public RTextScrollPane mLeftCodePane;
    public WebPanel mLeftCodePanel;
    public RSyntaxTextArea mRightCodeText;
    public RTextScrollPane mRightCodePane;
    public WebPanel mRightCodePanel;
    public WebSplitPane mCodePanes;

    private void tuneCodeText(final RSyntaxTextArea codeText) {
        codeText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        codeText.setTabSize(4);
        codeText.setMarkOccurrences(true);
        codeText.setPaintMarkOccurrencesBorder(true);
        codeText.setClearWhitespaceLinesEnabled(false);
        codeText.setAnimateBracketMatching(true);
        codeText.setAntiAliasingEnabled(true);
        codeText.convertTabsToSpaces();
        codeText.setTabsEmulated(true);
        codeText.setAutoIndentEnabled(true);
        codeText.setEditable(false);
        // codeText.setWhitespaceVisible(true);
    }

    private void tuneCodePane(final RTextScrollPane codePane) {
        // Bookmarks
        final URL bookmarkIcon = getClass().getClassLoader().getResource("bookmark.png");
        codePane.getGutter().setBookmarkingEnabled(true);
        codePane.getGutter().setBookmarkIcon(new ImageIcon(bookmarkIcon));
    }

    private void tuneCodeTextTheme(final RSyntaxTextArea codeText) {
        // Theme
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("dark.xml");
        try {
            Theme theme = Theme.load(inputStream);
            theme.apply(codeText);
        } catch (IOException exception) {
            // Do nothing
        } finally {
            try {
                inputStream.close();
            } catch (IOException exception) {
                // Do nothing
            }
        }
    }

    // finaA

    WebTextField searchFieldA = new WebTextField(30);
    final WebButton prevButtonA = new WebButton("<<");
    final WebButton nextButtonA = new WebButton(">>");
    private WebSwitch regexCBAFlag = new WebSwitch(false);
    final WebLabel regexCBA = new WebLabel(".?");
    private WebSwitch matchCaseCBAFlag = new WebSwitch(false);
    final WebLabel matchCaseCBA = new WebLabel("f<>F");

    ActionListener findACallback = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            boolean forward = "FindNext".equals(command);

            SearchContext context = new SearchContext();
            String text = searchFieldA.getText();
            if (text.length() == 0) return;

            context.setSearchFor(text);
            context.setMatchCase(matchCaseCBAFlag.isSelected());
            context.setRegularExpression(regexCBAFlag.isSelected());
            context.setSearchForward(forward);
            context.setWholeWord(false);

            SearchResult result = SearchEngine.find(mLeftCodeText, context);
            if (!result.wasFound()) {
                context.setSearchForward(!forward);
                SearchEngine.find(mLeftCodeText, context);
            }
        }
    };

    // finaB

    WebTextField searchFieldB = new WebTextField(30);
    final WebButton prevButtonB = new WebButton("<<");
    final WebButton nextButtonB = new WebButton(">>");
    private WebSwitch regexCBBFlag = new WebSwitch(false);
    final WebLabel regexCBB = new WebLabel(".?");
    private WebSwitch matchCaseCBBFlag = new WebSwitch(false);
    final WebLabel matchCaseCBB = new WebLabel("f<>F");

    ActionListener findBCallback = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            boolean forward = "FindNext".equals(command);

            SearchContext context = new SearchContext();
            String text = searchFieldB.getText();
            if (text.length() == 0) return;

            context.setSearchFor(text);
            context.setMatchCase(matchCaseCBBFlag.isSelected());
            context.setRegularExpression(regexCBBFlag.isSelected());
            context.setSearchForward(forward);
            context.setWholeWord(false);

            SearchResult result = SearchEngine.find(mRightCodeText, context);
            if (!result.wasFound()) {
                context.setSearchForward(!forward);
                SearchEngine.find(mRightCodeText, context);
            }
        }
    };

    private JPanel initResultsPanel() {
        mLeftCodeText = new RSyntaxTextArea(120, 120);
        tuneCodeTextTheme(mLeftCodeText);
        tuneCodeText(mLeftCodeText);
        mLeftCodePane = new RTextScrollPane(mLeftCodeText);
        mLeftCodePanel = new WebPanel();
        mLeftCodePanel.setLayout(new BorderLayout());
        mLeftCodePanel.add(mLeftCodePane, BorderLayout.CENTER);
        tuneCodePane(mLeftCodePane);

        // >>> Find inside code area #A
        // >>> ...
        // >>> ..
        // >>> .
        // >>>
        WebToolBar toolBarA = new WebToolBar();
        toolBarA.add(searchFieldA);
        nextButtonA.setActionCommand("FindNext");
        nextButtonA.addActionListener(findACallback);
        toolBarA.add(nextButtonA);
        searchFieldA.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) { nextButtonA.doClick(0); }
            @Override
            public void removeUpdate(DocumentEvent e) { nextButtonA.doClick(0); }
            @Override
            public void changedUpdate(DocumentEvent e) { nextButtonA.doClick(0); }
        });
        prevButtonA.setActionCommand("FindPrev");
        prevButtonA.addActionListener(findACallback);
        toolBarA.add(prevButtonA);
        toolBarA.add(regexCBAFlag);
        toolBarA.add(regexCBA);
        toolBarA.add(matchCaseCBAFlag);
        toolBarA.add(matchCaseCBA);
        mLeftCodePanel.add(toolBarA, BorderLayout.NORTH);
        // <<<.
        // <<< .
        // <<< ..
        // <<< ...
        // <<< Find inside code area #A

        mRightCodeText = new RSyntaxTextArea(120, 120);
        tuneCodeTextTheme(mRightCodeText);
        tuneCodeText(mRightCodeText);
        mRightCodePane = new RTextScrollPane(mRightCodeText);
        mRightCodePanel = new WebPanel();
        mRightCodePanel.setLayout(new BorderLayout());
        mRightCodePanel.add(mRightCodePane, BorderLayout.CENTER);
        tuneCodePane(mRightCodePane);

        // >>> Find inside code area #B
        // >>> ...
        // >>> ..
        // >>> .
        // >>>
        WebToolBar toolBarB = new WebToolBar();
        toolBarB.add(searchFieldB);
        nextButtonB.setActionCommand("FindNext");
        nextButtonB.addActionListener(findBCallback);
        toolBarB.add(nextButtonB);
        searchFieldB.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) { nextButtonB.doClick(0); }
            @Override
            public void removeUpdate(DocumentEvent e) { nextButtonB.doClick(0); }
            @Override
            public void changedUpdate(DocumentEvent e) { nextButtonB.doClick(0); }
        });
        prevButtonB.setActionCommand("FindPrev");
        prevButtonB.addActionListener(findBCallback);
        toolBarB.add(prevButtonB);
        toolBarB.add(regexCBBFlag);
        toolBarB.add(regexCBB);
        toolBarB.add(matchCaseCBBFlag);
        toolBarB.add(matchCaseCBB);
        mRightCodePanel.add(toolBarB, BorderLayout.NORTH);
        // <<<.
        // <<< .
        // <<< ..
        // <<< ...
        // <<< Find inside code area #B

        // Split pane
        mCodePanes = new WebSplitPane(JSplitPane.HORIZONTAL_SPLIT, mLeftCodePanel, mRightCodePanel);
        mCodePanes.setOneTouchExpandable(true);
        mCodePanes.setPreferredSize(new Dimension((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(), 100));
        mCodePanes.setDividerLocation((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2);
        mCodePanes.setContinuousLayout(true);

        final JPanel resultsPanel = new JPanel();
        resultsPanel.setLayout(new BorderLayout());
        resultsPanel.add(mCodePanes, BorderLayout.CENTER);

        return resultsPanel;
    }

    private void populateResultPanel() {
        int [] selectionIndices = mResultsTable.getSelectedRows();
        if (1 != selectionIndices.length) return;

        TableModel model = mResultsTable.getModel();
        List<Match> selections = new ArrayList<Match>(selectionIndices.length);

        for (int i = 0; i < selectionIndices.length; i++) {
            selections.add((Match) model.getValueAt(selectionIndices[i], 99));
        }

        // String report = new SimpleRenderer(mTrimLeadingWhitespaceMenuFlag).render(selections.iterator());
        // mLeftCodeText.setText(report);
        // mLeftCodeText.setCaretPosition(0);

        final Match match = selections.get(0);
        final Iterator<TokenEntry> occurrences = match.iterator();
        final Set<Integer> highlightedLinesFirst = new HashSet<Integer>();
        final Set<Integer> highlightedLinesSecond = new HashSet<Integer>();

        final TokenEntry entryFirst = occurrences.next();
        mLeftCodeText.setSyntaxEditingStyle(LanguageStuff.syntaxByCpd(sCurrentConfig));
        mLeftCodeText.setText(entryFirst.formSource(sCurrentConfig));
        mLeftCodeText.removeAllLineHighlights();
        customizeBorder(mLeftCodePanel, entryFirst);
        for (int lineIndex = entryFirst.getBeginLine(); lineIndex < entryFirst.getBeginLine() + match.getLineCount(); ++ lineIndex) {
            if (!highlightedLinesFirst.contains(lineIndex - 1)) {
                highlightedLinesFirst.add(lineIndex - 1);
                highlightLine(mLeftCodeText, lineIndex - 1);
            }
        }
        moveToVisiblePart(mLeftCodeText, mLeftCodePane, entryFirst, match);
        mLeftCodeText.setActiveLineRange(entryFirst.getBeginLine() - 1, entryFirst.getBeginLine() + match.getLineCount() - 2);

        final TokenEntry entrySecond = occurrences.next();
        mRightCodeText.setSyntaxEditingStyle(LanguageStuff.syntaxByCpd(sCurrentConfig));
        mRightCodeText.setText(entrySecond.formSource(sCurrentConfig));
        mRightCodeText.removeAllLineHighlights();
        customizeBorder(mRightCodePanel, entrySecond);
        for (int lineIndex = entrySecond.getBeginLine(); lineIndex < entrySecond.getBeginLine() + match.getLineCount(); ++ lineIndex) {
            if (!highlightedLinesSecond.contains(lineIndex - 1)) {
                highlightedLinesSecond.add(lineIndex - 1);
                highlightLine(mRightCodeText, lineIndex - 1);
            }
        }
        moveToVisiblePart(mRightCodeText, mRightCodePane, entrySecond, match);
        mRightCodeText.setActiveLineRange(entrySecond.getBeginLine() - 1, entrySecond.getBeginLine() + match.getLineCount() - 2);

        // Highlight sub-matches
        for (final Match subMatch : sSourceToMatchMapping.get(MatchStuff.formSubMatchKey(entryFirst, entrySecond))) {
            // Check if match objects are equal
            if (subMatch == match) continue;

            final Iterator<TokenEntry> subOccurrences = subMatch.iterator();

            final TokenEntry subEntryFirst = subOccurrences.next();
            for (int lineIndex = subEntryFirst.getBeginLine(); lineIndex < subEntryFirst.getBeginLine() + subMatch.getLineCount(); ++ lineIndex) {
                if (!highlightedLinesFirst.contains(lineIndex - 1)) {
                    highlightedLinesFirst.add(lineIndex - 1);
                    highlightLine(mLeftCodeText, lineIndex - 1, new Color(200, 0, 113, 100));
                }
            }

            final TokenEntry subEntrySecond = subOccurrences.next();
            for (int lineIndex = subEntrySecond.getBeginLine(); lineIndex < subEntrySecond.getBeginLine() + subMatch.getLineCount(); ++ lineIndex) {
                if (!highlightedLinesSecond.contains(lineIndex - 1)) {
                    highlightedLinesSecond.add(lineIndex - 1);
                    highlightLine(mRightCodeText, lineIndex - 1, new Color(200, 0, 113, 100));
                }
            }
        }
    }

    private void customizeBorder(final WebPanel panel, final TokenEntry entry) {
        TitledBorder titledBorder = BorderFactory.createTitledBorder(entry.getTokenSrcID());
        titledBorder.setBorder(BorderFactory.createEmptyBorder());
        titledBorder.setTitleJustification(TitledBorder.CENTER);
        titledBorder.setTitleColor(new Color(13, 89, 156));
        panel.setBorder(titledBorder);

        // TIP
        GUIStuff.utip(
                panel,
                "<font color=\"yellow\">" + entry.getTokenSrcID() + "</font>",
                true
        );
    }

    private void moveToVisiblePart(final RSyntaxTextArea codeText, final RTextScrollPane pane, final TokenEntry entry, final Match match) {
        try {
            final int caretPosition = entry.calcSliceCaretPosition(sCurrentConfig, match.getLineCount());
            final Rectangle visiblePart = mLeftCodeText.modelToView(caretPosition);
            codeText.scrollRectToVisible(visiblePart);
            pane.scrollRectToVisible(visiblePart);
            codeText.setCaretPosition(caretPosition);
            codeText.moveCaretPosition(caretPosition);
        } catch (Exception exception) {
            // Do nothing
        }
    }

    private void highlightLine(final RSyntaxTextArea codeText, final int lineIndex, final Color color) {
        try {
            if (lineIndex >= 0) {
                codeText.addLineHighlight(lineIndex, color);
            }
        } catch (Exception exception) {
            // Do nothing
        }
    }

    private void highlightLine(final RSyntaxTextArea codeText, final int lineIndex) {
        highlightLine(codeText, lineIndex, new Color(0, 125, 200, 100));
    }

    //
    // +------------------------------------------------------------+
    // | Construct table of matches                                 |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private JComponent makeMatchList() {
        mResultsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        mResultsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                populateResultPanel();
            }
        });

        int [] alignments = new int[Stuff.MATH_COLUMNS.length];
        for (int i = 0; i < alignments.length; i++) {
            alignments[i] = Stuff.MATH_COLUMNS[i].alignment();
        }

        mResultsTable.setDefaultRenderer(Object.class, new TableStuff.AlignmentRenderer(alignments));

        final JTableHeader header = mResultsTable.getTableHeader();
        header.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                sortOnColumn(header.columnAtPoint(new Point(e.getX(), e.getY())));
            }
        });

        return new JScrollPane(mResultsTable);
    }

    public static String formMatchLabel(final Match match) {
        List<String> sourceIDs = new ArrayList<String>(match.getMarkCount());
        for (Iterator<TokenEntry> occurrences = match.iterator(); occurrences.hasNext(); ) {
            sourceIDs.add(occurrences.next().getTokenSrcID());
        }

        String label;

        if (sourceIDs.size() == 1) {
            String sourceId = sourceIDs.iterator().next();
            int separatorPos = sourceId.lastIndexOf(File.separatorChar);
            label = "..." + sourceId.substring(separatorPos);
        } else {
            String sourceId = sourceIDs.get(0);
            int separatorPos = sourceId.lastIndexOf(File.separatorChar);
            label = ".." + sourceId.substring(separatorPos);
            sourceId = sourceIDs.get(1);
            separatorPos = sourceId.lastIndexOf(File.separatorChar);
            label += " <[Vs]> .." + sourceId.substring(separatorPos);
        }

        // match.setLabel(label);
        return label;
    }

    private void setProgressControls(boolean isRunning) {
        mProgressPanel.setVisible(isRunning);
        mRunCpdBtn.setEnabled(!isRunning);
        mCancelCpdBtn.setEnabled(isRunning);
    }

    //
    // +------------------------------------------------------------+
    // | Main run/go method                                         |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    static CPDConfiguration sCurrentConfig = new CPDConfiguration();
    static Map<String, List<Match>> sSourceToMatchMapping = new HashMap<String, List<Match>>();

    private void go() {
        Timer timer = null;

        try {
            // Start
            timer = createTimer();
            timer.start();

            // Get dir
            File dirPath = new File(mSourceDir.getCurrentFile().getAbsolutePath());
            if (!dirPath.exists()) {
                JOptionPane.showMessageDialog(mFrame,
                        "Can't read from that root source directory",
                        "Error", JOptionPane.ERROR_MESSAGE);
                timer.stop();
                return;
            }

            // Configure visibility of buttons
            setProgressControls(true);

            // Configuration
            Properties p = new Properties();
            sCurrentConfig = new CPDConfiguration();
            sCurrentConfig.setMinimumTileSize(Integer.parseInt(mCpdMinLength.getValue().toString()));
            sCurrentConfig.setEncoding(mFileEncoding.getText());
            sCurrentConfig.setIgnoreIdentifiers(mIgnoreIdentifiersFlag.isSelected());
            sCurrentConfig.setIgnoreLiterals(mIgnoreLiteralsFlag.isSelected());
            sCurrentConfig.setIgnoreAnnotations(mIgnoreAnnotationsFlag.isSelected());
            sCurrentConfig.setSkipLexicalErrors(mSkipLexicalErrorsFlag.isSelected());
            p.setProperty(LanguageFactory.EXTENSION, mExtensions.getText());

            // Language configuration
            LanguageConfig conf = LanguageStuff.languageConfigFor((String) mLanguages.getSelectedItem());
            Language language = conf.languageFor(p);
            sCurrentConfig.setLanguage(language);

            // Cpd configuration
            CPDConfiguration.setSystemProperties(sCurrentConfig);
            CPD cpd = new CPD(sCurrentConfig);
            cpd.setCpdListener(this);

            // mTokenizingProgress.setMinimum(0);
            mPhase.setText("Tokenizing");

            // Add all files
            if (LanguageStuff.isLegalPath(dirPath.getPath(), conf)) {    // should use the language file filter instead?
                cpd.add(dirPath);
            } else {
                if (mSubDirsFlag.isSelected()) {
                    cpd.addRecursively(dirPath);
                } else {
                    cpd.addAllInDirectory(dirPath);
                }
            }

            // Cpd process (find matches)
            cpd.go();

            mCpdMatches.clear();
            sSourceToMatchMapping.clear();

            // Grouping process
            for (Iterator<Match> itMatch = cpd.getMatches(); itMatch.hasNext();) {
                final List<TokenEntry> subMatches = new ArrayList<TokenEntry>();
                final Match match = itMatch.next();

                for (Iterator<TokenEntry> occurrences = match.iterator(); occurrences.hasNext();) {
                    subMatches.add(occurrences.next());
                }

                for (int i = 0; i < subMatches.size(); ++ i) {
                    for (int j = i + 1; j < subMatches.size(); ++j) {
                        final TokenEntry entryFirst = subMatches.get(i);
                        final TokenEntry entrySecond = subMatches.get(j);

                        // We want only different files (by path)
                        if (entryFirst.getTokenSrcID().equals(entrySecond.getTokenSrcID())) {
                            // Self-copy-paste is useless for my issue
                            continue;
                        }

                        final Match subMatch = new Match(match.getTokenCount(), entryFirst, entrySecond);
                        subMatch.setSourceCodeSlice(match.getSourceCodeSlice());
                        subMatch.setLineCount(match.getLineCount());
                        subMatch.setLabel(formMatchLabel(subMatch));

                        final String firstKey = MatchStuff.formSubMatchKey(entryFirst, entrySecond);
                        if (!sSourceToMatchMapping.containsKey(firstKey)) sSourceToMatchMapping.put(firstKey, new ArrayList<Match>());
                        final String secondKey = MatchStuff.formSubMatchKey(entrySecond, entryFirst);
                        if (!sSourceToMatchMapping.containsKey(secondKey)) sSourceToMatchMapping.put(secondKey, new ArrayList<Match>());
                        sSourceToMatchMapping.get(firstKey).add(subMatch);
                        sSourceToMatchMapping.get(secondKey).add(subMatch);

                        boolean ignoreSubmatch = false;
                        if (mSkipSubmatchesFlag.isSelected()) {
                            ignoreSubmatch = (sSourceToMatchMapping.get(firstKey).size() > 1 || sSourceToMatchMapping.get(secondKey).size() > 1);
                        }

                        if (!ignoreSubmatch) {
                            mCpdMatches.add(subMatch);
                        }
                    }
                }
            }

            setListDataFrom();

            // String report = new SimpleRenderer().render(cpd.getMatches());
            if (mCpdMatches.isEmpty()) {
                JOptionPane.showMessageDialog(
                        mFrame,
                        "Done. Couldn't find any duplicates " +
                        "longer than " + mCpdMinLength.getValue() + " tokens"
                );
            }
        } catch (IOException t) {
            t.printStackTrace();
            JOptionPane.showMessageDialog(mFrame, "Halted due to " + t.getClass().getName() + "; " + t.getMessage());
        } catch (RuntimeException t) {
            t.printStackTrace();
            JOptionPane.showMessageDialog(mFrame, "Halted due to " + t.getClass().getName() + "; " + t.getMessage());
        } finally {
            // End
            if (null != timer) timer.stop();

            setProgressControls(false);
        }
    }

    //
    // +------------------------------------------------------------+
    // | Timers                                                     |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private Timer createTimer() {
        final long start = System.currentTimeMillis();

        Timer t = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long now = System.currentTimeMillis();
                long elapsedMillis = now - start;
                long elapsedSeconds = elapsedMillis / 1000;
                long minutes = (long) Math.floor(elapsedSeconds / 60);
                long seconds = elapsedSeconds - (minutes * 60);
                mTimer.setText(Stuff.formatTime(minutes, seconds));
            }
        });

        return t;
    }

    //
    // +------------------------------------------------------------+
    // | Sort rows by column                                        |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private void sortOnColumn(int columnIndex) {
        Comparator<Match> comparator = Stuff.MATH_COLUMNS[columnIndex].sorter();
        TableStuff.SortingTableModel<Match> model = (TableStuff.SortingTableModel<Match>) mResultsTable.getModel();

        if (model.sortColumn() == columnIndex) {
            model.sortDescending(!model.sortDescending());
        }

        model.sortColumn(columnIndex);
        model.sort(comparator);

        mResultsTable.getSelectionModel().clearSelection();
        mResultsTable.repaint();
    }

    //
    // +------------------------------------------------------------+
    // | Tune table from matches                                    |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    private void setListDataFrom() {
        mResultsTable.setModel(TableStuff.tableModelFrom(mCpdMatches));

        TableColumnModel colModel = mResultsTable.getColumnModel();
        TableColumn column;
        int width;

        for (int i = 0; i < Stuff.MATH_COLUMNS.length; i++) {
            if (Stuff.MATH_COLUMNS[i].width() > 0) {
                column = colModel.getColumn(i);
                width = Stuff.MATH_COLUMNS[i].width();
                column.setPreferredWidth(width);
                column.setMinWidth(width);
                column.setMaxWidth(width);
            }
        }
    }

    //
    // +------------------------------------------------------------+
    // | Phases                                                     |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public void phaseUpdate(int phase) {
        mPhase.setText(Stuff.getPhaseText(phase));
    }

    public void addedFile(int fileCount, File file) {
        // mTokenizingProgress.setMaximum(fileCount);
        // mTokenizingProgress.setValue(mTokenizingProgress.getValue() + 1);
    }

    //
    // +------------------------------------------------------------+
    // | Main for GUI                                               |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public static void main(String [] args) {
        // This should prevent the disk not found popup
        // System.setSecurityManager(null);

        // We should work with UI (including installing L&F) inside Event Dispatch Thread (EDT)
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                // Install application L&F
                try {
                    UIManager.setLookAndFeel(com.alee.laf.WebLookAndFeel.class.getName());
                } catch (Exception e) {
                    // Oops
                }

                // Set font
                GUIStuff.setUIFont(new javax.swing.plaf.FontUIResource(new Font("Trebuchet MS", Font.BOLD, 13)));

                // Create GUI
                new GUI();
            }
        });
    }

}
