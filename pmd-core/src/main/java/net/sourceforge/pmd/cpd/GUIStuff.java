package net.sourceforge.pmd.cpd;

import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;

import java.awt.Component;
import java.util.Iterator;

import javax.swing.UIManager;

/**
 * Created by brainail on 1/29/15.
 * @author Malyshev Yegor
 */
public final class GUIStuff {

    //
    // +------------------------------------------------------------+
    // | Report types                                               |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    //  private interface Renderer {
    //      String render(Iterator<Match> items);
    //  }

    static final Object[][] RENDERER_SETS = new Object[][]{
            {"Text", new Renderer() {
                public String render(Iterator<Match> items) {
                    return new SimpleRenderer().render(items);
                }
            }},
            {"XML", new Renderer() {
                public String render(Iterator<Match> items) {
                    return new XMLRenderer().render(items);
                }
            }},
            {"CSV (comma)", new Renderer() {
                public String render(Iterator<Match> items) {
                    return new CSVRenderer(',').render(items);
                }
            }},
            {"CSV (tab)", new Renderer() {
                public String render(Iterator<Match> items) {
                    return new CSVRenderer('\t').render(items);
                }
            }}
    };

    //
    // +------------------------------------------------------------+
    // | Global fonts                                               |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public static void setUIFont(final javax.swing.plaf.FontUIResource f) {
        final java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            final Object key = keys.nextElement();
            final Object value = UIManager.get(key);
            if (value instanceof javax.swing.plaf.FontUIResource) {
                UIManager.put(key, f);
            }
        }
    }

    public static void tip(final Component component, final String text, final boolean useHtml) {
        final String tipText = useHtml ? "<html><center>" + text + "</center></html>" : text;
        TooltipManager.setTooltip(component, tipText, TooltipWay.down, 0);
    }

    public static void utip(final Component component, final String text, final boolean useHtml) {
        final String tipText = useHtml ? "<html><center>" + text + "</center></html>" : text;
        TooltipManager.setTooltip(component, tipText, TooltipWay.up, 0);
    }

}

