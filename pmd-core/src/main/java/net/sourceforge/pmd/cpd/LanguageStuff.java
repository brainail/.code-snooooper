package net.sourceforge.pmd.cpd;

import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by brainail on 1/29/15.
 * @author Malyshev Yegor
 */
public final class LanguageStuff {

    public static boolean isLegalPath(String path, LanguageConfig config) {
        String[] extensions = config.extensions();
        for (int i = 0; i < extensions.length; ++i) {
            if (path.endsWith(extensions[i]) && extensions[i].length() > 0) {
                return true;
            }
        }

        return false;
    }

    //
    // +------------------------------------------------------------+
    // | Single language config                                     |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public static abstract class LanguageConfig {
        public abstract Language languageFor(Properties p);

        public boolean canIgnoreIdentifiers() {
            return false;
        }

        public boolean canIgnoreLiterals() {
            return false;
        }

        public boolean canIgnoreAnnotations() {
            return false;
        }

        public abstract String [] extensions();
    }

    //
    // +------------------------------------------------------------+
    // | Configure langs                                            |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public static final Object [][] LANGUAGE_SETS;

    static {
        LANGUAGE_SETS = new Object [LanguageFactory.supportedLanguages.length + 1][2];

        int index;
        for (index = 0; index < LanguageFactory.supportedLanguages.length; ++ index) {
            final String terseName = LanguageFactory.supportedLanguages[index];
            final Language lang = LanguageFactory.createLanguage(terseName);

            LANGUAGE_SETS [index][0] = lang.getName();
            LANGUAGE_SETS [index][1] = new LanguageStuff.LanguageConfig() {
                @Override
                public Language languageFor(Properties p) {
                    lang.setProperties(p);
                    return lang;
                }

                @Override
                public String[] extensions() {
                    List<String> exts = lang.getExtensions();
                    return exts.toArray(new String [exts.size()]);
                }

                @Override
                public boolean canIgnoreAnnotations() {
                    return "java".equals(terseName);
                }

                @Override
                public boolean canIgnoreIdentifiers() {
                    return "java".equals(terseName) || "cpp".equals(terseName);
                }

                @Override
                public boolean canIgnoreLiterals() {
                    return "java".equals(terseName) || "cpp".equals(terseName);
                }
            };
        }

        LANGUAGE_SETS [index][0] = "by extension ...";
        LANGUAGE_SETS [index][1] = new LanguageStuff.LanguageConfig() {
            @Override
            public Language languageFor(Properties p) {
                return LanguageFactory.createLanguage(LanguageFactory.BY_EXTENSION, p);
            }

            @Override
            public String[] extensions() {
                return new String [] {""};
            }
        };
    }

    private static final Map<String, LanguageConfig> LANGUAGE_CONFIGS_BY_LABEL = new HashMap<String, LanguageConfig>(LANGUAGE_SETS.length);

    static {
        for (int i = 0; i < LANGUAGE_SETS.length; ++ i) {
            LANGUAGE_CONFIGS_BY_LABEL.put((String) LANGUAGE_SETS [i][0], (LanguageConfig) LANGUAGE_SETS [i][1]);
        }
    }

    public static LanguageConfig languageConfigFor(final String label) {
        return LANGUAGE_CONFIGS_BY_LABEL.get(label);
    }

    public static String syntaxByCpd(final CPDConfiguration configuration) {
        if (configuration.getLanguage() instanceof PythonLanguage) return SyntaxConstants.SYNTAX_STYLE_PYTHON;
        if (configuration.getLanguage() instanceof JavaLanguage) return SyntaxConstants.SYNTAX_STYLE_JAVA;
        if (configuration.getLanguage() instanceof RubyLanguage) return SyntaxConstants.SYNTAX_STYLE_RUBY;
        if (configuration.getLanguage() instanceof CPPLanguage) return SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS;
        if (configuration.getLanguage() instanceof CsLanguage) return SyntaxConstants.SYNTAX_STYLE_CSHARP;
        if (configuration.getLanguage() instanceof ScalaLanguage) return SyntaxConstants.SYNTAX_STYLE_SCALA;
        return SyntaxConstants.SYNTAX_STYLE_NONE;
    }

}
