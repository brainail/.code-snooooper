package net.sourceforge.pmd.cpd;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by brainail on 2/10/15.
 *
 * @author Malyshev Yegor
 */
public final class MatchStuff {

    public static String formMatchPercent(final Match match) {
        final Iterator<TokenEntry> occurrences = match.iterator();
        final Set<Integer> highlightedLinesFirst = new HashSet<Integer>();
        final Set<Integer> highlightedLinesSecond = new HashSet<Integer>();

        final TokenEntry entryFirst = occurrences.next();
        for (int lineIndex = entryFirst.getBeginLine(); lineIndex < entryFirst.getBeginLine() + match.getLineCount(); ++ lineIndex) {
            if (!highlightedLinesFirst.contains(lineIndex - 1)) {
                highlightedLinesFirst.add(lineIndex - 1);
            }
        }

        final TokenEntry entrySecond = occurrences.next();
        for (int lineIndex = entrySecond.getBeginLine(); lineIndex < entrySecond.getBeginLine() + match.getLineCount(); ++ lineIndex) {
            if (!highlightedLinesSecond.contains(lineIndex - 1)) {
                highlightedLinesSecond.add(lineIndex - 1);
            }
        }

        // Highlight sub-matches
        for (final Match subMatch : GUI.sSourceToMatchMapping.get(formSubMatchKey(entryFirst, entrySecond))) {
            // Check if match objects are equal
            if (subMatch == match) continue;

            final Iterator<TokenEntry> subOccurrences = subMatch.iterator();

            final TokenEntry subEntryFirst = subOccurrences.next();
            for (int lineIndex = subEntryFirst.getBeginLine(); lineIndex < subEntryFirst.getBeginLine() + subMatch.getLineCount(); ++ lineIndex) {
                if (!highlightedLinesFirst.contains(lineIndex - 1)) {
                    highlightedLinesFirst.add(lineIndex - 1);
                }
            }

            final TokenEntry subEntrySecond = subOccurrences.next();
            for (int lineIndex = subEntrySecond.getBeginLine(); lineIndex < subEntrySecond.getBeginLine() + subMatch.getLineCount(); ++ lineIndex) {
                if (!highlightedLinesSecond.contains(lineIndex - 1)) {
                    highlightedLinesSecond.add(lineIndex - 1);
                }
            }
        }

        float allSize = 0, matchSize = 0;

        final List<String> codeFirst = entryFirst.formSourceLines(GUI.sCurrentConfig);
        for (int lineIndex = 0; lineIndex < codeFirst.size(); ++ lineIndex) {
            allSize += codeFirst.get(lineIndex).length();
            if (highlightedLinesFirst.contains(lineIndex)) {
                matchSize += codeFirst.get(lineIndex).length();
            }
        }

        final List<String> codeSecond = entrySecond.formSourceLines(GUI.sCurrentConfig);
        for (int lineIndex = 0; lineIndex < codeSecond.size(); ++ lineIndex) {
            allSize += codeSecond.get(lineIndex).length();
            if (highlightedLinesSecond.contains(lineIndex)) {
                matchSize += codeSecond.get(lineIndex).length();
            }
        }

        return String.format("%.02f", ((matchSize / allSize) * 100));
    }

    public static String formSubMatchKey(final TokenEntry entryFirst, final TokenEntry entrySecond) {
        return entryFirst.getTokenSrcID() + "/" + entrySecond.getTokenSrcID();
    }

}
