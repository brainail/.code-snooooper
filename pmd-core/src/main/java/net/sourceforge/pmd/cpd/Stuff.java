package net.sourceforge.pmd.cpd;

import javax.swing.SwingConstants;

/**
 * Created by brainail on 1/31/15.
 *
 * @author Malyshev Yegor
 */
public final class Stuff {

    public static final int MINIMUM_CPD_MINIMUM_LENGTH = 10;
    public static final int DEFAULT_CPD_MINIMUM_LENGTH = 77;

    public static final TableStuff.ColumnSpec[] MATH_COLUMNS = new TableStuff.ColumnSpec[] {
            new TableStuff.ColumnSpec("Source", SwingConstants.LEFT, -1, Match.LABEL_COMPARATOR),
            new TableStuff.ColumnSpec("Matches Lines %", SwingConstants.RIGHT, 120, Match.MATCHES_COMPARATOR),
            new TableStuff.ColumnSpec("Tokens", SwingConstants.RIGHT, 99, Match.TOKEN_COMPARATOR),
    };

    public static String formatTime(long minutes, long seconds) {
        StringBuilder sb = new StringBuilder(5);
        if (minutes < 10) {
            sb.append('0');
        }

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }

        sb.append(seconds);
        return sb.toString();
    }

    public static String getPhaseText(int phase) {
        switch (phase) {
            case CPDListener.INIT:
                return "Initializing";
            case CPDListener.HASH:
                return "Hashing";
            case CPDListener.MATCH:
                return "Matching";
            case CPDListener.GROUPING:
                return "Grouping";
            case CPDListener.DONE:
                return "Done";
            default:
                return "Unknown";
        }
    }

}
