package net.sourceforge.pmd.cpd;

import java.awt.Component;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

/**
 * Created by brainail on 1/31/15.
 *
 * @author Malyshev Yegor
 */
public final class TableStuff {

    //
    // +------------------------------------------------------------+
    // | Table model by matches                                     |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    public static TableModel tableModelFrom(final List<Match> items) {
        TableModel model = new SortingTableModel<Match>() {

            private int sortColumn;
            private boolean sortDescending;

            public Object getValueAt(int rowIndex, int columnIndex) {
                Match match = items.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return match.getLabel();
                    case 2:
                        return Integer.toString(match.getTokenCount());
                    case 1:
                        // return match.getMarkCount() > 2 ? Integer.toString(match.getMarkCount()) : "";
                        return match.retrieveMatchPercent();
                    case 99:
                        return match;
                    default:
                        return "";
                }
            }

            public int getColumnCount() {
                return Stuff.MATH_COLUMNS.length;
            }

            public int getRowCount() {
                return items.size();
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }

            public Class<?> getColumnClass(int columnIndex) {
                return Object.class;
            }

            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}

            public String getColumnName(int i) {
                return Stuff.MATH_COLUMNS[i].label();
            }

            public void addTableModelListener(TableModelListener l) {}

            public void removeTableModelListener(TableModelListener l) {}

            public int sortColumn() {
                return sortColumn;
            }

            public void sortColumn(int column) {
                sortColumn = column;
            }

            public boolean sortDescending() {
                return sortDescending;
            }

            public void sortDescending(boolean flag) {
                sortDescending = flag;
            }

            public void sort(Comparator<Match> comparator) {
                Collections.sort(items, comparator);
                if (sortDescending) {
                    Collections.reverse(items);
                }
            }
        };

        return model;
    }

    public static interface SortingTableModel<E> extends TableModel {
        int sortColumn();
        void sortColumn(int column);
        boolean sortDescending();
        void sortDescending(boolean flag);
        void sort(Comparator<E> comparator);
    }

    public static class ColumnSpec {

        private String mLabel;
        private int mAlignment;
        private int mWidth;
        private Comparator<Match> mSorter;

        public ColumnSpec(String aLabel, int anAlignment, int aWidth, Comparator<Match> aSorter) {
            mLabel = aLabel;
            mAlignment = anAlignment;
            mWidth = aWidth;
            mSorter = aSorter;
        }

        public String label() {
            return mLabel;
        }

        public int alignment() {
            return mAlignment;
        }

        public int width() {
            return mWidth;
        }

        public Comparator<Match> sorter() {
            return mSorter;
        }

    }

    //
    // +------------------------------------------------------------+
    // | Table renderer                                             |
    // +------------------------------------------------------------+
    // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    static class AlignmentRenderer extends DefaultTableCellRenderer {

        private int [] mAlignments;

        public AlignmentRenderer(int[] theAlignments) {
            mAlignments = theAlignments;
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            setHorizontalAlignment(mAlignments[column]);
            return this;
        }

    }

}
